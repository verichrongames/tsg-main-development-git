﻿using System;

public class LoginMessageType
{
    public LoginMessageType() { }

    public LoginMessageType(string _username, string _password, string _IP)
    {
        username = _username;
        password = _password;
        ipAddress = _IP;
    }

    public string username;
    public string password;
    public string ipAddress;
}
