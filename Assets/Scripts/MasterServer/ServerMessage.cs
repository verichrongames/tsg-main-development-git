﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//Primary function of this class is to be an instanced message type that is used on both ends of the communication.
//Message Types:
//Class 1(int 1): Login Request/Login Accepted -- Data(Login) == [ Type(int 1), Username(senderUsername), Password(senderPassword), IPADDR(senderIP) ) -- Data(Connection Response) == [ Type(int 1), userUID(int(Possibly Token)), Acceptance(messageBodyInt 1/0) ]
//Class 2(int 2): Registration Request -- Data(Register) == [ Type(int 2), Username(senderUsername), Password(senderPassword), IPADDR(senderIP) ) -- Data(response)
//Class 3(int 3): Chat(Channel) -- Data == [ Type(int 2), Username(senderUsername), Message(messageBody), Channel(string) ]
//Class 4(int 4): Chat(Global/AdminOnly) -- Data == [ Type(int 2), Username(senderUsername), Message(messageBody)
//Class 11: End of Stream(Client Disconnect)
//Chat, Private Message, Matchmaking Request, Exit(endstream), Login


public class ServerMessage
{
    public int messageType;
    public int userId;
    public int messageBodyInt;
    public string channelName;
    public List<string> channelLog, clientList;
    public GameVersionMessage gameVersion;
    public LoginMessageType loginMessage;
    public PublicMessageType chatMessage;
    public PrivateMessageType privateMessage;

    public ServerMessage() { }

    public ServerMessage(int type)//Class 11: Disconnect
    {
        messageType = type;
    }

    public ServerMessage(int type, int body) //Class 2: Registration Response
    {
        messageType = type;
        messageBodyInt = body;
    }

    public ServerMessage(int type, int id, int body) //Class 1: Login Response// Body == [ Success 0(Unused), Failure-UNLOOKUP 1, Failure-PASS 2 ]
    {
        messageType = type;
        userId = id;
        messageBodyInt = body;
    }

    public ServerMessage(int type, LoginMessageType _loginMessage, GameVersionMessage _gameVersion) //Login/Register Request
    {
        messageType = type;
        loginMessage = _loginMessage;
        gameVersion = _gameVersion;
    }

    /// <summary>
    /// Used for sending chat messages via the channel system
    /// Also used for updating if a user left or joined
    /// </summary>
    /// <param name="type">Always 3 for this constructor</param>
    /// <param name="_messageBodyInt">0 for Join, 1 for Leave, 2 for normal message</param>
    /// <param name="_chatMessage"></param>
    public ServerMessage(int type, int _messageBodyInt, PublicMessageType _chatMessage) //Type 3 Public Chat
    {
        messageType = type;
        messageBodyInt = _messageBodyInt;
        chatMessage = _chatMessage;
    }

    /// <summary>
    /// Used to update other users when someone joins
    /// </summary>
    /// <param name="type"></param>
    /// <param name="_messageBodyInt"></param>
    /// <param name="_clientList"></param>
    public ServerMessage(int type, int _messageBodyInt, List<string> _clientList) //Type 3 Public Chat
    {
        messageType = type;
        messageBodyInt = _messageBodyInt;
        clientList = _clientList;
    }

    /// <summary>
    /// This constructor is used for when a client joins a new channel
    /// </summary>
    /// <param name="type">Use type 3</param>
    /// <param name="_messageBodyInt">Use type 4</param>
    /// <param name="_chatMessage"></param>
    /// <param name="clientList"></param>
    public ServerMessage(int type, int _messageBodyInt, List<string> _clientList, List<string> _channelLog, string channelName) //Type 3 Public Chat Channel
    {
        messageType = type;
        messageBodyInt = _messageBodyInt;
        clientList = _clientList;
        channelLog = _channelLog;
    }

    /// <summary>
    /// Private messages constructor
    /// </summary>
    /// <param name="type">Type 4</param>
    /// <param name="body">0 for standard message, 1 for failure response</param>
    /// <param name="message"></param>
    /// <remarks>
    /// Message type 4 only constructor
    /// </remarks>
    public ServerMessage(int type, int body, PrivateMessageType message) //Type 4 Private Message
    {
        messageType = type;
        messageBodyInt = body;
        privateMessage = message;
    }
}