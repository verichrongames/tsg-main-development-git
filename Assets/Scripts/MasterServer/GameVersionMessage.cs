﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class GameVersionMessage
{
    public int versionA, versionB, versionC, versionD;
    public string versionDa;
    public GameVersionMessage() { }
    public GameVersionMessage(int A, int B, int C, int D, string Da)
    {
        versionA = A;
        versionB = B;
        versionC = C;
        versionD = D;
        versionDa = Da;
    }
}
