﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Xml.Serialization;

namespace CoreSys.Networking
{
    public class ServerInterface : MonoBehaviour
    {
        private bool loggedIn;
        public string username, password, r_username, r_password, rr_password;
        public InputField m_Username; //Username(login section)
        public InputField m_Password; //Pass(login section)
        public InputField r_Username; //Username(register section)
        public InputField r_Password; //Pass(register section)
        public InputField rr_Password; //Retype pass(register section)
        private ServerCommand server = new ServerCommand();


        public void Awake()
        {
            StartCoroutine(GetIP());
        }

        public void FixedUpdate()
        {
            if (!loggedIn)
            {
                if (m_Username != null)
                    username = m_Username.text;
                if (m_Password != null)
                    password = m_Password.text;
                if (r_Username != null)
                    r_username = r_Username.text;
                if (r_Password != null)
                    r_password = r_Password.text;
                if (rr_Password != null)
                    rr_password = rr_Password.text;
            }
        }

        public void Login()
        {
            if (username != string.Empty && password != string.Empty)
            {
                StartCoroutine(LoginProcess());
            }
            else
            {
                if (username == string.Empty)
                {
                    Debug.LogWarning("User Name is Empty");
                }
                if (password == string.Empty)
                {
                    Debug.LogWarning("Password is Empty");
                }
                bl_LoginManager.UpdateDescription("Complete all fields");
            }
        }

        public void Register()
        {
            if (r_username != string.Empty && r_password != string.Empty && rr_password != string.Empty)
            {
                if (r_password == rr_password)
                    StartCoroutine(RegistrationProcess());
            }
            else
            {
                if (r_username == string.Empty)
                {
                    bl_LoginManager.UpdateDescription("Complete all fields");
                    Debug.LogWarning("User Name is Empty");
                }
                if (r_password == string.Empty)
                {
                    bl_LoginManager.UpdateDescription("Complete all fields");
                    Debug.LogWarning("Password is Empty");
                }
                if (rr_password == string.Empty)
                {
                    Debug.LogWarning("Re Pass is Empty");
                    bl_LoginManager.UpdateDescription("Complete all fields");
                }
                else
                {
                    Debug.LogWarning("Passwords do not Match");
                    bl_LoginManager.UpdateDescription("Passwords do not Match");
                }
            }
        }

        public void ServerDisconnect(TcpClient serverSocket, NetworkStream serverStream)
        {
            ServerMessage disonnectMessage = new ServerMessage(11);
            server.SendServerMessage(disonnectMessage, serverSocket);
            serverSocket.Close();
            serverStream.Close();
        }

        IEnumerator LoginProcess()
        {
            bl_LoginManager.UpdateDescription("Logging In...");

            bl_LoginManager.LoadingCache.ChangeText("Submitting Login Request...", true);

            bl_LoginManager.LoadingCache.ChangeText("Waiting for Response", true, 1.2f);

            //Begin Server Connection
            Debug.Log("Connecting");
            GameManager.serverSocket = new TcpClient();
            //GameManager.serverSocket.Connect("174.29.67.100", 8888);
            GameManager.serverSocket.Connect("127.0.0.1", 8888);
            NetworkStream serverStream = default(NetworkStream);
            serverStream = GameManager.serverSocket.GetStream();

            LoginMessageType loginMessage = new LoginMessageType(username, password, GameManager.UIP);
            ServerMessage sendLogin = new ServerMessage(1, loginMessage, GameManager.GetGameVersionMessage()); //FIXTHIS -- Game version needs to be checked before connection allows for messages to be sent.
            server.SendServerMessage(sendLogin, GameManager.serverSocket);

            ServerMessage serverMessage = server.ReceiveServerMessage(GameManager.serverSocket, serverStream);

            if (serverMessage.messageType == 1)
            {
                if (serverMessage.messageBodyInt == 0)
                {
                    bl_LoginManager.UpdateDescription(String.Format("Login Success! Welcome {0}!", username));
                    ServerListener.StartListener();  //Starts the listener
                    GameManager.SetUID(serverMessage.userId);
                    GameManager.SetUserName(username);
                    Application.LoadLevel(3);
                    bl_LoginManager.menuManager.ShowMenu(bl_LoginManager.mainMenu);//Move and handle connection
                    
                }
                else if (serverMessage.messageBodyInt == 1)
                {
                    bl_LoginManager.UpdateDescription(String.Format("Username Incorrect"));
                }
                else if (serverMessage.messageBodyInt == 2)
                {
                    bl_LoginManager.UpdateDescription(String.Format("Password Incorrect"));
                }
            }
            else
            {
                Debug.Log("Server failure to respond with proper message type");
                bl_LoginManager.UpdateDescription("Server Error! : Disconnecting!");
                ServerDisconnect(GameManager.serverSocket, serverStream);
            }
            yield break;
        }

        IEnumerator RegistrationProcess()
        {
            bl_LoginManager.UpdateDescription("Registering...");

            bl_LoginManager.LoadingCache.ChangeText("Submitting Registration Request...", true);

            bl_LoginManager.LoadingCache.ChangeText("Waiting for Response", true, 1.2f);

            //Begin Server Connection
            Debug.Log("Connecting");
            GameManager.serverSocket = new TcpClient();
            //GameManager.serverSocket.Connect("174.29.67.100", 8888);
            GameManager.serverSocket.Connect("127.0.0.1", 8888);
            NetworkStream serverStream = default(NetworkStream);
            serverStream = GameManager.serverSocket.GetStream();

            LoginMessageType loginMessage = new LoginMessageType(r_username, r_password, GameManager.UIP);
            ServerMessage sendRegister = new ServerMessage(2, loginMessage, GameManager.GetGameVersionMessage()); //FIXTHIS -- Game version needs to be checked before connection allows for messages to be sent.
            server.SendServerMessage(sendRegister, GameManager.serverSocket);

            ServerMessage serverMessage = server.ReceiveServerMessage(GameManager.serverSocket, serverStream);

            if (serverMessage.messageType == 2)
            {
                if (serverMessage.messageBodyInt == 0)
                {
                    bl_LoginManager.UpdateDescription("Registration Success! Please login.");

                    bl_LoginManager.menuManager.ShowMenu(bl_LoginManager.loginMenu);//Move and handle connection
                }
                else if (serverMessage.messageBodyInt == 1)
                {
                    bl_LoginManager.UpdateDescription("Username in Use.");
                }
            }
            else
            {
                Debug.Log("Server failure to respond with proper message type");
                bl_LoginManager.UpdateDescription("Server Error! : Disconnecting!");
                ServerDisconnect(GameManager.serverSocket, serverStream);
            }
            yield break;
        }

        IEnumerator GetIP()
        {
            WWW w = new WWW("http://verichrongames.net/ProjectLevnos/Login/Levnos_IP.php");
            if (w == null) yield return null;
            yield return w;
            string t;
            t = w.text;
            GameManager.SetUIP(t);
        }

        private string RichColor(string text, string color)
        {
            return "<color=" + color + ">" + text + "</color>";
        }
    }
}
