﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using CoreSys.Lobby.Chat;

namespace CoreSys.Networking
{
    public static class ServerListener
    {
        private static NetworkStream serverStream = default(NetworkStream);
        private static ServerCommand server = new ServerCommand();
        private static bool listening = false;
        public static ServerIntermediary chatIntermediary { get; set; }

        public static void StartListener()
        {
            serverStream = GameManager.serverSocket.GetStream();
            Debug.Log("Start Server Listener");
            listening = true;
        }

        public static void CheckForData()
        {
            if (listening)
            {
                if (serverStream.DataAvailable)
                {
                    List<ServerMessage> receivedMessages = server.ReceiveServerMessages(GameManager.serverSocket, serverStream);
                    for (int i = 0; i < receivedMessages.Count; i++)
                    {
                        if (receivedMessages[i].messageType == 3 || receivedMessages[i].messageType == 4)
                        {
                            chatIntermediary.RecieveHandle(receivedMessages[i]);
                        }
                        else if (receivedMessages[i].messageType == 11)
                        {
                            serverStream.Close();
                            GameManager.serverSocket.Close();
                        }
                    }
                }
            }
        }
    }
}