﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CoreSys.Objects;
using CoreSys.Exceptions;

/*============================================
 * Class manages setting up the UI for a selected
 * object. I decided to split this off of 
 * player selection manager and remove UI calls from
 * building to reduce uneeded coupling and 
 * try and give some classes fewer resposabilities. 
 * 
 * It also clears the way to have a single 
 * entry and exit point for calls for object-specific UI
 * elements. Making it easier to call those elements as well 
 * as making it simpler to change how they are called, or what 
 * sort of data validation or modification is
 * required by the mediator (this class).
 * 
 * Blame: Doug
 * ==========================================*/

public class SelectedObjectUIManager : MonoBehaviour {

    InstantiatedObjectsList objectsList;
    Player player;

    ObjectHUDPanel objectHUD;
    BuildPanel buildPanel;
    BuildBar buildBar;
    BuildQueueBar buildQueueBar;
    BuildQueueManager buildQueueManager;
    PlayerSelectionManager playerSelectionManager;

    private bool disabledInfoPanel = false;
    private bool buildBarRetrieved = false;
    private bool buildQueueBarRetrieved = false;
    private bool objectIsSelected = false;

    void OnEnable()
    {
        MouseEvents.MouseHoverObjectEvent += SetObjectHoverUI;
        MouseEvents.MouseHoverGroundEvent += SetGroundHoverUI;
    }

    void OnDisable()
    {
        MouseEvents.MouseHoverObjectEvent -= SetObjectHoverUI;
        MouseEvents.MouseHoverGroundEvent -= SetGroundHoverUI;
    }

	void Start () 
    {
        objectsList = GetComponent<InstantiatedListAdder>().instantiatedList;
        buildQueueManager = transform.root.GetComponentInChildren<BuildQueueManager>();
        playerSelectionManager = transform.root.GetComponentInChildren<PlayerSelectionManager>();
        player = transform.root.GetComponent<Player>();
        GetUIReferances();
	}
	
	void Update () 
    {
	
	}

    //recieves call for soemthing from selection manager
    //retrieve information on what UI objects to instantiate from somewhere
    //Instantiate UI elements or tell UI elemenbts what to instantiate and they do it themselves
    //Provide way to update UI elements from things such as the build queue manager while they are still up

    public void UpdateSelectedBuildBar()
    {
    }

    //Recieves public call to update the build queue progress bar every frame
    public void UpdateQueueProgress(string itEmptype, float progress)
    {
        if(objectIsSelected)
        {
            UpdateBuildQueueItemProgress(itEmptype, progress);
        }
    }

    //Recieves public call to add new build queue item
    public void NewBuildQueueItem(string itEmptype, int count)
    {
        NewBuildQueueBarItem(itEmptype, count);
    }

    //Recieves public call to incriment new build queue item
    public void IncrimentBuildQueueItem(string itEmptype, int count)
    {
        IncrimentBuildQueueBarItem(itEmptype, count);
    }

    //Recieves public call to deincriment new build queue item
    public void DeincrimentBuildQueueItem(string itEmptype, int count)
    {
        if(objectIsSelected)
        {
            DeincrimentBuildQueueBarItem(itEmptype, count);
        }
        else
        {
            Debug.Log("no object selected yet method was called");
        }
    }

    //Starts setting up the selected objects UI. Recieves call
    public void SetSelectedUI(GameObject gameObject, IPlayerObject playerObject)
    {
        ICanBuild canBuildObject = (ICanBuild)gameObject.GetComponent(typeof(ICanBuild));
        if(canBuildObject != null)
        {
            StartCoroutine(SetBuildPanel(gameObject, playerObject, canBuildObject));
        }
        else
        {
            throw new MissingComponentException("Object was passed with \"CanBuild\" tag without an \"ICanBuild\" interface");
        }
    }

    //Changes the UI to a new object's UI
    public void ChangeSelectedUI(GameObject gameObject, IPlayerObject playerObject)
    {
        RemoveBuildPanel();
        ICanBuild canBuildObject = (ICanBuild)gameObject.GetComponent(typeof(ICanBuild));
        if (canBuildObject != null)
        {
            StartCoroutine(SetBuildPanel(gameObject, playerObject, canBuildObject));
        }
        else
        {
            throw new MissingComponentException("Object was passed with \"CanBuild\" tag without an \"ICanBuild\" interface");
        }
    }

    //Clears the selected UI
    public void ClearSelectedUI()
    {
        RemoveBuildPanel();
        objectIsSelected = false;
    }

    /*=========================================================
    *           ==== Hovering UI Methods ====
    *======================================================== */

    private void SetGroundHoverUI(string type, MouseHoverEventArgs e)
    {
        if(!disabledInfoPanel)
        {
            DisableInfoPanel();
        }
    }

    //Recieved object hover event when mouse hovers over object
    private void SetObjectHoverUI(string hoverType, MouseHoverEventArgs e)
    {
        if(hoverType != "UserInterface")
        {
            IPlayerObject playerObject = (IPlayerObject)e.HitObject.GetComponent(typeof(IPlayerObject));
            if (playerObject != null)
            {
                UpdatePlayerObjectInfoPanel(e.HitObject, playerObject);
            }
        }
    }

    //Disables the info panel
    private void DisableInfoPanel() //TOFIX
    {
        //objectHUD.RemoveObjectinfoPanel();
        disabledInfoPanel = true;
    }

    //Sends info to the object info panel to create or update itself.
    private void UpdatePlayerObjectInfoPanel(GameObject gameObject, IPlayerObject playerObject)
    {
        objectHUD.CreateObjectInfoPanel(new UIObjectInfoType(playerObject.HitPoints, playerObject.MaxHitPoints, playerObject.ObjectName, playerObject.player.name));


        ////GameObject spriteRendererGO = gameObject.transform.FindChild("Object Sprite").gameObject;
        //if(true)
        //{
        //    //SpriteRenderer objectSpriteRenderer = gameObject.transform.FindChild("Object Sprite").GetComponent<SpriteRenderer>();
        //    //SpriteRenderer objectSpriteRenderer = gameObject.transform.FindChild("Object Sprite").GetComponent<SpriteRenderer>();

        //    if (objectSpriteRenderer != null && CheckForPlayer(gameObject, playerObject))
        //    {
        //        objectHUD.CreateObjectInfoPanel(new UIObjectInfoType(objectSpriteRenderer.sprite, playerObject.HitPoints, playerObject.MaxHitPoints, playerObject.ObjectName, playerObject.player.name));
        //        disabledInfoPanel = false;
        //    }
        //    else
        //    {
        //        MeshRenderer objectMeshrenderer = gameObject.GetComponent<MeshRenderer>();

        //        if (objectMeshrenderer != null && CheckForPlayer(gameObject, playerObject))
        //        {
        //            objectHUD.CreateObjectInfoPanel(new UIObjectInfoType(objectMeshrenderer.material, playerObject.HitPoints, playerObject.MaxHitPoints, playerObject.ObjectName, playerObject.player.name));
        //            disabledInfoPanel = false;
        //        }
        //        else
        //        {
        //            throw new MissingComponentException("There is no spriterenderer or mesh renderer attached to this IPlayerObject: " + playerObject.ObjectName);
        //        }
        //    }
        //}
    }

    /*=========================================================
     *        ==== Selected Object UI Methods ====
     *======================================================== */

    /*=========================
     *  New Build Panel Setup
     * =======================*/

    //Sets up new build panel. Also sends updated information to build panel if needed. Is ran as a coroutine that aits till the next frame
    IEnumerator SetBuildPanel(GameObject gameObject, IPlayerObject playerObject, ICanBuild canBuildObject)
    {
        yield return 0;

        buildPanel.SetupPanelSwitch(canBuildObject.buildPanelItems);
        if(!buildBarRetrieved)
        {
            buildBar = objectsList.GetGameObject("BuildBar", "User Interface", "Active", playerObject.player).GetComponent<BuildBar>();
            buildBarRetrieved = true;
            SetNewBuildBar(gameObject, playerObject, canBuildObject);
        }
        if (!buildQueueBarRetrieved && canBuildObject.buildPanelItems.Equals("Both") || canBuildObject.buildPanelItems.Equals("BuildQueueBar"))
        {
            buildQueueBar = objectsList.GetGameObject("BuildQueueBar", "User Interface", "Active", playerObject.player).GetComponent<BuildQueueBar>();
            SetNewBuildQueueBar(gameObject, playerObject);
        }
        objectIsSelected = true; //Has to be at end of coroutine so that null referances are not passed the frame before. 
    }

    //Sets up the build bar after the build panel has been set up.
    private void SetNewBuildBar(GameObject gameObject, IPlayerObject playerObject, ICanBuild canBuildObject)
    {
        for (int i = 0; i < canBuildObject.buildableObjects.Count; i++)
        {
            buildBar.NewBuildBarItem(GetUIObjects.GetBuildButtons(canBuildObject.buildableObjects[i] + "OrderButton"));
        }
    }

    //Sets up a new build queue bar after the build bar as been setup on object selection
    private void SetNewBuildQueueBar(GameObject gameObject, IPlayerObject playerObject)
    {
        BuildQueueType buildQueue = buildQueueManager.GetBuildQueue(gameObject);
        if (buildQueue != null)
        {
            for(int i = 0; i < buildQueue.BuildQueueItems.Count; i++)
            {
                buildQueueBar.NewQueueItem(GetUIObjects.GetBuildQueueItems(buildQueue.BuildQueueItems[i].QueueItEmptype), buildQueue.BuildQueueItems[i].Count);
            }
        }
    }

    /*=========================
     *  Build Queue Bar Updates
     * =======================*/

    private void NewBuildQueueBarItem(string queueItEmptype, int count)
    {
        if(count >= 1)
        {
            buildQueueBar.NewQueueItem(GetUIObjects.GetBuildQueueItems(queueItEmptype), count);
        }
    }

    private void IncrimentBuildQueueBarItem(string queueItEmptype, int count)
    {
        if (count > 0)
        {
            buildQueueBar.NewQueueItem(GetUIObjects.GetBuildQueueItems(queueItEmptype), count);
        }
    }

    private void DeincrimentBuildQueueBarItem(string queueItEmptype, int count)
    {
        if(count > 0)
        {
            buildQueueBar.DeincrementQueueItem(GetUIObjects.GetBuildQueueItems(queueItEmptype), count);
        }
    }

    private void UpdateBuildQueueItemProgress(string queueItEmptype, float progress)
    {
        buildQueueBar.SetQueueItemProgress(GetUIObjects.GetBuildQueueItems(queueItEmptype), progress);
    }

    //Calls for the build panel to be removed
    public void RemoveBuildPanel()
    {
        buildBarRetrieved = false;
        buildQueueBarRetrieved = false;
        buildPanel.ClearPanel();
        buildBar = null;
        buildQueueBar = null;
    }

    /*=======================================================
     *       ===== Referance Getters and Setters ====
     * ====================================================*/

    //Gets the UI Referance
    private void GetUIReferances()
    {
        objectHUD = objectsList.GetGameObject("ObjectHUD", "User Interface", "Active", player).GetComponent<ObjectHUDPanel>();
        buildPanel = objectsList.GetGameObject("Build Panel", "User Interface", "Active", player).GetComponent<BuildPanel>();
        if(buildPanel != null)
        {
            Debug.Log("Got the:" + buildPanel.name);
        }
    }

    //Recieved Callback From the Build Panel with referances to child objects. 
    public void SetBuildPanelReferances()
    {

    }

    /*=======================================================
     *       ===== Referance Checking Methods ====
    * ====================================================*/

    //Throws exception if player is not instantiated
    private bool CheckForPlayer(GameObject gameObject, IPlayerObject playerObject)
    {
        if (object.ReferenceEquals(playerObject, null))
        {
            string message = "Player Object Not Instatiated For: " + gameObject.name + " InstanceID: " + gameObject.GetInstanceID();
            throw new ObjectNotInstantiatedException(message);
        }
        else if (object.ReferenceEquals(playerObject.player, null))
        {
            string message = "Player Not Instantiated For: " + playerObject.ObjectName + " InstanceID: " + gameObject.GetInstanceID();
            throw new PlayerNotFoundException(message);
        }
        else
            return true;
    }
}

