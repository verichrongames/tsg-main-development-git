﻿using UnityEngine;
using System.Collections;
using CoreSys;
/*==================================================================
 * This Class handles all keyboard-related camera movement
 * Blame: Doug 
 * =================================================================
 * 
 */
public class CameraMovement_Keyboard : VCamera{

    //make private
    public bool up;
    public bool down;
    public bool right;
    public bool left;

	protected override void Start () {
        base.Start();
         up = false;
         down = false;
         right = false;
         left = false;
	}
	protected override void Update (){
        base.Update();
	}

    void OnEnable()
    {
        KeyboardEvents.MovementButtonHeldEvent += DetermineButtonHeld;
        KeyboardEvents.MovementButtonUpEvent += DetermineButtonUp;
    }
    void OnDisable()
    {
        KeyboardEvents.MovementButtonHeldEvent -= DetermineButtonHeld;
        KeyboardEvents.MovementButtonUpEvent -= DetermineButtonUp;
    }

    /*=========================================================
     * ==== End Initialization and per-frame loops ====
     * ==== Start Method Calling Switch Statements ====
     *======================================================== */

    private void DetermineButtonHeld(string whichButton)
    {
        switch(whichButton)
        {
            case "KeyUpHeld":
                CameraUpButton();
                break;
            case "KeyDownHeld":
                CameraDownButton();
                break;
            case "KeyRightHeld":
                CameraRightButton();
                break;
            case "KeyLeftHeld":
                CameraLeftButton();
                break;
        }
    }

    private void DetermineButtonUp(string whichButton)
    {
        switch(whichButton)
        {
            case "KeyUpUp":
                CameraUpButtonUp();
                break;
            case "KeyDownUp":
                CameraDownButtonUp();
                break;
            case "KeyRightUp":
                CameraRightButtonUp();
                break;
            case "KeyLeftUp":
                CameraLeftButtonUp();
                break;
        }

    }

    /*=========================================================
     *              ==== End Switches ====
     *         ==== Start Held Time Counter ====
     *======================================================== */

    //Counts How Long a Button Was Held. Filters opposing buttons
    private void ButtonHeldCounter()
    {
        if (up || down || left || right)
        {
            if (up && down)
            {
                return;
            }
            if (left && right)
            {
                return;
            }
            else
            {
                timeButtonHeld++;
                ButtonHeldAdjust();
            }
        }
        else
        {
            timeButtonHeld = 0;
        }
    }


    /*=========================================================
     *            ==== End Held Time Counter ====
     *       ==== Start Camera Movement Methods ====
     *======================================================== */

    private void CameraUpButton()
    {
        up = true;
        ButtonHeldCounter();
        Vector3 cameraPos = Camera.main.transform.position;
        cameraPos.z += scrollSpeed * Time.deltaTime * 30;
        Camera.main.transform.position = cameraPos;
    }

    private void CameraDownButton()
    {
        down = true;
        ButtonHeldCounter();
        Vector3 cameraPos = Camera.main.transform.position;
        cameraPos.z -= scrollSpeed * Time.deltaTime * 30;
        Camera.main.transform.position = cameraPos;
    }

    private void CameraRightButton()
    {
        right = true;
        ButtonHeldCounter();
        Vector3 cameraPos = Camera.main.transform.position;
        cameraPos.x += scrollSpeed * Time.deltaTime * 30;
        Camera.main.transform.position = cameraPos;
    }

    private void CameraLeftButton()
    {
        left = true;
        ButtonHeldCounter();
        Vector3 cameraPos = Camera.main.transform.position;
        cameraPos.x -= scrollSpeed * Time.deltaTime * 30;
        Camera.main.transform.position = cameraPos;
    }

    /*=========================================================
     *        ==== End Camera Movement Methods ====
     *          ==== Start Button Up Checks ====
     *======================================================== */


    private void CameraUpButtonUp()
    {
        up = false;
        ButtonHeldCounter();
    }

    private void CameraDownButtonUp()
    {
        down = false;
        ButtonHeldCounter();
    }

    private void CameraRightButtonUp()
    {
        right = false;
        ButtonHeldCounter();
    }

    private void CameraLeftButtonUp()
    {
        left = false;
        ButtonHeldCounter();
    }
}
