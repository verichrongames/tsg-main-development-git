﻿using UnityEngine;
using System.Collections;
using CoreSys;
/*==================================================================
 * This Class handles all mouse-related camera movement
 * ## There are a LOT of event subscribers. This is fine, they do not loop or use CPU Cycles
 * 
 * Event subscribers must subscribe to their respective events on Enable, and unsubscribe on Disable unless memoryleaks are a feature we want. 
 * Blame: Doug
 * =================================================================
 * 
 */
public class CameraMovement_Mouse : VCamera {

	protected override void Start () {
        base.Start();
    }
	protected override void Update () {
        base.Update();
      }
    void OnEnable()
    {
        MouseEvents.EdgeOfScreenEvent += DetermineScreenEdge;
        MouseEvents.EdgeOfScreenEventEnded += timeButtonHeldReset;

        MouseEvents.ScrollDownEvent += ZoomOut;
        MouseEvents.ScrollUpEvent += ZoomIn;
    }
    void OnDisable()
    {
        MouseEvents.EdgeOfScreenEvent -= DetermineScreenEdge;
        MouseEvents.EdgeOfScreenEventEnded -= timeButtonHeldReset;

        MouseEvents.ScrollDownEvent -= ZoomOut;
        MouseEvents.ScrollUpEvent -= ZoomIn;

    }

    /*=========================================================
     * ==== End Initialization and per-frame loops ====
     * ==== Start Screen Movement Modifyers ====
     *======================================================== */

    private void Counter()
    {
        timeButtonHeld += 1f;
        ButtonHeldAdjust();
    }

    private void timeButtonHeldReset(string whichEdge)
    {
        timeButtonHeld = 0;
    }

    /*=========================================================
     * ==== End Screen Movement Modifyers ====
     *    ==== Start Switch Statements ====
     *======================================================== */

    private void DetermineScreenEdge(string whichEdge)
    {
        switch (whichEdge)
        {
            case "right":
                EdgeOfScreenRight();
                break;
            case "left":
                EdgeOfScreenLeft();
                break;
            case "up":
                EdgeOfScreenUp();
                break;
            case "down":
                EdgeOfScreenDown();
                break;
            case "top right":
                EdgeOfScreenTopRight();
                break;
            case "bottom right":
                EdgeOfScreenBottomRight();
                break;
            case "bottom left":
                EdgeOfScreenBottomLeft();
                break;
            case "top left":
                EdgeOfScreenTopLeft();
                break;
        }
    }

    /*=========================================================
     *      ==== End Switch Statements  ====
     * ==== Start Edge Of Screen Movement ====
     *======================================================== */

    private void EdgeOfScreenRight()
    {
        Counter();
        Vector3 CamMovement = new Vector3(0, 0, 0);
        CamMovement.x += scrollSpeed * Time.deltaTime * 30 /3;
        Camera.main.transform.position += CamMovement;
    }

    private void EdgeOfScreenLeft()
    {
        Counter();
        Vector3 CamMovement = new Vector3(0, 0, 0);
        CamMovement.x -= scrollSpeed * Time.deltaTime * 30 / 3;
        Camera.main.transform.position += CamMovement;
    }

    private void EdgeOfScreenUp()
    {
        Counter();
        Vector3 CamMovement = new Vector3(0, 0, 0);
        CamMovement.z += scrollSpeed * Time.deltaTime * 30 / 3;
        Camera.main.transform.position += CamMovement;
    }

    private void EdgeOfScreenDown()
    {
        Counter();
        Vector3 CamMovement = new Vector3(0, 0, 0);
        CamMovement.z -= scrollSpeed * Time.deltaTime * 30 / 3;
        Camera.main.transform.position += CamMovement;
    }

    private void EdgeOfScreenTopRight()
    {
        Counter();
        Vector3 CamMovement = new Vector3(0, 0, 0);
        CamMovement.x += scrollSpeed * Time.deltaTime * 30 / 3;
        CamMovement.z += scrollSpeed * Time.deltaTime * 30 / 3;
        Camera.main.transform.position += CamMovement;
    }

    private void EdgeOfScreenBottomRight()
    {
        Counter();
        Vector3 CamMovement = new Vector3(0, 0, 0);
        CamMovement.x += scrollSpeed * Time.deltaTime * 30 / 3;
        CamMovement.z -= scrollSpeed * Time.deltaTime * 30 / 3;
        Camera.main.transform.position += CamMovement;
    }

    private void EdgeOfScreenBottomLeft()
    {
        Counter();
        Vector3 CamMovement = new Vector3(0, 0, 0);
        CamMovement.x -= scrollSpeed * Time.deltaTime * 30 / 3;
        CamMovement.z -= scrollSpeed * Time.deltaTime * 30 / 3;
        Camera.main.transform.position += CamMovement;
    }

    private void EdgeOfScreenTopLeft()
    {
        Counter();
        Vector3 CamMovement = new Vector3(0, 0, 0);
        CamMovement.x -= scrollSpeed * Time.deltaTime * 30 / 3;
        CamMovement.z += scrollSpeed * Time.deltaTime * 30 / 3;
        Camera.main.transform.position += CamMovement;
    }

    /*=================================================================
     * ==== End Edge-Of-Screen Movement ====
     * ==== Start Zoom-Scrolling Movement ====
     * ===============================================================*/

    private void ZoomOut()
    {
        if(camOrtho <= ResourceManager.MaxCameraHeight)
        {
            zoomAmount = (Mathf.Pow(camOrtho/6, (float)1.15));
            if(camOrtho + zoomAmount > ResourceManager.MaxCameraHeight)
            {
                zoomAmount = ResourceManager.MaxCameraHeight - camOrtho;
                Camera.main.orthographicSize += zoomAmount;
            }
            else
                Camera.main.orthographicSize+= zoomAmount;
        }
    }

    private void ZoomIn()
    {
        if (camOrtho >= ResourceManager.MinCameraHeight)
        {
            zoomAmount = (Mathf.Pow(camOrtho / 6, (float)0.85));
            if(camOrtho - zoomAmount < ResourceManager.MinCameraHeight)
            {
                zoomAmount = camOrtho - ResourceManager.MinCameraHeight;
                Camera.main.orthographicSize -= zoomAmount;
            }
            else
                Camera.main.orthographicSize-= zoomAmount;
        }
    }
}
