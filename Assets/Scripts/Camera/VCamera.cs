﻿using UnityEngine;
using System.Collections;
using CoreSys;
/*====================================================
 * Wanted a Class to hold camera related limitations and values
 * This is that class, for now
 * Blame: Doug
 * =====================================================
 * 
 *     ==============  Conforms to CQS  ==============
 */
public class VCamera : MonoBehaviour {

    public float speedToHeightRatio;
    public float speedButtonHeldAdjust;
    public float userDefinedMaxSpeedAdjust; //From 1-10
    public float scrollSpeed;
    public float timeButtonHeld;

    public float camOrtho;
    public float zoomAmount;
    protected virtual void Start()
    {
        userDefinedMaxSpeedAdjust = 5f;
        scrollSpeed = 1f;
    }
    protected virtual void Update()
    {
        camOrtho = Camera.main.orthographicSize;
    }

    //Calculates The Camera Movement Speed For All Children
    //Is in the base class so all camera movement can rely on it.
    //Math.floor used to 10,000 to reduce miniscule calculations
    protected void ButtonHeldAdjust()
    {
        speedToHeightRatio = Mathf.Floor((Mathf.Pow(Camera.main.orthographicSize, (float)1.4) + (ResourceManager.MaxCameraHeight/2)) / (100) * 10000)/10000;
        speedButtonHeldAdjust = Mathf.Floor((Mathf.Log(Mathf.Pow(timeButtonHeld, (float)userDefinedMaxSpeedAdjust)))*10000)/10000;
        scrollSpeed = Mathf.Floor((ResourceManager.ScrollSpeed * speedToHeightRatio * speedButtonHeldAdjust) *1000f) /10000f;
    }

    
}
