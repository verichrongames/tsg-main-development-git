﻿using UnityEngine;
using System.Collections;
using CoreSys;

public class Resource : MonoBehaviour {
	
	//Public variables
	public float capacity;
	
	//Variables accessible by subclass
	protected float amountLeft;
	protected ResourceType resourceType;
	
	/*** Game Engine methods, all can be overridden by subclass ***/
	
	protected virtual void Start () {
		amountLeft = capacity;
		resourceType = ResourceType.Unknown;
	}

    protected virtual void Update()
    {

    }
	
	/*** Public methods ***/
	
	public void Remove(float amount) {
		amountLeft -= amount;
		if(amountLeft < 0) amountLeft = 0;
	}
	
	public bool isEmpty() {
		return amountLeft <= 0;
	}
	
	public ResourceType GetResourceType() {
		return resourceType;
	}

	protected void CalculateCurrentHealth (float lowSplit, float highSplit) {
		//healthPercentage = amountLeft / capacity;
		//healthStyle.normal.background = ResourceManager.GetResourceHealthBar(resourceType);
	}
}