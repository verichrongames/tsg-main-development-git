﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CoreSys.Buildings;

[BoltGlobalBehaviour(BoltNetworkModes.Host, "TSGBlueNebulaBolt")]
public class NetworkCallbacksHost : Bolt.GlobalEventListener
{
    public override void SceneLoadLocalBegin(string map)
    {
        Vector3 mapOrigin = new Vector3(0,0,0);
        base.SceneLoadLocalBegin(map);
        int x = 0;
        GameObject resourcesParent = BoltNetwork.Instantiate(BoltPrefabs.ResourcesContainer, mapOrigin, Quaternion.identity);
        while (x < 200)
        {
            var origin = new Vector3(Random.Range(-290, 290), Random.Range(-290, 290), 0);
            GameObject newAsteroid;
            newAsteroid = BoltNetwork.Instantiate(BoltPrefabs.Asteroid, origin, Quaternion.identity);
            newAsteroid.transform.parent = resourcesParent.transform;
            x++;
        }
    }

    public override void SceneLoadRemoteDone(BoltConnection connection)
    {
        base.SceneLoadRemoteDone(connection);
    }

    public override void SceneLoadLocalDone(string map)
    {
        base.SceneLoadLocalBegin(map);
        Vector3 mapOrigin = new Vector3(0, 0, 0);
        GameObject player = BoltNetwork.Instantiate(BoltPrefabs.Player, mapOrigin, Quaternion.identity);
        BlockHandler buildingManager = player.GetComponentInChildren<BlockHandler>();
        int spawnArea = Random.Range(0, 3);
        switch (spawnArea)
        {
            case 0:
                Debug.Log("Spawn Area 0");
                Vector3 spawnLocation0 = new Vector3(Random.Range(-290, -225), 1, Random.Range(225, 290));
                GameObject coreSpawn0 = BoltNetwork.Instantiate(BoltPrefabs.MainCore, spawnLocation0, Quaternion.identity);
                Core core0 = coreSpawn0.GetComponent<Core>();
                core0.player = player.GetComponent<Player>();
                coreSpawn0.transform.parent = buildingManager.transform;
                Camera.main.transform.position = new Vector3(spawnLocation0.x, spawnLocation0.y, -10);
                break;
            case 1:
                Debug.Log("Spawn Area 1");
                Vector3 spawnLocation1 = new Vector3(Random.Range(-290, -225), 1, Random.Range(-225, -290));
                GameObject coreSpawn1 = BoltNetwork.Instantiate(BoltPrefabs.MainCore, spawnLocation1, Quaternion.identity);
                Core core1 = coreSpawn1.GetComponent<Core>();
                core1.player = player.GetComponent<Player>();
                coreSpawn1.transform.parent = buildingManager.transform;
                Camera.main.transform.position = new Vector3(spawnLocation1.x, spawnLocation1.y, -10);
                break;
            case 2:
                Debug.Log("Spawn Area 2");
                Vector3 spawnLocation2 = new Vector3(Random.Range(290, 225), 1, Random.Range(225, 290));
                GameObject coreSpawn2 = BoltNetwork.Instantiate(BoltPrefabs.MainCore, spawnLocation2, Quaternion.identity);
                Core core2 = coreSpawn2.GetComponent<Core>();
                core2.player = player.GetComponent<Player>();
                coreSpawn2.transform.parent = buildingManager.transform;
                Camera.main.transform.position = new Vector3(spawnLocation2.x, spawnLocation2.y, -10);
                break;
            case 3:
                Debug.Log("Spawn Area 3");
                Vector3 spawnLocation3 = new Vector3(Random.Range(290, 225), 1,  Random.Range(-225, -290));
                GameObject coreSpawn3 = BoltNetwork.Instantiate(BoltPrefabs.MainCore, spawnLocation3, Quaternion.identity);
                Core core3 = coreSpawn3.GetComponent<Core>();
                core3.player = player.GetComponent<Player>();
                coreSpawn3.transform.parent = buildingManager.transform;
                Camera.main.transform.position = new Vector3(spawnLocation3.x, spawnLocation3.y, -10);
                break;
        }
    }

    public override void PortMappingChanged(Bolt.INatDevice device, Bolt.IPortMapping portMapping)
    {
        base.PortMappingChanged(device, portMapping);
        Debug.Log("Port Mapping Changed, UPnP Port Successful");
    }
}