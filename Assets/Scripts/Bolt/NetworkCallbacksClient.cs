﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[BoltGlobalBehaviour(BoltNetworkModes.Client, "TSGBlueNebulaBolt")]
public class NetworkCallbacksClient : Bolt.GlobalEventListener
{
    public override void SceneLoadLocalBegin(string map)
    {
        base.SceneLoadLocalBegin(map);
    }

    public override void SceneLoadLocalDone(string map)
    {
        base.SceneLoadLocalBegin(map);
    }

    public override void PortMappingChanged(Bolt.INatDevice device, Bolt.IPortMapping portMapping)
    {
        base.PortMappingChanged(device, portMapping);
        Debug.Log("Port Mapping Changed, UPnP Port Successful");
    }
}