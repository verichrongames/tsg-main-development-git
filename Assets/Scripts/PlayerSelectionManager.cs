﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System;
using TagFrenzy;

/*============================================
 * Class manages player object selections
 * Some Sections could be split into seperate classes
 * Blame: Doug
 * ==========================================*/

public class PlayerSelectionManager : MonoBehaviour
{

    public List<KeyValuePair<GameObject, IPlayerObject>> selectedObjectsList = new List<KeyValuePair<GameObject, IPlayerObject>>();



    UserInput userInput;
    public Player player;
    ObjectHUDPanel objectHUDPanel;
    ObjectMovementManager movementManager;
    SelectedObjectUIManager objectUIManager;

    public bool objectIsSelected;
    bool shiftHeld;

    void OnEnable()
    {
        MouseEvents.LeftClickEvent += LeftClickSwitches;
        MouseEvents.RightClickEvent += RightClickSwitches;
        MouseEvents.DoubleLeftClickEvent += LeftClickSwitches;
        KeyboardEvents.MiscButtonHeldEvent += ModKeySwitches;
        KeyboardEvents.MiscButtonUpEvent += ModKeySwitches;
    }

    void OnDisable()
    {
        MouseEvents.LeftClickEvent -= LeftClickSwitches;
        MouseEvents.RightClickEvent -= RightClickSwitches;
        MouseEvents.DoubleLeftClickEvent -= LeftClickSwitches;
        KeyboardEvents.MiscButtonHeldEvent -= ModKeySwitches;
        KeyboardEvents.MiscButtonUpEvent -= ModKeySwitches;
    }

    void Start()
    {
        player = GetComponent<Player>();
        userInput = GetComponent<UserInput>();
        objectUIManager = GetComponentInChildren<SelectedObjectUIManager>();
        objectHUDPanel = GameObject.Find("ObjectHUD").GetComponent<ObjectHUDPanel>();
        movementManager = GetComponentInChildren<ObjectMovementManager>();
    }

    void Update()
    {
        IsObjectSelected();
    }

    /*=========================================================
     *    ==== Start Right/Left Click/Keyboard Handling ====
     *======================================================== */

    //Handles mod key's such as shift or ctrl
    private void ModKeySwitches(string button)
    {
        switch (button)
        {
            case "Shift":
                shiftHeld = true;
                break;
            case "ShiftUp":
                shiftHeld = false;
                break;
        }
    }

    //Determines where Left Click event data needs to be forwarded
    private void LeftClickSwitches(string type, LClickEventArgs e)
    {
        switch (type)
        {
            case "DoubleObject":
                DoubleLeftClickObject(e);
                break;
            case "Object":
                LeftClickObject(e);
                break;
            case "Ground":
                LeftClickGround();
                break;
        }
    }

    //Determines where right click event data needs to be forwarded
    private void RightClickSwitches(string type, RClickEventArgs e)
    {
        switch (type)
        {
            case "Object":
                RightClickObject(e);
                break;
            case "Ground":
                RightClickGround(e);
                break;
        }
    }

    //Recieves  call from switch. Determines what methods needs to be called
    private void LeftClickObject(LClickEventArgs e)
    {
        IPlayerObject playerObject = (IPlayerObject)e.HitObject.GetComponent(typeof(IPlayerObject));
        if (CheckIPlayerObject(playerObject))
        {
            bool alreadySelected = FindObjectInList(e.HitObject);
            if (selectedObjectsList.Count > 0 && !alreadySelected)
            {
                if (shiftHeld)
                {
                    AddSelectedObject(e);
                }
                else
                {
                    ChangeSelectedObjectFilter(e);
                }
            }
            else if (selectedObjectsList.Count > 0 && alreadySelected)
            {
                ClearSelectionList(e.HitObject, playerObject);
            }
            else if (!alreadySelected)
            {
                AddSelectedObject(e);
            }
        }
    }

    //Deselects all objects
    private void LeftClickGround()
    {
        ClearSelectionList();
        ClearObjectUI();
    }

    //Passes call onto DoubleClickSelection
    private void DoubleLeftClickObject(LClickEventArgs e)
    {
        IPlayerObject playerObject = (IPlayerObject)e.HitObject.GetComponent(typeof(IPlayerObject));
        if (CheckIPlayerObject(playerObject))
        {
            DoubleClickSelection(e);
        }
    }

    //Sends rightclick information to the selected objects
    private void RightClickObject(RClickEventArgs e)
    {
        if (selectedObjectsList.Count > 0)
        {
            foreach (KeyValuePair<GameObject, IPlayerObject> playerObject in selectedObjectsList)
            {
                playerObject.Value.RightClickObject(e);
            }
        }
    }

    //Sends rightclick information to the selected objects
    private void RightClickGround(RClickEventArgs e)
    {
        if (selectedObjectsList.Count > 0)
        {
            foreach (KeyValuePair<GameObject, IPlayerObject> playerObject in selectedObjectsList)
            {
                if (CheckIMoveableObject((IMoveableObject)playerObject.Key.GetComponent(typeof(IMoveableObject))))
                {
                    movementManager.NewDestination(playerObject.Key, e, shiftHeld);
                }
                else if (playerObject.Key.GetComponent<ITestInterface>() != null)
                {
                    playerObject.Key.GetComponent<ITestInterface>().AddNewDestination(e.HitPoint);
                }

                ICanBuild canBuild = playerObject.Key.GetComponent<ICanBuild>();

                if (canBuild != null)
                {
                    canBuild.SetRallyPoint(e.HitPoint);
                }
            }
        }
    }

    /*=========================================================
     *    ==== Start Object Selection/Deselection ====
     *======================================================== */

    //Public Method to change selection without a click
    public void ChangeSelection(GameObject objectToSelect, Player objectPlayer)
    {
        ChangeSelectedObjectFilter(objectToSelect, objectPlayer);
    }

    //This selects all objects of a certain tag on the screen
    private void DoubleClickSelection(LClickEventArgs e)
    {
        if (selectedObjectsList.Count > 0 && FindObjectInList(e.HitObject))
        {
            foreach (GameObject gameObject in GameObject.FindGameObjectsWithTag(e.HitObject.tag))
            {
                if (gameObject.GetComponent<Renderer>().isVisible)
                {
                    AddSelectedObject(gameObject);
                }
            }
        }
        else
        {
            AddSelectedObject(e);
        }
    }

    //Adds newly selected object to list of selected objects
    private void AddSelectedObject(LClickEventArgs e)
    {
        //PlayerObject playerObject = e.HitObject.GetComponent<PlayerObject>(); //Gets the player object class of the newly selected object
        IPlayerObject playerObject = (IPlayerObject)e.HitObject.GetComponent(typeof(IPlayerObject));
        if (CheckIPlayerObject(playerObject) && !CheckIfInBuildMode())
        {
            if (playerObject.player.GetInstanceID() == player.GetInstanceID())
            {
                selectedObjectsList.Add(new KeyValuePair<GameObject, IPlayerObject>(e.HitObject, playerObject));
                playerObject.SetIsSelected(true);
                e.HitObject.AddCollectionReferance(this);
                CheckUI(e.HitObject, playerObject, "NewSelection");
            }
        }
    }

    //Override that does not require an eventargs
    private void AddSelectedObject(GameObject e)
    {
        //PlayerObject playerObject = e.GetComponent<PlayerObject>(); //Gets the player object class of the newly selected object
        IPlayerObject playerObject = (IPlayerObject)e.GetComponent(typeof(IPlayerObject));
        if (CheckIPlayerObject(playerObject))
        {
            if (playerObject.player.GetInstanceID() == player.GetInstanceID())
            {
                selectedObjectsList.Add(new KeyValuePair<GameObject, IPlayerObject>(e, playerObject));
                playerObject.SetIsSelected(true);
                CheckUI(e, playerObject, "NewSelection");
            }
        }
    }

    // Filters the call to change the selected object
    private void ChangeSelectedObjectFilter(LClickEventArgs e)
    {
        if (selectedObjectsList.Count > 0 && e.HitObject.name != "Ground" && !CheckIfInBuildMode())
        {
            IPlayerObject playerObject = (IPlayerObject)e.HitObject.GetComponent(typeof(IPlayerObject));
            if (CheckIPlayerObject(playerObject))
            {
                if (playerObject.player.GetInstanceID() == player.GetInstanceID())
                {
                    ChangeSelectedObject(e.HitObject, playerObject, e.Player);
                }
            }
        }
    }

    // Overload to allow for a non-clicked change of selection
    private void ChangeSelectedObjectFilter(GameObject gameObject, Player player)
    {
        if (selectedObjectsList.Count > 0 && gameObject.name != "Ground")
        {
            IPlayerObject playerObject = (IPlayerObject)gameObject.GetComponent(typeof(IPlayerObject));
            if (CheckIPlayerObject(playerObject))
            {
                if (playerObject.player.GetInstanceID() == player.GetInstanceID())
                {
                    ChangeSelectedObject(gameObject, playerObject, player);
                }
            }
        }
    }

    //Changes the selected object if one is already selected
    private void ChangeSelectedObject(GameObject gameObject, IPlayerObject playerObject, Player controller)
    {
        ClearSelectionList(gameObject, playerObject);
        selectedObjectsList.Add(new KeyValuePair<GameObject, IPlayerObject>(gameObject, playerObject));
        playerObject.SetIsSelected(true);
        CheckUI(gameObject, playerObject, "ChangeSelection");
    }

    /*=========================================================
    *    ==== User Interface Related Methods ====
    *======================================================== */

    //Checks to see if object needs a UI set.
    private void CheckUI(GameObject gameObject, IPlayerObject playerObject, string checkType)
    {
        if (gameObject.HasTag("CanBuild"))
        {
            switch (checkType)
            {
                case "NewSelection":
                    NewObjectUI(gameObject, playerObject);
                    break;
                case "ChangeSelection":
                    ChangeObjectUI(gameObject, playerObject);
                    break;
                case "Ground":
                    ClearObjectUI();
                    break;
            }
        }
        else
        {
            ClearObjectUI();
        }
    }

    //Calls for a new object UI when an object is selected and no rpevious objects where selected
    private void NewObjectUI(GameObject gameObject, IPlayerObject playerObject)
    {
        objectUIManager.SetSelectedUI(gameObject, playerObject);
    }

    //Calls to change the UI from previously selected object to the currently selected objects UI
    private void ChangeObjectUI(GameObject gameObject, IPlayerObject playerObject)
    {
        objectUIManager.ChangeSelectedUI(gameObject, playerObject);
    }

    //Calls to clear the UI
    private void ClearObjectUI()
    {
        objectUIManager.ClearSelectedUI();
    }


    /*=========================================================
     *    ==== Start Supportive Misc. Methods ====
     *======================================================== */

    //Clears the selection list, except for the newly selected object if it's already in the list
    private void ClearSelectionList(GameObject gameObject, IPlayerObject playerObject)
    {
        int arraySize = selectedObjectsList.Count;
        for (int i = arraySize - 1; i >= 0; i--)
        {
            if (selectedObjectsList[i].Key.GetInstanceID() != gameObject.GetInstanceID())
            {
                selectedObjectsList[i].Key.RemoveCollectionReferance(this);
                selectedObjectsList[i].Value.SetIsSelected(false);
                selectedObjectsList.RemoveAt(i);
            }
        }
    }

    //Overload to just clear the entire list
    private void ClearSelectionList()
    {
        int arraySize = selectedObjectsList.Count;
        for (int i = arraySize - 1; i >= 0; i--)
        {
            selectedObjectsList[i].Key.RemoveCollectionReferance(this);
            selectedObjectsList[i].Value.SetIsSelected(false);
            selectedObjectsList.RemoveAt(i);
        }
    }

    //Finds a player object already in the list of selected player objects.
    private bool FindObjectInList(GameObject gameObject)
    {
        int instanceID = gameObject.GetInstanceID();

        for (int i = 0; i + 1 <= selectedObjectsList.Count; i++)
        {
            if (selectedObjectsList[i].Key.GetInstanceID() == instanceID)
            {
                return true;
            }
        }
        return false;
    }

    //Finds a player object already in the list of selected player objects.
    private int FindObjectInList(int ID)
    {
        for (int i = 0; i + 1 <= selectedObjectsList.Count; i++)
        {
            if (selectedObjectsList[i].Key.GetInstanceID() == ID)
            {
                return i;
            }
        }
        return -1;
    }

    //Checks to see if there is a currently sleected object to set public bool
    private void IsObjectSelected()
    {
        if (selectedObjectsList.Count > 0)
            objectIsSelected = true;
        else
            objectIsSelected = false;
    }

    public GameObject GetSelectedObject()
    {
        if (selectedObjectsList.Count == 1)
        {
            return selectedObjectsList[0].Key;
        }
        else return null;
    }

    /*=========================================================
     *        ==== Start Player Hover Methods  ====
     * == (These will need to be their own class eventually) ==
     *======================================================== */

    //Calls for the creation of the object info panel when hovering over an object
    //Handles sprites and textures
    //This is a frankenstein of a method, disgusting, I need to fix it //Youre disgusting
    private void ObjectHover(string type, MouseHoverEventArgs e)
    {
        if (type != "UserInterface")
        {
            SpriteRenderer objectSpriteRenderer = e.HitObject.GetComponent<SpriteRenderer>();

            if (objectSpriteRenderer != null)
            {
                IPlayerObject playerObject = (IPlayerObject)e.HitObject.GetComponent(typeof(IPlayerObject));
                objectHUDPanel.CreateObjectInfoPanel(new UIObjectInfoType(objectSpriteRenderer.sprite, playerObject.HitPoints, playerObject.MaxHitPoints, playerObject.ObjectName, playerObject.player.name));
            }
            else
            {
                MeshRenderer objectMeshrenderer = e.HitObject.GetComponent<MeshRenderer>();
                if (objectMeshrenderer != null)
                {
                    IPlayerObject playerObject = (IPlayerObject)e.HitObject.GetComponent(typeof(IPlayerObject));
                    objectHUDPanel.CreateObjectInfoPanel(new UIObjectInfoType(objectMeshrenderer.material, playerObject.HitPoints, playerObject.MaxHitPoints, playerObject.ObjectName, playerObject.player.name));

                }
                else
                {
                    Debug.Log("Put a fucking sprite on this bitch. Or tell me what kind of image/texture...etc you will normally be using"); //k
                }

            }
        }
    }

    //Calls to remove object info panel if not hovering over object 
    private void GroundHover(string type, MouseHoverEventArgs e)
    {
        objectHUDPanel.RemoveObjectinfoPanel();
    }

    /*=========================================================
     *        ==== Start Misc Methods  ====
     *======================================================== */

    //Public method for referance removal once an object is destroyed
    public void RemoveReferance(int ID)
    {
        int key = FindObjectInList(ID);
        if (key >= 0)
        {
            selectedObjectsList.RemoveAt(key);
        }
    }

    //Checks to see if there is an IplayerObject present
    private bool CheckIPlayerObject(IPlayerObject playerObject)
    {
        if (object.ReferenceEquals(playerObject, null))
        {
            return false;
        }
        else
        {
            return true;
        }

    }

    //checks to see if there is an IMoveableObject
    private bool CheckIMoveableObject(IMoveableObject moveableObject)
    {
        if (object.ReferenceEquals(moveableObject, null))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    ////checks to see if there is an IMoveableObject
    //private bool CheckIMoveableObject(IMoveableObject moveableObject)
    //{
    //    if (object.ReferenceEquals(moveableObject, null))
    //    {
    //        return false;
    //    }
    //    else
    //    {
    //        return true;
    //    }
    //}

    //checks to see if there is an IBuildableObject
    private bool CheckIBuildableObject(IBuildableObject buildableObject)
    {
        if (object.ReferenceEquals(buildableObject, null))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    //checks to see if there is an IBuildableObject
    private bool CheckICanBuildObject(ICanBuild canBuildObject)
    {
        if (object.ReferenceEquals(canBuildObject, null))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    //Checks for ICanBuild Objects in the build queue
    private bool CheckIfInBuildMode()
    {
        for (int i = 0; i < selectedObjectsList.Count; i++)
        {
            if (selectedObjectsList[i].Key.GetComponent<ICanBuild>() != null)
            {
                return selectedObjectsList[i].Key.GetComponent<ICanBuild>().InBuildMode;
            }
        }

        return false;
    }

}
