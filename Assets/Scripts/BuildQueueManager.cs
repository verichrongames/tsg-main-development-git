﻿using UnityEngine;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using CoreSys.Objects;
using CoreSys;
using CoreSys.Buildings;

/*============================================
 * This class manages the build queue
 * for whatever object has a build queue
 * 
 * This class is very tightly coupled to the 
 * other managers.
 * 
 * Blame: Doug
 * ==========================================*/


public class BuildQueueManager : MonoBehaviour {

    Player player;

    Dictionary<int, BuildQueueType> buildQueues = new Dictionary<int, BuildQueueType>();
    List<int> activeBuildQueues = new List<int>();
    InstantiatedObjectsList instantiatedList;
    SelectedObjectUIManager objectUIManager;
    PlayerSelectionManager playerSelectionManager;
    ObjectMovementManager objectMovementManager;
    GridResourceManager resourceManager;

    GameObject unitsHolder;
    GameObject buildingsHolder;

	void Start () 
    {
        player = GetComponentInParent<Player>();
        objectUIManager = GetComponent<SelectedObjectUIManager>();
        playerSelectionManager = transform.root.GetComponent<PlayerSelectionManager>();
        instantiatedList = GetComponent<InstantiatedListAdder>().instantiatedList;
        objectMovementManager = GetComponent<ObjectMovementManager>();

        unitsHolder = instantiatedList.GetGameObject("Units", "Manager", "Active", player);
        buildingsHolder = instantiatedList.GetGameObject("Buildings", "Manager", "Active", player);
        resourceManager = instantiatedList.GetGameObject("Grid", "Manager", "Active", player).GetComponent<GridResourceManager>();
	}
	
	void Update () 
    {
        CycleThroughBuildQueues();
	}

    /*=======================================================
     *     ======= Queue Management and Creation =======
     * ====================================================*/

    //Public method to deincriment or remove a build queue item
    public void DeIncrimentBuildQueue(GameObject queueOwner, string orderType, int incriment)
    {
        int ownerID = queueOwner.GetInstanceID();

        if(buildQueues.ContainsKey(ownerID))
        {
            KeyValuePair<bool, int> searchResults = FindBuildQueueItem(ownerID, orderType);
            if(searchResults.Key)
            {
                DeIncrimentBuildQueue(ownerID, searchResults.Value, incriment);
            }
        }
    }

    //Public method to recieve call to add a new item to a queue or make a brand new queue, as well as incriment.
    public void NewBuildQueueItem(GameObject queueOwner, string orderType, int itemCount)
    {
        int ownerID = queueOwner.GetInstanceID();


        if(buildQueues.ContainsKey(ownerID))
        {
            KeyValuePair<bool, int> searchResults = FindBuildQueueItem(ownerID, orderType);
            if(searchResults.Key)
            {
                IncrimentBuildQueue(ownerID, searchResults.Value, itemCount);
            }
            else
            {
                AddNewQueueItem(ownerID, orderType, itemCount);
            }
        }
        else
        {
            AddNewQueue(queueOwner, ownerID, orderType, itemCount);
        }
    }

    public BuildQueueType GetBuildQueue(GameObject queueOwner)
    {
        if (buildQueues.ContainsKey(queueOwner.GetInstanceID()))
        {
            return buildQueues[queueOwner.GetInstanceID()];
        }
        return null;
    }

    //This will create a new queue for a building that does not have a queue. This does not add an item to a building queue.
    private void AddNewQueue(GameObject queueOwner, int ownerID , string orderType, int itemCount )
    {
        GameObject objectToBuild = GetBuildableObjects.GetBuildables(orderType);
        IBuildableObject buildableObject = (IBuildableObject)objectToBuild.GetComponent(typeof(IBuildableObject));
        ICanBuild canBuildOwner = queueOwner.GetComponent<ICanBuild>();
        Grid ownerGrid = queueOwner.transform.parent.GetComponent<Grid>();

        if(ownerGrid == null)
        {
            BuildQueueType newQueue = new BuildQueueType(queueOwner, new BuildQueueItEmptype(orderType, objectToBuild, buildableObject, itemCount), canBuildOwner, player);
            buildQueues.Add(ownerID, newQueue);
            activeBuildQueues.Add(ownerID);
            NewUIBuildQueueItem(orderType, itemCount);
            queueOwner.AddCollectionReferance(this);
        }
        else
        {
            Core ownerCore = queueOwner.GetComponent<Core>();
            BuildQueueType newQueue = new BuildQueueType(queueOwner, ownerGrid, ownerCore, new BuildQueueItEmptype(orderType, objectToBuild, buildableObject, itemCount), canBuildOwner, player);
            buildQueues.Add(ownerID, newQueue);
            activeBuildQueues.Add(ownerID);
            NewUIBuildQueueItem(orderType, itemCount);
            queueOwner.AddCollectionReferance(this);
        }
        SetCurrentlyBuildingBool(true, buildQueues[ownerID].ICanBuildOwner);
    }

    //Adds a new item to a buildqueue
    private void AddNewQueueItem(int ownerID, string orderType, int itemCount)
    {
        if(buildQueues[ownerID].BuildQueueItems.Count == 0)
        {
            activeBuildQueues.Add(ownerID);
        }
        GameObject objectToBuild = GetBuildableObjects.GetBuildables(orderType);
        IBuildableObject buildableObject = (IBuildableObject)objectToBuild.GetComponent(typeof(IBuildableObject));

        buildQueues[ownerID].BuildQueueItems.Add(new BuildQueueItEmptype(orderType, objectToBuild, buildableObject, itemCount));
        NewUIBuildQueueItem(orderType, itemCount);

        SetCurrentlyBuildingBool(true, buildQueues[ownerID].ICanBuildOwner);

    }

    //Incriments the number of a specific build queue item
    private void IncrimentBuildQueue(int ownerID, int queueIndex, int incriment)
    {
        buildQueues[ownerID].BuildQueueItems[queueIndex].Count += incriment;
        IncrimentBuildQueueItem(buildQueues[ownerID].BuildQueueOwner, buildQueues[ownerID].BuildQueueItems[queueIndex].QueueItEmptype, incriment);
    }

    //DeIncriments the number of a specific build queue item. Will call for deletion if it deincriments to 0 or less.
    private void DeIncrimentBuildQueue(int ownerID, int queueIndex, int incriment)
    {
        DeIncrimentUIBuildQueueItem(buildQueues[ownerID].BuildQueueOwner, buildQueues[ownerID].BuildQueueItems[queueIndex].QueueItEmptype, incriment);
        if(buildQueues[ownerID].BuildQueueItems[queueIndex].Count > incriment)
        {
            buildQueues[ownerID].BuildQueueItems[queueIndex].Count -= incriment;
        }
        else if(buildQueues[ownerID].BuildQueueItems[queueIndex].Count <= incriment)
        {
            RemoveBuildQueueItem(ownerID, queueIndex);
        }
    }

    //Will delete a build queue item
    private void RemoveBuildQueueItem(int ownerID, int queueIndex)
    {
        SetCurrentlyBuildingBool(false, buildQueues[ownerID].ICanBuildOwner);
        StopResourceUsage(ownerID);
        activeBuildQueues.Remove(ownerID);
        buildQueues[ownerID].BuildQueueItems.RemoveAt(queueIndex);
    }

    //Will remove an objects entire queue. Used when object is destroyed or deactivated.
    private void RemoveBuildQueue(int ownerID)
    {
        buildQueues[ownerID].BuildQueueOwner.RemoveCollectionReferance(this);
        buildQueues.Remove(ownerID);
    }

    //Find a build queue item in the queue of a game object
    private KeyValuePair<bool, int> FindBuildQueueItem(int ownerID, string queueObject)
    {
        if(buildQueues[ownerID].BuildQueueItems.Count > 0)
        {
            for (int i = 0; i < buildQueues[ownerID].BuildQueueItems.Count; i++)
            {
                if(buildQueues[ownerID].BuildQueueItems[i].QueueItEmptype == queueObject)
                {
                    return new KeyValuePair<bool, int>(true, i);
                }
            }
        }

        return new KeyValuePair<bool, int>(false, 0);
    }

    //Will find and return index of a queue owner in the active build queues list
    private KeyValuePair<bool, int> FindActiveQueueItems(int ownerID)
    {
        if (activeBuildQueues.Count > 0)
        {
            for(int i = 0; i < activeBuildQueues.Count; i++)
            {
                if(activeBuildQueues[i] == ownerID)
                {
                    return new KeyValuePair<bool,int>(true, i);
                }
            }
        }

        return new KeyValuePair<bool,int>(false, 0);
    }

    //Public method for referance removal once an object is destroyed
    public void RemoveReferance(int ID)
    {
        if (buildQueues.ContainsKey(ID))
        {
            buildQueues.Remove(ID);
        }
    }

    /*=======================================================
     *          ===== UI Updating Methods =====
     * ====================================================*/

    //Sends call for the UI to add/update the build queue bar with a new item
    private void NewUIBuildQueueItem(string queueItEmptype, int count)
    {
        objectUIManager.NewBuildQueueItem(queueItEmptype, count);
    }

    //De-Incriments the build queue bar when it is called, only proceeds if there is an object selected and that object matches the build queues owner
    private void DeIncrimentUIBuildQueueItem(GameObject queueOwner, string queueItEmptype, int count)
    {
        if (playerSelectionManager.GetSelectedObject() != null)
        {
            if (playerSelectionManager.GetSelectedObject().GetInstanceID() == queueOwner.GetInstanceID())
            {
                objectUIManager.DeincrimentBuildQueueItem(queueItEmptype, count);
            }
        }
    }

    //Incriments the build queue bar when it is called, only proceeds if there is an object selected and that object matches the build queues owner
    private void IncrimentBuildQueueItem(GameObject queueOwner, string queueItEmptype, int count)
    {
        if (playerSelectionManager.GetSelectedObject() != null)
        {
            if (playerSelectionManager.GetSelectedObject().GetInstanceID() == queueOwner.GetInstanceID())
            {
                objectUIManager.IncrimentBuildQueueItem(queueItEmptype, count);
            }
        }
    }

    //Updates the build queue bar every frame with the progress of the current item.
    private void UpdateBuildQueueProgress(GameObject queueOwner, string queueItEmptype, float progress)
    {
        if(progress <= 1 && progress >= 0)
        {
            if (playerSelectionManager.GetSelectedObject() != null)
            {
                if(playerSelectionManager.GetSelectedObject().GetInstanceID() == queueOwner.GetInstanceID())
                {
                    objectUIManager.UpdateQueueProgress(queueItEmptype, progress);
                }
            }
        }
    }

    /*=======================================================
     *  ===== Queue Running and Resource Manager hooks =====
     * ====================================================*/

    //Cycles through each build queue
    private void CycleThroughBuildQueues()
    {
        foreach(int key in buildQueues.Keys)
        {
            if (buildQueues[key].BuildQueueItems.Count > 0)
            {
                SetCurrentlyBuildingBool(true, buildQueues[key].ICanBuildOwner);
                SendResourceUsage(key);
                GetBuildSpeed(key);
                CheckBuildQueueProgress(key);
            }
        }
    }

    //Checks if the build queue item has reached 
    private void CheckBuildQueueProgress(int ID)
    {
        if (buildQueues[ID].BuildQueueItems[0].BuildProgress >= 1)
        {
            if (buildQueues[ID].QueueOwnerCore != null)
            {
                buildQueues[ID].BuildQueueItems[0].SentResourceUsage = false;
                StopResourceUsage(ID);
            }
            buildQueues[ID].BuildQueueItems[0].BuildProgress = 0;
            SetICanBuildProgress(buildQueues[ID].BuildQueueItems[0].BuildProgress, buildQueues[ID].ICanBuildOwner);
            InstantiateQueuedObject(buildQueues[ID].BuildQueueItems[0].ObjectToBuild, buildQueues[ID].BuildQueueOwner);
            DeIncrimentBuildQueue(buildQueues[ID].BuildQueueOwner, buildQueues[ID].BuildQueueItems[0].QueueItEmptype, 1);

        }
    }

    //Sends Resource Usage to resource manager
    private void SendResourceUsage(int ID)
    {
        if(!buildQueues[ID].BuildQueueItems[0].SentResourceUsage)
        {
            if(buildQueues[ID].QueueOwnerCore != null)
            {
                float metalUsage = ((float)buildQueues[ID].BuildQueueItems[0].BuildableObject.metalCost / buildQueues[ID].BuildQueueItems[0].BuildableObject.baseBuildTime);
                float powerUsage = ((float)buildQueues[ID].BuildQueueItems[0].BuildableObject.powerCost / buildQueues[ID].BuildQueueItems[0].BuildableObject.baseBuildTime);
                buildQueues[ID].QueueOwnerCore.MakingUnit(metalUsage, powerUsage);
                buildQueues[ID].BuildQueueItems[0].SentResourceUsage = true;
            }
        }
    }

    //Gets the build speed from resource manager
    private void GetBuildSpeed(int ID)
    {
        float buildSpeed = resourceManager.GetBuildPercentage();
        ChangeBuildProgress(ID, buildSpeed);
    }

    //Changes the queue'd objects progress
    private void ChangeBuildProgress(int ID, float buildSpeed)
    {
        float progress = Time.deltaTime / (float)buildQueues[ID].BuildQueueItems[0].BuildableObject.baseBuildTime * buildSpeed;

        buildQueues[ID].BuildQueueItems[0].BuildProgress += progress;
        UpdateBuildQueueProgress(buildQueues[ID].BuildQueueOwner, buildQueues[ID].BuildQueueItems[0].QueueItEmptype, buildQueues[ID].BuildQueueItems[0].BuildProgress);
        SetICanBuildProgress(buildQueues[ID].BuildQueueItems[0].BuildProgress, buildQueues[ID].ICanBuildOwner);
    }

    //Called to clear resource usage to the Core
    private void StopResourceUsage(int ID)
    {
        buildQueues[ID].QueueOwnerCore.FinishedUnit();
    }


    /*=======================================================
    *  ===== Queue End Object Instantiation =====
    * ====================================================*/

    //Will instantiate a queue object when it is requested
    private void InstantiateQueuedObject(GameObject toInstantiate, GameObject queueOwner)
    {
        GameObject newUnit = (GameObject)Instantiate(toInstantiate, queueOwner.transform.position, new Quaternion(0, 0, 0, 0));
        SetInstantiatedObjectParent(newUnit);
        StartCoroutine(SetNewObjectDestination(newUnit, queueOwner));
    }

    //Will set the newly instantiated opbjects parent to the holder
    private void SetInstantiatedObjectParent(GameObject newObject)
    {
        newObject.transform.SetParent(unitsHolder.transform);
    }

    //Will set the newly instantiated objects destiantion on the next frame
    IEnumerator SetNewObjectDestination(GameObject newObject, GameObject queueOwner)
    {
        yield return 0;

        ICanBuild canBuild = (ICanBuild)queueOwner.GetComponent(typeof(ICanBuild));

        objectMovementManager.NewDestination(newObject, canBuild.RallyPoint);
    }

    /*=======================================================
     *          ======= Type Checking=======
     * ====================================================*/

    //checks to see if there is an IBuildableObject
    private bool CheckIBuildableObject(IBuildableObject buildableObject)
    {
        if (object.ReferenceEquals(buildableObject, null))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    /*=======================================================
     *          ======= Misc Methods=======
     * ====================================================*/

    //Sets the CurrentBuilding bool on the ICanBuild object
    private void SetCurrentlyBuildingBool(bool currentlyBuilding, ICanBuild canbuildObject)
    {
        canbuildObject.CurrentlyBuilding = currentlyBuilding;
    }

    //Sets the ICanBuild building progress 
    private void SetICanBuildProgress(float progress, ICanBuild canbuildObject)
    {
        canbuildObject.BuildProgress = progress;
    }
}
