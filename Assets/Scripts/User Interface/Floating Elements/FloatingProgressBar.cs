﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FloatingProgressBar : MonoBehaviour
{

    Image blackProgress;
    Image orangeProgress;

    GameObject canvas;

    IPlayerObject parentPlayerObject;
    ICanBuild parentCanBuildObject;
    ICanBuildSelf parentCanBuildSelfObject;



    bool isCanBuild;
    bool isCanBuildSelf;

    void Start()
    {
        GetObjectComponents();
        GetParentRefernaces();
    }

    void Update()
    {
        GetParentProgress();
    }

    //Gets the buildings build progress on Update
    private void GetParentProgress()
    {
        if(isCanBuild && !isCanBuildSelf)
        {
            if (parentCanBuildObject.CurrentlyBuilding)
            {
                EnableImages();
                orangeProgress.fillAmount = parentCanBuildObject.BuildProgress;
            }
            else
            {
                DisableImages();
            }
        }
        else if(isCanBuildSelf)
        {
            if (parentCanBuildSelfObject.CurrentlyBuilding)
            {
                EnableImages();
                orangeProgress.fillAmount = parentCanBuildSelfObject.BuildProgress/100;
            }
            else
            {
                DisableImages();
            }
        }
    }

    private void EnableImages()
    {
        blackProgress.enabled = true;
        orangeProgress.enabled = true;
    }

    private void DisableImages()
    {
        blackProgress.enabled = false;
        orangeProgress.enabled = false;
    }

    //Gets the components needed by this script.
    private void GetObjectComponents()
    {
        Image[] images = GetComponentsInChildren<Image>();
        foreach (Image image in images)
        {
            if (image.name.Contains("Black"))
                blackProgress = image;
            else if (image.name.Contains("Orange"))
                orangeProgress = image;
        }
    }

    //Gets parent Referances on Start
    private void GetParentRefernaces()
    {
        canvas = transform.parent.gameObject;
        parentPlayerObject = canvas.transform.parent.GetComponent<IPlayerObject>();
        parentCanBuildObject = canvas.transform.parent.GetComponent<ICanBuild>();
        parentCanBuildSelfObject = canvas.transform.parent.GetComponent<ICanBuildSelf>();

        if (parentCanBuildObject != null)
            isCanBuild = true;
        else
            isCanBuild = false;
        if (parentCanBuildSelfObject != null)
            isCanBuildSelf = true;
        else
            isCanBuildSelf = false;
    }
}
