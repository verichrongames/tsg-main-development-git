﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FloatingHealthBar : MonoBehaviour{

    Image redHealth;
    Image greenHealth;

    GameObject canvas;
    IPlayerObject parentPlayerObject;
    IMoveableObject parentMoveableObject;
    ICanBuildSelf parentCanBuildSelfObject;

    bool isMoveable;
    bool isCanBuildSelf;

	void Start() 
    {
        GetObjectComponents();
        GetParentRefernaces();
	}

    void Update()
    {
        GetParentHealth();
    }

    //Gets parents health and enabled/disables the health bars
    private void GetParentHealth()
    {
        if(!isCanBuildSelf)
        {
            greenHealth.fillAmount = parentPlayerObject.HitPoints / parentPlayerObject.MaxHitPoints;
        }
        else
        {
            if(parentPlayerObject.HitPoints / parentPlayerObject.MaxHitPoints == 1 || parentCanBuildSelfObject.CurrentlyBuilding)
            {
                greenHealth.enabled = false;
                redHealth.enabled = false;
            }
            else
            {
                greenHealth.enabled = true;
                redHealth.enabled = false;
                greenHealth.fillAmount = parentPlayerObject.HitPoints / parentPlayerObject.MaxHitPoints;

            }
        }
    }

    private void EnableImages()
    {
        redHealth.enabled = true;
        greenHealth.enabled = true;
    }

    private void DisableImages()
    {
        redHealth.enabled = false;
        greenHealth.enabled = false;
    }


    //Gets the components needed by this script.
    private void GetObjectComponents()
    {
        Image[] images = GetComponentsInChildren<Image>();
        foreach(Image image in images)
        {
            if (image.name.Contains("Red"))
                redHealth = image;
            else if (image.name.Contains("Green"))
                greenHealth = image;
        }
    }

    //Gets parent Referances on Start
    private void GetParentRefernaces()
    {
        canvas = transform.parent.gameObject;
        parentPlayerObject = canvas.transform.parent.GetComponent<IPlayerObject>();
        parentMoveableObject = canvas.transform.parent.GetComponent<IMoveableObject>();
        parentCanBuildSelfObject = canvas.transform.parent.GetComponent<ICanBuildSelf>();


        if (parentMoveableObject != null)
            isMoveable = true;
        else
            isMoveable = false;
        if (parentCanBuildSelfObject != null)
            isCanBuildSelf = true;
        else
            isCanBuildSelf = false;
    }
}
