﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using CoreSys.Objects;


public class FloatingBarCanvas : MonoBehaviour
{

    GameObject parentObject;
    GameObject healthBar;
    GameObject progressBar;

    RectTransform canvasTransform;
    RectTransform healthBarTransform;
    RectTransform progressBarTransform;

    ICanBuild parentCanBuildObject;
    ICanBuildSelf parentCanBuildSelfObject;
    IMoveableObject parentMoveableObject;
    IPlayerObject parentPlayerObject;

    bool isCanBuild;
    bool isCanBuildSelf;
    bool isMoveable;

    protected void Start()
    {
        canvasTransform = GetComponent<RectTransform>();
        GetParentInterfaces();
        GetChildrenReferances();
        SetBars();
        SetSize();
        SetRectTransforms();
    }

    protected void Update()
    {
        SetRotation();
    }

    //Sets the health bar rotation on Update
    private void SetRotation()
    {
        if (isMoveable)
        {
            if (parentMoveableObject.Turning)
            {
                Vector3 eulers = new Vector3(90, 0, 0);
                canvasTransform.rotation = Quaternion.Euler(eulers);
            }
        }
    }

    //Sets the progress and health bars on Start
    private void SetBars()
    {
        if (!isCanBuild && !isCanBuildSelf)
        {
            progressBar.SetActive(false);
        }
    }

    //Sets the Canvas size
    private void SetSize()
    {
        Vector3 upperRight = parentPlayerObject.BoundingPoints.UpperRightPoint;
        Vector3 upperLeft = parentPlayerObject.BoundingPoints.UpperLeftPoint;
        Vector3 lowerRight = parentPlayerObject.BoundingPoints.LowerRightPoint;
        Debug.DrawLine(upperRight, lowerRight, Color.red);

        if (!isCanBuildSelf)
            canvasTransform.sizeDelta = new Vector3(upperRight.x - upperLeft.x, (upperRight.z - lowerRight.z) * 1.6f, 2);
        else
            canvasTransform.sizeDelta = new Vector3((upperRight.x - upperLeft.x) * 0.9f, (upperRight.z - lowerRight.z) * 0.8f, 2);
    }

    //Checks if the parent has the ICanBuild interface, IPlayerObject interface, and the IMoveableObject interface
    private void GetParentInterfaces()
    {
        parentCanBuildObject = transform.parent.GetComponent<ICanBuild>();
        parentMoveableObject = transform.parent.GetComponent<IMoveableObject>();
        parentPlayerObject = transform.parent.GetComponent<IPlayerObject>();
        parentCanBuildSelfObject = transform.parent.GetComponent<ICanBuildSelf>();
        parentObject = transform.parent.gameObject;

        if (parentCanBuildObject != null)
            isCanBuild = true;
        else
            isCanBuild = false;

        if (parentMoveableObject != null)
            isMoveable = true;
        else
            isMoveable = false;
        if (parentCanBuildSelfObject != null)
            isCanBuildSelf = true;
        else
            isCanBuildSelf = false;
    }

    //Gets Child Referances
    private void GetChildrenReferances()
    {
        healthBar = transform.FindChild("HealthBar").gameObject;
        progressBar = transform.FindChild("ProgressBar").gameObject;

        healthBarTransform = healthBar.GetComponent<RectTransform>();
        progressBarTransform = progressBar.GetComponent<RectTransform>();
    }

    private void SetRectTransforms()
    {
        Vector3 position = canvasTransform.position;
        position.y = 4;
        canvasTransform.position = position;

        if (isCanBuildSelf)
        {

        }
    }

}
