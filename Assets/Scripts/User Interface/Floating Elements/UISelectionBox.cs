﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UISelectionBox : MonoBehaviour
{

    float width;
    float height;

    public float barWidthPercentage;
    public float barLengthPercentage;

    float widthToHeightRatio = 1;

    GameObject parentObject;
    IPlayerObject parentPlayerObject;

    RectTransform canvasTransform;

    //Left and Right side Bars
    RectTransform topRight;
    RectTransform bottomRight;
    RectTransform topLeft;
    RectTransform bottomLeft;

    //Top and Bottom bars
    RectTransform rightTop;
    RectTransform leftTop;
    RectTransform rightBottom;
    RectTransform leftBottom;

    void Start()
    {

    }

    void Update()
    {

    }

    public void SetupSelectionBox()
    {
        GetImageReferances();
        SetCanvasSize();
        DetermineWidthToHeightRatio();
        SetBarSizes();
    }

    //Sets the canvas size. Offsets the width and height by the thickness of the bars so the selection box is always outside the actual render of the object
    private void SetCanvasSize()
    {
        canvasTransform.sizeDelta = new Vector2((
            parentPlayerObject.BoundingPoints.LowerRightPoint.x -
            parentPlayerObject.BoundingPoints.LowerLeftPoint.x) *
            (1 + barWidthPercentage),
            parentPlayerObject.BoundingPoints.UpperRightPoint.z -
            parentPlayerObject.BoundingPoints.LowerRightPoint.z) *
            (1 + barWidthPercentage);
    }

    private void DetermineWidthToHeightRatio()
    {
        height = canvasTransform.sizeDelta.y;
        width = canvasTransform.sizeDelta.x;

        widthToHeightRatio = width / height;
    }

    /* Sets the bar size ratios depending on the width/height ratio of the selection box
     * It adjusts the width and the length of each bar depending on the width/height ratio*/
    private void SetBarSizes()
    {
        if (widthToHeightRatio < 1) //If taller than it is wide
        {
            float anchorWidthPercentageMin = 1 - (barWidthPercentage * widthToHeightRatio);
            float anchorWidthPercentageMax = 0 + (barWidthPercentage * widthToHeightRatio);

            float anchorHeightPercentageMin = 1 - (barLengthPercentage * widthToHeightRatio);
            float anchorHeightPercentageMax = 0 + (barLengthPercentage * widthToHeightRatio);

            //Top Bar Adjustments
            rightTop.anchorMin = new Vector2(rightTop.anchorMin.x, anchorWidthPercentageMin);
            leftTop.anchorMin = new Vector2(leftTop.anchorMin.x, anchorWidthPercentageMin);
            rightBottom.anchorMax = new Vector2(rightBottom.anchorMax.x, anchorWidthPercentageMax);
            leftBottom.anchorMax = new Vector2(leftBottom.anchorMax.x, anchorWidthPercentageMax);

            //Side Bar Adjustments
            topRight.anchorMin = new Vector2(topRight.anchorMin.x, anchorHeightPercentageMin);
            topLeft.anchorMin = new Vector2(topLeft.anchorMin.x, anchorHeightPercentageMin);
            bottomRight.anchorMax = new Vector2(bottomRight.anchorMax.x, anchorHeightPercentageMax);
            bottomLeft.anchorMax = new Vector2(bottomLeft.anchorMax.x, anchorHeightPercentageMax);

        }
        else if (widthToHeightRatio > 1) //If wider than it is tall
        {
            float anchorWidthPercentageMin = 1 - (barWidthPercentage / widthToHeightRatio);
            float anchorWidthPercentageMax = 0 + (barWidthPercentage / widthToHeightRatio);

            float anchorHeightPercentageMin = 1 - (0.3f / widthToHeightRatio);
            float anchorHeightPercentageMax = 0 + (0.3f / widthToHeightRatio);

            //Side Bar Adjustments
            topRight.anchorMin = new Vector2(anchorWidthPercentageMin, topRight.anchorMin.y);
            bottomRight.anchorMin = new Vector2(anchorWidthPercentageMin, bottomRight.anchorMin.y);
            topLeft.anchorMax = new Vector2(anchorWidthPercentageMax, topLeft.anchorMax.y);
            bottomLeft.anchorMax = new Vector2(anchorWidthPercentageMax, bottomLeft.anchorMax.y);

            //Top Bar Adjustments
            rightTop.anchorMin = new Vector2(anchorHeightPercentageMin, rightTop.anchorMin.y);
            rightBottom.anchorMin = new Vector2(anchorHeightPercentageMin, rightBottom.anchorMin.y);
            leftTop.anchorMax = new Vector2(anchorHeightPercentageMax, leftTop.anchorMax.y);
            leftBottom.anchorMax = new Vector2(anchorHeightPercentageMax, leftBottom.anchorMax.y);
        }
    }

    //Gets all the image referances from children. Is a bit expensive. Alternative: All children give this object referances rather than them being retrieved.
    private void GetImageReferances()
    {
        topRight = transform.FindChild("Top-Right").GetComponent<RectTransform>();
        bottomRight = transform.FindChild("Bottom-Right").GetComponent<RectTransform>();
        topLeft = transform.FindChild("Top-Left").GetComponent<RectTransform>();
        bottomLeft = transform.FindChild("Bottom-Left").GetComponent<RectTransform>();

        rightTop = transform.FindChild("Right-Top").GetComponent<RectTransform>();
        leftTop = transform.FindChild("Left-Top").GetComponent<RectTransform>();
        rightBottom = transform.FindChild("Right-Bottom").GetComponent<RectTransform>();
        leftBottom = transform.FindChild("Left-Bottom").GetComponent<RectTransform>();

        canvasTransform = GetComponent<RectTransform>();

        parentPlayerObject = transform.parent.GetComponent<IPlayerObject>();

    }
}
