﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using CoreSys.Objects;

/*==============================================
 *  Add This Class To an Object Or Prefab and
 *  select the appropriate UI elements from
 *  the inspector and they will be auto-added.
 * ===========================================*/

public class ObjectUIElements : MonoBehaviour
{

    public bool healthAndProgressBars;
    public bool selectionBox;

    GameObject selectionBoxRef;

    void Start()
    {
        AddElements();
    }



    private void AddElements()
    {
        if (healthAndProgressBars)
        {
            CreateNewBarCanvas();
        }

        if (selectionBox)
        {
            CreateSelectionBox();
        }
    }

    public void ToggleSelectionBox(bool status)
    {
        selectionBoxRef.SetActive(status);
    }

    private void CreateSelectionBox()
    {
        selectionBoxRef = Instantiate(GetUIObjects.GetUserInterface("SelectionBox"));
        SetParent(selectionBoxRef);
        SetRectTransform(selectionBoxRef);
        selectionBoxRef.GetComponent<UISelectionBox>().SetupSelectionBox();
    }

    private void CreateNewBarCanvas()
    {
        GameObject newBar = Instantiate(GetUIObjects.GetUserInterface("BarCanvas"));
        SetParent(newBar);
        SetRectTransform(newBar);
    }

    private void SetParent(GameObject gameObject)
    {
        gameObject.transform.SetParent(transform);
    }

    private void SetRectTransform(GameObject gameObject)
    {
        RectTransform objectRect = gameObject.GetComponent<RectTransform>();

        objectRect.position = transform.position;
    }
}
