﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using CoreSys.Objects;

public class QuitButton : MonoBehaviour, IPointerClickHandler{

    Player player;

    InstantiatedObjectsList objectsList;
    PauseMenu pauseMenu;

	void Start () 
    {
        player = transform.root.GetComponent<Player>();
        objectsList = GetComponent<InstantiatedListAdder>().instantiatedList;
        pauseMenu = GetComponentInParent<PauseMenu>();
	}
	
	void Update () 
    {
	
	}

    //Recieves button click event
    public void OnPointerClick(PointerEventData e)
    {
        if (e.pointerId == -1)
        {
            pauseMenu.Quit();
        }
    }
}
