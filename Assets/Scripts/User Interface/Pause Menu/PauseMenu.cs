﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using CoreSys.Objects;

public class PauseMenu : MonoBehaviour {

    Player player;

    InstantiatedObjectsList objectsList;

    Button resumeButton;
    Button optionsButton;
    Button quitButton;

    MainCanvas mainCanvas;

    void OnEnable()
    {
        KeyboardEvents.EscButtonUpEvent += Resume;
    }

    void OnDisable()
    {
        KeyboardEvents.EscButtonUpEvent -= Resume;
    }

	void Start () 
    {
        player = transform.root.GetComponent<Player>();
        mainCanvas = GetComponentInParent<MainCanvas>();

	}
	
	void Update () 
    {
	
	}


    public void Resume()
    {
        Debug.Log("resumed");
        Time.timeScale = 1;
        objectsList = GetComponent<InstantiatedListAdder>().instantiatedList;

        GameObject pauseMask = objectsList.GetGameObject("Pause Mask", "User Interface", "Active", player);
        mainCanvas.pauseMenuUp = false;
        Destroy(pauseMask.gameObject);
        Destroy(this.gameObject);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
    }
}
