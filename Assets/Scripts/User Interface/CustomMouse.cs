﻿using UnityEngine;
using System.Collections;
using CoreSys;
using CoreSys.Exceptions;
using TagFrenzy;

/*===========================================================
 * This Class handles changing and setting the cursors sprite
 *=========================================================
 *
 *   ==============  Conforms to CQS  ==============
 */


public class CustomMouse : MonoBehaviour
{

    Player player;
    PlayerSelectionManager playerSelectionManager;
    UserInput userInput;
    MouseHoverEventArgs hoverArgs;

    Rect defaultCursorRect;
    Rect otherCursorRect;

    public Texture2D defaultCursor;
    public Texture2D leftCursor;
    public Texture2D rightCursor;
    public Texture2D downCursor;
    public Texture2D upCursor;
    public Texture2D selectHoverCursor;
    public Texture2D selectCursor;
    public Texture2D[] moveCursor;
    public Texture2D[] attackCursor;
    public Texture2D[] harvestCursor;

    public float defaultCursorSizeX = 19;
    public float defaultCursorSizeY = 27;
    public float otherCursorSizeX = 16;
    public float otherCursorSizeY = 16;

    public int currentFrame;
    public string panningdirection;

    public bool groundHover = true;
    public bool objectHover = false;
    public bool UserInterfaceHover = false;
    public bool panning = false;

    /*============================================
     *  ======== Temporary Members ========
     * ==========================================*/
    public bool isAlly = true; //Temp  member to verify if the unit hovered over is an ally. This really belong somewhere else.
    public int currentPlayerID;
    public int objectPlayerID;
    public bool isplayerobject;
    public Unit units;

    /*============================================
     * ====== Initialization and Unity Loops======
     * ==========================================*/

    void OnEnable()
    {
        MouseEvents.EdgeOfScreenEvent += PanningEventSwitch;
        MouseEvents.EdgeOfScreenEventEnded += PanningEventSwitchEnd;
        MouseEvents.MouseHoverGroundEvent += HoverEventSwitch;
        MouseEvents.MouseHoverObjectEvent += HoverEventSwitch;
    }

    void OnDisable()
    {
        MouseEvents.EdgeOfScreenEvent -= PanningEventSwitch;
        MouseEvents.EdgeOfScreenEventEnded -= PanningEventSwitchEnd;
        MouseEvents.MouseHoverGroundEvent -= HoverEventSwitch;
        MouseEvents.MouseHoverObjectEvent -= HoverEventSwitch;
    }

    void Start()
    {
        player = transform.root.GetComponent<Player>();
        playerSelectionManager = transform.GetComponentInParent<PlayerSelectionManager>();
        userInput = transform.GetComponent<UserInput>();
        currentPlayerID = player.GetInstanceID();
    }

    void Update()
    {
        Cursor.visible = false;
    }

    void OnGUI()
    {
        GUI.depth = 0;
        CursorTypeSwitch();
    }

    //Method Called to create the Rect for all non-default cursors
    private void MakeOtherRect()
    {
        otherCursorRect = new Rect(Event.current.mousePosition.x - otherCursorSizeX / 2, Event.current.mousePosition.y - otherCursorSizeY / 2, otherCursorSizeX, otherCursorSizeY);
    }

    //Method Called to creat the Rect for the default cursor
    private void MakeDefaultRect()
    {
        defaultCursorRect = new Rect(Event.current.mousePosition.x + 9 - defaultCursorSizeX / 2, Event.current.mousePosition.y + 13 - defaultCursorSizeY / 2, defaultCursorSizeX - 3.69f, defaultCursorSizeY - 6);
    }

    /*============================================
     *  ======== Event Switches ========
     * ==========================================*/

    //Signals End Of Panning
    private void PanningEventSwitchEnd(string direction)
    {
        panning = false;
    }

    //Handles Events For Panning
    private void PanningEventSwitch(string direction)
    {
        switch (direction)
        {
            case "right":
                panningdirection = direction;
                panning = true;
                break;
            case "left":
                panningdirection = direction;
                panning = true;
                break;
            case "up":
                panningdirection = direction;
                panning = true;
                break;
            case "down":
                panningdirection = direction;
                panning = true;
                break;
        }
    }

    //Handles Hover Events
    private void HoverEventSwitch(string type, MouseHoverEventArgs e)
    {
        Debug.Log("Mouse Hovering Over: " + type);
        switch (type)
        {
            case "Ground":
                objectHover = false;
                UserInterfaceHover = false;
                groundHover = true;
                break;
            case "Object":
                hoverArgs = e;
                groundHover = false;
                UserInterfaceHover = false;
                objectHover = true;
                break;
            case "UserInterface":
                objectHover = false;
                groundHover = false;
                UserInterfaceHover = true;
                break;
        }
    }

    //This method is called by OnGUI and determines which cursor needs to be used.
    private void CursorTypeSwitch()
    {
        if (!panning)
        {
            if (groundHover)
            {
                SetHoverGround();
            }
            else if (objectHover)
            {
                SetHoverObject();
            }
            else if (UserInterfaceHover)
            {
                SetHoverUserInterface();
            }
        }
        else if (panning)
        {
            SetPan();
        }
    }

    /*============================================
     *  ==== Methods To Determine Cursor ====
     * ==========================================*/

    //Determines what to do when the cursor is hovering over an object
    private void SetHoverObject()
    {
        IPlayerObject playerObject = (IPlayerObject)hoverArgs.HitObject.GetComponent(typeof(IPlayerObject));
        if (playerSelectionManager.objectIsSelected)
        {
            GameObject gameObject = hoverArgs.HitObject;
        }


        if(playerSelectionManager.objectIsSelected)
        {
            //CheckForPlayer(playerObject);
            if (CheckForPlayer(playerObject, hoverArgs.HitObject) && gameObject.transform.parent != null && gameObject && (player.GetInstanceID() == playerObject.player.GetInstanceID()))
            {
                SetDefaultCursor();
            }
            else if (isAlly)
            {
                SetDefaultCursor();
            }
            else if (!isAlly)
            {
                SetAttackCursor();
            }
        }
        else if (!playerSelectionManager.objectIsSelected)
        {
            if (CheckForPlayer(playerObject, hoverArgs.HitObject) && gameObject.transform.parent != null && gameObject && (player.GetInstanceID() == playerObject.player.GetInstanceID()))
            {
                SetDefaultCursor();
            }
            else
            {
                SetDefaultCursor();
            }
        }
    }

    //Determines what to do when the cursor is hovering over ground
    private void SetHoverGround()
    {
        if (playerSelectionManager.objectIsSelected)
        {
            SetMoveCursor();
        }
        else
        {
            SetDefaultCursor();
        }
    }

    //Sets the mouse if hovering over user interface
    private void SetHoverUserInterface()
    {
        SetDefaultCursor();
    }

    //Determines which cursoe to set when panning
    private void SetPan()
    {
        switch (panningdirection)
        {
            case "right":
                SetPanRightCursor();
                break;
            case "left":
                SetPanLeftCursor();
                break;
            case "up":
                SetPanUpCursor();
                break;
            case "down":
                SetPanDownCursor();
                break;
        }
    }

    /*==========================================
     *  ==== Panning Cursor Methods ====
     * =======================================*/

    private void SetPanRightCursor()
    {
        MakeOtherRect();
        GUI.DrawTexture(otherCursorRect, rightCursor);
    }

    private void SetPanLeftCursor()
    {
        MakeOtherRect();
        GUI.DrawTexture(otherCursorRect, leftCursor);
    }

    private void SetPanUpCursor()
    {
        MakeOtherRect();
        GUI.DrawTexture(otherCursorRect, upCursor);
    }

    private void SetPanDownCursor()
    {
        MakeOtherRect();
        GUI.DrawTexture(otherCursorRect, downCursor);
    }

    /*==========================================
     *  ==== Hover Based Cursor Methods ====
     * =======================================*/
    private void SetDefaultCursor()
    {
        MakeDefaultRect();
        GUI.DrawTexture(defaultCursorRect, defaultCursor);
    }

    private void SetSelectHoverCursor()
    {
        MakeOtherRect();
        GUI.DrawTexture(otherCursorRect, selectHoverCursor);
    }

    private void SetAttackCursor()
    {
        MakeOtherRect();
        currentFrame = (int)Time.time % attackCursor.Length;
        GUI.DrawTexture(otherCursorRect, attackCursor[currentFrame]);
    }

    private void SetMoveCursor()
    {
        MakeOtherRect();
        currentFrame = (int)Time.time % moveCursor.Length;
        GUI.DrawTexture(otherCursorRect, moveCursor[currentFrame]);
    }

    private void SetHarvestcursor()
    {
        MakeOtherRect();
        currentFrame = (int)Time.time % harvestCursor.Length;
        GUI.DrawTexture(otherCursorRect, harvestCursor[currentFrame]);
    }

    /*==========================================
     *      ==== Exception Throwers ====
     * =======================================*/

    //Throws exception if player is not instantiated
    private bool CheckForPlayer(IPlayerObject playerObject, GameObject gameObject)
    {
        if (object.ReferenceEquals(playerObject, null))
        {
            return false; //TOFIX will need to expand functionality for asteroids and the like.
            string message = "Player Object Not Instatiated For: " + gameObject.name + " InstanceID: " + gameObject.GetInstanceID();
            throw new ObjectNotInstantiatedException(message);
        }
        else if (object.ReferenceEquals(playerObject.player, null))
        {
            string message = "Player Not Instantiated For: " + playerObject.ObjectName + " InstanceID: " + gameObject.GetInstanceID();
            throw new PlayerNotFoundException(message);
        }
        else
            return true;
    }
}
