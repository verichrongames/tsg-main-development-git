﻿//using UnityEngine;
//using System.Collections;
//using CoreSys;

///*===========================================================
// * ========== Work In progress class to create  ===========
// * =========circles around movement path endpoints. =========
// *=========================================================
// *
// *   ==============  Conforms to CQS  ============== 
// */

//public class MovementCircle : MonoBehaviour {

//    private Unit unit;
//    private int segments = 25;
//    private float xRadius = 0.2f;
//    private float yRadius = 0.2f;
//    LineRenderer lineCircle;
//    private bool circleMadeBool;
//    Vector3 destination;

//    void OnEnable()
//    {
//        KeyboardEvents.MiscButtonHeldEvent += RecieveEvent;
//        KeyboardEvents.MiscButtonUpEvent+= RecieveEvent;
//    }

//    void OnDisable()
//    {
//        KeyboardEvents.MiscButtonHeldEvent -= RecieveEvent;
//        KeyboardEvents.MiscButtonUpEvent -= RecieveEvent;
//    }

//    void Start () 
//    {
//        unit = GetComponentInParent<Unit>();
//        lineCircle = GetComponent<LineRenderer>();
//        lineCircle.SetVertexCount(segments);
//    }
	
//    void Update ()
//    {
	
//    }


//    private void RecieveEvent(string button)
//    {
//        switch (button)
//        {
//            case "Shift":
//                CreateCircle();
//                break;
//            case "ShiftUp":
//                DestroyCircle();
//                break;
//        }
//    }


//    //Needs to be split up into several methods
//    private void CreateCircle()
//    {
//        if(destination != unit.destination)
//        {
//            circleMadeBool = false;
//        }
//        if(!circleMadeBool && unit.Moving)
//        {
//            destination = unit.destination;
//            circleMadeBool = true;
//            if(unit.IsSelected)
//            {
//                Color linecolor = Color.white;
//                linecolor.a = 1;
//                lineCircle.SetColors(linecolor, linecolor);
//            }
//            else
//            {
//                Color linecolor = Color.white;
//                linecolor.a = 0.0039f * 75;
//                lineCircle.SetColors(linecolor, linecolor);
//            }


//            lineCircle.SetVertexCount(segments);
//            float x;
//            float y;
//            float z = 0f;

//            float angle = 40;

//            for(int i = 0; i < (segments); i++)
//            {
//                x = (Mathf.Sin(Mathf.Deg2Rad * angle) * xRadius) + destination.x;
//                y = (Mathf.Cos(Mathf.Deg2Rad * angle) * yRadius) + destination.y;

//                lineCircle.SetPosition(i, new Vector3(x, y, z));

//                angle += (375f / segments);
//            }
//        }
//        else if(circleMadeBool != true)
//        {
//            DestroyCircle();
//        }

//    }

//    private void DestroyCircle()
//    {
//        lineCircle.SetVertexCount(1);
//        lineCircle.SetPosition(0, Vector3.zero);
//        circleMadeBool = false;
//    }
//}

