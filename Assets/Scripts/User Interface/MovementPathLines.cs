﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Vectrosity;
using System.Collections;
using CoreSys.Objects;

/*======================================================
 *  This class can be attached as a component to any object
 *  that needs movement paths. This draws the paths
 * ====================================================*/

 class MovementPathLines : MonoBehaviour {

    List<Vector3> destinationsList;
    public Material lineMaterial;
    public Material lineMaterialGlow;

    Color lineColor;
    Color lineColorDark;
    Color lineColorBright;

    VectorLine destinationsPath;
    VectorLine destinationsPathGlow;
    VectorLine currentPath;
    VectorLine currentPathGlow;

    IMoveableObject moveableObject;
    IPlayerObject playerObject;
    Player player;
    ObjectMovementManager movementManager;
    InstantiatedObjectsList objectsList;

    Transform transformV;


    private bool linesInstantiated;
    private bool hasDestinations;

    void OnEnable()
    {
        KeyboardEvents.MiscButtonHeldEvent += EventSwitch;
        KeyboardEvents.MiscButtonUpEvent += EventSwitch;
    }

    void OnDisable()
    {
        KeyboardEvents.MiscButtonHeldEvent -= EventSwitch;
        KeyboardEvents.MiscButtonUpEvent -= EventSwitch;
    }

	void Start () 
    {
        playerObject = GetComponent<IPlayerObject>();
        moveableObject = GetComponent<IMoveableObject>();
        player = transform.root.GetComponent<Player>();
        transformV = transform;
        StartCoroutine(GetReferances());
        CreateColors();
	}
	
	void Update () 
    {
	
	}

    private void EventSwitch(string button)
    {
        if(button == "Shift")
        {
            GetMovementList();
            DrawPaths();
        }
        else if(button == "ShiftUp")
        {
            ClearLines();
        }
    }

    //Get the movement list for the parent moveable object from the movement manager
    private void GetMovementList()
    {
        destinationsList = movementManager.GetDestinationsList(gameObject.GetInstanceID());
        if (destinationsList != null)
            hasDestinations = true;
        else
            hasDestinations = false;
    }

    //Calls for the paths to be drawn
    private void DrawPaths()
    {
        if(hasDestinations && !linesInstantiated)
        {
            DrawDestinationPaths(destinationsList);
            DrawCurrentPath(destinationsList);
            linesInstantiated = true;
        }
        else if (linesInstantiated)
        {
            UpdateDestinationPath();
            UpdateCurrentPath();
        }
    }

    //Deletes all the lines
    private void ClearLines()
    {
        VectorLine.Destroy(ref currentPath);
        VectorLine.Destroy(ref currentPathGlow);
        VectorLine.Destroy(ref destinationsPath);
        VectorLine.Destroy(ref destinationsPathGlow);
        linesInstantiated = false;
    }

    //Updates the current line
    private void UpdateCurrentPath()
    {
        List<Vector3> currentLinePoints = currentPath.points3;
        List<Vector3> currentLinePointsGlow = currentPathGlow.points3;

        currentLinePoints[0] = transformV.position;
        currentLinePoints[1] = destinationsList[0];

        currentLinePointsGlow[0] = transformV.position;
        currentLinePointsGlow[1] = destinationsList[0];

        SetLineColors(currentPath);
        SetLineColors(currentPathGlow);
        currentPath.Draw();
        currentPathGlow.Draw();
    }

    //Updates the festination lines
    private void UpdateDestinationPath()
    {
        List<Vector3> currentLinePoints = destinationsPath.points3;
        List<Vector3> currentLinePointsGlow = destinationsPathGlow.points3;

        currentLinePoints = destinationsList;
        currentLinePointsGlow = destinationsList;

        SetLineColors(destinationsPath);
        SetLineColors(destinationsPathGlow);
        destinationsPath.Draw();
        destinationsPathGlow.Draw();
    }

    //Creates and Draws the current lines.
    private void DrawCurrentPath(List<Vector3> destinations)
    {
        Vector3[] firstLinePoints = new Vector3[2]
        {
            transformV.position,
            destinations[0],
        };
        currentPath = new VectorLine(name + " Line", firstLinePoints, lineMaterial, 5, LineType.Continuous, Joins.Weld);
        currentPathGlow = new VectorLine(name + " Line", firstLinePoints, lineMaterialGlow, 5, LineType.Continuous, Joins.Weld);
        SetLineColors(currentPath);
        SetLineColors(currentPathGlow);
        currentPath.Draw();
        currentPathGlow.Draw();
    }

    //Creates and draws the line for all destinations
    private void DrawDestinationPaths(List<Vector3> destinations)
    {
        destinationsPath = new VectorLine(name + " Line", destinations, lineMaterial, 5, LineType.Continuous, Joins.Weld);
        destinationsPathGlow = new VectorLine(name + " Line", destinations, lineMaterialGlow, 5, LineType.Continuous, Joins.Weld);
        SetLineColors(destinationsPath);
        SetLineColors(destinationsPathGlow);
        destinationsPath.Draw();
        destinationsPathGlow.Draw();
    }

    private void SetLineColors(VectorLine line)
    {
        if(playerObject.IsSelected)
        {
            line.SetColor(lineColor);
        }
        else
        {
            line.SetColor(lineColorDark);
        }
    }

    //Initially sets the colors for the lines
    private void CreateColors()
    {
        lineColor = Color.black;
        lineColor.r = 0.0546f; //14
        lineColor.g = 0.6435f; //165
        lineColor.b = 0.7956f; //204
        lineColor.a = 1; //255

        lineColorDark = Color.black;
        lineColorDark.r = 0.0546f; //14
        lineColorDark.g = 0.6435f; //165
        lineColorDark.b = 0.7956f; //204
        lineColorDark.a = 0.2925f; //75

        lineColorBright = Color.black;
        lineColorBright.r = 0.0039f * 14;
        lineColorBright.g = 0.0039f * 179;
        lineColorBright.b = 0.0039f * 203;
        lineColorBright.a = 1;
    }

    private IEnumerator GetReferances()
    {
        yield return 0;

        objectsList = GetComponent<InstantiatedListAdder>().instantiatedList;
        movementManager = objectsList.GetGameObject("ObjectManager", "Manager", "Active", player).GetComponent<ObjectMovementManager>();
    }
}
