﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using CoreSys.Objects;

public class MainCanvas : MonoBehaviour {

    public bool pauseMenuUp { get; set; }

    void OnEnable()
    {
        KeyboardEvents.EscButtonUpEvent += PauseGame;
    }

    void OnDisable()
    {
        KeyboardEvents.EscButtonUpEvent -= PauseGame;
    }

	void Start () 
    {
        pauseMenuUp = false;
	}
	
	void Update () 
    {
	
	}


    //This will pause the game, as well as instantiate the pause mask and the pause menu
    private void PauseGame()
    {
        if(!pauseMenuUp)
        {
            InstantiatePause();
            pauseMenuUp = true;
        }
    }

    //When esc is hit this will instantiate and initialize the pause menu
    private void InstantiatePause()
    {
        GameObject pauseMask = Instantiate(GetUIObjects.GetUserInterface("Pause Mask"));
        GameObject pauseMenu = Instantiate(GetUIObjects.GetUserInterface("Pause Menu Panel"));


        SetParent(pauseMask);
        SetParent(pauseMenu);

        SetZeroOffset(pauseMask);
        SetZeroOffset(pauseMenu);

        pauseMenu.GetComponent<PauseMenu>().PauseGame();
    }

    //Sets the objects parents
    private void SetParent(GameObject gameObject)
    {
        gameObject.transform.SetParent(this.transform);
    }

    //Sets the rect offset to 0
    private void SetZeroOffset(GameObject gameObject)
    {
        RectTransform rectT = gameObject.GetComponent<RectTransform>();

        rectT.offsetMax = new Vector2(0, 0);
        rectT.offsetMin = new Vector2(0, 0);
    }
}
