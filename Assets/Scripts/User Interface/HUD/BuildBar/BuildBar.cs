﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TagFrenzy;

public class BuildBar : MonoBehaviour {

    public List<GameObject> buildBarItems;
    GameObject buildBar;

    void Awake()
    {
        buildBar = GameObject.Find("BuildBar(Clone)");
    }
	void Start ()
    {
	}
	
	void Update () 
    {
	
	}

    //Recieves call to add a new item to the build bar. Not all calls are honored
    public void NewBuildBarItem(GameObject objectType)
    {
        KeyValuePair<bool, int> objectSearch = FindExistingBuildBarItem(objectType);
        if (objectSearch.Key)
        {

        }
        else if (!objectSearch.Key)
        {
            AddNewBuildBarItem(objectType);
            SetNewObjectParent(buildBarItems[buildBarItems.Count - 1]);
            SetNewObjectTransform(buildBarItems[buildBarItems.Count - 1]);
            buildBarItems[buildBarItems.Count - 1].SetActive(true);
        }
    }

    //To add the new queue item to the queue
    private void AddNewBuildBarItem(GameObject objectType)
    {
        buildBarItems.Add((GameObject)Instantiate(objectType));
    }

    //Necessary so the build bar item is under the build bar
    private void SetNewObjectParent(GameObject gameObject)
    {
        gameObject.transform.SetParent(buildBar.transform);
    }

    //Necessary so the object is centered and visible within it's anchors
    private void SetNewObjectTransform(GameObject gameObject)
    {
        RectTransform objectRect = gameObject.GetComponent<RectTransform>();
        objectRect.anchorMax = new Vector2((0.07f * buildBarItems.Count) + 0.025f + ((float)buildBarItems.Count / 100), 0.93f);
        objectRect.anchorMin = new Vector2((0.07f * (buildBarItems.Count - 1)) + 0.025f + ((float)buildBarItems.Count / 100), 0.07f);
        objectRect.offsetMax = new Vector2(0, 0);
        objectRect.offsetMin = new Vector2(0, 0);
    }

    //Finds and returns a bool and an int representing if the item was found and it's index location in the queue
    private KeyValuePair<bool, int> FindExistingBuildBarItem(GameObject objectType)
    {
        if (object.ReferenceEquals(buildBarItems, null) || buildBarItems.Count == 0)
        {
            return new KeyValuePair<bool, int>(false, 0);
        }
        else
        {
            string newObjectName;
            if (objectType.name.Contains("(Clone)"))
            {
                newObjectName = objectType.name;
            }
            else
            {
                newObjectName = objectType.name + "(Clone)";
            }
            for (int i = 0; i + 1 <= buildBarItems.Count; i++)
            {
                if (buildBarItems[i].name == newObjectName)
                {
                    return new KeyValuePair<bool, int>(true, i);
                }
            }
        }
        return new KeyValuePair<bool, int>(false, 0);
    }
}
