﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using CoreSys;
using CoreSys.Objects;
using CoreSys.Buildings;


public class CoreUpgradeButton : MonoBehaviour, IPointerClickHandler{

    PlayerSelectionManager selectionManager;

    void OnEnable()
    {

    }

    void OnDisable()
    {

    }

    void Start()
    {
        selectionManager = GetComponentInParent<PlayerSelectionManager>();
    }

    void Update()
    {

    }

    //This Recieves the Unity generated event on click. This will need to hook back into 
    //whatever system we are going to use to actually build shit.
    public void OnPointerClick(PointerEventData e)
    {
        if (e.pointerId == -1)
        {
            GameObject selectedObject = selectionManager.GetSelectedObject();
            Core targetCore = selectedObject.GetComponent<Core>();
            if (targetCore)
            {
                targetCore.StartUpgrade();
            }
        }
    }
}
