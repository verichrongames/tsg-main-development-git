﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using CoreSys;
using CoreSys.Objects;

public class BuildButtons : MonoBehaviour, IPointerClickHandler {

    public string orderName;
    public BuildQueueManager queueManager;
    public PlayerSelectionManager selecteionManager;

    private bool shiftHeld;
    private bool ctrlHeld;

    void OnEnable()
    {
        KeyboardEvents.MiscButtonHeldEvent += SetModKey;
        KeyboardEvents.MiscButtonUpEvent += ClearModKeys;
    }
    void OnDisable()
    {
        KeyboardEvents.MiscButtonHeldEvent -= SetModKey;
        KeyboardEvents.MiscButtonUpEvent -= ClearModKeys;
    }
	void Start () 
    {
        queueManager = transform.root.GetComponentInChildren<BuildQueueManager>();
        selecteionManager = transform.root.GetComponentInChildren<PlayerSelectionManager>();
	}
	
	void Update () 
    {
	
	}

    //This Recieves the Unity generated event on click. This will need to hook back into 
    //whatever system we are going to use to actually build units. This is the click data hub
    public void OnPointerClick(PointerEventData e)
    {
        if (e.pointerId == -1)
        {
            if(shiftHeld)
            {
                queueManager.NewBuildQueueItem(selecteionManager.GetSelectedObject(), orderName, 5); //CHANGE BACK
            }
            else
            {
                queueManager.NewBuildQueueItem(selecteionManager.GetSelectedObject(), orderName, 1);
            }
        }
    }

    //Clears the modkey bools when button is no longer held
    private void ClearModKeys(string button)
    {
        switch (button)
        {
            case "ShiftUp":
                shiftHeld = false;
                break;
        }
    }

    //Will Set the Ctrl or Shift modkey to true to modify the output of OnPointerClick
    private void SetModKey(string button)
    {
        switch (button)
        {
            case "Shift":
                shiftHeld = true;
                break;
        }
    }
}
