﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using CoreSys;


public class UIBuildQueueItem : MonoBehaviour, IPointerClickHandler{

    public BuildQueueManager queueManager;
    public PlayerSelectionManager selecteionManager;
    public string buildOrderName;

    public int buildCount { get; set; }
    private bool shiftHeld;

    void OnEnable()
    {
        KeyboardEvents.MiscButtonHeldEvent += SetModKey;
        KeyboardEvents.MiscButtonUpEvent += ClearModKeys;
    }

    void OnDisable()
    {
        KeyboardEvents.MiscButtonHeldEvent -= SetModKey;
        KeyboardEvents.MiscButtonUpEvent -= ClearModKeys;
    }
    void Awake()
    {
    }
	void Start () 
    {
        queueManager = transform.root.GetComponentInChildren<BuildQueueManager>();
        selecteionManager = GetComponentInParent<PlayerSelectionManager>();
	}
	void Update () 
    {
	
	}

    //Recieves click event from the queue item
    public void OnPointerClick(PointerEventData e)
    {
        if (e.pointerId == -2)
        {
            if(shiftHeld)
            {
                queueManager.DeIncrimentBuildQueue(selecteionManager.GetSelectedObject(), buildOrderName, 5);
            }
            else
            {
                queueManager.DeIncrimentBuildQueue(selecteionManager.GetSelectedObject(), buildOrderName, 1);
            }

        }
    }

    //Clears the modkey bools when button is no longer held
    private void ClearModKeys(string button)
    {
        switch (button)
        {
            case "ShiftUp":
                shiftHeld = false;
                break;
        }
    }

    //Will Set the Ctrl or Shift modkey to true to modify the output of OnPointerClick
    private void SetModKey(string button)
    {
        switch (button)
        {
            case "Shift":
                shiftHeld = true;
                break;
        }
    }
}
