﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using CoreSys;
using CoreSys.Objects;

public class BuildQueueBar : MonoBehaviour
{

    public List<GameObject> queueItems;
    Player player;
    GameObject buildQueueBar;
    RectTransform itemTransform;
    Image queueItemMask;
    InstantiatedObjectsList instantiatedList;

    void Start()
    {
        buildQueueBar = this.gameObject;
        instantiatedList = this.GetComponent<InstantiatedListAdder>().instantiatedList;
        player = transform.root.GetComponent<Player>();
    }


    void Update()
    {

    }

    //Recieves a call when a current queue item is right clicked to remove or deincrement it
    public void DeincrementQueueItem(GameObject objectType, int modAmount)
    {
        KeyValuePair<bool, int> objectSearch = FindExistingQueueItem(objectType);
        DeIncrimentQueueItem(queueItems[objectSearch.Value], modAmount, objectSearch.Value);
    }

    //Public method to recieve call to adjust queueitems completion amount
    public void SetQueueItemProgress(GameObject objectType, float progress)
    {
        Transform test = queueItems[0].transform;
        queueItemMask = queueItems[0].transform.FindChild("BuildMask").GetComponent<Image>();

        queueItemMask.fillAmount = progress;
    }

    //Recieves call to add a new item to the queue. Not all calls are honored
    public void NewQueueItem(GameObject objectType, int modAmount)
    {
        KeyValuePair<bool, int> objectSearch = FindExistingQueueItem(objectType);
        if (objectSearch.Key)
        {
            IncrimentQueueItem(queueItems[objectSearch.Value], modAmount);
        }
        else if (!objectSearch.Key)
        {
            AddNewQueueItem(objectType);
            SetNewObjectParent(queueItems[queueItems.Count - 1]);
            SetNewObjectTransform(queueItems[queueItems.Count - 1]);
            IncrimentQueueItem(queueItems[queueItems.Count - 1], modAmount);
        }
    }

    //Incriments the text of the queue item based on input
    private void IncrimentQueueItem(GameObject gameObject, int incriment)
    {
        Text orderCountText = gameObject.GetComponentInChildren<Text>();
        UIBuildQueueItem buildQueueItem = gameObject.GetComponent<UIBuildQueueItem>();

        buildQueueItem.buildCount += incriment;
        orderCountText.text = buildQueueItem.buildCount.ToString();
        if(!orderCountText.enabled)
        {
            orderCountText.enabled = true;
        }
    }

    //Deinriments the text on the queue item and deletes item if necessary
    private void DeIncrimentQueueItem(GameObject gameObject, int incriment, int i)
    {
        Text orderCountText = gameObject.GetComponentInChildren<Text>();
        UIBuildQueueItem buildQueueItem = gameObject.GetComponent<UIBuildQueueItem>();

        if (buildQueueItem.buildCount <= incriment)
        {
            queueItems.RemoveAt(i);
            orderCountText.enabled = false;
            DestroyQueueItem(gameObject);
        }
        else if (buildQueueItem.buildCount > incriment)
        {
            buildQueueItem.buildCount -= incriment;
            orderCountText.text = buildQueueItem.buildCount.ToString();
        }
    }

    //Destroys the queue object
    private void DestroyQueueItem(GameObject gameObject)
    {
        Destroy(gameObject);
        ReOrderQueue();
    }

    //ReOrders the build queue is any items are removed
    private void ReOrderQueue()
    {
        for(int i = 0; i + 1 <= queueItems.Count; i++)
        {
            SetObjectTransform(queueItems[i], i+1);
        }
    }

    //Moves a queue item to a specific location
    private void SetObjectTransform(GameObject gameObject, int position)
    {
        RectTransform objectRect = gameObject.GetComponent<RectTransform>();
        objectRect.anchorMax = new Vector2((0.07f * position) + 0.025f + (position / 100), 0.93f);
        objectRect.anchorMin = new Vector2((0.07f * (position - 1)) + 0.025f + (position / 100), 0.07f);
        objectRect.offsetMax = new Vector2(0, 0);
        objectRect.offsetMin = new Vector2(0, 0);
    }

    /*==============================================
     *  ======== New Queue Item Methods ========
     *============================================*/

    //To add the new queue item to the queue
    private void AddNewQueueItem(GameObject objectType)
    {
        queueItems.Add((GameObject)Instantiate(objectType));
    }

    //Necessary so the queue object is under the queue panel
    private void SetNewObjectParent(GameObject gameObject)
    {
        gameObject.transform.SetParent(this.transform);
    }

    //Necessary so the object is centered and visible within it's anchors
    private void SetNewObjectTransform(GameObject gameObject)
    {
        RectTransform objectRect = gameObject.GetComponent<RectTransform>();
        objectRect.anchorMax = new Vector2((0.07f * queueItems.Count) + 0.025f + ((float)queueItems.Count / 100), 0.93f);
        objectRect.anchorMin = new Vector2((0.07f * (queueItems.Count - 1)) + 0.025f + ((float)queueItems.Count / 100), 0.07f);
        objectRect.offsetMax = new Vector2(0, 0);
        objectRect.offsetMin = new Vector2(0, 0);
    }

    /*==============================================
     *  ======== Queue Search/Query Methods ========
     *============================================*/

    //Finds and returns a bool and an int representing if the item was found and it's index location in the queue
    private KeyValuePair<bool, int> FindExistingQueueItem(GameObject objectType)
    {
        if (object.ReferenceEquals(queueItems, null) || queueItems.Count == 0)
        {
            return new KeyValuePair<bool, int>(false, 0);
        }
        else
        {
            string newObjectName;
            if(objectType.name.Contains("(Clone)"))
            {
                newObjectName = objectType.name;
            }
            else
            {
                newObjectName = objectType.name + "(Clone)";
            }
            for (int i = 0; i + 1 <= queueItems.Count; i++)
            {
                if (queueItems[i].name == newObjectName)
                {
                    return new KeyValuePair<bool, int>(true, i);
                }
            }
        }
        return new KeyValuePair<bool, int>(false, 0);
    }
}
