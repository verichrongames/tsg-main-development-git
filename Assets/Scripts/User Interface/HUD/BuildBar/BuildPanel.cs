﻿using UnityEngine;
using System.Collections;
using System;
using System.Reflection;
using System.Collections.Generic;
using CoreSys;
using CoreSys.Objects;

public class BuildPanel : MonoBehaviour {

    public List<GameObject> buildAndQueueBar;
    GameObject buildBar;
    GameObject buildQueueBar;

    void Awake()
    {
        Type type = Type.GetType("Mono.Runtime");
        if (type != null)
        {
            MethodInfo displayName = type.GetMethod("GetDisplayName", BindingFlags.NonPublic | BindingFlags.Static);
            if (displayName != null)
                Debug.Log(displayName.Invoke(null, null));
        }

    }
	void Start () 
    {
        //SetupPanelSwitch("Both");
	}
	
	void Update () 
    {
	
	}

    //Recieves call to setup the panel
    public void SetupPanelSwitch(string bars)
    {
        switch (bars)
        {
            case "Both":
                buildBar = GetUIObjects.GetUserInterface("BuildBar");
                buildQueueBar = GetUIObjects.GetUserInterface("BuildQueueBar");
                buildAndQueueBar.Add((GameObject)Instantiate(buildQueueBar));
                buildAndQueueBar.Add((GameObject)Instantiate(buildBar));
                OrganizePanel();
                break;
            case "BuildBar":
                buildBar = (GameObject)Instantiate(GetUIObjects.GetUserInterface("BuildBar"));
                buildAndQueueBar.Add(buildBar);
                OrganizePanel();
                break;
            case "BuildQueueBar":
                buildQueueBar = (GameObject)Instantiate(GetUIObjects.GetUserInterface("BuildQueueBar"));
                buildAndQueueBar.Add(buildQueueBar);
                OrganizePanel();
                break;
            default :
                throw new ArgumentException("Incorrect Argument Sent to BuildPanel.SetupPanelSwitch \n Acceptable Arguments are: Both, BuildBar, or BuildQueueBar");
        }
    }

    //Erases all panel contents
    public void ClearPanel()
    {
        int count = buildAndQueueBar.Count;
        for(int i = 1; i  <= count; i++)
        {
            GameObject gameObject = buildAndQueueBar[0];
            buildAndQueueBar.RemoveAt(0);
            DestroyImmediate(gameObject);
        }
    }

    private void OrganizePanel()
    {
        if(buildAndQueueBar.Count == 1)
        {
            SetObjectParent(buildAndQueueBar[0]);
            SetTransform(buildAndQueueBar[0], 0);
            buildAndQueueBar[0].SetActive(true);
        }
        else if (buildAndQueueBar.Count == 2)
        {
            for (int i = 0; i + 1 <= buildAndQueueBar.Count; i++)
            {
                SetObjectParent(buildAndQueueBar[i]);
                SetTransform(buildAndQueueBar[i], i);
                buildAndQueueBar[i].SetActive(true);
            }
        }
    }


    private void SetTransform(GameObject gameObject, int location)
    {
        RectTransform objectRect = gameObject.GetComponent<RectTransform>();

        float xMin = 0;
        float yMin = 0;
        float xMax = 0;
        float yMax = 0;

        switch (location)
        {
            case 0:
                xMin = 0;
                yMin = 0;
                xMax = 1;
                yMax = 0.5f;
                break;
            case 1:
                xMin = 0;
                yMin = 0.5f;
                xMax = 1;
                yMax = 1;
                break;
        }

        objectRect.anchorMax = new Vector2(xMax, yMax);
        objectRect.anchorMin = new Vector2(xMin, yMin);

        objectRect.offsetMax = Vector2.zero;
        objectRect.offsetMin = Vector2.zero;
    }

    private void SetObjectParent(GameObject gameObject)
    {
        gameObject.transform.SetParent(this.transform);
    }
}
