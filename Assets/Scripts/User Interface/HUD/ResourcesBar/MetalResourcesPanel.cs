﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MetalResourcesPanel : MonoBehaviour
{

Text[] metalTexts;

    Text metalText;
    Text valueText;
    Text productionTotalText;

	void Start () 
    {
        GetReferances();
	}
	
	void Update () 
    {
	}

    //Sets the value of the current metal
    public void SetMetalValue(int value, int maxValue)
    {
        valueText.text = value.ToString() + "/" + maxValue.ToString();
    }

    //Sets production value text and color
    public void SetProductionValue(int value)
    {
        if(value >= 0)
        {
            productionTotalText.text = "+" + value.ToString();
            productionTotalText.color = Color.green;
        }
        else if (value < 0)
        {
            productionTotalText.text = value.ToString();
            productionTotalText.color = Color.red;
        }
    }

    //Gets initial component referances on Start
    private void GetReferances()
    {
        metalTexts = GetComponentsInChildren<Text>();

        foreach(Text t in metalTexts)
        {
            if(t.name == "MetalText")
            {
                metalText = t;
            }
            else if(t.name == "MetalValueText")
            {
                valueText = t;
            }
            else if(t.name == "MetalProductionTotalText")
            {
                productionTotalText = t;
            }
        }
    }
}

