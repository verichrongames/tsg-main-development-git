﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ResourceBar : MonoBehaviour {

    PowerResourcesPanel powerPanel;
    MetalResourcesPanel metalPanel;


	void Start () 
    {
        GetReferances();
	}
	
	void Update () 
    {	
    }

    //Recieves call and updates the resource panel approprietly
    public void UpdateResourcesPanel(ResourcesPanelInfoType e)
    {
        if(!object.ReferenceEquals(e.MetalProductionTotal, null))
        {
            metalPanel.SetProductionValue(e.MetalProductionTotal);
            metalPanel.SetMetalValue(e.CurrentMetal, e.MaxMetal);
        }
        else
        {
            metalPanel.SetMetalValue(e.CurrentMetal, e.MaxMetal);
        }

        if(!object.ReferenceEquals(e.PowerProductionTotal, null))
        {
            powerPanel.SetProductionValue(e.PowerProductionTotal);
            powerPanel.SetPowerValue(e.CurrentPower, e.MaxPower);
        }
        else
        {
            powerPanel.SetPowerValue(e.CurrentPower, e.MaxPower);
        }

    }

    //Gets initial referances on Start
    private void GetReferances()
    {
        powerPanel = GetComponentInChildren<PowerResourcesPanel>();
        metalPanel = GetComponentInChildren<MetalResourcesPanel>();
    }

}
