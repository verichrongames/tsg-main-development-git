﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PowerResourcesPanel : MonoBehaviour
{
    Text[] powerTexts;

    Text powerText;
    Text valueText;
    Text productionTotalText;

	void Start () 
    {
        GetReferances();
	}
	
	void Update () 
    {
	}

    //Sets the value of the current power
    public void SetPowerValue(int value, int maxValue)
    {
        valueText.text = value.ToString() + "/" + maxValue.ToString();
    }

    //Sets production value text and color
    public void SetProductionValue(int value)
    {
        if(value >= 0)
        {
            productionTotalText.text = "+" + value.ToString();
            productionTotalText.color = Color.green;
        }
        else if (value < 0)
        {
            productionTotalText.text = value.ToString();
            productionTotalText.color = Color.red;
        }
    }

    //Gets initial component referances on Start
    private void GetReferances()
    {
        powerTexts = GetComponentsInChildren<Text>();

        foreach(Text t in powerTexts)
        {
            if(t.name == "PowerText")
            {
                powerText = t;
            }
            else if(t.name == "PowerValueText")
            {
                valueText = t;
            }
            else if(t.name == "PowerProductionTotalText")
            {
                productionTotalText = t;
            }
        }
    }
}
