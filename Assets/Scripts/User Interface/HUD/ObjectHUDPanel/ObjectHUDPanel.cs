﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using CoreSys;
using CoreSys.Objects;

/*======================================
 *  This class manages the object
 *  information panel. This panel shows up
 *  when an object is hovered over.
 *  Blame: Doug
 * ===================================*/

public class ObjectHUDPanel : MonoBehaviour {

    public List<GameObject> panelItems = new List<GameObject>();
    GameObject infoPanel;
    GameObject objectNamePanel;
    ObjectHealthBar objectHealthBar;

    Text objectNameText;

    Image objectImage;

	void Start () 
    {
        //CreateObjectInfoPanel(new UIObjectInfoType());
	}
	

	void Update () 
    {
	
	}

    //Retrieves referances for all the children of the info panel so they can be accessed.
    private void GetInfoPanelChildren(GameObject gameObject)
    {
        objectNamePanel = gameObject.transform.Find("ObjectName").gameObject;
        objectImage = gameObject.transform.Find("ObjectImage").GetComponent<Image>();
        objectHealthBar = gameObject.transform.Find("ObjectHealth").gameObject.GetComponent<ObjectHealthBar>();

        objectNameText = objectNamePanel.GetComponentInChildren<Text>();


    }

    //Goes through steps to create a new object info panel. Also updates the panel if it already exists.
    public void CreateObjectInfoPanel(UIObjectInfoType e)
    {
        KeyValuePair<bool, int> objectSearch = FindExistingPanel("Info");
        if(objectSearch.Key)
        {
            GetInfoPanelChildren(panelItems[objectSearch.Value]);
            UpdateInfoPanel(panelItems[objectSearch.Value], e);
        }
        else if(!objectSearch.Key)
        {
            InstantiatePanel("info");
            SetPanelParent(panelItems[panelItems.Count - 1]);
            SetPanelTransform(panelItems[panelItems.Count - 1]);
            panelItems[objectSearch.Value].SetActive(true);
            GetInfoPanelChildren(panelItems[objectSearch.Value]);
            UpdateInfoPanel(panelItems[objectSearch.Value], e);

        }
    }

    /*===============================================
     *  ===== Creating New Object Methods =====
     * =============================================*/

    private void EnablePanelAndChildren(GameObject panel)
    {

    }

    //Instantiates the new panel
    private void InstantiatePanel(string type)
    {
        if(type == "info")
        {
            panelItems.Add((GameObject)Instantiate(GetUIObjects.GetUserInterface("ObjectInfoPanel")));
        }
    }

    //Sets the new panels parent to the HUDPanel
    private void SetPanelParent(GameObject gameObject)
    {
        gameObject.transform.SetParent(this.transform);
    }

    //Sets initial transform for the new panel item. 
    private void SetPanelTransform(GameObject gameObject)
    {
        if(panelItems.Count == 1)
        {
            RectTransform objectRect = gameObject.GetComponent<RectTransform>();

            objectRect.anchorMin = new Vector2(0, 0);
            objectRect.anchorMax = new Vector2(1, 0.5f);

            objectRect.offsetMin = Vector2.zero;
            objectRect.offsetMax = Vector2.zero;
        }
        else if (panelItems.Count == 2)
        {
            RectTransform objectRect = gameObject.GetComponent<RectTransform>();

            objectRect.anchorMin = new Vector2(0, 0.5f);
            objectRect.anchorMax = new Vector2(1, 1);

            objectRect.offsetMin = Vector2.zero;
            objectRect.offsetMax = Vector2.zero;
        }
    }

    /*===============================================
     *  ===== Destroying Existing Object Methods =====
     * =============================================*/

    public void RemoveObjectinfoPanel()
    {
        if(panelItems.Count >= 1)
        {
            KeyValuePair<bool, int> objectSearch = FindExistingPanel("Info");
            if(objectSearch.Key)
            {
                RemovePanelItem(panelItems[objectSearch.Value], objectSearch.Value);
            }
        }
    }

    private void RemovePanelItem(GameObject gameObject, int index)
    {
        panelItems.RemoveAt(index);
        Destroy(gameObject);
        OrganizePanel();
    }

    /*===============================================
     *  ===== Modying Existing Object Methods =====
     * =============================================*/

    //Updates the information on the info panel
    private void UpdateInfoPanel(GameObject gameObject, UIObjectInfoType e)
    {
        SetObjectName(e.PlayerName, e.ObjectName);
        SetHealthBar(e.HealthFloat, e.MaxHealth);
        if(e.ObjectSprite != null)
        {
            SetObjectImage(e.ObjectSprite);
        }
        else
            SetObjectImage(e.ObjectMaterial, e.ObjectSprite);

    }

    //Sets the name of the object in the name box
    private void SetObjectName(string playerName, string objectName)
    {
        objectNameText.text = playerName + ": " + objectName;
    }

    //Sets the healthbar to an appropriate health amount.
    private void SetHealthBar(float health, float maxHealth)
    {
        objectHealthBar.SetHealth(health, maxHealth);
    }

    //Sets the Objects Image (Sprite)
    private void SetObjectImage(Sprite objectSprite)
    {
        objectImage.sprite = objectSprite;
    }

    //Sets the Objects Image (Sprite)
    private void SetObjectImage(Material objectMaterial, Sprite objectSprite)
    {
        objectImage.sprite = objectSprite;
        objectImage.material = objectMaterial;
    }

    //Organizes the panel so that the info bar is on top and the commands bar is on the bottom if both are present.
    private void OrganizePanel()
    {
        if(panelItems.Count == 2)
        {
            for (int i = 0; i - 1 <= panelItems.Count; i++)
            {
                if(panelItems[i].name.Contains("Commands"))
                {
                    RectTransform objectRect = gameObject.GetComponent<RectTransform>();

                    objectRect.anchorMin = new Vector2(0, 0);
                    objectRect.anchorMax = new Vector2(1, 0.5f);

                    objectRect.offsetMin = Vector2.zero;
                    objectRect.offsetMax = Vector2.zero;
                }
                else if(panelItems[i].name.Contains("Info"))
                {
                    RectTransform objectRect = gameObject.GetComponent<RectTransform>();

                    objectRect.anchorMin = new Vector2(0, 0.5f);
                    objectRect.anchorMax = new Vector2(1, 1);

                    objectRect.offsetMin = Vector2.zero;
                    objectRect.offsetMax = Vector2.zero;
                }
            }
        }
    }

    //Searches for existing panel items by name.
    private KeyValuePair<bool, int> FindExistingPanel(string name)
    {
        for(int i = 0; i+1 <= panelItems.Count; i++)
        {
            if(panelItems[i].name.Contains(name))
            {
                return new KeyValuePair<bool, int>(true, i);
            }
        }
        return new KeyValuePair<bool, int>(false, 0);
    }
}
