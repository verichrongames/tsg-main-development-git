﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/*======================================
 *  This class manages the health bar in the
 *  information panel.
 * ===================================*/

public class ObjectHealthBar : MonoBehaviour {

    Image healthBar;
    Text healthText;
    RectTransform healthBarRect;

    float barWidth;
    float healthPercentage;

    void Awake()
    {
        GameObject healthObject = this.gameObject.transform.Find("RedHealth").gameObject.transform.Find("GreenHealth").gameObject;
        healthBar = this.gameObject.transform.Find("RedHealth").transform.Find("GreenHealth").GetComponent<Image>();
        healthText = this.gameObject.transform.Find("HealthText").GetComponent<Text>();
        healthBarRect = healthObject.GetComponent<RectTransform>();
    }
	void Start () 
    {

	}
	

	void Update () 
    {
        //Debug.Log(healthBarRect.offsetMax);
        //SetHealthBar();
	
	}

    //Public method to recieve call
    public void SetHealth(float health, float maxHealth)
    {
        DetermineHealthPercentage(health, maxHealth);
        GetRenderWidth();
        SetHealthBar(health, maxHealth);
        SetHealthText(health, maxHealth);
        SetHealthColor();
    }

    //Gets the width of the health bar
    private void GetRenderWidth()
    {
        barWidth = healthBarRect.rect.width;
    }

    //Determines the health percentage in decimal (0-1)
    private void DetermineHealthPercentage(float health, float maxHealth)
    {
        healthPercentage = health / maxHealth;
    }

    //Sets the Rect Transforms of the health bar based on health
    private void SetHealthBar(float health, float maxHealth)
    {
        if(health != maxHealth)
        {
            healthBarRect.offsetMax = new Vector2((barWidth* healthPercentage) - barWidth, 0);
            healthBarRect.offsetMin = new Vector2((barWidth* healthPercentage) - barWidth, 0);
        }
        else
        {
            healthBarRect.offsetMax = new Vector2(0, 0);
            healthBarRect.offsetMin = new Vector2(0, 0);
        }

    }

    //Sets the text property
    private void SetHealthText(float health, float maxHealth)
    {
        healthText.text = Mathf.RoundToInt(health) + "/" + maxHealth;
    }

    //Sets the health bar color based on health percentage
    private void SetHealthColor()
    {
        if(healthPercentage >= 0.5f)
        {
            float colorKeyR = 1 - (healthPercentage);
            float colorKeyG = healthPercentage * 0.588f;
            healthBar.color = new Color(colorKeyR * 1.4f, colorKeyG /healthPercentage, 0);
        }
        else if(healthPercentage < 0.5f)
        {
            //float colorKeyG = healthPercentage * 0.588f;
            healthBar.color = new Color(1, healthPercentage , 0);
        }
    }
}
