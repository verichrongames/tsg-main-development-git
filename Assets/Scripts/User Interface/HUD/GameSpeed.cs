﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameSpeed : MonoBehaviour {

    Text text;

    void OnEnable()
    {
        KeyboardEvents.PlusButtonDownEvent += IncreaseGameSpeed;
        KeyboardEvents.MinusButtonDownEvent += DecreaseGameSpeed;
    }

    void OnDisable()
    {
        KeyboardEvents.PlusButtonDownEvent -= IncreaseGameSpeed;
        KeyboardEvents.MinusButtonDownEvent -= DecreaseGameSpeed;
    }

	void Start () 
    {
        text = GetComponent<Text>();
	}
	
	void Update () 
    {
        SetGameSpeedText();
	}

    private void SetGameSpeedText()
    {
        text.text = Mathf.RoundToInt(Time.timeScale * 100).ToString() + "%";
    }

    private void IncreaseGameSpeed()
    {
        Time.timeScale += 0.1f;
    }

    private void DecreaseGameSpeed()
    {
        if(Time.timeScale - 0.1f > 0)
        {
            Time.timeScale -= 0.1f;
        }
    }
}
