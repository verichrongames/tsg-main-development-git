﻿using UnityEngine;
//using UnityEditor;
using UnityEngine.UI;
using System.Collections;

public class GameVersion : MonoBehaviour {

    Text versionNo;
    string version;

	public void Start () {
        versionNo = GetComponent<Text>();
        if (versionNo.name == "VersionTitle")
            //versionNo.text = PlayerSettings.productName;
            version = GameManager.GetGameVersion();
            versionNo.text = string.Format("PREALPHA " + version);
	}

}
