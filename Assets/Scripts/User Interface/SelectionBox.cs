﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreSys;
using CoreSys.Objects;
/*===========================================================
 * This class handles the creation of the  selection box around 
 * whatever unit is currently selected. It recieves an event from 
 * the playerObject object when it's selection is toggled.
 * Shit is 100% hacked together right now. 
 * Though the hack will not create much overhead so it may end up staying.
 * 
 * If this is scaled to a few thousand units, it should not be too bad (hopefully). 
 * We would have to use the profiler to properly determine this. 
 *========================================================= 
 *
 */

public class SelectionBox : MonoBehaviour {

    //Create override methods to be used for building or unit
    //Modify methods to pass referances to each other instead of using global fields

    public bool selected;
    private Rect selectionBox;
    private Vector3 maxBounds;
    private Vector3 minBounds;
    private GameObject SelectionBoxUR, SelectionBoxLR, SelectionBoxUL, SelectionBoxLL;
    private Vector3 UpperRightPoint, LowerRightPoint, LowerLeftPoint, UpperLeftPoint;
    private Quaternion URRotate, LRRotate, LLRotate, ULRotate;

    private float CamOrtho;
    private float scaleModifyer;
    private Vector3 selectionBoxScale;

    void OnEnable()
    {
        MouseEvents.ScrollUpEvent += ScaleSelectionBox;
        MouseEvents.ScrollDownEvent += ScaleSelectionBox;
    }

    void OnDisable()
    {
        MouseEvents.ScrollUpEvent -= ScaleSelectionBox;
        MouseEvents.ScrollDownEvent -= ScaleSelectionBox;
    }
	void Start ()
    {
	}
	
	void Update () 
    {
        //if (selected)
        //{
        //    ScaleSelectionBox();
        //}
    }


    //Recieved the event and processes it accordingly
    public void RecieveBoxCall(bool toggle, GameObject selectedObj, IPlayerObject selectedPlayerObject)
    {
        if(toggle)
        {
            if(!selected) //Used to ignore the event if this is already selected
            {
                selected = toggle;
                debugdraw(selectedPlayerObject);
                CreateSelectionBox(selectedObj);
                HackRect(selectedObj, selectedPlayerObject);
                //DetermineRenderRect();
            }
        }
        else if(!toggle)
        {
            if(selected) //Used to ignore the event if this is not selected already
            {
                selected = toggle;
                DestroySelectionBox();
            }
        }
    }

    //Scales The selection box based on orthographic size. On Update ()
    private void ScaleSelectionBox()
    {
        if (false) //This is fucked up
        {
            CamOrtho = Camera.main.orthographicSize;
            scaleModifyer = (Mathf.Pow((0.046f * CamOrtho), 1.5f) + 1f);
            selectionBoxScale = new Vector3(scaleModifyer, scaleModifyer, 0f);

            SelectionBoxUR.transform.localScale = selectionBoxScale;
            SelectionBoxLR.transform.localScale = selectionBoxScale;
            SelectionBoxUL.transform.localScale = selectionBoxScale;
            SelectionBoxLL.transform.localScale = selectionBoxScale;
        }
        else
        {
            //thisshitfuckedup
        }
    }

    //Hacked together solution for the normalized selection box. Part of calls tack.
    private void HackRect(GameObject gameObject, IPlayerObject playerObject)
    {
        //This rotates the object back to a 0 position, gets the rectangle, then puts it back where it was
        Quaternion parentPositionHolder = gameObject.transform.rotation;
        gameObject.transform.rotation = new Quaternion(0, 0, 0, 0);

        SetZeroSelectionBoxRotation();
        SetZeroSelectionBoxTransform();
        SetSelectionBoxTransforms(playerObject);

        gameObject.transform.rotation = parentPositionHolder;
    }

    //Sets the transforms for the selectionbox objects. Part of call stack.
    private void SetSelectionBoxTransforms(IPlayerObject playerObject)
    {
        CalculateSelectionBoxTransforms(playerObject);
        SelectionBoxUR.transform.position = UpperRightPoint;
        SelectionBoxLR.transform.position = LowerRightPoint;
        SelectionBoxUL.transform.position = UpperLeftPoint;
        SelectionBoxLL.transform.position = LowerLeftPoint;

        selectionBox = new Rect(minBounds.x, maxBounds.x, minBounds.z, maxBounds.z);
    }

    //This is necessary to adjust the transforms for the size of the sprite, so it fits snug onto the object. Part of call stack
    private void CalculateSelectionBoxTransforms(IPlayerObject playerObject)
    {
        UpperRightPoint.x = playerObject.BoundingPoints.UpperRightPoint.x - SelectionBoxUR.GetComponent<Renderer>().bounds.max.x + 0.04f;
        UpperRightPoint.z = playerObject.BoundingPoints.UpperRightPoint.z - SelectionBoxUR.GetComponent<Renderer>().bounds.max.z + 0.04f;
        UpperRightPoint.y = -5;

        LowerRightPoint.x = playerObject.BoundingPoints.LowerRightPoint.x - SelectionBoxLR.GetComponent<Renderer>().bounds.max.x + 0.04f;
        LowerRightPoint.z = playerObject.BoundingPoints.LowerRightPoint.z + SelectionBoxLR.GetComponent<Renderer>().bounds.max.z - 0.04f;
        LowerRightPoint.y = -5;

        LowerLeftPoint.x = playerObject.BoundingPoints.LowerLeftPoint.x + SelectionBoxLL.GetComponent<Renderer>().bounds.max.x - 0.04f;
        LowerLeftPoint.z = playerObject.BoundingPoints.LowerLeftPoint.z + SelectionBoxLL.GetComponent<Renderer>().bounds.max.z - 0.04f;
        LowerLeftPoint.y = -5;

        UpperLeftPoint.x = playerObject.BoundingPoints.UpperLeftPoint.x + SelectionBoxUL.GetComponent<Renderer>().bounds.max.x - 0.04f;
        UpperLeftPoint.z = playerObject.BoundingPoints.UpperLeftPoint.z - SelectionBoxUL.GetComponent<Renderer>().bounds.max.z + 0.04f;
        UpperLeftPoint.y = -5;
    }

    private void debugdraw(IPlayerObject playerObject)
    {
        Debug.DrawLine(playerObject.BoundingPoints.UpperRightPoint, playerObject.BoundingPoints.LowerRightPoint, Color.red);
        Debug.DrawLine(playerObject.BoundingPoints.LowerRightPoint, playerObject.BoundingPoints.LowerLeftPoint, Color.red);
        Debug.DrawLine(playerObject.BoundingPoints.LowerLeftPoint, playerObject.BoundingPoints.UpperLeftPoint, Color.red);
        Debug.DrawLine(playerObject.BoundingPoints.UpperLeftPoint, playerObject.BoundingPoints.UpperRightPoint, Color.red);
    }

    //Necessary to set the selection box rotation to 0 to line up with parent. Part of call stack
    private void SetZeroSelectionBoxRotation()
    {
        SelectionBoxUR.transform.rotation = new Quaternion(0, 0, 0, 0);
        SelectionBoxLR.transform.rotation = new Quaternion(0, 0, 0, 0);
        SelectionBoxUL.transform.rotation = new Quaternion(0, 0, 0, 0);
        SelectionBoxLL.transform.rotation = new Quaternion(0, 0, 0, 0);
    }

    //Necessary to retrieve accurante render bounds for the textures.
    private void SetZeroSelectionBoxTransform()
    {
        SelectionBoxUR.transform.position = Vector3.zero;
        SelectionBoxLR.transform.position = Vector3.zero;
        SelectionBoxUL.transform.position = Vector3.zero;
        SelectionBoxLL.transform.position = Vector3.zero;
    }

    //Sets the parent of the selection boxes to this object. Part of call stack 
    private void SetParentObject(GameObject gameObject)
    {
        SelectionBoxUR.transform.parent = gameObject.transform;
        SelectionBoxLR.transform.parent = gameObject.transform;
        SelectionBoxLL.transform.parent = gameObject.transform;
        SelectionBoxUL.transform.parent = gameObject.transform;
    }

    //Destroys the prefabs when selection is set to false.
    private void DestroySelectionBox()
    {
        GameObject.Destroy(SelectionBoxLL);
        GameObject.Destroy(SelectionBoxLR);
        GameObject.Destroy(SelectionBoxUL);
        GameObject.Destroy(SelectionBoxUR);
    }

    //Instantiates the 4 corner prefabs. Part of call stack 
    private void CreateSelectionBox(GameObject gameObject)
    {
        SelectionBoxUR = (GameObject)Instantiate(GetGameObjects.GetSelectionBox("UpperRight"));
        SelectionBoxLR = (GameObject)Instantiate(GetGameObjects.GetSelectionBox("LowerRight"));
        SelectionBoxUL = (GameObject)Instantiate(GetGameObjects.GetSelectionBox("UpperLeft"));
        SelectionBoxLL = (GameObject)Instantiate(GetGameObjects.GetSelectionBox("LowerLeft"));
        SelectionBoxUR.GetComponent<Renderer>().enabled = true;
        SelectionBoxLR.GetComponent<Renderer>().enabled = true;
        SelectionBoxLL.GetComponent<Renderer>().enabled = true;
        SelectionBoxUL.GetComponent<Renderer>().enabled = true;

        SetParentObject(gameObject);
    }
}
