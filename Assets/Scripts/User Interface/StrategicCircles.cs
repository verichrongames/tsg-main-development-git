﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Vectrosity;

/* ================================================
 *  This Class Will Be added to any game object
 *  that can have strategic circles for it's view, 
 *  action, or attack range.
 * ===============================================*/

public class StrategicCircles : MonoBehaviour {

    public Material lineMaterial;

    VectorLine attackRangeLine;
    VectorLine viewRangeLine;

    Transform transformV;

    ICanAttack canAttackObject;
    IPlayerObject playerObject;
    IMoveableObject moveableObject;

    private bool linesInstantiated;
    private bool isMoveableObject;

	void Start () 
    {
        transformV = transform;
        canAttackObject = GetComponent<ICanAttack>();
        playerObject = GetComponent<IPlayerObject>();
        GetInterfaces();
	}
	
	void Update () 
    {
        DrawLines();
	}

    private void ClearLines()
    {
        int count = VectorLine.canvases3D.Count;
        //for(int i =0; i< count; i++)
        //{
        //    Destroy(VectorLine.canvases3D[i]);
        //}
        VectorLine.Destroy(ref attackRangeLine);
        linesInstantiated = false;
    }

    //Calls the appropriate methods to draw or update the new lines
    private void DrawLines()
    {
        if(playerObject.IsSelected)
        {
            if(!linesInstantiated)
            {
                DrawStrategicCircle();
                linesInstantiated = true;
            }

        }
        else if(linesInstantiated)
        {
            ClearLines();
        }
    }

    private void DrawStrategicCircle()
    {
        Vector3[] myLinePoints = new Vector3[251];
        attackRangeLine = new VectorLine(name + "AttackRange Circle", myLinePoints, lineMaterial, 5, LineType.Continuous);
        attackRangeLine.MakeCircle(transformV.position, canAttackObject.AttackRange, 250);
        attackRangeLine.SetColor(Color.red);

        int canvasIndex;
        if(VectorLine.canvases3D[0].transform.childCount == 0)
        {
            canvasIndex = VectorLine.canvases3D.Count - 1;
            attackRangeLine.canvasID = canvasIndex;
            attackRangeLine.rectTransform.parent = transformV;
            VectorLine.canvases3D[canvasIndex].transform.SetParent(transformV);
            VectorLine.canvases3D[canvasIndex].transform.localPosition = new Vector3(0, 0, -1);
            attackRangeLine.Draw3D();
        }
        else
        {
            canvasIndex = VectorLine.canvases3D.Count;
            attackRangeLine.canvasID = canvasIndex;
            attackRangeLine.rectTransform.parent = transformV;

            attackRangeLine.Draw3D();

            VectorLine.canvases3D[canvasIndex].transform.SetParent(transformV);
            VectorLine.canvases3D[canvasIndex].transform.localPosition = new Vector3(0, 0, -1);
        }
    }

    private void GetInterfaces()
    {
        moveableObject = GetComponent<IMoveableObject>();

        if (moveableObject != null)
            isMoveableObject = true;
        else
            isMoveableObject = false;
    }
}
