﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace CoreSys.Objects
{
    [Serializable]
    public class InstantiatedObjectsListType
    {
        public InstantiatedObjectsListType(GameObject newObject, Player player, int objectID, string objectName)
        {
            GameObjectReferance = newObject;
            OwningPlayer = player;
            ObjectID = objectID;
            ObjectName = objectName;
        }

        public InstantiatedObjectsListType(GameObject newObject, int objectID)
        {
            GameObjectReferance = newObject;
            ObjectID = objectID;
        }

        private string _objectName;
        public string ObjectName 
        {
            get
            {
                return _objectName;
            }
            set
            {
                _objectName = value;
                _objectName = _objectName.Replace("(Clone)", string.Empty);
                _objectName = _objectName.Trim();
            }
        }

        public int ObjectID { get; set; }
        public Player OwningPlayer { get; set; }
        public GameObject GameObjectReferance { get; set; }

    }
}
