﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace CoreSys.Objects
{
    [Serializable]
    class InstantiatedObjectListCollection : KeyedCollection<int, InstantiatedObjectsListType>
    {
        protected override int GetKeyForItem(InstantiatedObjectsListType item)
        {
            return item.ObjectID;
        }
    }
}
