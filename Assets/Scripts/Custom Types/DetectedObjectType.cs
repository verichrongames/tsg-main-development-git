﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*============================================
 *  This Class is a type that holds the information
 *  and referances about an object that has been
 *  detected by a unit/buildings sphere collider
 * Blame: Doug
 * ==========================================*/

public class DetectedObjectType
{
    public DetectedObjectType(GameObject gameObject, IPlayerObject playerObject, Player player, DetectedObjectTypes objectType, int instanceID, int priority)
    {
        DetectedGameObject = gameObject;
        DetectedPlayerObject = playerObject;
        DetectedObjectsPlayer = player;

        Priority = priority;
        InstanceID = instanceID;
        ObjectType = objectType;
    }

    public DetectedObjectType(GameObject gameObject, Player player, Attack attackType, DetectedObjectTypes objectType, int instanceID, int priority)
    {
        DetectedGameObject = gameObject;
        DetectedObjectsPlayer = player;
        AttackObject = attackType;

        Priority = priority;
        InstanceID = instanceID;
        ObjectType = objectType;
    }

    public DetectedObjectType(GameObject gameObject, DetectedObjectTypes objectType, int instanceID)
    {
        DetectedGameObject = gameObject;

        InstanceID = instanceID;
        ObjectType = objectType;
    }

    private GameObject detectedGameObjectBacker;
    public GameObject DetectedGameObject 
    { 
        get 
        { 
            return detectedGameObjectBacker; 
        } 
        set 
        { 
            transformV = value.transform;
            detectedGameObjectBacker = value; 
        } 
    }
    public Attack AttackObject { get; set; }
    public IPlayerObject DetectedPlayerObject { get; set; }
    public Player DetectedObjectsPlayer { get; set; }
    public Transform transformV { get; set; }

    public int InstanceID { get; set; }
    public float Priority { get; set; }
    public DetectedObjectTypes ObjectType { get; set; }
}

public enum DetectedObjectTypes
{
    EnemyObject,
    FriendlyObject,
    EnviornmentalObject,
    ProjectileObjects
}

