﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*============================================
 *  This Class is a Type used to hold an item
 *  within a build queue.
 * Blame: Doug
 * ==========================================*/

public class BuildQueueItEmptype
{
    public BuildQueueItEmptype(string queueItEmptype, GameObject objectToBuild, IBuildableObject buildableObject, int count)
    {
        QueueItEmptype = queueItEmptype;
        ObjectToBuild = objectToBuild;
        BuildableObject = buildableObject;
        Count = count;
        BuildProgress = 0;
    }

    public string QueueItEmptype { get; set; } //the name of the object type (ie. Battleship, Fighter, Transport)
    public GameObject ObjectToBuild { get; set; }
    public IBuildableObject BuildableObject { get; set; }
    public int Count { get; set; }
    public float BuildProgress { get; set; }
    public bool SentResourceUsage { get; set; }
}
