﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace CoreSys.Objects
{
    [Serializable]
    class DetectedObjectTypeCollection : KeyedCollection<int, DetectedObjectType>
    {
        protected override int GetKeyForItem(DetectedObjectType item)
        {
            return item.InstanceID;
        }
    }
}
