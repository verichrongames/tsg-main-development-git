﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*==============================================
 *   This Class Is Used To Store 
 *   the bounding angles for an object .
 *   
 *  These represent the angles required
 *  by a side of that object. 
 * ===========================================*/

namespace CoreSys.CustomTypes
{
    public class BoundingAnglesType
    {
        //====== Square Bounding Angles =======
        public BoundingAnglesType(float frontRight, float frontLeft,
                                  float rightFront, float rightBack,
                                  float backRight, float backLeft,
                                  float leftBack, float leftFront)
        {
            FrontRightAllowance = frontRight;
            FrontLeftAllowance = frontLeft;
            RightFrontAllowance = rightFront;
            RightBackAllowance = rightBack;
            BackRightAllowance = backRight;
            BackLeftAllowance = backLeft;
            LeftBackAllowance = leftBack;
            LeftFrontAllowance = leftFront;

            FrontRange = frontRight;
            RightRange = rightFront;
            BackRange = backRight;
            LeftRange = leftFront;
        }


        public float FrontRightAllowance { get; set; }
        public float FrontLeftAllowance { get; set; }

        public float RightFrontAllowance { get; set; }
        public float RightBackAllowance { get; set; }

        public float BackRightAllowance { get; set; }
        public float BackLeftAllowance { get; set; }

        public float LeftBackAllowance { get; set; }
        public float LeftFrontAllowance { get; set; }

        //Square side ranges.
        public float FrontRange { get; set; } //Less than this angle
        public float RightRange { get; set; } //Less than this angle
        public float BackRange { get; set; } //Greater than this angle
        public float LeftRange { get; set; } //Greater than this angle

    }
}
