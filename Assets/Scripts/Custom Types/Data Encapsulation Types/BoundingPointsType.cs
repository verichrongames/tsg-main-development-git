﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*==============================================
 *   This Class Is Used To Store 
 *   the bounding points for an object   
 * ===========================================*/

namespace CoreSys.CustomTypes
{
    public class BoundingPointsType
    {

        public BoundingPointsType(Vector3 upperRight, Vector3 upperLeft, Vector3 lowerRight, Vector3 lowerLeft, bool local)
        {
            if(local)
            {
                UpperRightPoint_L = upperRight;
                UpperLeftPoint_L = upperLeft;
                LowerRightPoint_L = lowerRight;
                LowerLeftPoint_L = lowerLeft;
            }
            else
            {
                UpperRightPoint = upperRight;
                UpperLeftPoint = upperLeft;
                LowerRightPoint = lowerRight;
                LowerLeftPoint = lowerLeft;
            }
        }


        private Vector3 UpperRightPointBacker;
        private Vector3 UpperLeftPointBacker;
        private Vector3 LowerRightPointBacker;
        private Vector3 LowerLeftPointBacker;

        public Vector3 UpperRightPoint { get { return UpperRightPointBacker; } set { value.y = 1; UpperRightPointBacker = value; } }
        public Vector3 UpperLeftPoint { get { return UpperLeftPointBacker; } set { value.y = 1; UpperLeftPointBacker = value; } }
        public Vector3 LowerRightPoint { get { return LowerRightPointBacker; } set { value.y = 1; LowerRightPointBacker = value; } }
        public Vector3 LowerLeftPoint { get { return LowerLeftPointBacker; } set { value.y = 1; LowerLeftPointBacker = value; } }

        //_L is local coordinates
        public Vector3 UpperRightPoint_L { get; set; }
        public Vector3 UpperLeftPoint_L { get; set; }
        public Vector3 LowerRightPoint_L { get; set; }
        public Vector3 LowerLeftPoint_L { get; set; }

    }
}
