﻿using UnityEngine;
using System.Collections;

public class UIObjectInfoType{

    public UIObjectInfoType(Material objectMaterial, float health, float maxHealth, string objectName, string playerName)
    {
        ObjectMaterial = objectMaterial;
        HealthFloat = health;
        MaxHealth = maxHealth;
        ObjectName = objectName;
        PlayerName = playerName;
    }

    public UIObjectInfoType(Sprite objectSprite, float health, float maxHealth, string objectName, string playerName)
    {
        ObjectSprite = objectSprite;
        HealthFloat = health;
        MaxHealth = maxHealth;
        ObjectName = objectName;
        PlayerName = playerName;
    }

    public UIObjectInfoType(float health, float maxHealth, string objectName, string playerName)
    {
        HealthFloat = health;
        MaxHealth = maxHealth;
        ObjectName = objectName;
        PlayerName = playerName;
    }

    public UIObjectInfoType(float health)
    {
        HealthFloat = health;
    }

    public UIObjectInfoType()
    {}

    public Material ObjectMaterial { get; set; }
    public Sprite ObjectSprite { get; set; }
    public GameObject gameObject { get; set; }
    public float HealthFloat { get; set; }
    public float MaxHealth { get; set; }
    public string ObjectName { get; set; }
    public string PlayerName { get; set; }


}
