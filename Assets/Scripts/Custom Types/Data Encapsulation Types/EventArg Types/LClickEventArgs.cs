﻿using UnityEngine;
using System.Collections;
using System;

  /*===========================================================
   * ===== This Class is a type used to encapsualte  =====
   *   ===== and send event data for a left click =====
   *=========================================================
   *
   *    ==============  Conforms to CQS  ==============
   */
public class LClickEventArgs : EventArgs
{
    
    public LClickEventArgs(GameObject obj, Vector3 point, Player p)
    {
        HitObject = obj;
        HitPoint = point;
        Player = p;
    }

    public LClickEventArgs(Vector3 point, Player p)
    {
        HitPoint = point;
        HitObject = null;
        Player = p;
    }

    public LClickEventArgs() { }

    public Player Player { get; set; }
    public GameObject HitObject { get; set; }
    public Vector3 HitPoint { get; set; }

}
