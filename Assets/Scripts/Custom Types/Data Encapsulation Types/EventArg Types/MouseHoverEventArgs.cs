﻿using UnityEngine;
using System.Collections;
using System;

/*===========================================================
 * ===== This Class is a type used to encapsualte  =====
 *   ===== and send event data for a right click =====
 *=========================================================
 *
 *    ==============  Conforms to CQS  ==============
 */
public class MouseHoverEventArgs : EventArgs
{
    public MouseHoverEventArgs(GameObject obj, Vector3 point, Player p)
    {
        HitObject = obj;
        HitPoint = point;
        Player = p;
    }

    public MouseHoverEventArgs(Vector3 point, Player p)
    {
        HitPoint = point;
        HitObject = null;
        Player = p;
    }

    public MouseHoverEventArgs() { }

    public Player Player { get; set; }
    public GameObject HitObject { get; set; }
    public Vector3 HitPoint { get; set; }

}
