﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*==============================================
 *   This Class Is Used To Store GameObjects, 
 *   IMoveableObjects and
 *   Their List of movements (destinations)
 * ===========================================*/

public class GameObjectMovesList
{

    public GameObjectMovesList(GameObject gameObject, int instanceID, IMoveableObject moveableObject, List<Vector3> destinationsList, float rotation)
    {
        MovementRotation = rotation;
        GameObject = gameObject;
        InstanceID = instanceID;
        MoveableObject = moveableObject;
        DestinationList = destinationsList;
    }

    public GameObject GameObject { get; set; }
    public int InstanceID { get; set; }
    public float MovementRotation { get; set; }
    public IMoveableObject MoveableObject { get; set; }
    public List<Vector3> DestinationList { get; set; }
}

