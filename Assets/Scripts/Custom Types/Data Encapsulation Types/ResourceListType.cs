﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

//what are comments, baby don't tell me, don't tell me anymore.
//kk - Catalyse

public class ResourceListType
{
    public float metalProduction, powerProduction;
    public float metalUsage, powerUsage;
    public float metalStorage, powerStorage;
    public float fabrication;

    public ResourceListType(float m, float p, float f, float mU, float pU, float ms, float ps) //Everyone knows the shorter a variable name the faster the program runs ///// Yes.
    {
        metalProduction = m;
        powerProduction = p;
        fabrication = f;
        metalUsage = mU;
        powerUsage = pU;
        metalStorage = ms;
        powerStorage = ps;
    }

    public ResourceListType(float m, float p, float f, float mU, float pU) //Everyone knows the shorter a variable name the faster the program runs ///// Yes.
    {
        metalProduction = m;
        powerProduction = p;
        fabrication = f;
        metalUsage = mU;
        powerUsage = pU;
    }

    public ResourceListType(float mS, float pS) //Everyone knows the shorter a variable name the faster the program runs ///// Yes.
    {
        metalStorage = mS;
        powerStorage = pS;
    }

    public void ClearList()
    {
        metalProduction = 0.0f;
        powerProduction = 0.0f;
        metalUsage = 0.0f;
        powerUsage = 0.0f;
        metalStorage = 0.0f;
        powerStorage = 0.0f;
    }
}
