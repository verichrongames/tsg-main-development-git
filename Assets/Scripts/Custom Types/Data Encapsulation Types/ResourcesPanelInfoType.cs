﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class ResourcesPanelInfoType
{
    public ResourcesPanelInfoType()
    {

    }

    public ResourcesPanelInfoType(int maxMetal, int currentMetal, int maxPower, int currentPower, int powerProductionTotal, int metalProductionTotal )
    {
        MaxMetal = maxMetal;
        CurrentMetal = currentMetal;
        MaxPower = maxPower;
        CurrentPower = currentPower;
        PowerProductionTotal = powerProductionTotal;
        MetalProductionTotal = metalProductionTotal;
    }
    public ResourcesPanelInfoType(int maxMetal, int currentMetal, int maxPower, int currentPower)
    {
        MaxMetal = maxMetal;
        CurrentMetal = currentMetal;
        MaxPower = maxPower;
        CurrentPower = currentPower;
    }

    public int MaxMetal { get; set; }
    public int CurrentMetal { get; set; }
    public int MaxPower { get; set; }
    public int CurrentPower { get; set; }

    public int PowerProductionTotal { get; set; }
    public int MetalProductionTotal { get; set; }
}
