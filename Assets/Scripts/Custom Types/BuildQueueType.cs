﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreSys.Buildings;

/*============================================
 *  This Class is a Type used to hold the build queue 
 *  of a specific building/object. This is instantiated and added
 *  to a list in teh build queue manager
 * Blame: Doug
 * ==========================================*/

public class BuildQueueType
{
    public BuildQueueType(GameObject queueOwner ,BuildQueueItEmptype newBuildItem, ICanBuild canBuildOwner, Player player)
    {
        ICanBuildOwner = canBuildOwner;
        BuildQueueItems = new List<BuildQueueItEmptype>();
        BuildQueueOwner = queueOwner;
        BuildQueueItems.Add(newBuildItem);
        Player = player;
    }

    public BuildQueueType(GameObject queueOwner, Grid ownerGrid, Core ownerCore, BuildQueueItEmptype newBuildItem, ICanBuild canBuildOwner, Player player)
    {
        ICanBuildOwner = canBuildOwner;
        QueueOwnerCore = ownerCore;
        QueueOwnerGrid = ownerGrid;
        BuildQueueItems = new List<BuildQueueItEmptype>();
        BuildQueueOwner = queueOwner;
        BuildQueueItems.Add(newBuildItem);
        Player = player;
    }

    public ICanBuild ICanBuildOwner { get; set; }
    public Player Player { get; set; }
    public GameObject BuildQueueOwner { get; set; }
    public Grid QueueOwnerGrid { get; set; }
    public Core QueueOwnerCore { get; set; }
    public List<BuildQueueItEmptype> BuildQueueItems { get; set; }
    public float CurrentItemProgress { get; set; } //Progress from 0-1
    public bool HasQueue { get; set; } //Bool if this queue has items in it. 
}
