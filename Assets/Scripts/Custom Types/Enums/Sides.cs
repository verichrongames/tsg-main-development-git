﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoreSys.CustomTypes
{
    public enum RectangleSides
    {
        None,
        Front,
        Back,
        Left,
        Right
    }
}
