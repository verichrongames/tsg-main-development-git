﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoreSys
{
    public enum PlayerObjectType
    {
        Default,
        Fighter,
        Frigate,
        Destroyer,
        Cruiser,
        Battleship
    }
}
