﻿using UnityEngine;
using System.Collections;
using CoreSys.Objects;

public class InstantiatedListAdder : MonoBehaviour
{

    /*=================================================
     *  This class should be attached as a component to 
     *  any gameobject or prefab that needs to be added
     *  to the instantiated list.
     * ==============================================*/

    bool firstEnableRan = false;
    bool firstTryFailed = false;
    public InstantiatedObjectsList instantiatedList;
    Player player;

    void OnEnable()
    {
        if (firstEnableRan == true)
        {
            instantiatedList.MoveFromDisabledToActive(this.gameObject, player);
        }
        else
        {
            GetInstantiatedList();
        }

        if(!firstTryFailed)
        {
            player = transform.root.GetComponent<Player>();
            instantiatedList.NewInstantiatedObject(this.gameObject, player);
            firstEnableRan = true;
        }

    }

	void Start () 
    {
        if(firstTryFailed)
        {
            GetInstantiatedList();
            player = transform.root.GetComponent<Player>();
            instantiatedList.NewInstantiatedObject(this.gameObject, player);
            firstEnableRan = true;
        }
	}

    void OnDisable()
    {
        instantiatedList.MoveFromActiveToDisabled(this.gameObject, player);
    }

    void OnDestroy()
    {
        instantiatedList.RemoveObjectFromList(this.gameObject, player);
    }

    //Method to retieve the Instantiated Objects List
    private void GetInstantiatedList()
    {
        instantiatedList = transform.root.GetComponentInChildren<InstantiatedObjectsList>();

        if(instantiatedList == null)
        {
            if(firstTryFailed)
            {
                throw new MissingComponentException("The Instantiated Objects List Component was not found for: " + this.gameObject.name);
            }
            firstTryFailed = true;
        }
    }
}
