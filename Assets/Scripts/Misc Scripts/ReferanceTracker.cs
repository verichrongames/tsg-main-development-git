﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*=====================================================
 *  This class will be added as a component to most 
 *  instantiated objects. It can be aded as a component
 *  in the prefab, or added at runtime, either way. 
 *  
 * It will maintain collections to each object that 
 * may maintain a referance to this object.
 *  
 * This is necessary so that all referances to an object
 * can be erased in the event of it beign destroyed
 * ===================================================*/


class ReferanceTracker : MonoBehaviour
{
    int thisInstanceID;

    Dictionary<int, ObjectMovementManager> objectMovementManager_Dict = new Dictionary<int, ObjectMovementManager>();
    Dictionary<int, PlayerSelectionManager> playerSelectionManager_Dict = new Dictionary<int, PlayerSelectionManager>();
    Dictionary<int, BuildQueueManager> buildQueueManager_Dict = new Dictionary<int, BuildQueueManager>();

    void Start()
    {
        thisInstanceID = gameObject.GetInstanceID();
    }

    /*====================================================================
    *            ========= Add Referance Methods =========
    * =================================================================*/

    public void AddObjectMovementManagerReferance(ObjectMovementManager referance)
    {
        int ID = referance.GetInstanceID();
        objectMovementManager_Dict.Add(ID, referance);
    }

    public void AddPlayerSelectionManagerReferance(PlayerSelectionManager referance)
    {
        int ID = referance.GetInstanceID();
        playerSelectionManager_Dict.Add(ID, referance);
    }

    public void AddBuildQueueManagerReferance(BuildQueueManager referance)
    {
        int ID = referance.GetInstanceID();
        buildQueueManager_Dict.Add(ID, referance);
    }

    /*====================================================================
     *            ========= Remove Referance Methods =========
    * =================================================================*/

    public void RemoveObjectMovementManagerReferance(ObjectMovementManager referance)
    {
        int ID = referance.GetInstanceID();
        objectMovementManager_Dict.Remove(ID);
    }

    public void RemovePlayerSelectionManagerReferance(PlayerSelectionManager referance)
    {
        int ID = referance.GetInstanceID();
        playerSelectionManager_Dict.Remove(ID);
    }

    public void RemoveBuildQueueManagerReferance(BuildQueueManager referance)
    {
        int ID = referance.GetInstanceID();
        buildQueueManager_Dict.Remove(ID);
    }

    /*====================================================================
    *            ========= Destroy Referance Methods =========
    * =================================================================*/

    public void DestroyAllReferances()
    {
        DestroyObjectMovementManagerReferance();
        DestroyPlayerSelectionManagerReferance();
        DestroyBuildQueueManagerReferance();

    }

    private void DestroyObjectMovementManagerReferance()
    {
        if(objectMovementManager_Dict.Count > 0)
        {
            foreach(var referance in objectMovementManager_Dict.Values)
            {
                referance.RemoveReferance(thisInstanceID);

            }
        }
    }

    private void DestroyPlayerSelectionManagerReferance()
    {
        if (playerSelectionManager_Dict.Count > 0)
        {
            foreach (var referance in playerSelectionManager_Dict.Values)
            {
                referance.RemoveReferance(thisInstanceID);

            }
        }
    }

    private void DestroyBuildQueueManagerReferance()
    {
        if (buildQueueManager_Dict.Count > 0)
        {
            foreach (var referance in buildQueueManager_Dict.Values)
            {
                referance.RemoveReferance(thisInstanceID);

            }
        }
    }
}

