﻿using UnityEngine;
using System.Collections;
using System;
using CoreSys;
/*===========================================================
 * This Class's sole purpose is to publish keyboard events 
 *=========================================================
 *
 * =========== Complies with CQS ===========
 */


public class KeyboardEvents : MonoBehaviour
{

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        UpButton();
        UpButtonUp();
        LeftButton();
        LeftButtonUp();
        DownButton();
        DownButtonUp();
        RightButton();
        RightButtonUp();
        ShiftButton();
        ShiftbuttonUp();
        EnterButton();
        EnterButtonUp();
        EscButton();
        EscButtonUp();
        PlusButton();
        PlusButtonUp();
        MinusButton();
        MinusButtonUp();
	}

    /*=========================================================
     * ==== End Initialization and per-frame loops ===
     *======================================================== */
    public delegate void MovementButtonHandler(string whichButton);
    public static event MovementButtonHandler MovementButtonHeldEvent;
    public static event MovementButtonHandler MovementButtonUpEvent;

    // ### Up-Button
    private static void UpButton()
    {
        if (Input.GetButton("KeyUp"))
        {
            if (MovementButtonHeldEvent != null)
                MovementButtonHeldEvent("KeyUpHeld");
        }
    }
    private static void UpButtonUp()
    {
        if (Input.GetButtonUp("KeyUp"))
        {
            if (MovementButtonUpEvent != null)
                MovementButtonUpEvent("KeyUpUp");
        }
    }

    //## Left-Button
    private static void LeftButton()
    {
        if (Input.GetButton("KeyLeft"))
        {
            if (MovementButtonHeldEvent != null)
                MovementButtonHeldEvent("KeyLeftHeld");
        }
    }
    private static void LeftButtonUp()
    {
        if(Input.GetButtonUp("KeyLeft"))
        {
            if (MovementButtonUpEvent != null)
                MovementButtonUpEvent("KeyLeftUp");
        }
    }

    //## Down-Button
    private static void DownButton()
    {
        if (Input.GetButton("KeyDown"))
        {
            if (MovementButtonHeldEvent != null)
                MovementButtonHeldEvent("KeyDownHeld");
        }
    }
    private static void DownButtonUp() 
    {
        if (Input.GetButtonUp("KeyDown")) 
        {
            if (MovementButtonUpEvent != null)
                MovementButtonUpEvent("KeyDownUp");
        }
    }

    //## Right-Button
    private static void RightButton()
    {
        if (Input.GetButton("KeyRight"))
        {
            if (MovementButtonHeldEvent != null)
                MovementButtonHeldEvent("KeyRightHeld");
        }
    }
    private static void RightButtonUp() 
    {
        if (Input.GetButtonUp("KeyRight")) 
        {
            if (MovementButtonUpEvent != null)
                MovementButtonUpEvent("KeyRightUp");
        }
    }


    public delegate void MiscButtonHandler(string whichButton);
    public static event MiscButtonHandler MiscButtonHeldEvent;
    public static event MiscButtonHandler MiscButtonUpEvent;

    private static void ShiftButton()
    {
        if(Input.GetButton("Shift"))
        {
            if (MiscButtonHeldEvent != null)
            {
                MiscButtonHeldEvent("Shift");
            }
                
        }
    }
    private void ShiftbuttonUp()
    {
        if(Input.GetButtonUp("Shift"))
        {
            if(MiscButtonUpEvent != null)
            {
                MiscButtonUpEvent("ShiftUp");
            }
        }
    }

    private static void EnterButton()
    {
        if (Input.GetButton("Return"))
        {
            if (MiscButtonHeldEvent != null)
            {
                MiscButtonHeldEvent("Enter");
            }

        }
    }
    private void EnterButtonUp()
    {
        if (Input.GetButtonUp("Return"))
        {
            if (MiscButtonUpEvent != null)
            {
                MiscButtonUpEvent("Enter");
            }
        }
    }

    public delegate void EscButtonHandler();
    public static event EscButtonHandler EscButtonHeldEvent;
    public static event EscButtonHandler EscButtonUpEvent;

    private static void EscButton()
    {
        if (Input.GetButton("Escape"))
        {
            if (EscButtonHeldEvent != null)
            {
                EscButtonHeldEvent();
            }

        }
    }
    private static void EscButtonUp()
    {
        if (Input.GetButtonUp("Escape"))
        {
            if (MiscButtonUpEvent != null)
            {
                EscButtonUpEvent();
            }
        }
    }

    public delegate void PlusButtonHandler();
    public static event PlusButtonHandler PlusButtonDownEvent;
    public static event PlusButtonHandler PlusButtonUpEvent;

    private static void PlusButton()
    {
        if (Input.GetButtonDown("Plus"))
        {
            if (PlusButtonDownEvent != null)
            {
                PlusButtonDownEvent();
            }

        }
    }
    private static void PlusButtonUp()
    {
        if (Input.GetButtonUp("Plus"))
        {
            if (PlusButtonUpEvent != null)
            {
                PlusButtonUpEvent();
            }
        }
    }

    public delegate void MinusButtonHandler();
    public static event MinusButtonHandler MinusButtonDownEvent;
    public static event MinusButtonHandler MinusButtonUpEvent;

    private static void MinusButton()
    {
        if (Input.GetButtonDown("Minus"))
        {
            if (MinusButtonDownEvent != null)
            {
                MinusButtonDownEvent();
            }

        }
    }
    private static void MinusButtonUp()
    {
        if (Input.GetButtonUp("Minus"))
        {
            if (MinusButtonUpEvent != null)
            {
                MinusButtonUpEvent();
            }
        }
    }
}
