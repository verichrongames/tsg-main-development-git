﻿using UnityEngine;
using System.Collections;
using CoreSys;

/*===========================================================
 * This class provides additional information when requested by
 * user input events. Such as finding a hit object, or hit location.
 *=========================================================
 *
 * =========== Complies with CQS ===========
 */

public class InputValidations : MonoBehaviour {

    public UserInput userInput;

	void Start () 
    {
        userInput = GetComponent<UserInput>();
    }
	
	void Update () 
    {

    }

    //Finds what object, if any, where clicked
    public GameObject FindHitObject()
    {
        RaycastHit2D hit2D = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(new Vector3(userInput.MousePos.x, userInput.MousePos.y, 10)), Vector2.zero);
        if(hit2D.collider != null)
            return hit2D.collider.gameObject;
        else
        {
            Ray ray = Camera.main.ScreenPointToRay(userInput.MousePos);
            RaycastHit hit3D;
            if (Physics.Raycast(ray, out hit3D))
                return hit3D.collider.gameObject;
        }
        return null;
    }

    //Determines where a click was
    public Vector3 FindHitPoint()
    {
        //Ray ray = Camera.main.ScreenPointToRay(userInput.MousePos);
        //RaycastHit hit;
        //if (Physics.Raycast(ray, out hit))
        //    return hit.point;
        //return ResourceManager.InvalidPosition;

        RaycastHit2D hit2D = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10)), Vector2.zero);

        if(hit2D.collider != null)
            return hit2D.point;
        else
        {
            Ray ray = Camera.main.ScreenPointToRay(userInput.MousePos);
            RaycastHit hit3D;
            if (Physics.Raycast(ray, out hit3D))
                return hit3D.point;
        }



        return ResourceManager.InvalidPosition;
    }

}
