﻿using UnityEngine;
using System.Collections;
using CoreSys;
/*==========================================================
 * Provides always updating information to any classes
 * that have a referance. This provides updated
 * mouse location and screen size info.
 * ========================================================
 * 
 * =========== Complies with CQS ===========
 */
public class UserInput : MonoBehaviour {

    public Player player;
    public Vector3 MousePos { get; set; }
    public Rect screenRect { get; set; }
    public bool isMouseOnScreen { get; set; }

    public float edgeOfScreenSize { get { return 0.99f; } }
    public float cornerOfScreenSize { get { return 0.97f; } }

	protected void Start () 
    {
        player = GetComponent<Player>();
	}

    protected void Update() 
    {
        GetMousePosition();
        GetScreenSize();
        MouseOnScreenCheck();
    }


    //Updates the mouse postion every frame
    //May need a class for these sorts of things
    private void GetMousePosition()
    {
        MousePos = Input.mousePosition;
    }

    private void GetScreenSize()
    {
        screenRect = new Rect(0, 0, Screen.width, Screen.height);
    }


    //Verifies mouse is still on screen
    private void MouseOnScreenCheck()
    {
        if(!screenRect.Contains(MousePos))
        {
            isMouseOnScreen = false;
        }
        else
        {
            isMouseOnScreen = true;
        }
    }

    
}
