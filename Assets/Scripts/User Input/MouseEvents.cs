﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using CoreSys;
/*===========================================================
 * This Class's sole purpose is to publish mouse events 
 *=========================================================
 * The Edge of Screen Events work by detecting if the mouse is within a border of the edge of the screen.
 * While excluding a triangular area of each corner, which is then designated to the corner 
 * edge of screen movements. 
 * In addition to that each method has a bool that checks if the method has been sucessfully ran. 
 * If that bool is true and the mouse is no longer in the appropriate area, that fired an event ended event.
 * That event is used to stop a count for how long the mouse has been there. 
 * 
 * =========== Complies with CQS ===========
 */
public class MouseEvents : MonoBehaviour {

    public InputValidations inputValidations;
    public UserInput userInput;
    public Player player;

    void Start () {
        inputValidations = GetComponent<InputValidations>();
        userInput = GetComponent<UserInput>();
        player = GetComponent<Player>();
	}
	
	void Update () {
        
        EdgeOfScreenRight();
        EdgeOfScreenLeft();
        EdgeOfScreenUp();
        EdgeOfScreenDown();
        EdgeOfScreenTopRight();
        EdgeOfScreenBottomRight();
        EdgeOfScreenBottomLeft();
        EdgeOfScreenTopLeft();

        ScrollDown();
        ScrollUp();

        CheckDoubleLeftClick();
        RightClick();

        HoverMouse();
	}

    public Vector3 mouseposition;

    /*=========================================================
     * ==== End Initialization and per-frame loops ===
     *=========================================================
     *      ==== Start Edge of Screen Events ====
     * =========================================================*/

    public delegate void EdgeOfScreenHandler(string whichEdge);
    public static event EdgeOfScreenHandler EdgeOfScreenEvent;
    public static event EdgeOfScreenHandler EdgeOfScreenEventEnded;

    //Mouse Edge of Screen Right Event
    public bool eventFiredRight;
    public void EdgeOfScreenRight()
    {
        mouseposition = userInput.MousePos;
        if (userInput.MousePos.x >= (userInput.screenRect.width * userInput.edgeOfScreenSize) && EdgeOfScreenEvent != null) //thefuckisthisNULLBULLSHITBITCH
        {
            if (userInput.MousePos.y < userInput.screenRect.height * userInput.cornerOfScreenSize &&
                userInput.MousePos.y > userInput.screenRect.height - (userInput.screenRect.height * userInput.cornerOfScreenSize) &&
                userInput.isMouseOnScreen)
            {
                eventFiredRight = true;
                EdgeOfScreenEvent("right");
            }
        }
        else if (eventFiredRight == true && EdgeOfScreenEventEnded != null)
        {
            EdgeOfScreenEventEnded("right");
            eventFiredRight = false;
        }
    }

    //Mouse Edge of Screen Left Event
    public bool eventFiredLeft;
    private void EdgeOfScreenLeft()
    {
        if (userInput.MousePos.x <= ((0 + (userInput.screenRect.width - (userInput.screenRect.width * userInput.edgeOfScreenSize)))) && EdgeOfScreenEvent != null)
        {
            if(userInput.MousePos.y < userInput.screenRect.height * userInput.cornerOfScreenSize &&
               userInput.MousePos.y > userInput.screenRect.height -(userInput.screenRect.height * userInput.cornerOfScreenSize) &&
                userInput.isMouseOnScreen)
            {
                EdgeOfScreenEvent("left");
                eventFiredLeft = true;
            }     
        }
        else if (eventFiredLeft == true && EdgeOfScreenEventEnded != null)
        {
            EdgeOfScreenEventEnded("left");
            eventFiredLeft = false;
        }
    }

    //Mouse Edge of Screen Up Event
    public bool eventFiredUp;
    private void EdgeOfScreenUp()
    {
        if (userInput.MousePos.y >= userInput.screenRect.height * userInput.edgeOfScreenSize && EdgeOfScreenEvent != null)
        {
            if (userInput.MousePos.x > ((0 + (userInput.screenRect.width - (userInput.screenRect.width * userInput.cornerOfScreenSize)))) &&
                userInput.MousePos.x < userInput.screenRect.width * userInput.cornerOfScreenSize &&
                userInput.isMouseOnScreen)
            {
                EdgeOfScreenEvent("up");
                eventFiredUp = true;
            }  
        }
        else if (eventFiredUp == true && EdgeOfScreenEventEnded != null)
        {
            EdgeOfScreenEventEnded("up");
            eventFiredUp = false;
        }
    }

    //Mouse Edge of Screen Down Event
    public bool eventFiredDown;
    private void EdgeOfScreenDown()
    {
        if (userInput.MousePos.y <= userInput.screenRect.height - (userInput.screenRect.height * userInput.edgeOfScreenSize) && EdgeOfScreenEvent != null)
        {
            if (userInput.MousePos.x > ((0 + (userInput.screenRect.width - (userInput.screenRect.width * userInput.cornerOfScreenSize)))) &&
                userInput.MousePos.x < userInput.screenRect.width * userInput.cornerOfScreenSize &&
                userInput.isMouseOnScreen)
            {
                EdgeOfScreenEvent("down");
                eventFiredDown = true;
            }  
        }
        else if (eventFiredDown == true && EdgeOfScreenEventEnded != null)
        {
            EdgeOfScreenEventEnded("down");
            eventFiredDown = false;
        }
    }

    //Mouse Edge of Screen Top Right Event
    public bool eventFiredTopRight;
    private void EdgeOfScreenTopRight()
    {
        if(
            userInput.MousePos.x >= userInput.screenRect.width * userInput.cornerOfScreenSize &&
            userInput.MousePos.y >= userInput.screenRect.height * userInput.cornerOfScreenSize &&
            EdgeOfScreenEvent != null &&
            userInput.isMouseOnScreen)
        {
            EdgeOfScreenEvent("top right");
            eventFiredTopRight = true;
        }
        else if (eventFiredTopRight == true && EdgeOfScreenEventEnded != null)
        {
            EdgeOfScreenEventEnded("top right");
            eventFiredTopRight = false;
        }
    }

    //Mouse Edge of Screen Bottom Right Event
    public bool eventfiredBottomRight;
    private void EdgeOfScreenBottomRight()
    {
        if (
            userInput.MousePos.x >= userInput.screenRect.width * userInput.cornerOfScreenSize &&
            userInput.MousePos.y <= userInput.screenRect.height - (userInput.screenRect.height * userInput.cornerOfScreenSize) &&
            EdgeOfScreenEvent != null &&
            userInput.isMouseOnScreen)
        {
            EdgeOfScreenEvent("bottom right");
            eventfiredBottomRight = true;
        }
        else if (eventfiredBottomRight == true && EdgeOfScreenEventEnded != null)
        {
            EdgeOfScreenEventEnded("bottom right");
            eventfiredBottomRight = false;
        }
    }

    //Mouse Edge of Screen Bottom Left Event
    public bool eventFiredBottomLeft;
    private void EdgeOfScreenBottomLeft()
    {
        if (
            userInput.MousePos.x <= userInput.screenRect.width - (userInput.screenRect.width * userInput.cornerOfScreenSize) &&
            userInput.MousePos.y <= userInput.screenRect.height - (userInput.screenRect.height * userInput.cornerOfScreenSize) &&
            EdgeOfScreenEvent != null &&
            userInput.isMouseOnScreen)
        {
            EdgeOfScreenEvent("bottom left");
            eventFiredBottomLeft = true;
        }
        else if (eventFiredBottomLeft == true && EdgeOfScreenEventEnded != null)
        {
            EdgeOfScreenEventEnded("bottom left");
            eventFiredBottomLeft = false;
        }
    }

    //Mouse Edge of Screen Top Left Event
    public bool eventFiredTopLeft;
    private void EdgeOfScreenTopLeft()
    {
        if (
            userInput.MousePos.x <= userInput.screenRect.width - (userInput.screenRect.width * userInput.cornerOfScreenSize) &&
            userInput.MousePos.y >= userInput.screenRect.height * userInput.cornerOfScreenSize &&
            EdgeOfScreenEvent != null &&
            userInput.isMouseOnScreen)
        {
            EdgeOfScreenEvent("top left");
            eventFiredTopLeft = true;
        }
        else if (eventFiredTopLeft == true && EdgeOfScreenEventEnded != null)
        {
            EdgeOfScreenEventEnded("top left");
            eventFiredTopLeft = false;
        }
    }
    /* =================================================
     *        ==== MouseWheel Events ====
     * ================================================*/
    //ScrollWheel Backwards/Down
    public delegate void ScrollDownHandler();
    public static event ScrollDownHandler ScrollDownEvent;
    private void ScrollDown()
    {
        if (Input.GetAxis("Mouse ScrollWheel") < 0 && ScrollDownEvent != null)
        {
            if(userInput.isMouseOnScreen)
            {
                ScrollDownEvent();
            }

        }
    }

    //ScrollWheel Forwards/Up
    public delegate void ScrollUpHandler();
    public static event ScrollUpHandler ScrollUpEvent;
    private void ScrollUp()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0 && ScrollUpEvent != null)
        {
            if(userInput.isMouseOnScreen)
            {
                ScrollUpEvent();
            }
        }
    }

    /* =================================================
     *        ==== Mouse Hover Events ====
     * ================================================*/

    public delegate void MouseHoverHandler(string type, MouseHoverEventArgs e);
    public static event MouseHoverHandler MouseHoverGroundEvent;
    public static event MouseHoverHandler MouseHoverObjectEvent;

    private void HoverMouse()
    {
        if(userInput.isMouseOnScreen && MouseHoverGroundEvent != null)
        {
            GameObject hitObject = inputValidations.FindHitObject();
            Vector3 hitPoint = inputValidations.FindHitPoint();
            hitPoint.z = -1;
            {
                if (EventSystem.current.IsPointerOverGameObject())
                {
                    MouseHoverObjectEvent("UserInterface", new MouseHoverEventArgs(hitPoint, player));
                }
                else if (hitObject && hitPoint != ResourceManager.InvalidPosition && hitObject.name != "Ground" && !EventSystem.current.IsPointerOverGameObject())
                {
                    MouseHoverObjectEvent("Object", new MouseHoverEventArgs(hitObject, hitPoint, player));
                }
                else if (hitObject && hitPoint != ResourceManager.InvalidPosition && hitObject.name == "Ground" && !EventSystem.current.IsPointerOverGameObject())
                {
                    MouseHoverGroundEvent("Ground", new MouseHoverEventArgs(hitPoint, player));
                }
            }
        }
    }


    /* =================================================
     *        ==== Clicking Events ====
     * ================================================*/

    public delegate void RightClickHandler(string clickType, RClickEventArgs e);
    public delegate void LeftClickHandler(string clickType, LClickEventArgs e);
    public delegate void DoubleLeftClickHandler(string clickType, LClickEventArgs e);
    public static event RightClickHandler RightClickEvent;
    public static event LeftClickHandler LeftClickEvent;
    public static event DoubleLeftClickHandler DoubleLeftClickEvent;

    private void RightClick()
    {
        if(Input.GetMouseButtonDown(1))
        {
            if (userInput.isMouseOnScreen && !EventSystem.current.IsPointerOverGameObject())
            {
                GameObject hitObject = inputValidations.FindHitObject();
                Vector3 hitPoint = inputValidations.FindHitPoint();
                hitPoint.z = -1;

                if (hitObject && hitPoint != ResourceManager.InvalidPosition && hitObject.name != "Ground")
                {
                    RightClickEvent("Object", new RClickEventArgs(hitObject, hitPoint, player));
                }
                else if (hitObject.name == "Ground")
                {
                    RightClickEvent("Ground", new RClickEventArgs(hitPoint, player));
                }
            }

        }
    }

    private void LeftClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (userInput.isMouseOnScreen && !EventSystem.current.IsPointerOverGameObject())
            {
                GameObject hitObject = inputValidations.FindHitObject();
                Vector3 hitPoint = inputValidations.FindHitPoint();
                hitPoint.z = -1;

                if (hitObject && hitPoint != ResourceManager.InvalidPosition && hitObject.name != "Ground")
                {
                    LeftClickEvent("Object", new LClickEventArgs(hitObject, hitPoint, player));
                }
                else if (hitObject.name == "Ground")
                {
                    LeftClickEvent("Ground", new LClickEventArgs(hitPoint, player));
                }
            }
        }
    }




    bool clickOnce = false;
    float clickSpeed = 0.2f;
    float clickTimer;
    //Checks for a double left click
    private void CheckDoubleLeftClick()
    {
        if(Input.GetMouseButtonDown(0))
        {
            if(!clickOnce)
            {
                clickOnce = true;
                clickTimer = Time.time;
                LeftClick();
            }
            else
            {
                clickOnce = false;
                DoubleLeftClick();
            }
        }
        if (clickOnce)
        {
            if ((Time.time - clickTimer) > clickSpeed && (Time.time - clickTimer) != 0)
            {
                clickOnce = false;
            }
        }
    }

    //Publishes the Double Left Click event
    private void DoubleLeftClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (userInput.isMouseOnScreen && !EventSystem.current.IsPointerOverGameObject())
            {
                GameObject hitObject = inputValidations.FindHitObject();
                Vector3 hitPoint = inputValidations.FindHitPoint();
                hitPoint.z = -1;

                if (hitObject && hitPoint != ResourceManager.InvalidPosition && hitObject.name != "Ground")
                {
                    DoubleLeftClickEvent("DoubleObject", new LClickEventArgs(hitObject, hitPoint, player));
                }
                else if (hitObject.name == "Ground")
                {
                    DoubleLeftClickEvent("DoubleGround", new LClickEventArgs(hitPoint, player));
                }
            }
        }
    }
}
