﻿using UnityEngine;
using System.Collections;

public class ScreenRects : MonoBehaviour {

    public Rect screenRect;
    public Rect orderBarRect;
    public Rect resourceBarRect;

    public Texture2D orderBar;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        GetScreenSize();
        MakeResourceBarRect();
        MakeOrdersBar();
	}

    void OnGUI()
    {
        GUI.DrawTexture(resourceBarRect, orderBar);
    }

    private void GetScreenSize()
    {
        screenRect = new Rect(0, 0, Screen.width, Screen.height);
    }

    private void MakeResourceBarRect()
    {
        resourceBarRect = new Rect(0, 0, Screen.width * 0.1f, Screen.height * 0.1f);
    }

    private void MakeOrdersBar()
    {
        orderBarRect = new Rect(screenRect.width * 0.9f, 0, Screen.width * 0.1f, Screen.height);
    }
}
