﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public interface IMoveableObject
{
    bool Moving { get; set; }
    bool Turning { get; set; }
    float MoveSpeed { get; set; }
    float TurnSpeed { get; set; }
    float Angle { get; set; }
    Transform transformV { get; set; }
}

