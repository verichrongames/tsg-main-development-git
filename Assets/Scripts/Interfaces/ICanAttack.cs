﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

public interface ICanAttack
{
    int AttackDamage { get; set; }
    GameObject AttackType { get; set; }
    GameObject PointDefenseType { get; set; }
    float FireRate { get; set; }
    float AttackRange { get; set; }
    float PointDefenseRange { get; set; }
    void SetEnemyObjectsReferance(KeyedCollection<int, DetectedObjectType> collection, string type);
    void NewEnemyDetected(string colliderType);
    void EnemyGone(int ID, string type);
}
