﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public interface ICanBuild
{
    string buildPanelItems { get; set; }
    bool InBuildMode { get; set; }
    bool CurrentlyBuilding { get; set; }
    List<string> buildableObjects { get; set; }
    Vector3 RallyPoint { get; set; }
    float BuildProgress { get; set; }

    void SetRallyPoint(Vector3 rally);
}
