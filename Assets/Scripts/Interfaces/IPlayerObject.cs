﻿using UnityEngine;
using System.Collections;
using CoreSys;
using CoreSys.CustomTypes;

public interface IPlayerObject
{
    void SetIsSelected(bool selected);
    //void InitializeBuildPanel(bool selected);

    Player player { get; set; }
    //Transform transformV { get; set; }

    void TakeDamage(float amount);
    void RightClickGround(RClickEventArgs e);
    void RightClickObject(RClickEventArgs e);
    void UpdateBoundingPoints();

    PlayerObjectType objectType { get; set; }
    bool IsSelected { get; }
    string ObjectName { get; set; }
    float HitPoints { get; set; }
    float MaxHitPoints { get; set; }
    float ActionDistance { get; set; }
    float ViewRange { get; set; }

    //Vector3 UpperRightPoint { get; }
    //Vector3 LowerRightPoint { get; }
    //Vector3 LowerLeftPoint { get; }
    //Vector3 UpperLeftPoint { get; }

    BoundingPointsType BoundingPoints { get; }
    BoundingAnglesType BoundingAngles { get; }

}

