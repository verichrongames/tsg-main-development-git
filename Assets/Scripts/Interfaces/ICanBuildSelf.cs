﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public interface ICanBuildSelf
{
    bool CurrentlyBuilding { get; set; }
    float BuildProgress { get; set; }
}
