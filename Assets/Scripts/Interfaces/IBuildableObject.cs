﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public interface IBuildableObject
{
    int powerCost { get; set; }
    int metalCost { get; set; }
    int baseBuildTime { get; set; }
}
