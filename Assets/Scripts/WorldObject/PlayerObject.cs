﻿///* playerObject.cs
// * Parent script for all world objects: buildings, units, resources(coming soon bitches)
// * References from scripts like unit.cs and buildings.cs are called from here.
// * base.whatever
// * 
// * 
// * 
// */


//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using CoreSys;

//public class PlayerObject : MonoBehaviour {

//    public string objectName;
//    public Texture2D buildImage;
//    public float hitPoints, maxHitPoints;
//    public float metalCost, titaniumCost, powerCost, metalProduction, titaniumProduction, powerProduction; //Sellvalue needs to be destroyed//
//    public float ActionDistance; //Possibly irrelevant, though the concept is useful, may not be needed with new movement sys//

//    public Player player;
//    protected SelectionBox selectionBox;
//    ObjectHUDPanel HUDPanel;
//    protected string[] actions = {};
//    public bool currentlySelected = false;
//    protected Bounds selectionBounds;
//    public bool shiftHeld = false;

//    protected Rect playingArea = new Rect(0.0f, 0.0f, 0.0f, 0.0f);
//    protected GUIStyle healthStyle = new GUIStyle();
//    protected float healthPercentage = 1.0f;

//    public Vector3 maxBounds; //Max X/Y on object
//    public Vector3 minBounds; //Min X/Y on object
//    public Rect selectionBoxRect; // Selection Box
//    public Vector3 UpperRightPoint, LowerRightPoint, UpperLeftPoint, LowerLeftPoint;

//    //New Methods so I can test the UI
//    public float health = 500;
//    public int maxHealth = 500;

//    protected virtual void OnEnable()
//    {
//        KeyboardEvents.MiscButtonHeldEvent += ModKeySwitches;
//        KeyboardEvents.MiscButtonUpEvent += ModKeySwitches;
//    }

//    protected virtual void OnDisable()
//    {
//        KeyboardEvents.MiscButtonHeldEvent -= ModKeySwitches;
//        KeyboardEvents.MiscButtonUpEvent -= ModKeySwitches;
//    }

//    protected virtual void Awake() {
//        selectionBounds = ResourceManager.InvalidBounds;
//        HUDPanel = GameObject.Find("ObjectHUD").GetComponent<ObjectHUDPanel>();
//        //CalculateBounds();
//    }
	
//    protected virtual void Start () {
//        SetPlayer();
//        SetSelectionBox();
//    }
	
//    protected virtual void Update () {

//    }
	
//    protected virtual void OnGUI() {
//        //if (currentlySelected)
//        //        DrawSelection ();
//    }

//    /*=======================================================
//     * ==== End Initialization And Loops ====
//     * ======================================================*/

//    //Determines the 4 points for the box around the unit.
//    public void DetermineBoundingBoxPoints()
//    {
//        maxBounds = this.renderer.bounds.max;
//        minBounds = this.renderer.bounds.min;

//        UpperRightPoint = new Vector3(maxBounds.x, maxBounds.y, 0f); //UpperRight
//        LowerRightPoint = new Vector3(maxBounds.x, minBounds.y, 0f); //LowerRight
//        UpperLeftPoint = new Vector3(minBounds.x, maxBounds.y, 0f); //UpperLeft
//        LowerLeftPoint = new Vector3(minBounds.x, minBounds.y, 0f); //LowerLeft
//    }

//    //Handles mod key's such as shift or ctrl
//    private void ModKeySwitches(string button)
//    {
//        switch (button)
//        {
//            case "Shift":
//                shiftHeld = true;
//                break;
//            case "ShiftUp":
//                shiftHeld = false;
//                break;
//        }
//    }

//    //Sets the objects selected? bool
//    public virtual void SetSelection(bool selected)
//    {
//        currentlySelected = selected;
//        //CallSelectionBox(selected);
//    }

//    ////Calls for the Selection Box creation
//    //public void CallSelectionBox(bool selected)
//    //{
//    //    selectionBox.RecieveBoxCall(selected, this,);
//    //}


//    public string[] GetActions() {
//        return actions;
//    }

//    public virtual void RightClickGround(RClickEventArgs e) { }
//    public virtual void RightClickObject(RClickEventArgs e) { }

//    public virtual void SetSelection(bool selected, Rect playingArea)
//    {
//        currentlySelected = selected;
//    }

//    //private void DrawSelection() {
//    //    GUI.skin = ResourceManager.SelectBoxSkin;
//    //    Rect selectBox = WorkManager.CalculateSelectionBox(selectionBounds, playingArea);
//    //    //Draw the selection box around the currently selected object, within the bounds of the playing area
//    //    GUI.BeginGroup(playingArea);
//    //    DrawSelectionBox(selectBox);
//    //    GUI.EndGroup();
//    //}

//    //public void CalculateBounds() {
//    //    selectionBounds = new Bounds(transform.position, Vector3.zero);
//    //    foreach(Renderer r in GetComponentsInChildren< Renderer >()) {
//    //        selectionBounds.Encapsulate(r.bounds);
//    //    }
//    //}
	
//    //public virtual void PerformAction(string actionToPerform) {
//    //    //it is up to children with specific actions to determine what to do with each of those actions
//    //}

//    //protected virtual void DrawSelectionBox(Rect selectBox) {
//    //    GUI.Box(selectBox, "");
//    //    CalculateCurrentHealth(0.35f, 0.65f);
//    //    DrawHealthBar(selectBox, "");
//    //}
	
//    //What does this do, why is it here? What is lowsplit and highsplit? comments are needed.
//    protected virtual void CalculateCurrentHealth(float lowSplit, float highSplit) {
//        healthPercentage = (float)hitPoints / (float)maxHitPoints;
//        if(healthPercentage > highSplit) healthStyle.normal.background = ResourceManager.HealthyTexture;
//        else if(healthPercentage > lowSplit) healthStyle.normal.background = ResourceManager.DamagedTexture;
//        else healthStyle.normal.background = ResourceManager.CriticalTexture;
//    }
	
//    //protected void DrawHealthBar(Rect selectBox, string label) {
//    //    healthStyle.padding.top = -20;
//    //    healthStyle.fontStyle = FontStyle.Bold;
//    //    GUI.Label(new Rect(selectBox.x, selectBox.y - 7, selectBox.width * healthPercentage, 5), label, healthStyle);
//    //}

//    //public bool IsOwnedBy(Player owner) {
//    //    if(player && player.Equals(owner)) {
//    //        return true;
//    //    } else {
//    //        return false;
//    //    }
//    //}

//    //?????
//    //public Bounds GetSelectionBounds()
//    //{
//    //    return selectionBounds;
//    //}

//    //?????
//    //public void SetColliders(bool enabled)
//    //{
//    //    Collider[] colliders = GetComponentsInChildren<Collider>();
//    //    foreach (Collider collider in colliders) collider.enabled = enabled;
//    //}

//    ////?????
//    //public void SetPlayingArea(Rect playingArea)
//    //{
//    //    this.playingArea = playingArea;
//    //}

//    //Method to set player
//    public void SetPlayer() 
//    {
//        player = transform.root.GetComponentInChildren< Player >();
//    }

//    public void SetSelectionBox()
//    {
//        selectionBox = GetComponent<SelectionBox>();
//    }
//}