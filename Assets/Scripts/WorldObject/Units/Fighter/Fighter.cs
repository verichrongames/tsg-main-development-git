﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;


class Fighter : Unit, IMoveableObject, IBuildableObject
{
    //IBuildable Requirements
    [HideInInspector, SerializeField] int m_PowerCost;
    [HideInInspector, SerializeField] int m_MetalCost;
    [HideInInspector, SerializeField] int m_BaseBuildTime;
    [ExposeProperty] public int powerCost { get { return m_PowerCost; } set { m_PowerCost = value; } }
    [ExposeProperty] public int metalCost { get { return m_MetalCost; } set { m_MetalCost = value; } }
    [ExposeProperty] public int baseBuildTime { get { return m_BaseBuildTime; } set { m_BaseBuildTime = value; } }


    //IMoveableObject Requirements
    [HideInInspector, SerializeField] float m_MoveSpeed;
    [HideInInspector, SerializeField] float m_TurnSpeed;
    [ExposeProperty] public float MoveSpeed { get { return m_MoveSpeed; } set { m_MoveSpeed = value; } }
    [ExposeProperty] public float TurnSpeed { get { return m_TurnSpeed; } set { m_TurnSpeed = value; } }
    public float Angle { get; set; }
    public bool Moving { get; set; }
    public bool Turning { get; set; }
    public Transform transformV { get; set; }


    //ICanAttack Requirements
    [HideInInspector, SerializeField] int m_AttackDamage;
    [HideInInspector, SerializeField] string m_AttackType;
    [HideInInspector, SerializeField] float m_AttackRange;
    [HideInInspector, SerializeField] float m_FireRate;
    [ExposeProperty] public int AttackDamage { get { return m_AttackDamage; } set { m_AttackDamage = value; } }
    [ExposeProperty] public string AttackType { get { return m_AttackType; } set { m_AttackType = value; } }
    [ExposeProperty] public float AttackRange { get { return m_AttackRange; } set { m_AttackRange = value; } }
    [ExposeProperty] public float FireRate { get { return m_FireRate; } set { m_FireRate = value; } }

    void Start()
    {
        base.Start();
        transformV = GetTransform();
    }
}

