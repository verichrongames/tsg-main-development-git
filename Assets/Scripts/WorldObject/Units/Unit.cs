﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vectrosity;
using CoreSys;
using CoreSys.CustomTypes;

/*===========================================================
 * This is the base class for all units.
 *=========================================================
 */

public class Unit : Bolt.EntityBehaviour<IUnitState>, IPlayerObject
{
    public Player player { get; set; }

    public Vector3 destination;
    private Vector3 origin;
    protected Texture2D buildImage;
    private ObjectUIElements UIElementsComponent;
    private GameObject childSprite;
    protected Transform transformV;

    //IPlayerObject Requirements
    [HideInInspector, SerializeField] float m_HitPoints;
    [HideInInspector, SerializeField] float m_MaxHitPoints;
    [HideInInspector, SerializeField] float m_ViewRange;
    [HideInInspector, SerializeField] PlayerObjectType m_objectType;
    [ExposeProperty] public float ViewRange { get { return m_ViewRange; } set { m_ViewRange = value; } }
    [ExposeProperty] public float HitPoints { get { return m_HitPoints; } set { m_HitPoints = value; } }
    [ExposeProperty] public float MaxHitPoints { get { return m_MaxHitPoints; } set { m_MaxHitPoints = value; } }
    [ExposeProperty] public PlayerObjectType objectType { get { return m_objectType; } set { m_objectType = value; } }

    public BoundingPointsType BoundingPoints { get; set; }
    public BoundingAnglesType BoundingAngles { get; set; }
    public bool IsSelected { get; set; }
    public bool isShiftHeld;
    public string ObjectName { get; set; }
    public float ActionDistance { get; set; }

    public Rect selectionBoxRect; // Selection Box

    //Cached
    Renderer spriteRenderer;
    Mesh boundingMesh;

    public List<Vector3> destinationsList = new List<Vector3>();
    public int destinationIndex = 0;
    private bool boundingPointsCalculated; //If bounding points have been calculated this frame
    private int frameCalcualtedOn = 0;

    protected virtual void OnEnable()
    {
        KeyboardEvents.MiscButtonHeldEvent += ModKeySwitches;
        KeyboardEvents.MiscButtonUpEvent += ModKeySwitches;
    }

    protected virtual void OnDisable()
    {
        KeyboardEvents.MiscButtonHeldEvent -= ModKeySwitches;
        KeyboardEvents.MiscButtonUpEvent -= ModKeySwitches;
    }

    public virtual void Awake()
    {
        transformV = GetTransform();
        ObjectName = this.name;
        childSprite = transform.FindChild("Object Sprite").gameObject;
        SetUIElementsReferance();
        spriteRenderer = childSprite.GetComponent<Renderer>();
        BoundingMeshCreation();
    }

    protected virtual void Start() 
    {
        SetBoundingAngles();
        SetPlayer();
    }

    protected virtual void Update()
    {
        /*
        if(!linedrawn)
        {
            vectorPoint = new VectorPoints("bs", boundsList, null, 2);
            vectorPoint.SetColor(Color.green);
            linedrawn = true;
        }
        vectorPoint.Draw3D();
         */
    }

    public void RightClickGround(RClickEventArgs e) { }
    public void RightClickObject(RClickEventArgs e) { }

    //Public Method To Take Damage
    public void TakeDamage(float damage)
    {
        if(damage >= HitPoints)
        {
            StartCoroutine(DestroySelf());
        }
        else
        {
            HitPoints -= damage;
        }
    }

    //Handles mod key's such as shift or ctrl
    private void ModKeySwitches(string button)
    {
        switch (button)
        {
            case "Shift":
                isShiftHeld = true;
                break;
            case "ShiftUp":
                isShiftHeld = false;
                break;
        }
    }
    /*======================================================================
     *    ========== Mesh And Bounding Box Methods ==========
     * ==================================================================*/

    //Handles the method calls for the mesh creation
    private void BoundingMeshCreation()
    {
        Quaternion currentRotation = ResetPosition(new Quaternion(0, 0, 0, 0));
        BoundingPointsType boundingPoints = DetermineBounds();
        Mesh boundingMesh = CreateBoundingMesh(boundingPoints);
        AssignMesh(boundingMesh);
        ResetPosition(currentRotation);
        BoundingPoints = boundingPoints;
        this.boundingMesh = boundingMesh;
    }

    //Creates the mesh filter and assigns the mesh
    private void AssignMesh(Mesh boundingMesh)
    {
        MeshFilter newMeshFilter = gameObject.AddComponent<MeshFilter>();
        newMeshFilter.mesh = boundingMesh;
    }

    //Creates a bounding mesh for the unit
    private Mesh CreateBoundingMesh(BoundingPointsType boundingPoints)
    {

        Mesh boundingMesh = new Mesh();
        boundingMesh.name = "Bounding Quad";     

        Vector3[] vertices = new Vector3[4]
        {
            boundingPoints.LowerLeftPoint_L,
            boundingPoints.LowerRightPoint_L,
            boundingPoints.UpperRightPoint_L,
            boundingPoints.UpperLeftPoint_L
        };

        int[] triangles = new int[6]
        {
            0,
            2,
            1,

            0,
            3,
            2
        };

        boundingMesh.vertices = vertices;
        boundingMesh.triangles = triangles;

        return boundingMesh;
    }

    //Resets the units rotation to the specific quaternion and returns the quaterion from before the reset
    private Quaternion ResetPosition(Quaternion toResetTo)
    {
        Quaternion currentRotation = transformV.rotation;
        transformV.rotation = toResetTo;
        return currentRotation;
    }

    //Determines and returns the bounding box points
    private BoundingPointsType DetermineBounds()
    {
        Vector3 maxBounds = spriteRenderer.bounds.max;
        Vector3 minBounds = spriteRenderer.bounds.min;

        Vector3 maxBounds_L = transformV.InverseTransformPoint(spriteRenderer.bounds.max);
        Vector3 minBounds_L = transformV.InverseTransformPoint(spriteRenderer.bounds.min);

        BoundingPointsType boundingPoints = new BoundingPointsType(new Vector3(maxBounds_L.x, 0, maxBounds_L.z),
                                                                   new Vector3(minBounds_L.x, 0, maxBounds_L.z),
                                                                   new Vector3(maxBounds_L.x, 0, minBounds_L.z),
                                                                   new Vector3(minBounds_L.x, 0, minBounds_L.z),
                                                                   true);
        //Necessary to save the non-local coordinates
        boundingPoints.UpperRightPoint = new Vector3(maxBounds.x, 1, maxBounds.z);
        boundingPoints.UpperLeftPoint = new Vector3(minBounds.x, 1, maxBounds.z);
        boundingPoints.LowerRightPoint = new Vector3(maxBounds.x, 1, minBounds.z);
        boundingPoints.LowerLeftPoint = new Vector3(minBounds.x, 1, minBounds.z);
        return boundingPoints;
    }

    //Updates the bounding point information when requested
    public void UpdateBoundingPoints()
    {
        Vector3 maxBounds = spriteRenderer.bounds.max;
        Vector3 minBounds = spriteRenderer.bounds.min;

        Vector3 maxBounds_L = transformV.InverseTransformPoint(spriteRenderer.bounds.max);
        Vector3 minBounds_L = transformV.InverseTransformPoint(spriteRenderer.bounds.min);

        //Necessary to save the non-local coordinates
        BoundingPoints.UpperRightPoint_L = boundingMesh.vertices[2];
        BoundingPoints.UpperLeftPoint_L = boundingMesh.vertices[3];
        BoundingPoints.LowerRightPoint_L = boundingMesh.vertices[1];
        BoundingPoints.LowerLeftPoint_L = boundingMesh.vertices[0];

        //Necessary to save the non-local coordinates
        BoundingPoints.UpperRightPoint = transformV.TransformPoint(boundingMesh.vertices[2]);
        BoundingPoints.UpperLeftPoint = transformV.TransformPoint(boundingMesh.vertices[3]);
        BoundingPoints.LowerRightPoint = transformV.TransformPoint(boundingMesh.vertices[1]);
        BoundingPoints.LowerLeftPoint = transformV.TransformPoint(boundingMesh.vertices[0]);
    }

    //Sets the angles for each side of the unit. Assumes a rectangular shape. Only needs to run once
    private void SetBoundingAngles()
    {
        float frontRightAllowance = Vector3.Angle(transformV.position - BoundingPoints.UpperRightPoint, -transformV.forward);
        float frontLeftAllowance = Vector3.Angle(transformV.position - BoundingPoints.UpperLeftPoint, -transformV.forward);

        float rightFrontAllowance = Vector3.Angle(transformV.position - BoundingPoints.UpperRightPoint, -transformV.right);
        float rightBackAllowance = Vector3.Angle(transformV.position - BoundingPoints.LowerRightPoint, -transformV.right);

        float backRightAllowance = Vector3.Angle(transformV.position - BoundingPoints.LowerRightPoint, -transformV.forward);
        float backLeftallowance = Vector3.Angle(transformV.position - BoundingPoints.LowerLeftPoint, -transformV.forward);

        float leftBackAllowance = Vector3.Angle(transformV.position - BoundingPoints.LowerRightPoint, -transformV.right);
        float leftFrontAllowance = Vector3.Angle(transformV.position - BoundingPoints.UpperRightPoint, -transformV.right);

        BoundingAngles = new BoundingAnglesType(frontRightAllowance, frontLeftAllowance,
                                                           rightFrontAllowance, rightBackAllowance,
                                                           backRightAllowance, backLeftallowance,
                                                           leftBackAllowance, leftFrontAllowance);
    }

    //===============================================================================


    //Sets the objects selected? bool
    public virtual void SetIsSelected(bool selected)
    {
        IsSelected = selected;
        CallSelectionBox(selected);
    }

    //Calls for the Selection Box creation
    public void CallSelectionBox(bool selected)
    {
        UIElementsComponent.ToggleSelectionBox(selected);
    }

    //Sets the sphere colliders size to the view range.
    protected void SetViewRange()
    {
        transform.FindChild("ViewRange").GetComponent<SphereCollider>().radius = ViewRange;
    }

    public void SetPlayer()
    {
        player = GetComponentInParent<Player>();
    }

    public void SetUIElementsReferance()
    {
        UIElementsComponent = GetComponent<ObjectUIElements>();
    }

    //Returns this objects transform. Is used to cache the Transform referance to stop GetComponent calls
    protected Transform GetTransform()
    {
        return GetComponent<Transform>();
    }

    private IEnumerator DestroySelf()
    {
        transform.Translate(Vector3.up * 10000);
        yield return new WaitForSeconds(0.1f);
        Destroy(this.gameObject);
        yield break;
    }
}

