﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;


class Transport : Unit,IMoveableObject, IBuildableObject
{
    //IBuildable Requirements
    [HideInInspector, SerializeField] int m_PowerCost;
    [HideInInspector, SerializeField] int m_MetalCost;
    [HideInInspector, SerializeField] int m_BaseBuildTime;
    [ExposeProperty] public int powerCost { get { return m_PowerCost; } set { m_PowerCost = value; } }
    [ExposeProperty] public int metalCost { get { return m_MetalCost; } set { m_MetalCost = value; } }
    [ExposeProperty] public int baseBuildTime { get { return m_BaseBuildTime; } set { m_BaseBuildTime = value; } }


    //IMoveableObject Requirements
    [HideInInspector, SerializeField] float m_MoveSpeed;
    [HideInInspector, SerializeField] float m_TurnSpeed;
    [ExposeProperty] public float MoveSpeed { get { return m_MoveSpeed; } set { m_MoveSpeed = value; } }
    [ExposeProperty] public float TurnSpeed { get { return m_TurnSpeed; } set { m_TurnSpeed = value; } }
    public float Angle { get; set; }
    public bool Moving { get; set; }
    public bool Turning { get; set; }
    public Transform transformV { get; set; }

    void Start()
    {
        base.Start();
        transformV = GetTransform();
    }

}

