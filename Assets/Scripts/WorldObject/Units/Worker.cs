﻿/*
using UnityEngine;
using CoreSys;

public class Worker : Unit {
	
	public int buildSpeed;
	
	private Building currentProject;
	private bool building = false;
	private float amountBuilt = 0.0f;
	
	// Game Engine methods, all can be overridden by subclass 
	
	protected override void Start () {
		base.Start();
	}
	
	protected override void Update () {
		base.Update();
		if(building) {
			Build (currentProject);
		}
	}
	
	// Public Methods 

    //public override void MouseClick (GameObject hitObject, Vector3 hitPoint, Player controller) {
    //    bool doBase = true;
    //    //only handle input if owned by a human player and currently selected
    //    if(player && player.human && currentlySelected && hitObject && hitObject.name!="Ground") {
    //        Building building = hitObject.transform.parent.GetComponent< Building >();
    //        if(building) {
    //            if(building.UnderConstruction()) {
    //                //SetBuilding(building); //Broken by Merge
    //                doBase = false;
    //            }
    //        }
    //    }
    //    if(doBase){
    //        base.MouseClick(hitObject, hitPoint, controller);
    //        building = false;
    //        currentProject = null;
    //    }
    //}

    
	public override void SetBuilding (Building project) 
    {
		base.SetBuilding (project);
		StartMove(project.transform.position, project.actdist);
		currentProject = project;
		building = true;
	}
     
	
    //public override void PerformAction (string actionToPerform) {
    //    base.PerformAction (actionToPerform);
    //    CreateBuilding(actionToPerform);
    //}
	
    
	protected override void StartMove(Vector3 destination, float actdist) {
		base.StartMove(destination, actdist);
		amountBuilt = 0.0f;
	}
	
	private void CreateBuilding(string buildingName) {
		Vector3 buildPoint = new Vector3(transform.position.x, transform.position.y, -5);
		//if(player) player.OM.createBuilding(buildingName, buildPoint, this, playingArea);
	}

	private void Build(Building project){
		if (!moving && InRange(project)) {
			amountBuilt += buildSpeed * Time.deltaTime;
			int amount = Mathf.FloorToInt (amountBuilt);
			if (amount > 0) {
				amountBuilt -= amount;
				currentProject.Construct (amount);
				if (!currentProject.UnderConstruction ()){
					building = false;
					//project.RallyInit();
				}
			}
		}
	}

	private bool InRange(Building destination){
		bool RangeCheck;
		if (Vector3.Distance (transform.position, destination.transform.position) <= destination.actionDistance)
			RangeCheck = true;
		else 
			RangeCheck = false;
		return RangeCheck;
	}
}*/