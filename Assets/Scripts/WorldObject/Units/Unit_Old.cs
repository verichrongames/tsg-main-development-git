﻿///* Unit.cs
// * Unit.cs is a subclass of WorldEdit
// * **WorldEdit = WO in notes below
// * Class definition for all objects in game
// * @note: many functions are called from WO, refer to base class for more information if referenced
// * 
// *
// *
// * 
// */

//using UnityEngine;
//using System.Collections;
//using CoreSys;

//public class Unit_Old : PlayerObject {

//    protected bool moving;

//    public float moveSpeed, rotateSpeed; //obvious vars
//    private Vector3 destination; //location to move unit to
//    private Vector3 targetRotation; // vector of destination look in code for more info
//    private float curFwd, oldFwd; //testing var
//    public bool rotating; //how to stop rotation once target is reached
//    public static string rotstat = "Unused";
//    private float destinationTarget;
//    private bool isobject;
//    private bool target;

//    private Vector3 lastFwd;
//    private float curAngleX = 0;

//    public static float distcheck;
	
//    /*** Game Engine methods, all can be overridden by subclass ***/
	
//    protected override void Awake() {
//        base.Awake();
//    }
	
//    protected override void Start () {
//        base.Start();
//        lastFwd = transform.forward;
//    }
	
//    protected override void Update () {
//        base.Update();
//        if(rotating) TurnToTarget();
//        if(moving) MakeMove();
//    }
	
//    protected override void OnGUI() {
//        base.OnGUI();
//    }

//    public virtual void SetBuilding(Building creator) {
//        //specific initialization for a unit can be specified here
//    }

//    //Sets the state of the mouse cursor icon based on the worldobj version of the class. Refer to WorldObject.SetHoverState
//    public override void SetHoverState(GameObject hoverObject) {
//        base.SetHoverState(hoverObject);
//        //only handle input if owned by a human player and currently selected
//        if(player && player.human && currentlySelected) {
//            bool moveHover = false;
//            if(hoverObject.name == "Ground") {
//                moveHover = true;
//            } else {
//                Resource resource = hoverObject.transform.parent.GetComponent< Resource >();
//                if(resource && resource.isEmpty()) moveHover = true;
//            }
//            //if(moveHover) player.hud.SetCursorState(CursorState.Move);
//        }
//    }

//    //Pulls the mouseclick function from WorldObject, grabs info from the hit point called in WO to 
//    public override void MouseClick(GameObject hitObject, Vector3 hitPoint, Player controller) {
//        base.MouseClick(hitObject, hitPoint, controller);
//        //only handle input if owned by a human player and currently selected
//        if(player && player.human && currentlySelected) {
//            if((hitObject.name == "Ground" || !WorkManager.ResCheck(hitObject) )&& hitPoint != ResourceManager.InvalidPosition) {
//                float x = hitPoint.x;
//                //makes sure that the unit stays on top of the surface it is on
//                float y = hitPoint.y;
//                float z = -1f;
//                Vector3 destination = new Vector3(x, y, z);
//                StartMove(destination);
//            }
//        }
//    }


//    //Null function that is called during update, sets the state of an object.
//    public void StartMove(Vector3 destination) {
//        isobject = false;
//        this.destination = destination;
//        targetRotation = destination;
//        rotating = true; //decides whether the object is rotating.  Currently this is the issue setting this var to go to false once the object has been turned the correct direction
//        moving = true; //Initially this was set to false, waiting for the object to rotate first, this was inefficient so I set them both to positive initially.
//    } //This function is the basis of how movement is called

//    protected virtual void StartMove(Vector3 destination, float actdist) {
//        destinationTarget = actdist;
//        this.destination = destination;
//        targetRotation = destination;
//        rotating = true; //decides whether the object is rotating.  Currently this is the issue setting this var to go to false once the object has been turned the correct direction
//        moving = true; //Initially this was set to false, waiting for the object to rotate first, this was inefficient so I set them both to positive initially.
//        isobject = true;
//    }
		
//    //Makes unit movement based on destination set above.
//    private void MakeMove() {
//        transform.position = Vector3.MoveTowards(transform.position, destination, Time.deltaTime * moveSpeed);
//        distcheck = Vector3.Distance (transform.position, destination);
//        if (isobject) {
//            if (distcheck <= destinationTarget) moving = false;
//        }
//        if(distcheck <= .05) moving = false;
//        CalculateBounds();
//        }

//    private void checkrot (){ //This was a function in testing meant to stop the rotation if the unit rotated less that x amount, never worked. Not in use.
//        Vector3 curFwd = transform.forward;
//        // measure the angle rotated since last frame:
//        float ang = Vector3.Angle(curFwd, lastFwd);
//        if (ang > 0.01){ // if rotated a significant angle...
//            // fix angle sign...
//            if (Vector3.Cross(curFwd, lastFwd).x < 0) ang = -ang;
//            curAngleX += ang; // accumulate in curAngleX...
//            if(curFwd == lastFwd)
//                rotating = false;
//            lastFwd = curFwd; // and update lastFwd

//        }
//    }
		

//    //public class made for testing, updated the rotstat global var so it can be shown in the HUD
//    public void rotstatus () {
//        if (rotating == true)
//            rotstat = "True";
//        else
//            rotstat = "False";
//    }


//    //Turning function, works, although it may need to be rewritten
//    private void TurnToTarget() {
//        Vector3 vectorToTarget = targetRotation - transform.position;
//        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
//        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
//        transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * 4f);

//        checkrot ();
//        rotstatus ();
//        CalculateBounds();
//        //if(target) CalculateTargetDestination();
//    }

	
//    /*
//    private void CalculateTargetDestination() {
//        //calculate number of unit vectors from unit centre to unit edge of bounds
//        Vector3 originalExtents = selectionBounds.extents;
//        Vector3 normalExtents = originalExtents;
//        normalExtents.Normalize();
//        float numberOfExtents = originalExtents.x / normalExtents.x;
//        int unitShift = Mathf.FloorToInt(numberOfExtents);
		
//        //calculate number of unit vectors from target centre to target edge of bounds
//        WorldObject playerObject = destinationTarget.GetComponent< WorldObject >();
//        if(playerObject) originalExtents = playerObject.GetSelectionBounds().extents;
//        else originalExtents = new Vector3(0.0f, 0.0f, 0.0f);
//        normalExtents = originalExtents;
//        normalExtents.Normalize();
//        numberOfExtents = originalExtents.x / normalExtents.x;
//        int targetShift = Mathf.FloorToInt(numberOfExtents);
		
//        //calculate number of unit vectors between unit centre and destination centre with bounds just touching
//        int shiftAmount = targetShift + unitShift;
		
//        //calculate direction unit needs to travel to reach destination in straight line and normalize to unit vector
//        Vector3 origin = transform.position;
//        Vector3 direction = new Vector3(destination.x - origin.x, 0.0f, destination.z - origin.z);
//        direction.Normalize();
		
//        //destination = center of destination - number of unit vectors calculated above
//        //this should give us a destination where the unit will not quite collide with the target
//        //giving the illusion of moving to the edge of the target and then stopping
//        for(int i = 0; i < shiftAmount; i++) destination -= direction;
//        destination.y = destinationTarget.transform.position.y;
//    }*/
//}
