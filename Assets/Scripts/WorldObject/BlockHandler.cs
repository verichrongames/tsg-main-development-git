﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CoreSys;
using CoreSys.Objects;

//The point of this system is to facilitate the creation of all objects for a player, where the objects will handle their own damage and destruction.
//ObjectManager creates all temp objects during placement mode and facilitates the creation of the objects then passing off their construction to the block or unit making a new core.
namespace CoreSys.Buildings
{
    public class BlockHandler : MonoBehaviour
    {
        private Player player;

        public Material transparent, solid;

        public bool blockPlacement = false;
        private bool shiftDown = false;

        private Core activeCore;
        private List<Node> nodeList;
        public List<Block> temporaryBlocksList = new List<Block>();
        private string currentBlockType;

        //System Functions//-------------------------------------------------------------------------------------------------------//
        public void OnEnable()
        {
            MouseEvents.RightClickEvent += RightClickSwitches;
            MouseEvents.LeftClickEvent += LeftClickSwitches;
            KeyboardEvents.MiscButtonHeldEvent += ShiftHeldDown;
            KeyboardEvents.MiscButtonUpEvent += ShiftUp;
        }

        public void OnDisable()
        {
            MouseEvents.RightClickEvent -= RightClickSwitches;
            MouseEvents.LeftClickEvent -= LeftClickSwitches;
            KeyboardEvents.MiscButtonHeldEvent -= ShiftHeldDown;
            KeyboardEvents.MiscButtonUpEvent -= ShiftUp;
        }

        public void Awake()
        {

        }

        public void Start()
        {
            player = transform.root.GetComponent<Player>();
        }


        public void Update()
        {

        }
        //End System Functions//---------------------------------------------------------------------------------------------------//

        //Mode Checks//-----------------------------------------------------------------------------------------------------------//
        //These allow for outside checks of the OM status without allowing them to change its status. 
        //These are also the statuses that mean the Manager will need to be aware of clicks
        public bool BlockMode()
        {
            return blockPlacement;
        }

        //End Mode Checks//------------------------------------------------------------------------------------------------------//

        /*==========================================
         * All relevcant event data is forwarded in the EventArgs as a package //Baby dont spell me, dont spell me, no moar.
         * There are overloaded constructors, so if the ground
         * is clicked there is no game object set in EventArgs 
         * (the name, EventArgs is a .NET thing)
         * The switches are probably best used to forward the data onto 
         * a method or methods that can decide what do do with it
         * based on what type of object was hit...etc
         * 
         * Player may handle the clicks. Not sure yet. Added this directly in here
         * anyways, can always make an if statement before the switches if
         * you want to ignore all mouse click input under certain conditions
         * ======================================*/


        private void RightClickSwitches(string clickType, RClickEventArgs e)
        {
            switch (clickType)
            {
                case "Ground":
                    if (blockPlacement)
                        CancelBlockPlacement();
                    break;
                case "Object":
                    if (blockPlacement)
                        CancelBlockPlacement();
                    break;
            }
        }

        private void LeftClickSwitches(string clickType, LClickEventArgs e)
        {
            switch (clickType)
            {
                case "Ground":
                    if (blockPlacement)
                        CancelBlockPlacement();
                    break;
                case "Object":
                    if (blockPlacement)
                        BuildClick(e.HitObject);
                    break;
            }
        }

        private void ShiftHeldDown(string buttonType)
        {
            if (buttonType == "Shift")
                shiftDown = true;
        }

        private void ShiftUp(string buttonType)
        {
            if (buttonType == "ShiftUp")
                shiftDown = false;
        }

        //Block Creation Methods//--------------------------------------------------------------------------------------------------//

        public void HandleNodesAdded(List<Node> nodes, Core parent)
        {
            if (blockPlacement)
            {
                List<Node> nodesToAdd = nodes;
                foreach (Node node in nodesToAdd)
                {
                    if (node.tier <= parent.nodeTier)
                    {
                        Vector3 blockSpawn = new Vector3(node.transform.position.x, parent.transform.position.y, node.transform.position.z);
                        GameObject newBlock = (GameObject)Instantiate(GetGameObjects.GetBlock(currentBlockType), blockSpawn, new Quaternion());
                        Block tempBlock = newBlock.GetComponent<Block>();
                        tempBlock.transform.SetParent(parent.transform);
                        temporaryBlocksList.Add((Block)tempBlock); //Adds ghost to list for deletion later
                        tempBlock.creatorNode = node;
                        tempBlock.blockTier = node.tier;
                        tempBlock.PlacementMode(parent);
                    }
                }
            }
        }

        public void BlockBuildMode(string blockName, Core parent, List<Node> nodes)
        {
            CancelBlockPlacement();
            activeCore = parent;
            nodeList = nodes;
            currentBlockType = blockName;
            blockPlacement = true;
            activeCore.InBuildMode = true;
            var blockToSpawn = GetGameObjects.GetBlock(blockName);
            GhostBlocks(blockToSpawn, parent);
        }

        //GhostBlocks creates Transparent blocks of the given type on all nodes from the core that called it, which then allows for you to pick one.
        public void GhostBlocks(GameObject blockToSpawn, Core parent)
        {
            foreach (Node node in nodeList)
            {
                if (node.tier <= parent.nodeTier)
                {
                    Vector3 blockSpawn = new Vector3(node.transform.position.x, parent.transform.position.y, node.transform.position.z);
                    GameObject newBlock = (GameObject)Instantiate(blockToSpawn, blockSpawn, new Quaternion());
                    newBlock.name = blockToSpawn.name;
                    Block tempBlock = newBlock.GetComponent<Block>();
                    tempBlock.transform.SetParent(parent.transform);
                    temporaryBlocksList.Add((Block)tempBlock); //Adds ghost to list for deletion later
                    tempBlock.creatorNode = node;
                    tempBlock.blockTier = node.tier;
                    tempBlock.PlacementMode(parent);
                }
            }
        }

        //When block placement is true do buildclick
        //Handle Click from UserInput.cs to build the selected block
        public void BuildClick(GameObject hit) //This function is what needs to handle clicks if BlockPlacement = true;, this allows for the manager to decide if something has been hit.  
        {//In the new system I will need to expand this function to include things like turning off build mode which happens when you right click, click in the hud, or click on the ground.
            Block block = hit.GetComponent<Block>();
            if (block)
            {
                foreach (Block check in temporaryBlocksList)
                {
                    if (check == block)
                    { //Essentially checks if the object that was hit is one of the ghosts
                        BuildBlock(check);
                        break;
                    }
                }
            }
        }

        //Create the clicked block and clear out the remaining ghosted blocks.
        public void BuildBlock(Block build)
        {
            build.StartConstruction();
            Node nodeToDelete = build.creatorNode;
            nodeList.Remove(build.creatorNode);
            build.creatorNode = null;
            if (shiftDown)
                RemoveFromList(build); //If shift is held, this catches the click and stops the build mode from canceling
            else
            {
                blockPlacement = false; //Takes the objectmanager out of placement mode, so its stops looking for clicks
                ExitBuildMode(build); //Clears ghost blocks
            }
            Destroy(nodeToDelete.gameObject);
        }

        //Destroys all Ghosted Blocks once a Block is chosen to be built, with the exception of the block chosen.
        public void ExitBuildMode(Block built)
        {
            foreach (Block clear in temporaryBlocksList)
            {
                if (built != clear)
                {
                    Destroy(clear.gameObject);
                }
            }
            temporaryBlocksList.Clear();
            activeCore.InBuildMode = false;
            activeCore.ResetNodeList(nodeList);
            activeCore = null;
        }

        private void RemoveFromList(Block built) //Removes a built block from the list allowing the rest to be built still.
        {
            temporaryBlocksList.Remove(built);
        }

        //Clears all Ghost blocks from the Core its called on
        public void CancelBlockPlacement()
        {
            if (blockPlacement)
            {
                blockPlacement = false;
                foreach (Building erase in temporaryBlocksList)
                {
                    erase.transform.parent = transform.root;
                    Destroy(erase.gameObject);
                }
                temporaryBlocksList.Clear();
                activeCore.InBuildMode = false;
                activeCore.ResetNodeList(nodeList);
                activeCore = null;
            }
        }

        //End Block Creation Methods//-----------------------------------------------------------------------------------------------//

    }
}