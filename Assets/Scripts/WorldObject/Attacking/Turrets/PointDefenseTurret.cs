﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections;
using System.Linq;
using System.Text;


public class PointDefenseTurret : Turret
{

    PolygonCollider2D polygonCollider;
    private float defaultColliderRotationAdjust;

    private float delayToDefaultRotation = 2f;
    private float timeToDefaultRotation;
    private bool defaultRotationTimerStarted = false;
    private bool canDefaultRotate = false;

    protected override void Start()
    {
        base.Start();
        polygonCollider = GetComponentInChildren<PolygonCollider2D>();
        defaultColliderRotationAdjust = (360 - defaultRotationAngle) - 270;
    }

    protected override void Update()
    {
        base.Update();
        if (areTargets)
        {
            RotateToTarget();
            SetColliderOffsetRotation();
        }
        else if (isRotating)
        {
            if(canDefaultRotate)
            {
                DefaultRotation();
                SetColliderOffsetRotation();
            }
            else
            {
                DefaultRotationTimer();
            }
        }
        //SetColliderOffsetRotation();
    }

    //Rotates the turret towards it's target
    private void RotateToTarget()
    {
        isRotating = true;
        canDefaultRotate = false;
        defaultRotationTimerStarted = false; //So the default rotation timer is reset
        Transform targetTransform = currentlyAttackingObject.transformV;
        Vector3 angle = (targetTransform.position - transformV.position);
        float lookAngle = Mathf.Atan2(angle.y, angle.x) * Mathf.Rad2Deg - 90;
        Quaternion q = Quaternion.AngleAxis(lookAngle, Vector3.forward);
        transformV.rotation = Quaternion.Lerp(transformV.rotation, q, Time.deltaTime * rotationSpeed);
        CheckTargetLineOfSight(q);
    }

    private void SetColliderOffsetRotation()
    {
        if(Quaternion.Angle(transformV.rotation, Quaternion.Euler(0,0,40)) > 0.5f)
        {
            polygonCollider.transform.localRotation = Quaternion.Euler(0, 0, 360 - (transformV.localRotation.eulerAngles.z + defaultColliderRotationAdjust));
        }
    }

    private void DefaultRotationTimer()
    {
        if (!defaultRotationTimerStarted)
        {
            defaultRotationTimerStarted = true;
            timeToDefaultRotation = Time.time + delayToDefaultRotation;
            canDefaultRotate = false;
        }

        if (defaultRotationTimerStarted)
        {
            if (Time.time > timeToDefaultRotation)
            {
                defaultRotationTimerStarted = false;
                canDefaultRotate = true;
                Debug.Log("Can Default Rotate");
            }
        }
    }

    //Sets the turrets default rotation when there is no more targets
    private void DefaultRotation()
    {
        Quaternion q = Quaternion.AngleAxis(defaultRotationAngle, Vector3.forward);
        transformV.localRotation = Quaternion.Slerp(transformV.localRotation, q, Time.deltaTime * 5);
        CheckDefaultRotation();
    }

    //Checks if default rotation is complete to stop rotation loops for performance reasons
    private void CheckDefaultRotation()
    {
        if (Quaternion.Angle(transformV.localRotation, Quaternion.Euler(0, 0, defaultRotationAngle)) < 0.5f)
        {
            isRotating = false;
            canDefaultRotate = false;
        }
    }

    //Checks if the target is directly within the turrets line of sight
    private void CheckTargetLineOfSight(Quaternion targetRotation)
    {
        if (Quaternion.Angle(transformV.rotation, targetRotation) <= 4)
        {
            firingLocation = currentlyAttackingObject.DetectedGameObject.transform.position;
            canFire = true;
        }
        else
        {
            canFire = false;
        }
    }

    //Method that initiates the new attack
    protected override void AttackStart(Transform targetTransform)
    {
        GameObject projectile = (GameObject)Instantiate(attackType, transformV.position, transformV.rotation);
        BlasterPointDefense projectileAttack = projectile.GetComponent<BlasterPointDefense>();
        projectileAttack.OnCreation(attackDamage, player, targetTransform, currentlyAttackingObject.DetectedGameObject);
    }
}

