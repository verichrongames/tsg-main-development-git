﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections;
using System.Linq;
using System.Text;
/*=======================================
 *  This is the base class for all projectile
 *  turrets. 
 * =====================================*/

public class ProjectileAttackTurret : Turret
{

    public LayerMask layerMasks;
    public bool inLOS; //Test member


    protected override void Start()
    {
        base.Start();
        CheckForward();
    }

    protected override void Update()
    {
        base.Update();
        if (areTargets)
        {
            RotateToTarget();
        }
        else if(isRotating)
        {
            DefaultRotation();
        }
    }

    //Test member to visualize turret look direction
    private void CheckForward()
    {
        Transform fov = transform.FindChild("FieldOfView");
        fov.transform.position = transformV.position + transformV.forward * attackRange;
    }

    //Rotates the turret towards it's target
    private void RotateToTarget()
    {
        isRotating = true;
        Transform targetTransform = currentlyAttackingObject.transformV;
        Vector3 angle = (targetTransform.position - transformV.position);
        float lookAngle = Mathf.Atan2(angle.x, angle.z) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(lookAngle, Vector3.up);
        transformV.rotation = Quaternion.Slerp(transformV.rotation, q, Time.deltaTime * rotationSpeed);
        CheckTargetLineOfSight(q);
    }

    //Sets the turrets default rotation when there is no more targets
    private void DefaultRotation()
    {
        Quaternion q = Quaternion.AngleAxis(defaultRotationAngle, Vector3.up);
        transformV.localRotation = Quaternion.Slerp(transformV.localRotation, q, Time.deltaTime * 4);
        CheckDefaultRotation();
    }

    //Checks if default rotation is complete to stop rotation loops for performance reasons
    private void CheckDefaultRotation()
    {
        if(transformV.localRotation.eulerAngles == Vector3.zero)
        {
            isRotating = false;
        }
    }

    //Checks if the target is directly within the turrets line of sight
    private void CheckTargetLineOfSight(Quaternion targetRotation)
    {
        //if(Quaternion.Angle(transformV.rotation, targetRotation) <= 2)
        //{
        //    firingLocation = currentlyAttackingObject.DetectedGameObject.transform.position;
        //    canFire = true;
        //}
        RaycastHit hit;
        Vector3 angle = (currentlyAttackingObject.transformV.position - transformV.position);
        Vector3 angleNormal = angle.normalized;
        Vector3 forwardVertical = transformV.forward;
        forwardVertical.y += angleNormal.y; //Necessary to get the raycast to follow the firward facing vector

        Debug.DrawLine(transformV.position, transformV.position + forwardVertical.normalized * attackRange, Color.red);
        if (Physics.Raycast(transformV.position, forwardVertical.normalized, out hit, attackRange *1.1f, layerMasks)) //1.1x attackrange becuase hitboxes may not perfectly line up with render bounds
        {
            if (hit.transform.gameObject.GetInstanceID() == currentlyAttackingObject.InstanceID)
            {
                firingLocation = hit.point;
                canFire = true;
                inLOS = true;
            }
            else
            {
                canFire = false;
                inLOS = false;
            }
        }
        else
        {
            canFire = false;
            inLOS = false;
        }
        //RaycastHit2D hit = Physics2D.Linecast(transformV.position, transformV.position + transformV.forward * attackRange, 1 << 13); //1<<13 = layer 13 = player1
        //if (hit.transform != null)
        //{
        //    if (hit.transform.gameObject.GetInstanceID() == currentlyAttackingObject.InstanceID)
        //    {
        //        firingLocation = hit.point;
        //        canFire = true;
        //    }
        //}
        //else
        //{
        //    canFire = false;
        //}
    }


    //Method that initiates the new attack
    protected override void AttackStart(Transform targetTransform)
    {
        GameObject projectile = (GameObject)Instantiate(attackType, transformV.position, transformV.rotation);
        BlasterAttack projectileAttack = projectile.GetComponent<BlasterAttack>();
        projectileAttack.OnCreation(attackDamage, player, firingLocation);
    }

}

