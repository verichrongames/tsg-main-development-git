﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using CoreSys.Attacking;
using CoreSys.Misc;

/*==========================================
 *  Turrets recieve events from the RangeDetect script 
 *  whenever an enemy comes within range.
 * =======================================*/
[ExecuteInEditMode]
public class Turret : MonoBehaviour
{
    public int attackDamage;
    public GameObject attackType;
    public float timeToLive;
    public float fireRate;
    public float attackRange;


    //Rotating fields
    protected bool isRotating;
    public float defaultRotationAngle;
    public float rotationSpeed;

    //Firing Loop Fields
    private float newFireTime;
    private float firingDelay;
    private bool attackStarted;

    //Attacking related fields
    protected Vector3 firingLocation; //Location where the turret intercepts the target and will fire at this point.
    public bool areTargets = false; //If there are targets in view of parent
    public bool isAttacking = false; //If target is within range
    public bool canFire = false; //If a target is in LOS of turret
    protected DetectedObjectType currentlyAttackingObject;
    public KeyedCollection<int, DetectedObjectType> enemyAttackObjects; //Referance to collection in RangeDetection

    //Cached Components
    public Player player;
    public Transform transformV;
    public Weapons weaponsParent;
    public RangeDetection rangeDetection;

    //Debug Fields
    public GameObject currentlyAttackingGO;
    public Vector3 currentlyAttackingLocation;
    
    protected virtual void Start()
    {
        SetPlayer();
        SetTransformV();

        SetWeaponsObjectReferance();
        SetRangeDetectionReferance();

        SetEnemyObjectsReferance();
        GetEnemyArrayReferance();
        EventSubscription(true);
        firingDelay = 1 / fireRate;
    }

    protected virtual void Update()
    {
        if(areTargets)
        {
            CheckRangeSwitch();
        }

        if(isAttacking)
        {
            AttackTimer();
        }
    }

    /*============================================
     *     ===== Attacking Methods =====
     * =========================================*/

    //Recieves call when a new enemy enters the collider area (gets call from event)
    public void NewEnemyDetected()
    {
        if (!isAttacking) //Only checks for a new target when it is not actively attacking one
        {
            areTargets = true;
        }
    }

    //Recieves call when an enemy leaves the collider area
    public void EnemyGone(int ID)
    {
        if (ID == currentlyAttackingObject.DetectedGameObject.GetInstanceID())
        {
            isAttacking = false;
            currentlyAttackingObject = null;
            canFire = false;
        }

        if (enemyAttackObjects.Count == 0)
        {
            areTargets = false;
        }
    }

    //Checks for a valid attack target
    private void CheckAttackTarget(DetectedObjectType toAttack)
    {
        if (enemyAttackObjects.Count > 0 && toAttack != null && enemyAttackObjects.Contains(toAttack.InstanceID)) //Null check for enemy object, and that array holds object
        {
            currentlyAttackingObject = toAttack; 

            currentlyAttackingGO = currentlyAttackingObject.DetectedGameObject; //Testing
            currentlyAttackingLocation = currentlyAttackingGO.transform.position; //Testing

            if (!isAttacking)
            {
                isAttacking = true;
                AttackTimer();
            }
        }
        else
        {
            currentlyAttackingObject = null;
            isAttacking = false;
            canFire = false;
        }
    }

    //Timer To Run Attacking Loop
    private void AttackTimer()
    {
        if(canFire)
        {
            if (currentlyAttackingObject.DetectedGameObject != null) //Stops if the gameobject is null
            {
                Transform targetTransform = currentlyAttackingObject.DetectedGameObject.transform;
                if (targetTransform.position.x < 1000)
                {
                    if (!attackStarted)
                    {
                        attackStarted = true;
                        newFireTime = Time.time + UnityEngine.Random.Range(1 / (fireRate - (fireRate *0.1f)), 1 / (fireRate + (fireRate * 0.9f)));
                        AttackStart(targetTransform);
                    }

                    if (attackStarted)
                    {
                        if (Time.time > newFireTime)
                        {
                            attackStarted = false;
                        }
                    }
                }
                else
                {
                    isAttacking = false;
                    canFire = false;
                }
            }
            else
            {
                isAttacking = false;
                canFire = false;
            }
        }
    }

    //Method that initiates the new attack
    protected virtual void AttackStart(Transform targetTransform) {}

     /*============================================
     *     ===== Range Checking Methods =====
     * =========================================*/

    //Will check the range of all possible targets to the turret, will only check the range of the current target if isAttacking is true
    private void CheckRangeSwitch()
    {
        if(!isAttacking)
        {
            //CheckAllTargetsRange();
            GetDotProduct();
        }
        else
        {
            //CheckTargetRange();
        }
    }

    //Checks the range between the turret and it's currentlyAttacking target
    private void CheckTargetRange()
    {
        float distance = Vector3.Distance(currentlyAttackingObject.transformV.position, transformV.position);
        if(distance > attackRange)
        {
            TargetOutOfAttackRange();
        }
    }

    //Checks the range between the turret and all targets, will change the currentlyAttacking target based on priority and distance
    private void CheckAllTargetsRange()
    {
        bool inRange = false;
        foreach(DetectedObjectType target in enemyAttackObjects)
        {
            float distance = Vector3.Distance(target.transformV.position, transformV.position);
            if(distance <= attackRange)
            {
                TargetInRange(target);
                inRange = true;
                break;
            }
        }
        if (!inRange) //If no targte was found in range assign the highest priority target to track
        {
            currentlyAttackingObject = enemyAttackObjects.ElementAt(0);
        }
    }


    //Called when a potential target comes in range (might not be needed?)
    private void TargetInRange(DetectedObjectType newTarget)
    {
        CheckAttackTarget(newTarget);
    }

    //Called when the currently attacking target the turret is attacking leaves it's attack range
    private void TargetOutOfAttackRange()
    {
        isAttacking = false;
        canFire = false;
        CheckRangeSwitch();
    }

    //Will retrieve the closest side
    private void GetClosestSide()
    {

    }

    private void GetDotProduct()
    {
        //Angles are reversed right below becuase the vectors from the turret are typically facing the opposite direction as the vectors from the target
        currentlyAttackingObject = enemyAttackObjects.ElementAt(0);
        Vector3 enemyForward = -currentlyAttackingObject.transformV.forward.normalized;
        Vector3 enemySide = -currentlyAttackingObject.transformV.right.normalized;
        Vector3 towardsEnemy = currentlyAttackingObject.transformV.position - transformV.position;
        towardsEnemy = towardsEnemy.normalized;

        float dotProductForward = Vector3.Dot(towardsEnemy, enemyForward);
        float dotProductRight = Vector3.Dot(enemySide, towardsEnemy);

        currentlyAttackingObject.DetectedPlayerObject.UpdateBoundingPoints();

        //Angles That Define the "front" if an angle is greater than this it is not in the front
        float frontRightAllowance = Vector3.Angle(currentlyAttackingObject.transformV.position - currentlyAttackingObject.DetectedPlayerObject.BoundingPoints.UpperRightPoint, -currentlyAttackingObject.transformV.forward);
        float frontLeftAllowance = Vector3.Angle(currentlyAttackingObject.transformV.position - currentlyAttackingObject.DetectedPlayerObject.BoundingPoints.UpperLeftPoint, -currentlyAttackingObject.transformV.forward);

        float rightFrontAllowance = Vector3.Angle(currentlyAttackingObject.transformV.position - currentlyAttackingObject.DetectedPlayerObject.BoundingPoints.UpperRightPoint, -currentlyAttackingObject.transformV.right);
        float rightBackAllowance = Vector3.Angle(currentlyAttackingObject.transformV.position - currentlyAttackingObject.DetectedPlayerObject.BoundingPoints.LowerRightPoint, -currentlyAttackingObject.transformV.right);

        float backRightAllowance = Vector3.Angle(currentlyAttackingObject.transformV.position - currentlyAttackingObject.DetectedPlayerObject.BoundingPoints.LowerRightPoint, -currentlyAttackingObject.transformV.forward);
        float backLeftallowance = Vector3.Angle(currentlyAttackingObject.transformV.position - currentlyAttackingObject.DetectedPlayerObject.BoundingPoints.LowerLeftPoint, -currentlyAttackingObject.transformV.forward);

        float leftBackAllowance = Vector3.Angle(currentlyAttackingObject.transformV.position - currentlyAttackingObject.DetectedPlayerObject.BoundingPoints.LowerRightPoint, -currentlyAttackingObject.transformV.right);
        float leftFrontAllowance = Vector3.Angle(currentlyAttackingObject.transformV.position - currentlyAttackingObject.DetectedPlayerObject.BoundingPoints.UpperRightPoint, -currentlyAttackingObject.transformV.right);

        float frontRightDot = Vector3.Dot(currentlyAttackingObject.transformV.position - currentlyAttackingObject.DetectedPlayerObject.BoundingPoints.UpperRightPoint, -currentlyAttackingObject.transformV.forward);

        Debug.DrawLine(currentlyAttackingObject.transformV.position, currentlyAttackingObject.transformV.position + currentlyAttackingObject.transformV.forward * 2, Color.green);
        Debug.DrawLine(currentlyAttackingObject.transformV.position, currentlyAttackingObject.transformV.position + currentlyAttackingObject.transformV.forward * 2, Color.green);
        Debug.DrawLine(currentlyAttackingObject.transformV.position, transformV.position, Color.red);
        if(true)
        {
             //Debug.Log(Vector3.Angle(towardsEnemy.normalized, enemyForward.normalized));
             //Debug.Log("DotForward" + dotProductForward);
             //Debug.Log("DotRight" + dotProductRight);

             //Debug.Log("Angle to right: " + Vector3.Angle(towardsEnemy, enemySide));
             //Debug.Log("angle to forward: " + Vector3.Angle(towardsEnemy, enemyForward));
             //Debug.Log("Cosine of forward" + Mathf.Cos(Vector3.Angle(towardsEnemy, enemyForward)));
            //Debug.Log("Dot Product Forward: " + dotProductForward);
            //Debug.Log("FrontRightDot Allowance: " + frontRightDot);
        }


        //Debug.Log("Angle Forward vs Right corner: " + Mathf.Round(Vector3.Angle(currentlyAttackingObject.transformV.position - currentlyAttackingObject.DetectedPlayerObject.BoundingPoints.UpperRightPoint, -currentlyAttackingObject.transformV.forward) * 10) / 10);
        //Debug.Log("Angle Forward vs Left corner: " + Mathf.Round(Vector3.Angle(currentlyAttackingObject.transformV.position - currentlyAttackingObject.DetectedPlayerObject.BoundingPoints.UpperLeftPoint, -currentlyAttackingObject.transformV.forward) * 10) / 10);
        Debug.DrawLine(currentlyAttackingObject.transformV.position, currentlyAttackingObject.DetectedPlayerObject.BoundingPoints.UpperRightPoint, Color.yellow);
        Debug.DrawLine(currentlyAttackingObject.transformV.position, currentlyAttackingObject.DetectedPlayerObject.BoundingPoints.UpperLeftPoint, Color.yellow);
        Debug.DrawLine(currentlyAttackingObject.transformV.position, currentlyAttackingObject.DetectedPlayerObject.BoundingPoints.LowerRightPoint, Color.yellow);
        Debug.DrawLine(currentlyAttackingObject.transformV.position, currentlyAttackingObject.DetectedPlayerObject.BoundingPoints.LowerLeftPoint, Color.yellow);

        RangeChecking.DetermineSide(currentlyAttackingObject.transformV, transformV, currentlyAttackingObject.DetectedPlayerObject);

        //if (dotProductForward > 0 && dotProductRight > 0) //If Upper Right
        //{
        //    if (Vector3.Distance(currentlyAttackingObject.DetectedPlayerObject.BoundingPoints.UpperRightPoint, transformV.position) <= attackRange)
        //    {
        //        if (!isAttacking)
        //        {
        //            TargetInRange(currentlyAttackingObject);
        //        }
        //    }
        //    else
        //    {
        //        if (Vector3.Angle(towardsEnemy, enemyForward) <= frontRightAllowance)
        //        {
        //            Debug.Log("Front Facing -- Right Orientation");
        //        }
        //        else if(Vector3.Angle(towardsEnemy, enemySide) <= rightFrontAllowance)
        //        {
        //            Debug.Log("Right Facing -- Front Orientation");

        //        }
        //    }
        //}
        //else if (dotProductForward > 0 && dotProductRight < 0) //If Upper Left
        //{
        //    if (Vector3.Distance(currentlyAttackingObject.DetectedPlayerObject.BoundingPoints.UpperLeftPoint, transformV.position) <= attackRange)
        //    {
        //        if (!isAttacking)
        //        {
        //            TargetInRange(currentlyAttackingObject);
        //        }
        //    }
        //    else
        //    {
        //        if (Vector3.Angle(towardsEnemy, enemyForward) <= frontLeftAllowance)
        //        {
        //            Debug.Log("Front Facing -- Left Orientation");
        //        }
        //        else if (Vector3.Angle(towardsEnemy, enemySide) >= leftFrontAllowance)
        //        {
        //            Debug.Log("Left Facing -- Front Orientation");
        //        }
        //    }
        //}
        //else if (dotProductForward < 0 && dotProductRight < 0) //If lower left
        //{
        //    if (Vector3.Distance(currentlyAttackingObject.DetectedPlayerObject.BoundingPoints.LowerLeftPoint, transformV.position) <= attackRange)
        //    {
        //        if (!isAttacking)
        //        {
        //            TargetInRange(currentlyAttackingObject);
        //        }
        //    }
        //    else
        //    {
        //        if(Vector3.Angle(towardsEnemy, enemyForward) >= backLeftallowance)
        //        {
        //            Debug.Log("Back Facing -- Left Orientation");
        //        }
        //        else if(Vector3.Angle(towardsEnemy, enemySide) >= leftBackAllowance)
        //        {
        //            Debug.Log("Left Facing -- Back Orientation");
        //        }
        //    }
        //}
        //else if (dotProductForward < 0 && dotProductRight > 0) //If lower right
        //{
        //    if (Vector3.Distance(currentlyAttackingObject.DetectedPlayerObject.BoundingPoints.LowerRightPoint, transformV.position) <= attackRange)
        //    {
        //        if (!isAttacking)
        //        {
        //            TargetInRange(currentlyAttackingObject);
        //        }
        //    }
        //    else
        //    {
        //        if(Vector3.Angle(towardsEnemy, enemySide) <= rightBackAllowance)
        //        {
        //            Debug.Log("Right Facing -- Back Orientation");
        //        }
        //        else if(Vector3.Angle(towardsEnemy, enemyForward) >= backRightAllowance)
        //        {
        //            Debug.Log("Back Facing -- Right Orientation");
        //        }
        //    }
        //}
    }
    /*=======================================
     *  ==== Referance Settng Methods ====
     * =====================================*/

    private void SetEnemyObjectsReferance()
    {
        weaponsParent.viewRange.GetComponent<RangeDetection>();
    }

    private void SetPlayer()
    {
        player = transform.root.GetComponent<Player>();
    }

    //Cache of the transform component
    private void SetTransformV()
    {
        transformV = transform;
    }

    private void SetWeaponsObjectReferance()
    {
        weaponsParent = transformV.parent.GetComponent<Weapons>();
        if(weaponsParent == null)
        {
            throw new MissingComponentException("The Weapons component was not added to the parent of this turret");
        }
    }

    private void SetRangeDetectionReferance()
    {
        rangeDetection = weaponsParent.viewRange.GetComponent<RangeDetection>();
    }

    //Must be called after the RangeDetection Referance has bee retirieved
    private void EventSubscription(bool command)
    {
        if(command)
        {
            rangeDetection.NewEnemyObjectEvent += NewEnemyDetected;
            rangeDetection.EnemyObjectLeftRangeEvent += EnemyGone;
        }
        else
        {
            rangeDetection.NewEnemyObjectEvent -= NewEnemyDetected;
        }
    }

    //Gets the Array of enemy objects referance from the range detect script
    private void GetEnemyArrayReferance()
    {
        enemyAttackObjects = rangeDetection.GetObjectCollection("Enemy");
    }

    public void SetWeaponAndRangeDetectReferances(Weapons weapons, RangeDetection rangeDetect)
    {
        rangeDetection = rangeDetect;
        weaponsParent = weapons;
    }

}


