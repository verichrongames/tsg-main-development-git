﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

/*======================================
 *  This will define general attacking behavior.
 *  Behavior that should be similar among all
 *  attack types. 
 * ===================================*/

public class Attack : MonoBehaviour
{
    protected Transform transformV;
    protected Material attackMaterial;
    public Player player;
    public float speed;
    public float health;
    protected float damage;
    public float timeToLive;

    public virtual void OnCreation() { }

    void TriggerOnExit() { }

    protected virtual void OnDisable()
    {
        TriggerOnExit();
    }

    protected virtual void OnEnable()
    {
        transformV = transform;
    }

    //Applies damage to a projectile
    public virtual void TakeDamage(float damage)
    {
        if(health > damage)
        {
            health -= damage;
        }
        else if(health <= damage)
        {
            //Debug.Log("Blaster Bullet Destroyed");
            StartCoroutine(DestroyAttack());
        }
    }

    private IEnumerator DestroyAttack()
    {
        transformV.Translate(Vector3.up * 10000);
        yield return new WaitForSeconds(0.1f);
        Destroy(this.gameObject);
        yield break;
    }
}

