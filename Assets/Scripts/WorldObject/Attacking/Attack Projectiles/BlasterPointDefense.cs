﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreSys.Math;

/*===================================
 *  This class is THE attack. This is the
 *  projectile that is fired.
 * =================================*/

class BlasterPointDefense : Attack
{
    float projectileSize;
    public float velocity;
    public Rigidbody ridigbody;

    //Debug Members
    public Vector3 destiantion;
    public GameObject targetg;

    protected override void OnEnable()
    {
        base.OnEnable();
    }

    void Update()
    {
        velocity = GetComponent<Rigidbody2D>().velocity.magnitude;
    }


    public void OnCreation(float attackerDamage, Player ownerPlayer, Transform targetTransform, GameObject target)
    {
        damage = attackerDamage;
        player = ownerPlayer;
        RotateTowardsTarget(targetTransform);
        ApplyForce(targetTransform, target);
        StartCoroutine(TimeToLiveCountdown());
    }

    //Applies force towards the target
    private void ApplyForce(Transform targetTransform, GameObject target)
    {
        Vector2 velocityV2 = target.GetComponent<Rigidbody2D>().velocity;
        Vector3 targetVelocity = new Vector3(velocityV2.x, velocityV2.y, 0);
        Vector3 interceptAngle = GameMath.FindInterceptVector(transformV.position, 10, targetTransform.position, targetVelocity);
        GetComponent<Rigidbody2D>().AddForce(interceptAngle * speed);
        destiantion = targetTransform.position;
        targetg = target;
    }

    //Rotates the projectile to look at the target
    private void RotateTowardsTarget(Transform targetTransform)
    {
        Vector3 angle = (targetTransform.position - transformV.position);
        float lookAngle = Mathf.Atan2(angle.y, angle.x) * Mathf.Rad2Deg - 90;
        transformV.rotation = Quaternion.AngleAxis(lookAngle, Vector3.forward);
    }

    //Sets the size of the projectile based on the damage it does
    private void SetProjectileSize()
    {
        projectileSize = damage / 10;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        CheckObjectType(collision.gameObject);
    }

    private void CheckObjectType(GameObject gameObject)
    {
        IPlayerObject playerObject = gameObject.GetComponent<IPlayerObject>();
        Attack attackObject = gameObject.GetComponent<Attack>();
        if (playerObject != null)
        {
            if (gameObject.transform.root.GetComponent<Player>().name != player.name)
            {
                playerObject.TakeDamage(damage);
                //Debug.Log("Destroyed By Hitting Unit");
                Destroy(this.gameObject);
            }
        }
        else if(attackObject != null)
        {
            if(attackObject.player != player)
            {
                attackObject.TakeDamage(damage);
                //Debug.Log("Destroyed By Hitting Projectile");
                StartCoroutine(DestroyAttack());
            }
        }
    }

    private IEnumerator TimeToLiveCountdown()
    {
        yield return new WaitForSeconds(timeToLive);
        //Debug.Log("Destroyed By Time");
        StartCoroutine(DestroyAttack());
    }

    private IEnumerator DestroyAttack()
    {
        transformV.Translate(Vector3.up * 10000);
        yield return new WaitForSeconds(0.1f);
        Destroy(this.gameObject);
        yield break;
    }
}

