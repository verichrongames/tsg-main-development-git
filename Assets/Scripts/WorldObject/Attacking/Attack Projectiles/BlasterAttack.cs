﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

/*===================================
 *  This class is THE attack. This is the
 *  projectile that is fired.
 * =================================*/

class BlasterAttack : Attack
{
    float projectileSize;
    public float velocity;
    public int trigger;

    public Vector3 destiantion;

    protected override void OnEnable()
    {
        base.OnEnable();
    }

    protected override void OnDisable()
    {

    }

    void Update()
    {
        velocity = GetComponent<Rigidbody>().velocity.magnitude; //Debug member
    }


    public void OnCreation(float attackDamage, Player ownerPlayer, Vector3 firingLocation)
    {
        damage = attackDamage;
        player = ownerPlayer;
        RotateTowardsTarget(firingLocation);
        ApplyForce(firingLocation);
    }

    //Applies force towards the target
    private void ApplyForce(Vector3 firingLocation)
    {
        Vector3 angle = (firingLocation - transformV.position).normalized;
        GetComponent<Rigidbody>().AddForce(angle * speed);
        destiantion = firingLocation;
    }

    //Rotates the projectile to look at the target
    private void RotateTowardsTarget(Vector3 firingLocation)
    {
        Vector3 angle = (firingLocation - transformV.position);
        float lookAngle = Mathf.Atan2(angle.x, angle.z) * Mathf.Rad2Deg;
        transformV.rotation = Quaternion.AngleAxis(lookAngle, Vector3.up);
    }

    //Sets the size of the projectile based on the damage it does
    private void SetProjectileSize()
    {
        projectileSize = damage / 10;
    }

    void OnTriggerEnter(Collider collision)
    {
        CheckPlayer(collision.gameObject);
    }

    private void CheckPlayer(GameObject gameObject)
    {
        IPlayerObject playerObject = gameObject.GetComponent<IPlayerObject>();
        if (playerObject != null)
        {
            if (gameObject.transform.root.GetComponent<Player>() != player)
            {
                playerObject.TakeDamage(damage);
                StartCoroutine(DestroyAttack());
            }
        }
    }

    private IEnumerator DestroyAttack()
    {
        transformV.Translate(Vector3.up * 10000);
        yield return new WaitForSeconds(0.1f);
        Destroy(this.gameObject);
        yield break;
    }
}

