﻿using System;
using UnityEngine;
using TagFrenzy;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using CoreSys.Objects;
using CoreSys;
using CoreSys.Exceptions;

/*==================================
 *  Class for unit and building range detection
 *  This will handle detecting and
 *  storing referances to any
 *  enemy, friendly, and enviornmental objects
 *  within the objects view range. 
 *  
 *  This script will be attached to a 
 *  child object of the unit or building
 *  that contains a sphere collider which
 *  will be it's view range.
 *  
 * The collections here are sorted by priority.
 * Every insert into the collections
 * is inserted based on priority.
 * ================================*/
namespace CoreSys.Attacking
{
    public class RangeDetection : MonoBehaviour
    {
        Player player;
        SphereCollider sphereCollider;
        IPlayerObject parentPlayerObject;
        public int maxTargets; //Max # of targets it can manage

        public delegate void NewObjectDetectedHandler();
        public delegate void ObjectLeftRangeHandler(int ID);
        public event NewObjectDetectedHandler NewEnemyObjectEvent;
        public event NewObjectDetectedHandler NewFriendlyObjectEvent;
        public event ObjectLeftRangeHandler EnemyObjectLeftRangeEvent;

        KeyedCollection<int, DetectedObjectType> enemyObjects = new DetectedObjectTypeCollection();
        KeyedCollection<int, DetectedObjectType> friendlyObjects = new DetectedObjectTypeCollection(); //Unused for now
        KeyedCollection<int, DetectedObjectType> enviornmentalObjects = new DetectedObjectTypeCollection(); //Unused for now

        //Testing
        public int arraySize;

        void Start()
        {
            GetReferances();
            SetColliderSize();
        }

        void Update()
        {
            arraySize = enemyObjects.Count();
        }


        void OnTriggerEnter(Collider collider)
        {
            NewDetectedObject(collider.gameObject);
        }

        void OnTriggerExit(Collider collider)
        {
            RemoveDetectedObject(collider.gameObject);
        }

        //Determines if the new object is an enemy of friendly
        private void NewDetectedObject(GameObject gameObject)
        {
            if (gameObject.HasTag("Unit") || gameObject.HasTag("Building"))
            {
                IPlayerObject playerObject = gameObject.GetComponent<IPlayerObject>();
                if (playerObject != null)
                {
                    if (playerObject.player != player)
                    {
                        NewEnemyObject(gameObject, playerObject, player);
                    }
                }
            }
        }

        //Finds object in collections and removes it
        private void RemoveDetectedObject(GameObject gameObject)
        {
            int id = gameObject.GetInstanceID();
            if (gameObject.HasTag("Unit") || gameObject.HasTag("Building"))
            {
                if (enemyObjects.Contains(id))
                {
                    enemyObjects.Remove(id);
                    EnemyObjectLeftRangeEvent(id);
                }
                else if (friendlyObjects.Contains(id))
                {
                    friendlyObjects.Remove(id);
                }
                else if (enviornmentalObjects.Contains(id))
                {
                    enviornmentalObjects.Remove(id);
                }
            }
        }

        //Adds a new enemy object to the collection and fires the appropriate event
        private void NewEnemyObject(GameObject gameObject, IPlayerObject playerObject, Player player)
        {
            int priority = ResourceManager.GetBaseTargetPriority(playerObject.objectType);
            if (priority == 0)
            {
                throw new MissingFieldException("The PlayerObject" + gameObject.name + "Does not have an object type set in the inspector");
            }


            if (enemyObjects.Count() == 0) //Check for empty collection
            {
                enemyObjects.Add(new DetectedObjectType(gameObject, playerObject, player, DetectedObjectTypes.EnemyObject, gameObject.GetInstanceID(), priority));

                if (NewEnemyObjectEvent != null)
                {
                    NewEnemyObjectEvent();
                }
            }
            else if (enemyObjects.Count() <= maxTargets || enemyObjects.ElementAt(enemyObjects.Count() - 1).Priority <= priority)
            {
                DetectedObjectType newObject = new DetectedObjectType(gameObject, playerObject, player, DetectedObjectTypes.EnemyObject, gameObject.GetInstanceID(), priority);
                int index = BinarySearchForInsert(enemyObjects, priority);
                InsertIntocollection(enemyObjects, newObject, index);

                if (NewEnemyObjectEvent != null)
                {
                    NewEnemyObjectEvent();
                }
            }
        }

        //Adds a new friendly object to the collection and fires the appropriate event
        private void NewFriendlyObject(GameObject gameObject, IPlayerObject playerObject, Player player)
        {
            int priority = ResourceManager.GetBaseTargetPriority(playerObject.objectType);
            if (priority == 0)
            {
                throw new MissingFieldException("The PlayerObject" + gameObject.name + "Does not have an object type set in the inspector");
            }

            if(friendlyObjects.Count() == 0) //Check for empty collection
            {
                friendlyObjects.Add(new DetectedObjectType(gameObject, playerObject, player, DetectedObjectTypes.FriendlyObject, gameObject.GetInstanceID(), priority));
            }
            else if(friendlyObjects.Count() <= maxTargets || friendlyObjects.ElementAt(friendlyObjects.Count() - 1).Priority <= priority)
            {
                DetectedObjectType newObject = new DetectedObjectType(gameObject, playerObject, player, DetectedObjectTypes.FriendlyObject, gameObject.GetInstanceID(), priority);
                int index = BinarySearchForInsert(friendlyObjects, priority);
                InsertIntocollection(friendlyObjects, newObject, index);

                if (NewFriendlyObjectEvent != null)
                {
                    NewFriendlyObjectEvent();
                }
            }
        }

        //Unused
        private void NewEnviornmentalObject()
        {

        }

        //Uses a binary search to return the index of where the next insert needs to go
        private int BinarySearchForInsert(KeyedCollection<int, DetectedObjectType> collection, int value)
        {
            int low = 0, high = collection.Count - 1, midpoint = 0;
            while (low <= high)
            {
                midpoint = low + (high - low)/2;

                if (value == collection.ElementAt(midpoint).Priority)
                {
                    return midpoint;
                }
                else if(value > collection.ElementAt(midpoint).Priority)
                {
                    high = midpoint - 1;
                }
                else
                {
                    low = midpoint + 1;
                }
            }
            
            midpoint = low + (high - low) / 2;
            return midpoint;
        }

        //Performs the insertion
        private void InsertIntocollection(KeyedCollection<int, DetectedObjectType> collection, DetectedObjectType objectToInsert, int index)
        {
            if(collection.Count() + 1 > maxTargets) //Removes last member of collection if too alrge
            {
                collection.RemoveAt(collection.Count() - 1);
            }

            collection.Insert(index, objectToInsert);

        }

        /// <summary>
        /// Returns the requested collection referance
        /// </summary>
        /// <param name="collectionName">Acceptable Strings: Enemy, Friendly, Enviornmental, Projectile</param>
        /// <returns></returns>
        public KeyedCollection<int, DetectedObjectType> GetObjectCollection(string collectionName)
        {
            switch (collectionName)
            {
                case "Enemy":
                    return enemyObjects;
                case "Friendly":
                    return friendlyObjects;
                default:
                    return null;
            }

        }

        private void SetColliderSize()
        {
            sphereCollider.radius = parentPlayerObject.ViewRange;
        }

        //Gets initial starting referances
        private void GetReferances()
        {
            player = transform.root.GetComponent<Player>();
            parentPlayerObject = GetComponentInParent<IPlayerObject>();
            sphereCollider = GetComponent<SphereCollider>();
        }

    }
}

