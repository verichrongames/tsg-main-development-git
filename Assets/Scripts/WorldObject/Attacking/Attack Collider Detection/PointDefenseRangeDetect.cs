﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using CoreSys.Objects;

/*=====================================================
 *  This class is used to detect any objects that fall into
 *  an objects default weapon attack range. This gets attached
 *  to the AttackBoundary Object.
 *  
 * This class will only be in effect if the object is
 * of the ICanAttack type.
 * ===================================================*/

public class PointDefenseRangeDetect : MonoBehaviour
{

    Player player;
    PointDefenseTurret pointDefenseParent;
    SphereCollider sphereCollider;

    KeyedCollection<int, DetectedObjectType> enemyObjects = new DetectedObjectTypeCollection();
    KeyedCollection<int, DetectedObjectType> projectileObjects = new DetectedObjectTypeCollection();

    void Start()
    {
        player = transform.root.GetComponent<Player>();
        pointDefenseParent = GetComponentInParent<PointDefenseTurret>();
    }

    void Update()
    {

    }

    void OnTriggerEnter(Collider collider)
    {
        //Debug.Log(collider.gameObject.name + "Entered PointDefenseRange");
        NewDetectedObject(collider.gameObject);
    }

    void OnTriggerExit(Collider collider)
    {
        //Debug.Log(collider.gameObject.name + "Left PointDefenseRange");
        RemoveDetectedObject(collider.gameObject);
    }


    //Adds the new detected object to the list
    private void NewDetectedObject(GameObject gameObject)
    {
        int ID = gameObject.GetInstanceID();
        IPlayerObject playerObject = gameObject.GetComponent<IPlayerObject>();
        Attack attackObject = gameObject.GetComponent<Attack>();
        if (playerObject != null && !enemyObjects.Contains(ID))
        {
            Player objectOwner = playerObject.player;
            if (objectOwner.name != player.name)
            {
                AddEnemyDetectedObject(gameObject, playerObject, objectOwner, ID);
            }
        }
        else if (attackObject != null && !projectileObjects.Contains(ID))
        {
            Player objectOwner = attackObject.player;
            if(objectOwner.name != player.name)
            {
                AddEnemyDetectedProjectile(gameObject, objectOwner, attackObject, ID);
            }
        }
    }

    //Adds an enemy object to the list
    private void AddEnemyDetectedObject(GameObject gameObject, IPlayerObject playerObject, Player objectOwner, int ID)
    {
        enemyObjects.Add(new DetectedObjectType(gameObject, playerObject, objectOwner, DetectedObjectTypes.EnemyObject, ID, 1));
        pointDefenseParent.NewEnemyDetected();
    }

    //Adds an enemy projectile to the list
    private void AddEnemyDetectedProjectile(GameObject gameObject, Player objectOwner, Attack attackObject, int ID)
    {
        projectileObjects.Add(new DetectedObjectType(gameObject, objectOwner, attackObject, DetectedObjectTypes.ProjectileObjects, ID, 1));
        pointDefenseParent.NewEnemyDetected();
    }

    private void RemoveEnemyObject(int ID)
    {
        enemyObjects.Remove(ID);
        pointDefenseParent.EnemyGone(ID);
    }

    private void RemoveProjectileObject(int ID)
    {
        projectileObjects.Remove(ID);
        pointDefenseParent.EnemyGone(ID);
    }

    private void RemoveDetectedObject(GameObject gameObject)
    {
        int ID = gameObject.GetInstanceID();
        if (enemyObjects.Contains(ID))
        {
            RemoveEnemyObject(ID);
        }
        else if (projectileObjects.Contains(ID))
        {
            RemoveProjectileObject(ID);
        }
    }

}
