﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using CoreSys.Objects;

/*=====================================================
 *  This class is used to detect any objects that fall into
 *  an objects default weapon attack range. This gets attached
 *  to the AttackBoundary Object.
 *  
 * This class will only be in effect if the object is
 * of the ICanAttack type.
 * ===================================================*/

public class AttackRangeDetect : MonoBehaviour {

    Player player;
    ProjectileAttackTurret projectileTurretParent;
    SphereCollider sphereCollider;

    KeyedCollection<int, DetectedObjectType> enemyObjects = new DetectedObjectTypeCollection();
    Dictionary<int, DetectedObjectType> friendlyObjects = new Dictionary<int, DetectedObjectType>();
    Dictionary<int, DetectedObjectType> nonPlayerObjects = new Dictionary<int, DetectedObjectType>();

	void Start () 
    {
        player = transform.root.GetComponent<Player>();
        projectileTurretParent = GetComponentInParent<ProjectileAttackTurret>();
        sphereCollider = GetComponent<SphereCollider>();
        SetColliderSize();
	}
	
	void Update () 
    {

	}

    void OnTriggerEnter(Collider collider)
    {
        Debug.Log(collider.gameObject.name + "Entered AttackRange");
        NewDetectedObject(collider.gameObject);
    }

    void OnTriggerExit(Collider collider)
    {
        RemoveDetectedObject(collider.gameObject);
    }


    //Adds the new detected object to the list
    private void NewDetectedObject(GameObject gameObject)
    {
        int ID = gameObject.GetInstanceID();
        IPlayerObject playerObject = gameObject.GetComponent<IPlayerObject>();
        if(playerObject != null)
        {
            Player objectOwner = playerObject.player;
            if(objectOwner == player)
            {
                AddFriendlyDetectedObject(gameObject, playerObject, objectOwner, ID);
            }
            else
            {
                AddEnemyDetectedObject(gameObject, playerObject, objectOwner, ID);
            }
        }
    }

    //Adds a friendly object to the list
    private void AddFriendlyDetectedObject(GameObject gameObject, IPlayerObject playerObject, Player objectOwner, int ID)
    {
        friendlyObjects.Add(ID, new DetectedObjectType(gameObject, playerObject, objectOwner, DetectedObjectTypes.FriendlyObject, ID, 1));
    }

    //Adds an enemy object to the list
    private void AddEnemyDetectedObject(GameObject gameObject, IPlayerObject playerObject, Player objectOwner, int ID )
    {
        enemyObjects.Add(new DetectedObjectType(gameObject, playerObject, objectOwner, DetectedObjectTypes.EnemyObject, ID, 1));
        projectileTurretParent.NewEnemyDetected();
    }

    private void RemoveEnemyObject(int ID)
    {
        enemyObjects.Remove(ID);
        projectileTurretParent.EnemyGone(ID);
    }

    private void RemoveFriendlyObject(int ID)
    {
        friendlyObjects.Remove(ID);
    }

    private void RemoveDetectedObject(GameObject gameObject)
    {
        int ID = gameObject.GetInstanceID();
        if(enemyObjects.Contains(ID))
        {
            RemoveEnemyObject(ID);
        }
        else if(friendlyObjects.ContainsKey(ID))
        {
            RemoveFriendlyObject(ID);
        }
    }

    //Sets the collider size to the attack range determined by the projectile turret object
    private void SetColliderSize()
    {
        sphereCollider.radius = projectileTurretParent.attackRange;
    }
}
