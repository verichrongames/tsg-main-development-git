﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CoreSys;
using CoreSys.Objects;

namespace CoreSys.Buildings
{
    public class Core : Building
    {

        public CoreResourceManager CRM;

        public bool built = false; //This is for testing.  Its meant to handle when a core is placed in scene without being built. //FIXTHIS
        public List<Node> nodeList;
        public List<Block> blockList;
        private float currentUnitMetalCost, currentUnitPowerCost = 0;
        public int nodeCount;
        public int nodeTier;
        public float actionZone;//Defines the ring of placement for new buildings to be on grid
        public ActionZone actionSprite;

        //System Functions//----------------------------------------------------------------------------------------------//
        protected override void Awake()
        {
            base.Awake();
            player = transform.root.GetComponentInChildren<Player>();
            actionSprite = GetComponentInChildren<ActionZone>();
        }

        protected override void Start()
        {
            base.Start();
            actionSprite.SetSize(actionZone);
            actionSprite.Display(false);
            CRM = GetComponent<CoreResourceManager>();
            parentGrid = transform.parent.GetComponent<Grid>();
            SetRallyPoint();
            InBuildMode = false;
        }

        protected override void Update()
        {
            base.Update();
            if (!built) //Totally temporary for testing //FIXTHIS//
            {
                CompletedResources();
                SendResources();
                SendStorage();
                NodeDrop();
                built = true;
            }
        }

        //End System Functions//----------------------------------------------------------------------------------------------//

        //User Interface Overrides//

        public virtual void SetSelection(bool selected, Rect playingArea)
        {
            base.SetIsSelected(selected);
            if (player)
            {
                GameObject flagSpawn = (GameObject)Instantiate(GetGameObjects.GetSystem("rallyPoint"), this.transform.position, new Quaternion());
                RallyPoint flag = flagSpawn.GetComponent<RallyPoint>();
                if (selected)
                {
                    if (flag && player.human && spawnPoint != ResourceManager.InvalidPosition && rallyPoint != ResourceManager.InvalidPosition) //This needs to be cleaned up //FIXTHIS
                    {
                        flag.transform.localPosition = rallyPoint;
                        flag.Enable();
                    }
                }
                else
                {
                    if (flag && player.human) flag.Disable();
                }
            }
        }

        //Resource Handling//--------------------------------------------------------------------------------------//

        private void SendResources()
        {
            CRM.ResourceUpdate();
        }

        private void SendStorage()
        {
            CRM.StorageUpdate();
        }

        //Nodes Handling//-----------------------------------------------------------------------------------------//

        public void nodesAdded(List<Node> newNodes)
        {
            if (InBuildMode)
            {
                parentGrid.blockHandler.HandleNodesAdded(newNodes, this);
            }
        }

        public void ResetNodeList(List<Node> nodes)
        {
            nodeList = nodes;
        }

        protected virtual void NodeDrop()
        {
            Vector3 node1, node2, node3, node4, node5, node6;
            List<Vector3> nodeCheckList = new List<Vector3>();
            Vector3 corePosition = this.transform.position;
            float sqrt = (Mathf.Sqrt(3) / 2);
            nodeCheckList.Add(node1 = new Vector3(corePosition.x, corePosition.y, corePosition.z + (sqrt * 8.0f)));
            nodeCheckList.Add(node2 = new Vector3(corePosition.x, corePosition.y, corePosition.z - (sqrt * 8.0f)));
            nodeCheckList.Add(node3 = new Vector3(corePosition.x - 3.0f, corePosition.y, corePosition.z + (sqrt * 6.0f)));
            nodeCheckList.Add(node4 = new Vector3(corePosition.x - 3.0f, corePosition.y, corePosition.z - (sqrt * 6.0f)));
            nodeCheckList.Add(node5 = new Vector3(corePosition.x + 3.0f, corePosition.y, corePosition.z + (sqrt * 6.0f)));
            nodeCheckList.Add(node6 = new Vector3(corePosition.x + 3.0f, corePosition.y, corePosition.z - (sqrt * 6.0f)));
            nodeCheckList.Add(node1 = new Vector3(corePosition.x + 6.0f, corePosition.y, corePosition.z));
            nodeCheckList.Add(node2 = new Vector3(corePosition.x - 6.0f, corePosition.y, corePosition.z));
            nodeCheckList.Add(node3 = new Vector3(corePosition.x - 6.0f, corePosition.y, corePosition.z + (sqrt * 4.0f)));
            nodeCheckList.Add(node4 = new Vector3(corePosition.x - 6.0f, corePosition.y, corePosition.z - (sqrt * 4.0f)));
            nodeCheckList.Add(node5 = new Vector3(corePosition.x + 6.0f, corePosition.y, corePosition.z + (sqrt * 4.0f)));
            nodeCheckList.Add(node6 = new Vector3(corePosition.x + 6.0f, corePosition.y, corePosition.z - (sqrt * 4.0f)));
            foreach (Vector3 nodeCheck in nodeCheckList)
            {
                GameObject checkPoint = WorkManager.FindHitWorld(nodeCheck);
                if (checkPoint && checkPoint.name == "Ground")
                {
                    Vector3 nodeBuiltPoint = new Vector3(nodeCheck.x, this.transform.position.y - 1, nodeCheck.z);
                    GameObject newNode = (GameObject)Instantiate(GetGameObjects.GetSystem("Node"), nodeBuiltPoint, new Quaternion());
                    Node node = newNode.GetComponent<Node>();
                    node.transform.SetParent(this.transform);
                    this.nodeList.Add(node);
                }
            }
        }

        //Building Creation Modules //---------------------------------------------------------------------------------------------------//

        public void CreateBlock(string blockName)
        {
            InBuildMode = true;
            parentGrid.blockHandler.BlockBuildMode(blockName, this, nodeList);
        }

        //End Building Creation//-----------------------------------------------------------------------------------------------------//

        //Upgrading//

        protected override void SelfBuild()
        {
            float buildSpeed = parentGrid.GRM.GetBuildPercentage();
            float buildAmount = ((CRM.GetFabrication() * buildSpeed * (100 / buildTime)) * Time.deltaTime);
            float MaxRatio = (buildAmount / MaxBuildProgress);
            float HealthFromMax = (healthToAdd * MaxRatio);
            BuildProgress += buildAmount;
            HitPoints += HealthFromMax;
        }

        public override void StartUpgrade()
        {
            base.StartUpgrade();
            CRM.ResourceUpdate();
        }

        protected override void FinishBuild() //Finishes the core once the building is completed //Need to add transparency handling once cores can be dropped.
        {
            base.FinishBuild();
            NodeDrop();
            CRM.ResourceUpdate();
        }

        protected override void FinishUpgrade() //Finishes the upgrade for the core.
        {
            base.FinishUpgrade();
            CRM.CompleteUpdate();
        }

        protected override void upgradeFinishedTraits(int level)
        {
            base.upgradeFinishedTraits(level);
            nodeCount = upgradeStats.GetMaxNodes(level);
            nodeTier++;
        }

        //Unit Creation Methods//-----------------------------------------------------------------------------------------------------//
        public void MakingUnit(float metal, float power)
        {
            currentMetalUsage += metal;
            currentPowerUsage += power;
            currentUnitMetalCost = metal;
            currentUnitPowerCost = power;
            SendResources();
        }

        public void FinishedUnit()
        {
            currentMetalUsage += -currentUnitMetalCost;
            currentPowerUsage += -currentUnitPowerCost;
            currentUnitPowerCost = 0;
            currentUnitMetalCost = 0;
            SendResources();
        }
    }
}