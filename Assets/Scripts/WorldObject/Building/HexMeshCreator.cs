﻿/*
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class HexMeshCreator : MonoBehaviour 
{
    //Initialization
    public void Start()
    {
        MeshFilter meshFilter = this.GetComponent<MeshFilter>();
        MeshCollider meshCollider = this.GetComponent<MeshCollider>();
        Mesh mesh = new Mesh();
        meshFilter.mesh = Generate();
        meshCollider.sharedMesh = meshFilter.mesh;
    }

    public Mesh Generate()
    {
        Mesh mesh = new Mesh();

        // top chunk
        Vector3 v0 = new Vector3(0,-1,0); //Top center

        Vector3 v1 = new Vector3(-2, -1, 0);
        Vector3 v2 = new Vector3(-1, -1, (Mathf.Sqrt(3) / 2) * 2);
        Vector3 v3 = new Vector3(1, -1, (Mathf.Sqrt(3) / 2) * 2);
        Vector3 v4 = new Vector3(2, -1, 0);
        Vector3 v5 = new Vector3(1, -1, (Mathf.Sqrt(3) / 2) * -2);
        Vector3 v6 = new Vector3(-1, -1, (Mathf.Sqrt(3) / 2) * -2);
        // bottom chunk
        Vector3 v7 = new Vector3(-2,1,0);
        Vector3 v8 = new Vector3(-1, 1, (Mathf.Sqrt(3) / 2) * 2);
        Vector3 v9 = new Vector3(1, 1, (Mathf.Sqrt(3) / 2) * 2);
        Vector3 v10 = new Vector3(2, 1, 0);
        Vector3 v11 = new Vector3(1, 1, (Mathf.Sqrt(3) / 2) * -2);
        Vector3 v12 = new Vector3(-1, 1, (Mathf.Sqrt(3) / 2) * -2);

        Vector3 v13 = new Vector3(0, 1, 0); //Bottom center

        mesh.vertices = new Vector3[] 
        {
            v0,
            v1, v2,
            v3, v4,
            v5, v6,
            v7, v8,
            v9, v10,
            v11, v12,
            v13
        };

        mesh.triangles = new int[] 
        {
            //top
            1,0,2,
            2,0,3,
            3,0,4,
            4,0,5,
            5,0,6,
            1,6,0,
            //bottom
            8,13,7,
            9,13,8,
            10,13,9,
            11,13,10,
            12,13,11,
            7,13,12,
            //sides
            7,1,2,
            7,2,8,
            8,2,3,
            8,3,9,
            3,4,9,
            9,4,10,
            4,5,10,
            5,11,10,
            6,11,5,
            6,12,11,
            1,12,6,
            1,7,12,
        };

        mesh.RecalculateNormals();

        return mesh;
    }

}
*/