﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CoreSys.Buildings
{
    [System.Serializable]
    public class ListOfBuildableObjects
    {
        public List<string> listOfBuildableObjects;
    }
}