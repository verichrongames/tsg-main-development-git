﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CoreSys.Buildings
{
    public class UpgradeStats : MonoBehaviour
    {
        [SerializeField]
        private int maxLevel;
        [SerializeField]
        private List<float> metalProductionPerLevel;
        [SerializeField]
        private List<float> powerProductionPerLevel;
        [SerializeField]
        private List<float> metalStoragePerLevel;
        [SerializeField]
        private List<float> powerStoragePerLevel;
        [SerializeField]
        private List<float> metalCostPerLevel;
        [SerializeField]
        private List<float> powerCostPerLevel;
        [SerializeField]
        private List<float> metalUsagePerLevel;
        [SerializeField]
        private List<float> powerUsagePerLevel;
        [SerializeField]
        private List<float> maxHitPointsPerLevel;
        [SerializeField]
        private List<int> maxNodesPerLevel;
        [SerializeField]
        private List<int> buildTimePerLevel;
        [SerializeField]
        private List<float> fabricationPerLevel;
        [SerializeField]
        public List<ListOfBuildableObjects> SetBuildableObjects;

        private bool CheckRequestedLevel(int level)
        {
            if (level <= maxLevel)
                return true;
            else
                return false;
        }

        public float GetMetalProduction(int level)
        {
            if (CheckRequestedLevel(level))
                return metalProductionPerLevel[level - 1];
            else
                return -1.0f;
        }

        public float GetPowerProduction(int level)
        {
            if (CheckRequestedLevel(level))
                return powerProductionPerLevel[level - 1];
            else
                return -1.0f;
        }

        public float GetMetalStorage(int level)
        {
            if (CheckRequestedLevel(level))
                return metalStoragePerLevel[level - 1];
            else
                return -1.0f;
        }

        public float GetPowerStorage(int level)
        {
            if (CheckRequestedLevel(level))
                return powerStoragePerLevel[level - 1];
            else
                return -1.0f;
        }

        public float GetMetalCost(int level)
        {
            if (CheckRequestedLevel(level))
                return metalCostPerLevel[level - 1];
            else
                return -1.0f;
        }

        public float GetPowerCost(int level)
        {
            if (CheckRequestedLevel(level))
                return powerCostPerLevel[level - 1];
            else
                return -1.0f;
        }

        public float GetMetalUsage(int level)
        {
            if (CheckRequestedLevel(level))
                return metalUsagePerLevel[level - 1];
            else
                return -1.0f;
        }

        public float GetPowerUsage(int level)
        {
            if (CheckRequestedLevel(level))
                return powerUsagePerLevel[level - 1];
            else
                return -1.0f;
        }

        public float GetMaxHitPoints(int level)
        {
            if (CheckRequestedLevel(level))
                return maxHitPointsPerLevel[level - 1];
            else
                return -1.0f;
        }

        public int GetMaxNodes(int level)
        {
            if (CheckRequestedLevel(level))
                return maxNodesPerLevel[level - 1];
            else
                return -1;
        }

        public float GetBuildTime(int level)
        {
            if (CheckRequestedLevel(level))
                return buildTimePerLevel[level - 1];
            else
                return -1.0f;
        }

        public float GetFabrication(int level)
        {
            if (CheckRequestedLevel(level))
                return fabricationPerLevel[level - 1];
            else
                return -1.0f;
        }

        public List<string> GetBuildList(int level)
        {
            if (CheckRequestedLevel(level))
            {
                return SetBuildableObjects[level - 1].listOfBuildableObjects;
            }
            else
                return null;
        }
    }
}