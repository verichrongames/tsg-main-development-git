﻿using UnityEngine;
using System.Collections;
using CoreSys;

namespace CoreSys.Buildings
{
    public class SolarBlock : Block
    {
        public bool status;

        protected override void Start()
        {
            base.Start();
            status = true;
        }

        protected override void Update()
        {
            base.Update();
        }
    }
}