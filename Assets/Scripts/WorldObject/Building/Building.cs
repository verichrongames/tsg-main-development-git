﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CoreSys;
using CoreSys.Objects;
using CoreSys.CustomTypes;
using Vectrosity;

namespace CoreSys.Buildings
{
    public class Building : Bolt.EntityBehaviour<IBuildingState>, IPlayerObject, ICanBuildSelf, ICanBuild
    {
        public Grid parentGrid; //The grid the building is on
        protected UpgradeStats upgradeStats; //Holds upgrade info about each building
        private ObjectUIElements UIElementsComponent; //Component managed flaotign UI elements

        //Resource related variables
        private float currentPerSecondMetalCost, currentPerSecondPowerCost; //Used to reverse the unit cost when done
        public float currentMetalProduction, currentPowerProduction;
        public float currentMetalUsage, currentPowerUsage;
        public float currentMetalStorage, currentPowerStorage;
        public float metalProduction, powerProduction; //Stored production values
        public float metalUsage, powerUsage;
        public float metalStorage, powerStorage;
        public float totalMetalCost, totalPowerCost;
        public float fabrication;
        public float healthToAdd;

        private string baseObjectName;//Meant to retain name before appended levels

        //Building Vars//
        public float buildTime;
        private bool needsBuilding = false; //possibly depreciated
        public bool placement = false;
        public bool upgrading = false;
        public float BuildProgress { get; set; }
        public float MaxBuildProgress = 100.0f;
        public bool CurrentlyBuilding { get; set; }

        public int setMaxHitPoints;
        public float setHitPoints;
        public int currentLevel;

        //Player Object Requirements
        [HideInInspector, SerializeField] PlayerObjectType m_objectType;
        [ExposeProperty] public PlayerObjectType objectType { get { return m_objectType; } set { m_objectType = value; } }
        public BoundingPointsType BoundingPoints { get; set; }
        public BoundingAnglesType BoundingAngles { get; set; }
        //Materials//
        public Material Transparent, Default, Construction;

        //Bounding Box Points
        public Vector3 UpperRightPoint { get; set; }
        public Vector3 LowerRightPoint { get; set; }
        public Vector3 LowerLeftPoint { get; set; }
        public Vector3 UpperLeftPoint { get; set; }

        //Random Properties
        private SelectionBox selectionBox { get; set; }
        public Player player { get; set; }
        public string ObjectName { get; set; }
        public float HitPoints { get; set; }
        public float MaxHitPoints { get; set; }
        public float ActionDistance { get; set; }
        public float ViewRange { get; set; }
        public bool IsSelected { get; set; }
        public Transform transformV;

        //Cached
        Renderer meshRenderer;
        Mesh boundingMesh;

        //ICanBuild Requirements
        public List<string> buildableObjects { get; set; }
        public string buildPanelItems { get; set; }
        public Vector3 RallyPoint { get; set; }
        public List<string> setBuildableObjects;
        public string setBuildPanelItems;
        public bool InBuildMode { get; set; }

        protected Vector3 spawnPoint = new Vector3();
        protected Vector3 rallyPoint = new Vector3();

        //testing
        VectorPoints vectorPoint;
        bool linedrawn = false;

        //System//-------------------------------------------------------------------------------------------------------------------------------//
        public override void Attached() //Bolt Start Function
        {
            base.Attached();
            state.Health = HitPoints;
            state.MaxHealth = MaxHitPoints;
            state.ObjectName = this.name;
        }

        protected virtual void Awake()
        {
            transformV = GetTransform();
            meshRenderer = GetComponent<Renderer>();
            BoundingMeshCreation();
            SetUIElementsReferance();
        }

        protected virtual void Start()
        {
            MaxHitPoints = setMaxHitPoints;
            HitPoints = setHitPoints;
            ObjectName = this.name;
            FixName();
            SetPlayer();
            SetSelectionBox();
            parentGrid = transform.parent.GetComponent<Grid>();
            upgradeStats = GetComponent<UpgradeStats>();
            buildableObjects = setBuildableObjects;
            buildPanelItems = setBuildPanelItems;
            SetUIElementsReferance();
        }

        protected virtual void Update()
        {
            if (CurrentlyBuilding)
                HandleConstruction();

            //Testing
            DetermineBoundingBoxPoints();
            List<Vector3> boundsList = new List<Vector3>();
            boundsList.Add(LowerRightPoint); //Need to make this not run if the object hasnt moved. //FIXTHIS
            boundsList.Add(UpperRightPoint);
            boundsList.Add(UpperLeftPoint);
            boundsList.Add(LowerLeftPoint);

            if (!linedrawn)
            {
                vectorPoint = new VectorPoints("bs", boundsList, null, 2);
                vectorPoint.SetColor(Color.green);
                linedrawn = true;
            }
            vectorPoint.Draw3D();
        }

        public void OnDestroy()
        {
            gameObject.DestroyAllReferances();
        }

        //End System//---------------------------------------------------------------------------------------------------------------//

        /*======================================================================
         *    ========== Mesh And Bounding Box Methods ==========
         * ==================================================================*/

        //Handles the method calls for the mesh creation
        private void BoundingMeshCreation()
        {
            //Quaternion currentRotation = ResetPosition(new Quaternion(0, 0, 0, 0));
            BoundingPointsType boundingPoints = DetermineBounds();
            //Mesh boundingMesh = CreateBoundingMesh(boundingPoints);
            //AssignMesh(boundingMesh);
            //ResetPosition(currentRotation);
            BoundingPoints = boundingPoints;

            this.boundingMesh = GetComponent<MeshFilter>().mesh;
        }

        //Creates the mesh filter and assigns the mesh
        private void AssignMesh(Mesh boundingMesh)
        {
            MeshFilter newMeshFilter = gameObject.AddComponent<MeshFilter>();
            newMeshFilter.mesh = boundingMesh;
        }

        //Creates a bounding mesh for the unit
        private Mesh CreateBoundingMesh(BoundingPointsType boundingPoints)
        {

            Mesh boundingMesh = new Mesh();
            boundingMesh.name = "Bounding Quad";

            Vector3[] vertices = new Vector3[4]
        {
            boundingPoints.LowerLeftPoint_L,
            boundingPoints.LowerRightPoint_L,
            boundingPoints.UpperRightPoint_L,
            boundingPoints.UpperLeftPoint_L
        };

            int[] triangles = new int[6]
        {
            0,
            2,
            1,

            0,
            3,
            2
        };

            boundingMesh.vertices = vertices;
            boundingMesh.triangles = triangles;

            return boundingMesh;
        }

        //Resets the units rotation to the specific quaternion and returns the quaterion from before the reset
        private Quaternion ResetPosition(Quaternion toResetTo)
        {
            Quaternion currentRotation = transformV.rotation;
            transformV.rotation = toResetTo;
            return currentRotation;
        }

        //Determines and returns the bounding box points
        private BoundingPointsType DetermineBounds()
        {
            Vector3 maxBounds = meshRenderer.bounds.max;
            Vector3 minBounds = meshRenderer.bounds.min;

            Vector3 maxBounds_L = transformV.InverseTransformPoint(meshRenderer.bounds.max);
            Vector3 minBounds_L = transformV.InverseTransformPoint(meshRenderer.bounds.min);

            BoundingPointsType boundingPoints = new BoundingPointsType(new Vector3(maxBounds_L.x, 0, maxBounds_L.z),
                                                                       new Vector3(minBounds_L.x, 0, maxBounds_L.z),
                                                                       new Vector3(maxBounds_L.x, 0, minBounds_L.z),
                                                                       new Vector3(minBounds_L.x, 0, minBounds_L.z),
                                                                       true);
            //Necessary to save the non-local coordinates
            boundingPoints.UpperRightPoint = new Vector3(maxBounds.x, 1, maxBounds.z);
            boundingPoints.UpperLeftPoint = new Vector3(minBounds.x, 1, maxBounds.z);
            boundingPoints.LowerRightPoint = new Vector3(maxBounds.x, 1, minBounds.z);
            boundingPoints.LowerLeftPoint = new Vector3(minBounds.x, 1, minBounds.z);
            return boundingPoints;
        }

        //Updates the bounding point information when requested
        public void UpdateBoundingPoints()
        {
            Vector3 maxBounds = meshRenderer.bounds.max;
            Vector3 minBounds = meshRenderer.bounds.min;

            Vector3 maxBounds_L = transformV.InverseTransformPoint(meshRenderer.bounds.max);
            Vector3 minBounds_L = transformV.InverseTransformPoint(meshRenderer.bounds.min);

            //Necessary to save the non-local coordinates
            BoundingPoints.UpperRightPoint_L = boundingMesh.vertices[2];
            BoundingPoints.UpperLeftPoint_L = boundingMesh.vertices[3];
            BoundingPoints.LowerRightPoint_L = boundingMesh.vertices[1];
            BoundingPoints.LowerLeftPoint_L = boundingMesh.vertices[0];

            //Necessary to save the non-local coordinates
            BoundingPoints.UpperRightPoint = transformV.TransformPoint(boundingMesh.vertices[2]);
            BoundingPoints.UpperLeftPoint = transformV.TransformPoint(boundingMesh.vertices[3]);
            BoundingPoints.LowerRightPoint = transformV.TransformPoint(boundingMesh.vertices[1]);
            BoundingPoints.LowerLeftPoint = transformV.TransformPoint(boundingMesh.vertices[0]);
        }

        //Sets the angles for each side of the unit. Assumes a rectangular shape. Only needs to run once
        private void SetBoundingAngles()
        {
            float frontRightAllowance = Vector3.Angle(transformV.position - BoundingPoints.UpperRightPoint, -transformV.forward);
            float frontLeftAllowance = Vector3.Angle(transformV.position - BoundingPoints.UpperLeftPoint, -transformV.forward);

            float rightFrontAllowance = Vector3.Angle(transformV.position - BoundingPoints.UpperRightPoint, -transformV.right);
            float rightBackAllowance = Vector3.Angle(transformV.position - BoundingPoints.LowerRightPoint, -transformV.right);

            float backRightAllowance = Vector3.Angle(transformV.position - BoundingPoints.LowerRightPoint, -transformV.forward);
            float backLeftallowance = Vector3.Angle(transformV.position - BoundingPoints.LowerLeftPoint, -transformV.forward);

            float leftBackAllowance = Vector3.Angle(transformV.position - BoundingPoints.LowerRightPoint, -transformV.right);
            float leftFrontAllowance = Vector3.Angle(transformV.position - BoundingPoints.UpperRightPoint, -transformV.right);

            BoundingAngles = new BoundingAnglesType(frontRightAllowance, frontLeftAllowance,
                                                               rightFrontAllowance, rightBackAllowance,
                                                               backRightAllowance, backLeftallowance,
                                                               leftBackAllowance, leftFrontAllowance);
        }

        //===============================================================================







        private void FixName()
        {
            string fixname = this.name;
            baseObjectName = this.name;
            this.name = fixname;
        }

        public void SetIsSelected(bool selected)
        {
            IsSelected = selected;
            CallSelectionBox(selected);
        }

        //Public Method To Take Damage
        public void TakeDamage(float damage)
        {
            HitPoints -= damage;
        }

        //Resource Handling//---------------------------------------------------------------------------------------------------------------//
        public virtual void CompletedResources() //Used when the construction of the building is complete to equalize its usage.
        {
            currentMetalProduction = metalProduction;
            currentPowerProduction = powerProduction;
            currentMetalUsage += -currentPerSecondMetalCost;
            currentPowerUsage += -currentPerSecondPowerCost;
        }

        public virtual void ConstructionResources() //Ran at the start of the construction to prompt usage of resources.
        {
            currentPerSecondMetalCost = CalculatePerSecondCost(totalMetalCost, buildTime);
            currentPerSecondPowerCost = CalculatePerSecondCost(totalPowerCost, buildTime);
            currentMetalUsage += currentPerSecondMetalCost;
            currentPowerUsage += currentPerSecondPowerCost;
        }

        private float CalculatePerSecondCost(float totalCost, float totalTime) //calculates the cost per second based on time and total cost
        {
            float perSecond = totalCost / totalTime;
            //Add in fabrication value to handle modified cost.
            return perSecond;
        }

        //End Resources//---------------------------------------------------------------------------------------------------------------//

        //Determines the 4 points for the box around the unit.
        public void DetermineBoundingBoxPoints()
        {
            Vector3 maxBounds = this.GetComponent<Renderer>().bounds.max;
            Vector3 minBounds = this.GetComponent<Renderer>().bounds.min;

            UpperRightPoint = new Vector3(maxBounds.x, 1, maxBounds.z); //UpperRight
            LowerRightPoint = new Vector3(maxBounds.x, 1, minBounds.z); //LowerRight
            UpperLeftPoint = new Vector3(minBounds.x, 1, maxBounds.z); //UpperLeft
            LowerLeftPoint = new Vector3(minBounds.x, 1, minBounds.z); //LowerLeft
        }

        //Calls for the Selection Box creation
        public void CallSelectionBox(bool selected)
        {
            UIElementsComponent.ToggleSelectionBox(selected);
        }

        public void RightClickGround(RClickEventArgs e) { /*Set the rally point*/}
        public void RightClickObject(RClickEventArgs e) { }

        //End Bounding Box//---------------------------------------------------------------------------------------------------------------//

        //Damage Handling//--------------------------------------------------------------------------------------------------------------------//

        public void DamageBuilding(float damage)
        {

        }

        //Rally Points//----------------------------------------------------------------------------------------------------------------//
        //Public method to recieve call through ICanBuild interface to set the rally point
        public void SetRallyPoint(Vector3 rally)
        {
            RallyPoint = rally;
        }

        //private method to set the initial ICanBuild initial rally point
        protected void SetRallyPoint()
        {
            DetermineBoundingBoxPoints();

            float x = UpperRightPoint.x * 1.1f;
            float y = (UpperRightPoint.y + LowerRightPoint.y) / 2;

            Vector3 rally = new Vector3(x, y, 0);
            SetRallyPoint(rally);
        }

        //Unit Self Coloring Methods//---------------------------------------------------------------------------------------------------------//
        public void SetMaterial(Material material)
        {
            Renderer renderer = GetComponent<Renderer>();
            renderer.material = material;
        }
        //End Unit Self Coloring Methods//-----------------------------------------------------------------------------------------------------//

        public void SetPlayer()
        {
            player = transform.root.GetComponent<Player>();
        }

        //Gets the selection box for this object
        public void SetSelectionBox()
        {
            selectionBox = GetComponent<SelectionBox>();
        }

        //Upgrading//------------------------------------------------------------------------------------------------------------------------//
        private void HandleConstruction()
        {
            if (CurrentlyBuilding)
            {
                if (BuildProgress < MaxBuildProgress)
                {
                    SelfBuild();
                }
                else
                {
                    if (upgrading)
                        FinishUpgrade();
                    else
                        FinishBuild();
                }
            }
        }

        public virtual void StartUpgrade()
        {
            parentGrid.blockHandler.CancelBlockPlacement();
            upgrading = true;
            CurrentlyBuilding = true;
            upgradeConstructionTraits(currentLevel + 1);
            ConstructionResources();
        }

        private void upgradeConstructionTraits(int level)
        {
            BuildProgress = 0.0f;
            totalMetalCost = upgradeStats.GetMetalCost(level);
            totalPowerCost = upgradeStats.GetPowerCost(level);
            buildTime = upgradeStats.GetBuildTime(level);
            healthToAdd = upgradeStats.GetMaxHitPoints(level) - MaxHitPoints;
            MaxHitPoints = upgradeStats.GetMaxHitPoints(level);
        }

        protected virtual void upgradeFinishedTraits(int level)
        {
            metalProduction = upgradeStats.GetMetalProduction(level);
            powerProduction = upgradeStats.GetPowerProduction(level);
            metalStorage = upgradeStats.GetMetalStorage(level);
            powerStorage = upgradeStats.GetPowerStorage(level);
            metalUsage = upgradeStats.GetMetalUsage(level);
            powerUsage = upgradeStats.GetPowerUsage(level);
            fabrication = upgradeStats.GetFabrication(level);
            buildableObjects = upgradeStats.GetBuildList(level);
        }

        protected virtual void SelfBuild() //Adds to the total built based on various factors including resources.
        {
            float buildSpeed = parentGrid.GRM.GetBuildPercentage();
            float buildAmount = ((buildSpeed * (100 / buildTime)) * Time.deltaTime);
            float MaxRatio = (buildAmount / MaxBuildProgress);
            float HealthFromMax = (healthToAdd * MaxRatio);
            BuildProgress += buildAmount;
            HitPoints += HealthFromMax;
        }

        protected virtual void FinishBuild() //Finishes the core once the building is completed
        {
            CurrentlyBuilding = false;
            CompletedResources();
        }

        protected virtual void FinishUpgrade() //Finishes the upgrade for the building
        {
            CurrentlyBuilding = false;
            upgrading = false;
            currentLevel++;
            upgradeFinishedTraits(currentLevel);
            NameUpgrade(currentLevel);
            CompletedResources();
        }

        private void NameUpgrade(int level)
        {
            string upgradeName = baseObjectName + " L" + level;
            this.name = upgradeName;
            ObjectName = upgradeName;
        }

        //Building Self Construction Methods and Tracking//------------------------------------------------------------------------------------//

        public void Sell()  //Currently inactive //FIXTHIS//
        { //Used to Sell and Destroy Building
            if (IsSelected) SetIsSelected(false);
            Destroy(this.gameObject);
        }

        public void SetUIElementsReferance()
        {
            UIElementsComponent = GetComponent<ObjectUIElements>();
        }

        //Returns this objects transform. Is used to cache the Transform referance to stop GetComponent calls
        protected Transform GetTransform()
        {
            return GetComponent<Transform>();
        }
    }
}