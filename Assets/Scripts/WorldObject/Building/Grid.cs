﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CoreSys;
using CoreSys.Objects;

namespace CoreSys.Buildings
{
    public class Grid : MonoBehaviour
    {
        public Player player;
        public BlockHandler blockHandler;
        public GridResourceManager GRM;

        public void Start()
        {
            //This region needs to be deleted upon implementation of the game initializer
            GRM = GetComponent<GridResourceManager>();
            blockHandler = transform.parent.parent.GetComponent<BlockHandler>();
            player = transform.root.GetComponent<Player>();
            Vector3 coreStartLocation = new Vector3(transform.position.x, 1, transform.position.z);
            GameObject coreToSpawn = GetGameObjects.GetCore("MainCore");
            GameObject startCore = (GameObject)Instantiate(coreToSpawn, transform.position, new Quaternion());
            startCore.transform.SetParent(transform);
            startCore.name = coreToSpawn.name;
            Core findCore = startCore.GetComponent<Core>();
            GRM.coreList.Add(findCore);
        }

        public void Update()
        {

        }

        public void AddCore(Core coreToAdd)
        {
            GRM.coreList.Add(coreToAdd);
        }

        public void RemoveCore(Core coreToRemove)
        {
            int toRemoveIndex = GRM.coreList.IndexOf(coreToRemove);
            GRM.coreList.RemoveAt(toRemoveIndex);
        }
    }
}
