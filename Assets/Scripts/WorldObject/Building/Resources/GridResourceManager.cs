﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CoreSys;

//what are comments, baby don't tell me, don't tell me anymore. //fgt

//The general idea behind this class is that of managing the output of all resources through a cluster.
//Attached to core modules and the player
namespace CoreSys.Buildings
{
    public class GridResourceManager : MonoBehaviour
    {
        public Player player;
        public ResourceList resources;
        private ResourceBar resourceBar;
        public List<Core> coreList;
        private float currentMetal, currentPower, metalSupplied, powerSupplied;

        //System Functions//--------------------------------------------------------------------------------------------//
        public void Start()
        {
            player = transform.root.GetComponent<Player>();
            resourceBar = FindObjectOfType<ResourceBar>().GetComponent<ResourceBar>();
            resources = GetComponent<ResourceList>();
        }

        public void Update()
        {
            if (currentMetal > 0 || currentPower > 0)
            {
                HandleExcessResources();
            }
        }

        public void SendUserInterface() //Sends rounded values to the UI
        {
            ResourcesPanelInfoType sendResources = new ResourcesPanelInfoType();
            int sendMetalProduction = Mathf.RoundToInt(resources.metalProduction - resources.metalUsage);
            int sendPowerProduction = Mathf.RoundToInt(resources.powerProduction - resources.powerUsage);
            int sendMetalStorage = Mathf.RoundToInt(resources.metalStorage);
            int sendPowerStorage = Mathf.RoundToInt(resources.powerStorage);
            int sendMetalStored = Mathf.RoundToInt(resources.metalStored);
            int sendPowerStored = Mathf.RoundToInt(resources.powerStored);
            sendResources = new ResourcesPanelInfoType(sendMetalStorage, sendMetalStored, sendPowerStorage, sendPowerStored, sendPowerProduction, sendMetalProduction);
            resourceBar.UpdateResourcesPanel(sendResources);
        }
        //End System Functions//----------------------------------------------------------------------------------------//

        public void PlayerResourceUpdate() //Updates the current values of the resource production across the grid
        {
            resources.ClearResources();
            foreach (Core currentCore in coreList)
            {
                MergeResources(currentCore.CRM.GetResources());
            }
            currentMetal = resources.metalProduction - resources.metalUsage;
            currentPower = resources.powerProduction - resources.powerUsage;
            SendUserInterface();
        }

        public void PlayerStorageUpdate() //Updates the current values of the storage across the grid
        {
            resources.ClearStorage();
            Core[] cores = GetComponentsInChildren<Core>();
            foreach (Core currentCore in cores)
            {
                MergeStorage(currentCore.CRM.GetStorage());
            }
            SendUserInterface();
        }

        private void MergeResources(ResourceListType AList) //Adds an incoming resourcelisttype to the standing resourcelist for resources.
        {
            if (AList != null) //FIXTHIS
            {
                resources.metalProduction += AList.metalProduction;
                resources.powerProduction += AList.powerProduction;
                resources.metalUsage += AList.metalUsage;
                resources.powerUsage += AList.powerUsage;
            }
        }

        private void MergeStorage(ResourceListType AList) //Adds an incoming resourcelisttype to the standing resourcelist for storage
        {
            if (AList != null) //FIXTHIS
            {
                resources.metalStorage += AList.metalStorage;
                resources.powerStorage += AList.powerStorage;
            }
        }

        //Storage Handling//

        private void HandleExcessResources() //Handles resources when there is a surplus of production
        {
            float metalSpace = resources.metalStorage - resources.metalStored;
            float powerSpace = resources.powerStorage - resources.powerStored;
            if (currentMetal > 0 && metalSpace > 0)
            {
                float addToMetalStorage = currentMetal * Time.deltaTime; //Add to metal storage if there is a surplus and space
                resources.metalStored += addToMetalStorage;
                SendUserInterface();
            }
            if (currentPower > 0 && powerSpace > 0)
            {
                float addToPowerStorage = currentPower * Time.deltaTime; //Add to power if there is a surplus and space.
                resources.powerStored += addToPowerStorage;
                SendUserInterface();
            }
            if (resources.powerStored > resources.powerStorage) //Prevents overage numbers //Doesnt work 100% of the time FIXTHIS
            {
                resources.powerStored = resources.powerStorage;
                SendUserInterface();
            }
            if (resources.metalStored > resources.metalStorage) //Prevents overage numbers //Doesnt work 100% of the time FIXTHIS
            {
                resources.metalStored = resources.metalStorage;
                SendUserInterface();
            }
        }

        private void DrainMetal() //Pulls metal from storage if there is any available.
        {
            if (currentMetal < 0 && resources.metalStored > 0) //Redundant check to make sure that we arent double adding resources to storage.
            {
                float amountToDrain = currentMetal * Time.deltaTime;
                resources.metalStored += amountToDrain;
                metalSupplied = -currentMetal;
                SendUserInterface();
            }
            else if (resources.metalStored < 0.0f)
                resources.metalStored = 0;
            else
            {
                metalSupplied = 0.0f;
            }
        }

        private void DrainPower() //Pulls power from storage if there is any available.
        {
            if (currentPower < 0 && resources.powerStored > 0) //Redundant check to make sure that we arent double adding resources to storage.
            {
                float amountToDrain = currentPower * Time.deltaTime;
                resources.powerStored += amountToDrain;
                powerSupplied = -currentPower;
                SendUserInterface();
            }
            else if (resources.powerStored < 0.0f)
                resources.powerStored = 0;
            else
            {
                powerSupplied = 0.0f;
            }
        }

        //These four functions grab the ratios of the 2 primary resources, and make a float from 0 to 1.0, making sure to ceiling at 1.0f so there isnt an increase to build speed with excess resources.
        public float GetBuildPercentage()
        {
            float metalPercent = GetMetalMultiplier();
            float powerPercent = GetPowerMultiplier();
            return (metalPercent * powerPercent);
        }

        private float GetMetalMultiplier() //Grabs the ratio of production to usage, and calls to use stored resources if they are available if the ratio is below one.
        {
            float metalPercentage = (resources.metalProduction / resources.metalUsage);
            if (metalPercentage < 1.0f)
            {
                DrainMetal();
                metalPercentage = ((resources.metalProduction + metalSupplied) / resources.metalUsage);
            }
            if (metalPercentage > 1.0f)
                metalPercentage = 1.0f;
            return metalPercentage; //Obviously the idea is to try and keep the ratio at one as much as possible.
        }

        private float GetPowerMultiplier()
        {
            float powerPercentage = (resources.powerProduction / resources.powerUsage);
            if (powerPercentage < 1.0f)
            {
                DrainPower();
                powerPercentage = ((resources.powerProduction + powerSupplied) / resources.powerUsage);
            }
            if (powerPercentage > 1.0f)
                powerPercentage = 1.0f;
            return powerPercentage;
        }

        //End Multipliers
    }
}