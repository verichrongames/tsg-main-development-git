﻿using UnityEngine;
using System.Collections;

//what are comments, baby don't tell me, don't tell me anymore.
//kk - Catalyse
namespace CoreSys.Buildings
{
    public class ResourceList : MonoBehaviour
    {
        public float metalProduction, powerProduction;
        public float metalUsage, powerUsage;
        public float fabrication;
        public float metalStorage, powerStorage;
        public float metalStored, powerStored;

        public void ClearResources()
        {
            metalProduction = 0.0f;
            powerProduction = 0.0f;
            fabrication = 0.0f;
            metalUsage = 0.0f;
            powerUsage = 0.0f;
        }

        public void ClearStorage()
        {
            metalStorage = 0.0f;
            powerStorage = 0.0f;
        }
    }
}
