﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CoreSys;

//what are comments, baby don't tell me, don't tell me anymore. //Fine I wont then bitch

//The general idea behind this class is that of managing the output of all resources through a grid.
namespace CoreSys.Buildings
{
    //Attached to core modules and the player
    public class CoreResourceManager : MonoBehaviour
    {
        private Core childCore;
        public Player player;
        private ResourceList resources;
        private Grid grid;

        private float productionValue; //Base will be 10 from the core.

        //System Functions//--------------------------------------------------------------------------------------------//
        public void Start()
        {
            childCore = GetComponent<Core>();
            player = transform.root.GetComponent<Player>();
            resources = GetComponent<ResourceList>();
            grid = transform.parent.GetComponent<Grid>();
            ResourceUpdate();
        }

        public void Update()
        {

        }

        //End System Functions//----------------------------------------------------------------------------------------//

        public void ResourceUpdate() //Rolls through all blocks and updates the total resources production and usage.
        {
            resources.ClearResources();
            List<Block> blocksList = childCore.blockList;
            foreach (Block block in blocksList)
            {
                MergeResources(GrabBuildingResources(block));
            }

            MergeResources(GrabBuildingResources(childCore));
            grid.GRM.PlayerResourceUpdate();
        }

        public void StorageUpdate() //Rolls through all blocks and the core and updates the total amount of storage available.
        {
            resources.ClearStorage();
            List<Block> blocksList = childCore.blockList;
            foreach (Block block in blocksList)
            {
                MergeResources(GrabBuildingStorage(block));
            }

            MergeStorage(GrabBuildingStorage(childCore));
            grid.GRM.PlayerStorageUpdate();
        }

        public void CompleteUpdate() //Updates both
        {
            StorageUpdate();
            ResourceUpdate();
        }

        public float GetFabrication()
        {
            return resources.fabrication;
        }

        public ResourceListType GetResources()
        {
            ResourceListType sendResources = new ResourceListType(resources.metalProduction, resources.powerProduction, resources.fabrication, resources.metalUsage, resources.powerUsage);
            return sendResources;
        }

        public ResourceListType GetStorage()
        {
            ResourceListType sendStorage = new ResourceListType(resources.metalStorage, resources.powerStorage);
            return sendStorage;
        }

        private ResourceListType GrabBuildingResources(Building building) //Used to get resources from the target building and compile it into one list here to be sent off.
        {
            ResourceListType sendResources = new ResourceListType(building.currentMetalProduction, building.currentPowerProduction, building.fabrication, building.currentMetalUsage, building.currentPowerUsage);
            return sendResources;
        }

        private ResourceListType GrabBuildingStorage(Building building) //Used to get storage from the target building and compile it into one list here to be sent off.
        {
            ResourceListType sendStorage = new ResourceListType(building.metalStorage, building.powerStorage);
            return sendStorage;
        }

        private void MergeResources(ResourceListType AList) //Used to add an incoming resourcelisttype into the standing list
        {
            if (AList != null) //FIXTHIS
            {
                resources.metalProduction += AList.metalProduction;
                resources.powerProduction += AList.powerProduction;
                resources.fabrication += AList.fabrication;
                resources.metalUsage += AList.metalUsage;
                resources.powerUsage += AList.powerUsage;
                resources.metalStorage += AList.metalStorage;
                resources.powerStorage += AList.powerStorage;
            }
        }

        private void MergeStorage(ResourceListType AList) //Used to add an incoming resourcelisttype into the standing list
        {
            if (AList != null) //FIXTHIS
            {
                resources.metalProduction += AList.metalProduction;
                resources.powerProduction += AList.powerProduction;
                resources.fabrication += AList.fabrication;
                resources.metalUsage += AList.metalUsage;
                resources.powerUsage += AList.powerUsage;
                resources.metalStorage += AList.metalStorage;
                resources.powerStorage += AList.powerStorage;
            }
        }
    }
}