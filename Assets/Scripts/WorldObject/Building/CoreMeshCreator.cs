﻿/*
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CoreMeshCreator : MonoBehaviour 
{
    //Initialization
    public void Start()
    {
        MeshFilter meshFilter = this.GetComponent<MeshFilter>();
        MeshCollider meshCollider = this.GetComponent<MeshCollider>();
        Mesh mesh = new Mesh();
        meshFilter.mesh = Generate();
        meshCollider.sharedMesh = meshFilter.mesh;
    }

    public Mesh Generate()
    {
        Mesh mesh = new Mesh();
        float sqrt = (Mathf.Sqrt(3) / 2);

        Vector3 v0 = new Vector3(0, -1, 0);
        Vector3 v1 = new Vector3(-4, -1, 0);
        Vector3 v2 = new Vector3(-5, -1, sqrt * 2);
        Vector3 v3 = new Vector3(-4, -1, sqrt * 4);
        Vector3 v4 = new Vector3(-2, -1, sqrt * 4);
        Vector3 v5 = new Vector3(-1, -1, sqrt * 6);
        Vector3 v6 = new Vector3(1, -1, sqrt * 6);
        Vector3 v7 = new Vector3(2, -1, sqrt * 4);
        Vector3 v8 = new Vector3(4, -1, sqrt * 4);
        Vector3 v9 = new Vector3(5, -1, sqrt * 2);
        Vector3 v10 = new Vector3(4, -1, 0);
        Vector3 v11 = new Vector3(5, -1, -sqrt * 2);
        Vector3 v12 = new Vector3(4, -1, -sqrt * 4);
        Vector3 v13 = new Vector3(2, -1, -sqrt * 4);
        Vector3 v14 = new Vector3(1, -1, -sqrt * 6);
        Vector3 v15 = new Vector3(-1, -1, -sqrt * 6);
        Vector3 v16 = new Vector3(-2, -1, -sqrt * 4);
        Vector3 v17 = new Vector3(-4, -1, -sqrt * 4);
        Vector3 v18 = new Vector3(-5, -1, -sqrt * 2);
        Vector3 v19 = new Vector3(-4, 1, 0);
        Vector3 v20 = new Vector3(-5, 1, sqrt*2);
        Vector3 v21 = new Vector3(-4, 1, sqrt*4);
        Vector3 v22 = new Vector3(-2, 1, sqrt * 4);
        Vector3 v23 = new Vector3(-1, 1, sqrt * 6);
        Vector3 v24 = new Vector3(1, 1, sqrt * 6);
        Vector3 v25 = new Vector3(2, 1, sqrt * 4);
        Vector3 v26 = new Vector3(4, 1, sqrt * 4);
        Vector3 v27 = new Vector3(5, 1, sqrt * 2);
        Vector3 v28 = new Vector3(4, 1, 0);
        Vector3 v29 = new Vector3(5, 1, -sqrt * 2);
        Vector3 v30 = new Vector3(4, 1, -sqrt * 4);
        Vector3 v31 = new Vector3(2, 1, -sqrt * 4);
        Vector3 v32 = new Vector3(1, 1, -sqrt * 6);
        Vector3 v33 = new Vector3(-1, 1, -sqrt * 6);
        Vector3 v34 = new Vector3(-2, 1, -sqrt * 4);
        Vector3 v35 = new Vector3(-4, 1, -sqrt * 4);
        Vector3 v36 = new Vector3(-5, 1, -sqrt * 2);
        Vector3 v37 = new Vector3(0, 1, 0);

        mesh.vertices = new Vector3[] 
        {
            v0,
            v1, v2,
            v3, v4,
            v5, v6,
            v7, v8,
            v9, v10,
            v11, v12,
            v13, v14,
            v15, v16,
            v17, v18,
            v19, v20,
            v21, v22,
            v23, v24, 
            v25, v26,
            v27, v28, 
            v29, v30,
            v31, v32, 
            v33, v34, 
            v35, v36, 
            v37,
        };

        mesh.triangles = new int[] 
        {
            //top
            1,0,2,
            2,0,3,
            3,0,4,
            4,0,5,
            5,0,6,
            6,0,7,
            7,0,8,
            8,0,9,
            9,0,10,
            10,0,11,
            11,0,12,
            12,0,13,
            13,0,14,
            14,0,15,
            15,0,16,
            16,0,17,
            17,0,18,
            18,0,1,
            //Bottom
            20,37,19,
            21,37,20,
            22,37,21,
            23,37,22,
            24,37,23,
            25,37,24,
            26,37,25,
            27,37,26,
            28,37,27,
            29,37,28,
            30,37,29,
            31,37,30,
            32,37,31,
            33,37,32,
            34,37,33,
            35,37,34,
            36,37,35,
            19,37,36,
            //Sides
            23,5,6,
            23,6,24,
            24,6,7,
            24,7,25,
            25,7,8,
            25,8,26,
            26,8,9,
            26,9,27,
            9,10,27,
            27,10,28,
            28,10,11,
            28,11,29,
            11,12,29,
            12,30,29,
            13,30,12,
            30,13,31,
            31,13,14,
            31,14,32,
            15,32,14,
            15,33,32,
            33,15,16,
            33,16,34,
            34,16,17,
            34,17,35,
            17,18,35,
            35,18,36,
            18,1,36,
            36,1,19,
            19,1,2,
            19,2,20,
            20,2,3,
            20,3,21,
            21,3,4,
            21,4,22,
            22,4,5,
            22,5,23
        };

        mesh.RecalculateBounds();
        mesh.RecalculateNormals();

        return mesh;
    }

}
*/