﻿using UnityEngine;
using System.Collections;

namespace CoreSys.Buildings
{
    public class ActionZone : MonoBehaviour
    {
        [SerializeField]
        SpriteRenderer circleRenderer;

        public void Awake()
        {
            circleRenderer.enabled = false;
        }
        
        public void SetSize(float size)
        {
            transform.localScale += new Vector3(size, 0, size);
        }

        public void Display(bool status)
        {
            circleRenderer.enabled = status;
        }
    }
}