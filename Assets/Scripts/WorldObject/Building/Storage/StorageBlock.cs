﻿using UnityEngine;
using System.Collections;
using CoreSys;

namespace CoreSys.Buildings
{
    public class StorageBlock : Block
    {

        protected override void Start()
        {
            base.Start();
        }

        protected override void Update()
        {
            base.Update();
        }

        public override void CompletedResources()
        {
            base.CompletedResources();
            parentCore.CRM.StorageUpdate();
        }

        public override void ConstructionResources()
        {
            base.ConstructionResources();
        }
    }
}