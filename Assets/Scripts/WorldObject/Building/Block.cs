﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CoreSys;
using CoreSys.Objects;

namespace CoreSys.Buildings
{
    public class Block : Building
    {

        public Core parentCore;

        //Self Construction Variables//
        private Vector3 lLeft, lRight, top, bottom, uLeft, uRight;
        public Node creatorNode;
        private List<Node> updateNodeList = new List<Node>();
        //End Self Construction Vars//

        //Level Handling Vars//
        public int blockTier = 0;

        //System Functions//---------------------------------------//
        protected override void Awake()
        {
            base.Awake();
        }

        protected override void Start()
        {
            base.Start();
            parentGrid = parentCore.parentGrid;
            CurrentlyBuilding = false;
            BuildProgress = 0.0f;
        }

        protected override void Update()
        {
            base.Update();
        }

        private void OnMouseEnter()
        {
            if (placement)
                SetMaterial(Default);
        }
        private void OnMouseExit()
        {
            if (placement)
                SetMaterial(Transparent);
        }

        //End System Functions//-----------------------------------//

        //Node Handling Section//----------------------------------//

        private void NodeDrop()
        {
            List<Vector3> nodeCheckList = new List<Vector3>();
            Vector3 blockPosition = this.transform.position;
            float sqrt = (Mathf.Sqrt(3) / 2);

            lLeft = new Vector3(blockPosition.x - 3.0f, blockPosition.y - 2.0f, blockPosition.z - (sqrt * 2.0f));
            uLeft = new Vector3(blockPosition.x - 3.0f, blockPosition.y - 2.0f, blockPosition.z + (sqrt * 2.0f));
            lRight = new Vector3(blockPosition.x + 3.0f, blockPosition.y - 2.0f, blockPosition.z - (sqrt * 2.0f));
            uRight = new Vector3(blockPosition.x + 3.0f, blockPosition.y - 2.0f, blockPosition.z + (sqrt * 2.0f));
            top = new Vector3(blockPosition.x, blockPosition.y - 2.0f, blockPosition.z + (sqrt * 4.0f));
            bottom = new Vector3(blockPosition.x, blockPosition.y - 2.0f, blockPosition.z - (sqrt * 4.0f));
            nodeCheckList.Add(lLeft);
            nodeCheckList.Add(uLeft);
            nodeCheckList.Add(lRight);
            nodeCheckList.Add(uRight);
            nodeCheckList.Add(top);
            nodeCheckList.Add(bottom);
            foreach (Vector3 nodeCheck in nodeCheckList)
            {
                GameObject checkPoint = WorkManager.FindHitWorld(nodeCheck);
                if (checkPoint && checkPoint.name == "Ground")
                {
                    Vector3 nodeBuiltPoint = new Vector3(nodeCheck.x, nodeCheck.y, nodeCheck.z);
                    GameObject newNode = (GameObject)Instantiate(GetGameObjects.GetSystem("Node"), nodeBuiltPoint, new Quaternion());
                    Node node = newNode.GetComponent<Node>();
                    node.transform.SetParent(this.transform);
                    node.tier = blockTier + 1;
                    parentCore.nodeList.Add(node);
                    updateNodeList.Add(node);
                }
            }
            parentCore.nodesAdded(updateNodeList);
            updateNodeList.Clear();
        }

        //Resource Mgmt//------------------------------------------//

        private void SendResources()
        {
            parentCore.CRM.ResourceUpdate();
        }

        public override void CompletedResources()
        {
            base.CompletedResources();
            SendResources();
        }

        public override void ConstructionResources()
        {
            base.ConstructionResources();
            SendResources();
        }
        //End Resource MGMT//--------------------------------------//

        //Self Construction Methods//------------------------------//
        public void PlacementMode(Core parent)
        {
            placement = true;
            MakeTransparent();
            parentCore = parent;
            parentGrid = parentCore.parentGrid;
        }

        public void StartConstruction()
        {
            transform.SetParent(parentCore.transform);
            placement = false;
            CurrentlyBuilding = true;
            parentCore.blockList.Add(this);
            SetMaterial(Construction);
            HitPoints = 1;
            SetPlayer();
            ConstructionResources();
        }

        protected override void SelfBuild()
        {
            float healthFromMax;
            float buildSpeed = parentGrid.GRM.GetBuildPercentage();
            float buildAmount = ((parentCore.CRM.GetFabrication() * buildSpeed * (100 / buildTime)) * Time.deltaTime);
            float MaxRatio = (buildAmount / MaxBuildProgress);
            if (upgrading)
            {
                healthFromMax = (healthToAdd * MaxRatio);
            }
            else
            {
                healthFromMax = (MaxHitPoints * MaxRatio);
            }
            BuildProgress += buildAmount;
            HitPoints += healthFromMax;
        }

        protected override void FinishBuild()
        {
            base.FinishBuild();
            MakeSolid();
            NodeDrop();
            parentCore.CRM.ResourceUpdate();
        }

        protected override void FinishUpgrade()
        {
            base.FinishUpgrade();
            parentCore.CRM.ResourceUpdate();
        }

        //End Self Construction Methods//--------------------------//

        //Material Modification Methods//--------------------------//
        public void MakeSolid()
        {
            SetMaterial(Default);
        }

        public void MakeTransparent()
        {
            SetMaterial(Transparent);
        }
        //End Material Modification Methods//----------------------//
    }
}