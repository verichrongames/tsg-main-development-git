﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CoreSys;
using CoreSys.Objects;

namespace CoreSys.Buildings
{
    public class CoreHandler : MonoBehaviour
    {
        private int coreCount;
        private bool placingCore;
        private Core activeCore;

        //System Functions-----------------------------------------------------------------------//
        public void Awake()
        {
            coreCount = 0;
        }

        public void Start()
        {

        }

        public void Update()
        {
            if (placingCore)
            {
                CoreToMouse(activeCore);
            }
        }

        public void OnEnable()
        {
            MouseEvents.RightClickEvent += RightClickSwitches;
            MouseEvents.LeftClickEvent += LeftClickSwitches;
        }

        public void OnDisable()
        {
            MouseEvents.RightClickEvent -= RightClickSwitches;
            MouseEvents.LeftClickEvent -= LeftClickSwitches;
        }

        //End System Functions//------------------------------------------------------------//

        //Event Functions//-----------------------------------------------------------------//
        private void RightClickSwitches(string clickType, RClickEventArgs e)
        {
            if (placingCore)
            {
                CancelCorePlacement();
            }
        }

        private void LeftClickSwitches(string clickType, LClickEventArgs e)
        {
            switch (clickType)
            {
                case "Ground":
                    if (placingCore)
                        PlaceCore();
                    break;
                case "Object":
                    if (placingCore)
                        CancelCorePlacement();
                    break;
            }
        }

        //End Event Functions//-----------------------------------------------------------//

        public void CorePlacement(string coreType, Grid parentGrid)
        {
            var blockToSpawn = GetGameObjects.GetCore(coreType);

        }

        private void GhostCore(GameObject coreToSpawn, Grid parentGrid)
        {
            var mousePos = Input.mousePosition;
            mousePos = Camera.main.ScreenToWorldPoint(mousePos);
            Vector3 blockSpawn = mousePos;
            GameObject newCore = (GameObject)Instantiate(coreToSpawn, blockSpawn, new Quaternion());
            newCore.name = coreToSpawn.name;
            Core tempCore = newCore.GetComponent<Core>();
            tempCore.transform.SetParent(parentGrid.transform); //Assign it under the player thats supposed to be controlling it
        }

        private void DisableColliders(Core core)
        {

        }

        private void PlaceCore()
        {

        }

        private void CancelCorePlacement()
        {
            placingCore = false;
            Destroy(activeCore.gameObject);
        }

        private void CoreToMouse(Core placementCore)
        {
            var pos = Input.mousePosition;
            pos.z = 10;
            pos = Camera.main.ScreenToWorldPoint(pos);
            placementCore.transform.position = pos;
        }
    }
}