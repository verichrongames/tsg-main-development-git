﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;
using System.IO;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using TagFrenzy;
using CoreSys.Exceptions;

/*======================================================
 *  This class will keep record of every instantiated object
 *  in the game to make finding those objects easier. 
 * ===================================================*/

namespace CoreSys.Objects
{
    public class InstantiatedObjectsList : MonoBehaviour
    {
        void Update()
        {
            ActiveUnits = ActiveUnitList.ToList();
            ActiveBuilding = ActiveBuildingList.ToList();
            ActiveUI = ActiveUIList.ToList();
            ActiveManagers = ActiveManagersList.ToList();

            DisabledUnits = DisabledUnitList.ToList();
            DisabledBuilding = DisabledBuildingList.ToList();
            DisabledUI = DisabledUIList.ToList();
            DisabledManagers = DisabledManagersList.ToList();
        }
        /*======================================================================
        *                ======== Test Members ========
        * ====================================================================*/

        public List<InstantiatedObjectsListType> ActiveUnits = new List<InstantiatedObjectsListType>();
        public List<InstantiatedObjectsListType> ActiveBuilding = new List<InstantiatedObjectsListType>();
        public List<InstantiatedObjectsListType> ActiveUI = new List<InstantiatedObjectsListType>();
        public List<InstantiatedObjectsListType> ActiveManagers = new List<InstantiatedObjectsListType>();

        public List<InstantiatedObjectsListType> DisabledUnits = new List<InstantiatedObjectsListType>();
        public List<InstantiatedObjectsListType> DisabledBuilding = new List<InstantiatedObjectsListType>();
        public List<InstantiatedObjectsListType> DisabledUI = new List<InstantiatedObjectsListType>();
        public List<InstantiatedObjectsListType> DisabledManagers = new List<InstantiatedObjectsListType>();
        /*======================================================================
        *                ======== Collection Definitions ========
         * ====================================================================*/


        KeyedCollection<int, InstantiatedObjectsListType> ActiveUnitList = new InstantiatedObjectListCollection();
        KeyedCollection<int, InstantiatedObjectsListType> ActiveBuildingList = new InstantiatedObjectListCollection();
        KeyedCollection<int, InstantiatedObjectsListType> ActiveUIList = new InstantiatedObjectListCollection();
        KeyedCollection<int, InstantiatedObjectsListType> ActiveManagersList = new InstantiatedObjectListCollection();

        KeyedCollection<int, InstantiatedObjectsListType> DisabledUnitList = new InstantiatedObjectListCollection();
        KeyedCollection<int, InstantiatedObjectsListType> DisabledBuildingList = new InstantiatedObjectListCollection();
        KeyedCollection<int, InstantiatedObjectsListType> DisabledUIList = new InstantiatedObjectListCollection();
        KeyedCollection<int, InstantiatedObjectsListType> DisabledManagersList = new InstantiatedObjectListCollection();

        /*======================================================================
         *                ======== Public Methods ========
         * ====================================================================*/

        //Recieved call to add a new object to a list
        public void NewInstantiatedObject(GameObject newObject, Player player)
        {
            int ID = newObject.GetInstanceID();
            if(newObject.HasTag("Unit"))
            {
                if (newObject.activeInHierarchy == true)
                    ActiveUnitList.Add(new InstantiatedObjectsListType(newObject, player, ID, newObject.name));
                else
                    DisabledUnitList.Add(new InstantiatedObjectsListType(newObject, player, ID, newObject.name));
            }
            else if(newObject.HasTag("Building"))
            {
                if (newObject.activeInHierarchy == true)
                    ActiveBuildingList.Add(new InstantiatedObjectsListType(newObject, player, ID, newObject.name));
                else
                    DisabledBuildingList.Add(new InstantiatedObjectsListType(newObject, player, ID, newObject.name));
            }
            else if(newObject.HasTag("UserInterface"))
            {
                if (newObject.activeInHierarchy == true)
                    ActiveUIList.Add(new InstantiatedObjectsListType(newObject, player, ID, newObject.name));
                else
                    DisabledUIList.Add(new InstantiatedObjectsListType(newObject, player, ID, newObject.name));
            }
            else if (newObject.HasTag("Manager"))
            {
                if (newObject.activeInHierarchy == true)
                    ActiveManagersList.Add(new InstantiatedObjectsListType(newObject, player, ID, newObject.name));
                else
                    DisabledManagersList.Add(new InstantiatedObjectsListType(newObject, player, ID, newObject.name));
            }
            else
            {
                Debug.Log("No tag");
            }
        }

        //Used to remove object from list when it is destroyed 
        public void RemoveObjectFromList(GameObject gameObject, Player player)
        {
            int ID = gameObject.GetInstanceID();
            if (gameObject.HasTag("Unit"))
            {
                if (gameObject.activeInHierarchy)
                {
                    ActiveUnitList.Remove(ID);
                }
                else
                {
                    DisabledUnitList.Remove(ID);
                }
            }
            else if (gameObject.HasTag("Building"))
            {
                if(gameObject.activeInHierarchy)
                {
                    ActiveBuildingList.Remove(ID);
                }
                else
                {
                    DisabledBuildingList.Remove(ID);
                }

            }
            else if (gameObject.HasTag("UserInterface"))
            {
                if(gameObject.activeInHierarchy)
                {
                    ActiveUIList.Remove(ID);
                }
                else
                {
                    DisabledUIList.Remove(ID);
                }
            }
            else if (gameObject.HasTag("Manager"))
            {
                if(gameObject.activeInHierarchy)
                {
                    ActiveManagersList.Remove(ID);
                }
                else
                {
                    DisabledManagersList.Remove(ID);
                }
            }
        }

        //Public method to recieve call to move from the active list to the disabled list when the object is disabled
        public void MoveFromActiveToDisabled(GameObject gameObject, Player player)
        {
            int ID = gameObject.GetInstanceID();

            if (gameObject.HasTag("Unit"))
            {
                AddToDisabledList(FindUnitObject(ID, "Active"), "Unit", ID);
            }
            else if (gameObject.HasTag("Building"))
            {
                AddToDisabledList(FindBuildingObject(ID, "Active"), "Building", ID);
            }
            else if (gameObject.HasTag("UserInterface"))
            {
                AddToDisabledList(FindUIObject(ID, "Active"), "UserInterface", ID);
            }
            else if (gameObject.HasTag("Manager"))
            {
                AddToDisabledList(FindManagerObject(ID, "Active"), "Manager", ID);
            }
        }

        //Public method to recieve call to move an object from the disabled lsit to the active list when the object is re-enabled
        public void MoveFromDisabledToActive(GameObject gameObject, Player player)
        {
            int ID = gameObject.GetInstanceID();

            if (gameObject.HasTag("Unit"))
            {
                AddToActiveList(FindUnitObject(ID, "Disabled"), "Unit", ID);
            }
            else if (gameObject.HasTag("Building"))
            {
                AddToActiveList(FindBuildingObject(ID, "Disabled"), "Building", ID);
            }
            else if (gameObject.HasTag("UserInterface"))
            {
                AddToActiveList(FindUIObject(ID, "Disabled"), "UserInterface", ID);
            }
            else if (gameObject.HasTag("Manager"))
            {
                AddToActiveList(FindManagerObject(ID, "Disabled"), "Manager", ID);
            }
        }

        /// <summary>
        /// Returns GameObject referance from named search
        /// </summary>
        /// <param name="name">Objects Name. Not case sensative</param>
        /// <param name="listType">List Type (Unit, Building, User Interface, Manager)</param>
        /// <param name="activeStatus">Whether the object is "Active" or "Disabled"</param>
        /// <param name="player">The owner player</param>
        /// <returns></returns>
        public GameObject GetGameObject(string name, string listType, string activeStatus, Player player)
        {
            switch (listType)
            {
                case "Unit" :
                    return FindUnitObject(name, player, activeStatus);
                case "Building" :
                    return FindBuildingObject(name, player, activeStatus);
                case "User Interface" :
                    return FindUIObject(name, player, activeStatus);
                case "Manager" :
                    return FindManagerObject(name, player, activeStatus);
            }

            throw new ObjectNotFoundException("Object: " + name + " Has Invalid List Name:  " + activeStatus + " \"" + listType + "\" List" + "\n If this was unintentional, please add a list type or review the call for the correct string. ");
        }

        /// <summary>
        /// Does not work yet
        /// </summary>
        /// <param name="type"></param>
        /// <param name="partialName"></param>
        /// <param name="objectListTag"></param>
        /// <param name="player"></param>
        public void GetGameObject(Type type, string partialName, string objectListTag, Player player)
        {
            switch (objectListTag)
            {
                case "Unit":
                    break;
                case "Building":
                    break;
                case "User Interface":
                    break;
                case "Manager":
                    break;
            }
        }
        /// <summary>
        /// Returns object referance from instance ID
        /// </summary>
        /// <param name="instanceID"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        public GameObject GetGameObject(int instanceID, Player player)
        {
            return FindUnknownObject(instanceID, player);
        }

        /*======================================================================
         *             ======== Add To Specific List Methods ========
         * ====================================================================*/

        //Adds Object To Disabled List and Removes same object from activelist
        private void AddToDisabledList(InstantiatedObjectsListType objectToAdd, string list, int ID)
        {
            switch(list)
            {
                case "Unit":
                    DisabledUnitList.Add(objectToAdd);
                    ActiveUnitList.Remove(ID);
                    break;
                case "Building" :
                    DisabledBuildingList.Add(objectToAdd);
                    ActiveBuildingList.Remove(ID);
                    break;
                case "UserInterface" :
                    DisabledUIList.Add(objectToAdd);
                    ActiveUIList.Remove(ID);
                    break;
                case "Manager" :
                    DisabledManagersList.Add(objectToAdd);
                    ActiveManagersList.Remove(ID);
                    break;
            }
        }

        //Adds Object To Disabled List
        private void AddToActiveList(InstantiatedObjectsListType objectToAdd, string list, int ID)
        {
            switch (list)
            {
                case "Unit":
                    ActiveUnitList.Add(objectToAdd);
                    DisabledUnitList.Remove(ID);
                    break;
                case "Building":
                    ActiveBuildingList.Add(objectToAdd);
                    DisabledBuildingList.Remove(ID);
                    break;
                case "UserInterface":
                    ActiveUIList.Add(objectToAdd);
                    DisabledUIList.Remove(ID);
                    break;
                case "Manager":
                    ActiveManagersList.Add(objectToAdd);
                    DisabledManagersList.Remove(ID);
                    break;
            }
        }

        /*======================================================================
         *             ======== Find Methods and Overloads ========
         * ====================================================================*/

        //Finds Building objects with an object ID throws exception if not found
        private InstantiatedObjectsListType FindBuildingObject(int objectID, string list)
        {
            InstantiatedObjectsListType searchResult;
            if (list.ToLower() == "active")
            {
                searchResult = ActiveBuildingList[objectID];
                if (searchResult != null)
                {
                    return searchResult;
                }
            }
            else if (list.ToLower() == "disabled")
            {
                searchResult = DisabledBuildingList[objectID];
                if (searchResult != null)
                {
                    return searchResult;
                }
            }
            throw new ObjectNotFoundException("Object ID: " + objectID + " Not Found In " + list + "BuildingList");
        }

        //Uses a LINQ query to find a building object by name and player
        private GameObject FindBuildingObject(string name, Player player, string list)
        {
            GameObject returnObject;

            if (list.ToLower() == "active")
            {
                returnObject = (from o in ActiveBuildingList
                                           where o.OwningPlayer == player
                                           where o.ObjectName.ToLower().Replace(" ", string.Empty) == name.ToLower().Replace(" ", string.Empty)
                                           select o.GameObjectReferance).FirstOrDefault();
                if (returnObject != null)
                    return returnObject;
            }
            else if (list.ToLower() == "disabled")
            {
                returnObject = (from o in DisabledBuildingList
                                           where o.OwningPlayer == player
                                           where o.ObjectName.ToLower().Replace(" ", string.Empty) == name.ToLower().Replace(" ", string.Empty)
                                           select o.GameObjectReferance).FirstOrDefault();
                if (returnObject != null)
                    return returnObject;
            }

            //Same Thing as above except in command syntax instead of query syntax
            //GameObject returnObject = ActiveBuildingList.FirstOrDefault(o => (o.OwningPlayer == player) && (o.ObjectName == name)).GameObjectReferance;

            throw new ObjectNotFoundException("GameObject: " + name + ". Not Found in " + list + "BuildingList");
        }

        //Finds Unit objects with an object ID throws exception if not found
        private InstantiatedObjectsListType FindUnitObject(int objectID, string list)
        {
            InstantiatedObjectsListType searchResult;
            if (list.ToLower() == "active")
            {
                searchResult = ActiveUnitList[objectID];
                if (searchResult != null)
                {
                    return searchResult;
                }
            }
            else if (list.ToLower() == "disabled")
            {
                searchResult = DisabledUnitList[objectID];
                if (searchResult != null)
                {
                    return searchResult;
                }
            }
            throw new ObjectNotFoundException("Object ID: " + objectID + " Not Found In " +list + "UnitList");
        }

        //Uses a LINQ query to find a building object by name and player
        private GameObject FindUnitObject(string name, Player player, string list)
        {
            GameObject returnObject;

            if (list.ToLower() == "active")
            {
                returnObject = (from o in ActiveUnitList
                                where o.OwningPlayer == player
                                where o.ObjectName.ToLower().Replace(" ", string.Empty) == name.ToLower().Replace(" ", string.Empty)
                                select o.GameObjectReferance).FirstOrDefault();
                if (returnObject != null)
                    return returnObject;
            }
            else if (list.ToLower() == "disabled")
            {
                returnObject = (from o in DisabledUnitList
                                where o.OwningPlayer == player
                                where o.ObjectName.ToLower().Replace(" ", string.Empty) == name.ToLower().Replace(" ", string.Empty)
                                select o.GameObjectReferance).FirstOrDefault();
                if (returnObject != null)
                    return returnObject;
            }

            throw new ObjectNotFoundException("GameObject: " + name + ". Not Found in " + list + "UnitList");
        }

        //Finds UserInterface objects with an object ID throws exception if not found
        private InstantiatedObjectsListType FindUIObject(int objectID, string list)
        {
            InstantiatedObjectsListType searchResult;
            if (list.ToLower() == "active")
            {
                searchResult = ActiveUIList[objectID];
                if (searchResult != null)
                {
                    return searchResult;
                }
            }
            else if (list.ToLower() == "disabled")
            {
                searchResult = DisabledUIList[objectID];
                if (searchResult != null)
                {
                    return searchResult;
                }
            }

            throw new ObjectNotFoundException("Object ID: " + objectID + " Not Found In " + list + "UIList");
        }

        //Uses a LINQ query to find a UI object by name and player
        private GameObject FindUIObject(string name, Player player, string list)
        {
            GameObject returnObject;
            if (list.ToLower() == "active")
            {
                returnObject = (from o in ActiveUIList
                                where o.OwningPlayer == player
                                where o.ObjectName.ToLower().Replace(" ", string.Empty) == name.ToLower().Replace(" ", string.Empty)
                                select o.GameObjectReferance).FirstOrDefault();
                if (returnObject != null)
                    return returnObject;
            }
            else if (list.ToLower() == "disabled")
            {
                returnObject = (from o in DisabledUIList
                                where o.OwningPlayer == player
                                where o.ObjectName.ToLower().Replace(" ", string.Empty) == name.ToLower().Replace(" ", string.Empty)
                                select o.GameObjectReferance).FirstOrDefault();
                if (returnObject != null)
                    return returnObject;
            }

            throw new ObjectNotFoundException("GameObject: " + name + ". Not Found in " + list + "UIList");
        }

        //Finds Manager objects with an object ID throws exception if not found
        private InstantiatedObjectsListType FindManagerObject(int objectID, string list)
        {
            InstantiatedObjectsListType searchResult;
            if (list.ToLower() == "active")
            {
                searchResult = ActiveManagersList[objectID];
                if (searchResult != null)
                {
                    return searchResult;
                }
            }
            else if (list.ToLower() == "disabled")
            {
                searchResult = DisabledManagersList[objectID];
                if (searchResult != null)
                {
                    return searchResult;
                }
            }

            throw new ObjectNotFoundException("Object ID: " + objectID + " Not Found In " + list + "ManagersList");
        }

        //Uses a LINQ query to find a manager object by name and player
        private GameObject FindManagerObject(string name, Player player, string list)
        {
            GameObject returnObject;
            if (list.ToLower() == "active")
            {
                returnObject = (from o in ActiveManagersList
                                where o.OwningPlayer == player
                                where o.ObjectName.ToLower().Replace(" ", string.Empty) == name.ToLower().Replace(" ", string.Empty)  
                                select o.GameObjectReferance).FirstOrDefault();
                if (returnObject != null)
                    return returnObject;
            }
            else if (list.ToLower() == "disabled")
            {
                returnObject = (from o in DisabledManagersList
                                where o.OwningPlayer == player
                                where o.ObjectName.ToLower().Replace(" ", string.Empty) == name.ToLower().Replace(" ", string.Empty)
                                select o.GameObjectReferance).FirstOrDefault();
                if (returnObject != null)
                    return returnObject;
            }

            throw new ObjectNotFoundException("GameObject: " + name + ". Not Found in " + list + "ManagersList");
        }

        //Finds Unknown objects with an object ID throws exception if not found
        private InstantiatedObjectsListType FindUnknownObject(int objectID)
        {
            if(ActiveUnitList.Contains(objectID))
            {
                return ActiveUnitList[objectID];
            }
            else if(ActiveBuildingList.Contains(objectID))
            {
                return ActiveBuildingList[objectID];
            }
            else if(ActiveUIList.Contains(objectID))
            {
                return ActiveUIList[objectID];
            }
            else if(ActiveManagersList.Contains(objectID))
            {
                return ActiveManagersList[objectID];
            }


            throw new ObjectNotFoundException("Object ID: " + objectID + " Not Found In Any List");
        }

        //Finds Unknown objects with an object ID throws exception if not found. Is really long.....
        private GameObject FindUnknownObject(int objectID, Player player)
        {
            if (ActiveUnitList.Contains(objectID))
            {
                return ActiveUnitList[objectID].GameObjectReferance;
            }
            else if (ActiveBuildingList.Contains(objectID))
            {
                return ActiveBuildingList[objectID].GameObjectReferance;
            }
            else if (ActiveUIList.Contains(objectID))
            {
                return ActiveUIList[objectID].GameObjectReferance;
            }
            else if (ActiveManagersList.Contains(objectID))
            {
                return ActiveManagersList[objectID].GameObjectReferance;
            }
            else if(DisabledUnitList.Contains(objectID))
            {
                return DisabledUnitList[objectID].GameObjectReferance;
            }
            else if(DisabledBuildingList.Contains(objectID))
            {
                return DisabledBuildingList[objectID].GameObjectReferance;
            }
            else if(DisabledUIList.Contains(objectID))
            {
                return DisabledUIList[objectID].GameObjectReferance;
            }
            else if(DisabledManagersList.Contains(objectID))
            {
                return DisabledManagersList[objectID].GameObjectReferance;
            }


            throw new ObjectNotFoundException("Object ID: " + objectID + " Not Found In Any List");
        }
    }
}

