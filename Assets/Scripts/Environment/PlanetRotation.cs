﻿using UnityEngine;
using System;
using System.Collections;

public class PlanetRotation : MonoBehaviour {

    public int rotationSpeed;

	public void Start () 
    {
	
	}
	
	public void Update () 
    {
        RotatePlanet();
	}

    private void RotatePlanet()
    {
        //Vector3 rotation = transform.rotation.eulerAngles;
        //rotation.y = (rotation.y * (rotationSpeed * Time.deltaTime));
        //transform.eulerAngles = rotation;
        transform.Rotate(Vector3.up * (rotationSpeed * Time.deltaTime));
    }
}
