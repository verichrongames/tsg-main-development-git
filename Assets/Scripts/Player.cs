﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CoreSys;
using CoreSys.Buildings;


public class Player : Bolt.EntityBehaviour<IPlayerState>
{

    //Remove, has dependants in HUD. 
    public IPlayerObject SelectedObject { get; set; }


    public string username; //{ get; set; }
    public bool human; //{ get; set; }

    public BlockHandler blockHandler;

    public bool inBuildingBlockMode = false;

    public override void Attached()
    {
        base.Attached();

    }

    void OnEnable()
    {
    }

    void OnDisable()
    {
    }

    void Awake()
    {
    }


    void Start()
    {
    }

    /* Preventing Script execution if not neccesary.
    void Update()
    {
    }
    */ 
}
