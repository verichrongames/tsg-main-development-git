﻿using UnityEngine;
using System;
using System.Collections;
using System.Text;
using System.Net;
using System.Net.Sockets;

public static class GameManager
{
    public static TcpClient serverSocket;
    public static string Username, UIP;
    public static bool loggedIn = false;
    public static int UID;
    public static UsernameDisplay userHandler;
    public static int vA, vB, vC, vD;
    public static string vDa;

    private static void SetVersion()
    {
        vA = 0;
        vB = 0;
        vC = 0;
        vD = 1;
        vDa = "7Da";
    }

    public static string GetGameVersion()
    {
        SetVersion();
        string returnString = String.Format("{0}.{1}.{2}.{3}.{4}", vA, vB, vC, vD, vDa);
        return returnString;
    }

    public static GameVersionMessage GetGameVersionMessage()
    {
        SetVersion();
        GameVersionMessage versionMessage = new GameVersionMessage(vA, vB, vC, vD, vDa);
        return versionMessage;
    }

    public static void SetUserName(string username)
    {
        Username = username;
        if (userHandler)
            userHandler.resetUsername();
        else
        {
            Debug.Log("Error: userHandler not Initialized");
        }
    }

    public static void SetUID(int ID)
    {
        UID = ID;
        if (userHandler)
            userHandler.resetUID();
        else
        {
            Debug.Log("Error: userHandler not Initialized");
        }
    }

    public static void SetUIP(string IP)
    {
        UIP = IP;
        if (userHandler)
            userHandler.resetUIP();
        else
        {
            Debug.Log("Error: userHandler not Initialized");
        }
    }
}
