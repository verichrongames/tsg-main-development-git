﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;

/*============================================
 *  This Class provides management of object
 *  movement queues. It also moves objects, this will be
 *  split into another class shortly.
 * Blame: Doug
 * ==========================================*/

public class ObjectMovementManager : MonoBehaviour {

    //List<GameObjectMovesList> movementLists = new List<GameObjectMovesList>();
    Dictionary<int, GameObjectMovesList> movementLists = new Dictionary<int, GameObjectMovesList>();

    bool destinationsToRemove = false;
    List<int> removalDestiantionsList = new List<int>();

	void Start () 
    {
	}
	
	void Update () 
    {
        //if (movementLists.Count > 0)
        //{
        //    RotateObjects();
        //    MoveObjectToDestination();
        //    CheckUnitRotation();
        //    RemoveAllDestinationsForObject();
        //}
	}

    /*=============================================================
     *  Start Object and destination list creation and modification
     * ==========================================================*/

    //Recieves the public call to add a new object to the list or add additional destinations
    public void NewDestination(GameObject gameObject, RClickEventArgs e, bool shiftHeld)
    {
        int ID = gameObject.GetInstanceID();
        bool inList = movementLists.ContainsKey(ID);

        if(!shiftHeld)
        {
            if (inList)
            {
                ClearObjectDestinations(ID);
                AddDestination(e, ID);
                movementLists[ID].MoveableObject.Turning = true;
                movementLists[ID].MovementRotation = SetRotationAngle(movementLists[ID].MoveableObject, movementLists[ID].DestinationList[0]);
            }
            else
            {
                AddNewObjectToList(gameObject, e);
            }
        }
        else
        {
            if (inList)
            {
                AddDestination(e, ID);
            }
            else
            {
                AddNewObjectToList(gameObject, e);
            }
        }


    }

    //Public Call that just passes a gameobject and vector3 destiantion
    public void NewDestination(GameObject gameObject, Vector3 destination)
    {
        int ID = gameObject.GetInstanceID();
        bool inList = movementLists.ContainsKey(ID);

        if (inList)
        {
            ClearObjectDestinations(ID);
            AddDestination(destination, ID);
            movementLists[ID].MoveableObject.Turning = true;
            movementLists[ID].MovementRotation = SetRotationAngle(movementLists[ID].MoveableObject, movementLists[ID].DestinationList[0]);
        }
        else
        {
            AddNewObjectToList(gameObject, destination);
        }
    }

    //Removes current destination and angle, and sets next destination and angle
    private void NextDestination(GameObject gameObject)
    {
        int ID = gameObject.GetInstanceID();
        bool inList = movementLists.ContainsKey(ID);

        if (inList)
        {
            RemoveDestination(ID, 0);
            movementLists[ID].MovementRotation = SetRotationAngle(movementLists[ID].MoveableObject, movementLists[ID].DestinationList[0]);
        }
    }
    //Overload with index instead
    private void NextDestination(int instanceID)
    {
        RemoveDestination(instanceID, 0);
        movementLists[instanceID].MovementRotation = SetRotationAngle(movementLists[instanceID].MoveableObject, movementLists[instanceID].DestinationList[0]);
    }

    //Removes All Remaining Destinations for the object, as well as the object from the list.
    //Used if object is destroyed, or if it reaches it's end destination
    private void RemoveAllDestinationsForObject(GameObject gameObject)
    {
        int ID = gameObject.GetInstanceID();
        bool inList = movementLists.ContainsKey(ID);

        if (inList)
        {
            gameObject.RemoveCollectionReferance(this);
            movementLists.Remove(ID);
        }
    }
    //Overload with index instead   /old?
    private void RemoveAllDestinationsForObject()
    {
        if (destinationsToRemove)
        {
            foreach (int ID in removalDestiantionsList)
            {
                movementLists[ID].GameObject.RemoveCollectionReferance(this);
                movementLists.Remove(ID);
            }
            removalDestiantionsList.Clear();
        }
    }

    //Adds a new object entry to the list
    private void AddNewObjectToList(GameObject gameObject, RClickEventArgs e)
    {
        List<Vector3> newDestinationList = new List<Vector3>();
        newDestinationList.Add(e.HitPoint);
        IMoveableObject moveableObject = GetIMoveableObject(gameObject);
        float movementRotation = SetRotationAngle(moveableObject, e.HitPoint);

        moveableObject.Turning = true;
        moveableObject.Moving = true;

        movementLists.Add(gameObject.GetInstanceID(), new GameObjectMovesList(gameObject, gameObject.GetInstanceID(), moveableObject, newDestinationList, movementRotation));
        gameObject.AddCollectionReferance(this);
    }

    //Overload for just vector 3
    private void AddNewObjectToList(GameObject gameObject, Vector3 destination)
    {
        List<Vector3> newDestinationList = new List<Vector3>();
        newDestinationList.Add(destination);
        IMoveableObject moveableObject = GetIMoveableObject(gameObject);
        float movementRotation = SetRotationAngle(moveableObject, destination);

        moveableObject.Turning = true;
        moveableObject.Moving = true;

        movementLists.Add(gameObject.GetInstanceID(), new GameObjectMovesList(gameObject, gameObject.GetInstanceID(), moveableObject, newDestinationList, movementRotation));
        gameObject.AddCollectionReferance(this);
    }

    //Adds destination to the list of an object already in the list
    private void AddDestination(RClickEventArgs e, int instanceID)
    {
        movementLists[instanceID].DestinationList.Add(e.HitPoint);
    }

    //Overlaod for just a vector3
    private void AddDestination(Vector3 destination, int instanceID)
    {
        movementLists[instanceID].DestinationList.Add(destination);
    }

    //Clears the destinations list for an object
    private void ClearObjectDestinations(int instanceID)
    {
        movementLists[instanceID].DestinationList.Clear();
    }

    //Removes a specific destination from a specific object
    private void RemoveDestination(int instanceID, int destinationIndex)
    {
        movementLists[instanceID].DestinationList.RemoveAt(destinationIndex);
    }

    //Gets the IMoveableObject type of the game object
    private IMoveableObject GetIMoveableObject(GameObject gameObject)
    {
        return (IMoveableObject)gameObject.GetComponent(typeof(IMoveableObject));
    }

    //Determines the roatation angle for the next destination. Returns float
    private float SetRotationAngle(IMoveableObject moveableObject, Vector3 destination)
    {
        Vector3 angleAdjustment = destination - moveableObject.transformV.position;
        return (Mathf.Atan2(angleAdjustment.y, angleAdjustment.x) * Mathf.Rad2Deg - 90);
    }

    //Returns the list of destinations for an object.
    private List<Vector3> GetDestinationList(GameObject gameObject)
    {
        int ID = gameObject.GetInstanceID();
        bool inList = movementLists.ContainsKey(ID);

        if (inList)
        {
            return movementLists[ID].DestinationList;
        }
        return null; //This is bad, fix.
    }

    //Returns the movement list
    public List<GameObjectMovesList> GetMovementLists()
    {
        if(movementLists.Count > 0)
        {
            List<GameObjectMovesList> temp = movementLists.Values.ToList();
            return temp;
        }

        return null;
    }

    public List<Vector3> GetDestinationsList(int ID)
    {
        if (movementLists.Count > 0 && movementLists.ContainsKey(ID))
        {
            return movementLists[ID].DestinationList;
        }

        else return null;
    }

    //Public method for referance removal once an object is destroyed
    public void RemoveReferance(int ID)
    {
        if(movementLists.ContainsKey(ID))
        {
            movementLists.Remove(ID);
        }
    }


    /*===================================================================
     *        ====== Object Movement ======
     * ==============================================================*/

    //Moves objects to their destination. on Update()
    private void MoveObjectToDestination()
    {
        foreach(KeyValuePair<int, GameObjectMovesList> objectMovement in movementLists)
        {
            if (objectMovement.Value.MoveableObject.Moving && !objectMovement.Value.MoveableObject.Turning)
            {
                //objectMovement.Value.GameObject.GetComponent<Rigidbody2D>().AddForce(objectMovement.Value.MoveableObject.transformV.position + objectMovement.Value.MoveableObject.transformV.up * 100);
                //objectMovement.Value.GameObject.GetComponent<Rigidbody2D>().

                objectMovement.Value.MoveableObject.transformV.position = Vector3.MoveTowards(objectMovement.Value.MoveableObject.transformV.position, objectMovement.Value.DestinationList[0], Time.deltaTime * objectMovement.Value.MoveableObject.MoveSpeed);
                CheckDestination(objectMovement.Key);
            }
        }
    }

    //Checks if the destination has been reached. Called from movement
    private void CheckDestination(int instanceID)
    {
        if (movementLists[instanceID].MoveableObject.transformV.position == movementLists[instanceID].DestinationList[0])
        {
            if (movementLists[instanceID].DestinationList.Count > 1)
            {
                NextDestination(instanceID);
                movementLists[instanceID].MoveableObject.Turning = true;
            }
            else
            {
                movementLists[instanceID].MoveableObject.Moving = false;
                destinationsToRemove = true;
                removalDestiantionsList.Add(instanceID);
            }
        }
    }

    /*===================================================================
     *        ====== Object Rotations ======
     * ==============================================================*/

    //Will check each units rotation on update. If rotation is within certain bounds rotation will stop
    private void CheckUnitRotation()
    {
        foreach (KeyValuePair<int, GameObjectMovesList> objectMovement in movementLists)
        {
            if(objectMovement.Value.MoveableObject.Turning)
            {
                if (objectMovement.Value.MovementRotation < 0)
                {
                    float checkAngle = 360 + objectMovement.Value.MovementRotation;
                    if (checkAngle < (objectMovement.Value.MoveableObject.transformV.rotation.eulerAngles.z + 0.5f) && checkAngle > (objectMovement.Value.MoveableObject.transformV.rotation.eulerAngles.z - 0.5f))
                    {
                        objectMovement.Value.MoveableObject.Turning = false;
                    }
                }
                else if (objectMovement.Value.MovementRotation > 0)
                {
                    float checkAngle = objectMovement.Value.MovementRotation;
                    if (checkAngle < (objectMovement.Value.MoveableObject.transformV.rotation.eulerAngles.z + 0.5f) && checkAngle > (objectMovement.Value.MoveableObject.transformV.rotation.eulerAngles.z - 0.5f))
                    {
                        objectMovement.Value.MoveableObject.Turning = false;
                    }
                }
            }
        }
    }

    //Will Rotate Each Object on Update()
    private void RotateObjects()
    {
        foreach (KeyValuePair<int, GameObjectMovesList> objectMovement in movementLists)
        {
            if (objectMovement.Value.MoveableObject.Turning)
            {
                Quaternion q = Quaternion.AngleAxis(objectMovement.Value.MovementRotation, Vector3.forward);
                objectMovement.Value.MoveableObject.transformV.rotation = Quaternion.Slerp(objectMovement.Value.MoveableObject.transformV.rotation, q, Time.deltaTime * objectMovement.Value.MoveableObject.TurnSpeed);
            }
        }
    }

}
