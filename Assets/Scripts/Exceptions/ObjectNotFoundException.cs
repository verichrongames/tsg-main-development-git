﻿using UnityEngine;
using System;
using System.Collections;
using CoreSys;

namespace CoreSys.Exceptions
{
    public class ObjectNotFoundException : Exception
    {

        public ObjectNotFoundException(string message)
            : base(message)
        {
        }

    }
}

