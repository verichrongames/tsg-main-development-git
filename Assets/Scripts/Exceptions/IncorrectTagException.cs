﻿using UnityEngine;
using System;
using System.Collections;
using CoreSys;


namespace CoreSys.Exceptions
{
    public class IncorrectTagException : Exception
    {

        public IncorrectTagException(string message)
            : base(message)
        {
        }

    }
}

