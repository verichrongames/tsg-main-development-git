﻿using UnityEngine;
using System;
using System.Collections;
using CoreSys;


namespace CoreSys.Exceptions
{
    public class NotEnabledException : Exception
    {

        public NotEnabledException(string message)
            : base(message)
        {
        }

    }
}

