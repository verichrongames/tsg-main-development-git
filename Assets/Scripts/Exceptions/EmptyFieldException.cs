﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreSys;

namespace CoreSys.Exceptions
{
    public class EmptyFieldException : Exception
    {

        public EmptyFieldException(string message)
            : base(message)
        {
        }
    }
}
