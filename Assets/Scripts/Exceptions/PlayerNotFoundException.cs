﻿using UnityEngine;
using System;
using System.Collections;
using CoreSys;


namespace CoreSys.Exceptions
{
    public class PlayerNotFoundException : Exception
    {

        public PlayerNotFoundException(string message)
            : base(message)
        {
        }

    }
}

