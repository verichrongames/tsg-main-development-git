﻿using UnityEngine;
using System;
using System.Collections;
using CoreSys;


namespace CoreSys.Exceptions
{
    public class ObjectNotInstantiatedException : Exception
    {

        public ObjectNotInstantiatedException(string message)
            : base(message)
        {
        }

    }
}

