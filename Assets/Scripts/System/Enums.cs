/* Enums.cs
 * I believe this is where stuff like sprites for HUD objects and such are kept, not entirely sure
 * 
 * 
 * 
 * 
 */

using UnityEngine;
using System.Collections;

namespace CoreSys {
	public enum CursorState { Select, Move, Attack, PanLeft, PanRight, PanUp, PanDown, Harvest, RallyPoint }
	public enum ResourceType { Metal, Power, Ore, Unknown }
}