﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoreSys.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// A better string comparison method
        /// </summary>
        /// <param name="source"></param>
        /// <param name="toCheck"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            return source.IndexOf(toCheck, comp) >= 0;
        }
    }
}
