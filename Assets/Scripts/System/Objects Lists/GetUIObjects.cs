﻿using UnityEngine;
using System.Collections;
using CoreSys;
using CoreSys.Objects;

namespace CoreSys.Objects
{
    public class GetUIObjects : MonoBehaviour
    {

        private static UIObjectsList objectsList;

        void Awake()
        {
        }
        void Start()
        {

        }


        void Update()
        {

        }

        //Recieves a referance from the UIObjectslist
        public static void SetUIObjectsList(UIObjectsList list)
        {
            objectsList = list;
        }

        public static GameObject GetBuildButtons(string name)
        {
            return objectsList.GetBuildButtons(name);
        }

        public static GameObject GetBuildQueueItems(string name)
        {
            return objectsList.GetBuildQueueItems(name + "QueueItem");
        }

        public static GameObject GetUserInterface(string name)
        {
            return objectsList.GetUserInterface(name);
        }
    }
}

