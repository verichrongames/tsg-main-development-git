﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreSys.Objects;

namespace CoreSys.Objects
{
    public class BuildableObjectsList : MonoBehaviour
    {

        public GameObject[] buildableObjects;


        void Awake()
        {
            GetBuildableObjects.SetBuildableObjectsList(this);
        }

        void Start()
        {
            DontDestroyOnLoad(transform.gameObject);
        }


        void Update()
        {

        }

        public GameObject GetBuildables(string name)
        {
            for (int i = 0; i < this.buildableObjects.Length; i++)
            {
                if (this.buildableObjects[i].name == name) return this.buildableObjects[i];
            }
            return null;
        }
    }
}
