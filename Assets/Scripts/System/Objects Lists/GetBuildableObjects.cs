﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreSys;
using CoreSys.Objects;

namespace CoreSys.Objects
{
    public class GetBuildableObjects
    {
        static BuildableObjectsList objectsList;

        public static void SetBuildableObjectsList(BuildableObjectsList list)
        {
            objectsList = list;
        }

        public static GameObject GetBuildables(string name)
        {
            return objectsList.GetBuildables(name);
        }
    }
}
