﻿using UnityEngine;
using System.Collections;
using CoreSys;

/*==================================
 * This Class Provides a list of
 * UI Objects that can be retireved by 
 * name. These objects are set in the inspector
 * ================================*/

namespace CoreSys.Objects
{
    public class UIObjectsList : MonoBehaviour
    {

        public GameObject[] userInterface;
        public GameObject[] buildButtons;
        public GameObject[] buildQueueItems;


        void Awake()
        {
            GetUIObjects.SetUIObjectsList(this); //Sets list referance for Get class
        }

        void Start()
        {
            DontDestroyOnLoad(transform.gameObject);
        }


        void Update()
        {

        }

        public GameObject GetBuildButtons(string name)
        {
            for (int i = 0; i < this.buildButtons.Length; i++)
            {
                if (this.buildButtons[i].name == name) return this.buildButtons[i];
            }
            Debug.Log("return null");
            return null;
        }

        public GameObject GetBuildQueueItems(string name)
        {
            for (int i = 0; i < this.buildQueueItems.Length; i++)
            {
                if (this.buildQueueItems[i].name == name) return this.buildQueueItems[i];
            }
            return null;
        }

        public GameObject GetUserInterface(string name)
        {
            for (int i = 0; i < this.userInterface.Length; i++)
            {
                if (this.userInterface[i].name == name) return this.userInterface[i];
            }
            return null;
        }
    }
}
