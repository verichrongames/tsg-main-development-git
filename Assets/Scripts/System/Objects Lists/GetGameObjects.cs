﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreSys;
using CoreSys.Objects;

namespace CoreSys.Objects
{
    class GetGameObjects
    {
        static GameObjectList objectList;

        public static void SetGameObjectList(GameObjectList list)
        {
            objectList = list;
        }

        public static GameObject GetBuilding(string name)
        {
            return objectList.GetBuilding(name);
        }

        public static GameObject GetUnit(string name)
        {
            return objectList.GetUnit(name);
        }

        public static GameObject GetPlayerObject(string name)
        {
            return objectList.GetPlayerObject(name);
        }

        public static GameObject GetCore(string name)
        {
            return objectList.GetCore(name);
        }

        public static GameObject GetBlock(string name)
        {
            return objectList.GetBlock(name);
        }

        public static GameObject GetPlayerObject()
        {
            return objectList.GetPlayerObject();
        }

        public static GameObject GetSystem(string name)
        {
            return objectList.GetSystem(name);
        }

        public static GameObject GetSelectionBox(string name)
        {
            return objectList.GetSystem(name);
        }

        public static GameObject GetBuildableObjects(string name)
        {
            return objectList.GetSystem(name);
        }
    }
}
