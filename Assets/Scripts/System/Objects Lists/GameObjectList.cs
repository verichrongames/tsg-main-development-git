﻿using UnityEngine;
using System.Collections;
using CoreSys;
using CoreSys.Objects;
using CoreSys.Buildings;

namespace CoreSys.Objects
{
    public class GameObjectList : MonoBehaviour
    {

        public GameObject[] buildings;
        public GameObject[] units;
        public GameObject[] playerObjects;
        public GameObject[] cores;
        public GameObject[] blocks;
        public GameObject[] system;
        public GameObject player;
        private static bool created = false;


        void Awake()
        {
            if (!created)
            {
                DontDestroyOnLoad(transform.gameObject);
                GetGameObjects.SetGameObjectList(this);
                GetGameObjects.SetGameObjectList(this);
                created = true;
            }
            else
            {
                Destroy(this.gameObject);
            }
        }

        void Start()
        {

        }

        void Update()
        {

        }

        public GameObject GetBuilding(string name)
        {
            for (int i = 0; i < buildings.Length; i++)
            {
                Building building = buildings[i].GetComponent<Building>();
                if (building && building.name == name) return buildings[i];
            }
            return null;
        }

        public GameObject GetUnit(string name)
        {
            for (int i = 0; i < units.Length; i++)
            {
                Unit unit = units[i].GetComponent<Unit>();
                if (unit && unit.name == name) return units[i];
            }
            return null;
        }

        public GameObject GetPlayerObject(string name)
        {
            foreach (GameObject playerObject in playerObjects)
            {
                if (playerObject.name == name) return playerObject;
            }
            return null;
        }

        public GameObject GetCore(string name)
        {
            for (int i = 0; i < cores.Length; i++)
            {
                Core core = cores[i].GetComponent<Core>();
                if (core && core.name == name) return cores[i];
            }
            return null;
        }

        public GameObject GetBlock(string name)
        {
            for (int i = 0; i < blocks.Length; i++)
            {
                Block block = blocks[i].GetComponent<Block>();
                if (block && block.name == name) return blocks[i];
            }
            return null;
        }

        public GameObject GetSystem(string name)
        {
            for (int i = 0; i < system.Length; i++)
            {
                if (system[i].name == name) return system[i];
            }
            return null;
        }

        public GameObject GetPlayerObject()
        {
            return player;
        }

        //public Texture2D GetBuildImage(string name)
        //{
        //    for (int i = 0; i < buildings.Length; i++)
        //    {
        //        Building building = buildings[i].GetComponent<Building>();
        //        if (building && building.name == name) return building.buildImage;
        //    }
        //    for (int i = 0; i < units.Length; i++)
        //    {
        //        Unit unit = units[i].GetComponent<Unit>();
        //        if (unit && unit.name == name) return unit.buildImage;
        //    }
        //    return null;
        //}
    }
}

