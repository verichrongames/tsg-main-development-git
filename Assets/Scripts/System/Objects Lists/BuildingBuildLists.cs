﻿using UnityEngine;
using System.Collections;
using CoreSys;

/*==================================
 * This Class Provides a list of
 * UI Objects for the BuildQueue that can be retireved by 
 * name. These objects are set in the inspector
 * ================================*/

namespace CoreSys.Objects
{
    public class BuildingBuildLists : MonoBehaviour
    {

        public GameObject[] coreBuildingList;

        void Start()
        {

        }


        void Update()
        {

        }

        public GameObject GetCoreBuildingList(string name)
        {
            for (int i = 0; i < coreBuildingList.Length; i++)
            {
                if (coreBuildingList[i].name == name) return coreBuildingList[i];
            }
            return null;
        }
    }
}

