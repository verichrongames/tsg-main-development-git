﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/*==============================================================
 * Used to define custom structured types as part of the CoreSys namespace
 * ============================================================*/
namespace CoreSys
{
    public class Types
    {
        public struct MousePosition
        {
            public float x;
            public float y;
        }
    }
}
