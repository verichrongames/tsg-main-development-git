﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace CoreSys.Math
{
    public static class GameMath
    {
        private static Vector3 projectilePosition;
        private static Vector3 projectileVector;
        private static float projectileSpeed;


        private static Vector3 defencePosition;
        private static float defenceSpeed;

        private static float t1, t2;


        //public static Vector3 CalculateInterceptTrajectory(Rigidbody2D projectile, Vector3 projectileLocation, float projectileVelocity, Vector3 defenceLocation, float defenceVelocity)
        //{
        //    defenceSpeed = defenceVelocity;
        //    defencePosition = defenceLocation;
        //    projectilePosition = projectileLocation;
        //    projectileVector = projectile.velocity;
        //    projectileSpeed = projectileVelocity;
        //    DetermineImpactTime();
        //    float t = SmallestValidNumber();
        //    return GetTargetVector(t);
        //}

        //private static void DetermineImpactTime() //Some pythagoran shit up in this bitch
        //{
        //    float a = (projectileVector.x * projectileVector.x) + (projectileVector.y * projectileVector.y) - (defenceSpeed * defenceSpeed);
        //    float b = 2 * ((projectilePosition.x * projectileVector.x) + (projectilePosition.y * projectileVector.y) - (defencePosition.x * projectileVector.x) - (defencePosition.y * projectileVector.y));
        //    float c = (projectilePosition.x * projectilePosition.x) + (projectilePosition.y * projectilePosition.y) + (defencePosition.x * defencePosition.x) + (defencePosition.y * defencePosition.y) - (2 * defencePosition.x * projectilePosition.x) - (2 * defencePosition.y * projectilePosition.y);

        //    float t1 = (-b + Mathf.Sqrt((b * b) - (4 * a * c))) / (2 * a);
        //    float t2 = (-b - Mathf.Sqrt((b * b) - (4 * a * c))) / (2 * a);
        //}

        //private static float SmallestValidNumber()
        //{
        //    DetermineImpactTime();
        //    if (t1 > 0 || t2 > 0) //save checks if it cant intercept
        //    {
        //        if (t1 != float.NaN && t1 > 0)
        //        {
        //            if (t1 < t2 || t2 == float.NaN)
        //                return t1;
        //        }
        //        else if (t2 != float.NaN && t2 > 0)
        //        {
        //            if (t2 < t1 || t2 == float.NaN)
        //                return t2;
        //        }
        //        return -1.0f;
        //    }
        //    else
        //    {
        //        return -1.0f;
        //    }
        //}

        //private static Vector3 GetTargetVector(float t)
        //{
        //    Vector3 flip;
        //    Vector3 returnVector = -((projectilePosition - defencePosition + (t * projectileSpeed * projectileVector)) / (t * defenceSpeed));
        //    //flip.x = returnVector.x;
        //    //flip.y = returnVector.y;
        //    //returnVector.y = flip.x;
        //    //returnVector.x = flip.y;
        //    return returnVector;
        //}

        //Solves triangles using the law of sines
        public static float LawOfSines(SinSolveFor solveFor, float alpha, float beta, float side)
        {           
            if(solveFor == SinSolveFor.Side)
            {
                return side * (Mathf.Sin(alpha) / Mathf.Sin(beta));
            }
            else
            {
                return -1;
            }

        }

        public static Vector3 FindInterceptVector(Vector3 shotOrigin, float shotSpeed, Vector3 targetOrigin, Vector3 targetVel)
        {

            Vector3 dirToTarget = Vector3.Normalize(targetOrigin - shotOrigin);

            // Decompose the target's velocity into the part parallel to the
            // direction to the cannon and the part tangential to it.
            // The part towards the cannon is found by projecting the target's
            // velocity on dirToTarget using a dot product.
            Vector3 targetVelOrth =
            Vector3.Dot(targetVel, dirToTarget) * dirToTarget;

            // The tangential part is then found by subtracting the
            // result from the target velocity.
            Vector3 targetVelTang = targetVel - targetVelOrth;

            /*
            * targetVelOrth
            * |
            * |
            *
            * ^...7  <-targetVel
            * |  /.
            * | / .
            * |/ .
            * t--->  <-targetVelTang
            *
            *
            * s--->  <-shotVelTang
            *
            */

            // The tangential component of the velocities should be the same
            // (or there is no chance to hit)
            // THIS IS THE MAIN INSIGHT!
            Vector3 shotVelTang = targetVelTang;

            // Now all we have to find is the orthogonal velocity of the shot

            float shotVelSpeed = shotVelTang.magnitude;
            if (shotVelSpeed > shotSpeed)
            {
                // Shot is too slow to intercept target, it will never catch up.
                // Do our best by aiming in the direction of the targets velocity.
                return targetVel.normalized * shotSpeed;
            }
            else
            {
                // We know the shot speed, and the tangential velocity.
                // Using pythagoras we can find the orthogonal velocity.
                float shotSpeedOrth =
                Mathf.Sqrt(shotSpeed * shotSpeed - shotVelSpeed * shotVelSpeed);
                Vector3 shotVelOrth = dirToTarget * shotSpeedOrth;

                // Finally, add the tangential and orthogonal velocities.
                return shotVelOrth + shotVelTang;
            }
        }
    }

    public enum SinSolveFor
    {
        Side,
    }
}