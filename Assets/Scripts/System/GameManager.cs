﻿// GameServerBehaviour.cs
 /*
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
[BoltGlobalBehaviour(BoltNetworkModes.Server, "Game Scene")]
public class GameServerBehaviour : Bolt.GlobalEventListener
{
   private Queue<BoltConnection> queuedSpawns = new Queue<BoltConnection>();
 
   private bool loaded = false;
 
   List<PlayerData> readyPlayers = new List<PlayerData>();
 
   public override void SceneLoadLocalBegin(string map)
   {
       this.loaded = false;
       this.readyPlayers.Clear();
   }
 
   public override void SceneLoadLocalDone(string map)
   {
        // Configure server stuff here
 
       this.loaded = true;
 
       while (this.queuedSpawns.Count > 0)
       {
           this.SpawnPlayer(this.queuedSpawns.Dequeue());
       }
 
       this.StartCoroutine("ReadyGameCoroutine");
   }
 
   public override void SceneLoadRemoteDone(BoltConnection connection)
   {
       if (!loaded)
       {
           this.queuedSpawns.Enqueue(connection);
       }
       else
       {
           this.SpawnPlayer(connection);
       }
   }
 
   private void SpawnPlayer(BoltConnection connection)
   {
       var players = GameState.Current.ActivePlayers;
 
       for (int i = 0; i < players.Length; i++)
       {
           if (players[i].ConnectionID == connection.ConnectionId)
           {
               Vector2 position = LevelManager.Instance.GetSpawnPosition(i);
 
               var evnt = SpawnPlayerEvent.Create(connection);
 
               evnt.Position = position;
 
               evnt.Send();
           }
       }
   }
 
   public override void OnEvent(GameStartEvent evnt)
   {
       GameState.Current.state.Playing = true;
   }
 
   public override void OnEvent(PlayerWinEvent evnt)
   {
       GameState.Current.state.Playing = false;
 
       foreach (var player in GameState.Current.ActivePlayers)
       {
           player.CurrentPickup = 0;
           player.HasTreasure = false;
       }
   }
 
   public override void OnEvent(PlayerReadyEvent evnt)
   {
       this.readyPlayers.Add(GameState.Current.GetPlayer((int)evnt.RaisedBy.ConnectionId));
   }
 
   private bool AllPlayersReady()
   {
       foreach (var player in GameState.Current.ActivePlayers)
       {
           if (!this.readyPlayers.Contains(player))
           {
               return false;
           }
       }
 
       return true;
   }
 
   private IEnumerator ReadyGameCoroutine()
   {
       while (!this.AllPlayersReady())
       {
           yield return null;
       }
 
       var evnt = GameStartEvent.Create(Bolt.GlobalTargets.Everyone);
       evnt.Send();
 
       foreach (var t in LoadingScreenUI.Instance.GetComponentsInChildren<Transform>())
       {
           if (t != LoadingScreenUI.Instance.transform)
           {
               t.gameObject.SetActive(false);
           }
       }
 
       this.readyPlayers.Clear();
       this.loaded = false;
       AudioManager.Instance.SetMusic(AudioManager.Instance.Music.BackgroundOne, 1f);
   }
 
   // Lots of other stuff goes here for managing the game server side...
}

 
[BoltGlobalBehaviour(BoltNetworkModes.Client, "Game Scene")]
public class GameClientBehaviour : Bolt.GlobalEventListener
{
   public override void OnEvent(SpawnPlayerEvent evnt)
   {
       BoltNetwork.Instantiate(BoltPrefabs.Player, evnt.Position, Quaternion.identity);
   }
 
   // Lots of other stuff goes here as well for managing the game client side...
}*/