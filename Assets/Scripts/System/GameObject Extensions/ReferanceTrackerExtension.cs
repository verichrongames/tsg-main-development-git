﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreSys.Exceptions;

/*=====================================================
 *  This class is used to extend gameobject to provide
 *  methods to add referances to objects that maintain
 *  collections that will have this game object referanced.
 *  
 * This is necessary so that all referances to an object
 * can be erased in the event of it beign destroyed
 * ===================================================*/

public static class ReferanceTrackerExtension
{

    /*====================================================================
     *            ========= Add Referance Methods =========
     * =================================================================*/

    //Movement Manager Method
    public static void AddCollectionReferance(this GameObject gameObject, ObjectMovementManager referance)
    {
        ReferanceTracker tracker = gameObject.GetComponent<ReferanceTracker>();
        if(CheckTrackerReferance(tracker, gameObject))
        {
            tracker.AddObjectMovementManagerReferance(referance);
        }
    }

    //Selection Manager Method
    public static void AddCollectionReferance(this GameObject gameObject, PlayerSelectionManager referance)
    {
        ReferanceTracker tracker = gameObject.GetComponent<ReferanceTracker>();
        if (CheckTrackerReferance(tracker, gameObject))
        {
            tracker.AddPlayerSelectionManagerReferance(referance);
        }
    }

    //Build queue Manager Method
    public static void AddCollectionReferance(this GameObject gameObject, BuildQueueManager referance)
    {
        ReferanceTracker tracker = gameObject.GetComponent<ReferanceTracker>();
        if (CheckTrackerReferance(tracker, gameObject))
        {
            tracker.AddBuildQueueManagerReferance(referance);
        }
    }

    /*====================================================================
    *            ========= Remove Referance Methods =========
    * =================================================================*/

    //Movement Manager Method
    public static void RemoveCollectionReferance(this GameObject gameObject, ObjectMovementManager referance)
    {
        ReferanceTracker tracker = gameObject.GetComponent<ReferanceTracker>();
        if (CheckTrackerReferance(tracker, gameObject))
        {
            tracker.RemoveObjectMovementManagerReferance(referance);
        }
    }

    //Selection Manager Method
    public static void RemoveCollectionReferance(this GameObject gameObject, PlayerSelectionManager referance)
    {
        ReferanceTracker tracker = gameObject.GetComponent<ReferanceTracker>();
        if (CheckTrackerReferance(tracker, gameObject))
        {
            tracker.RemovePlayerSelectionManagerReferance(referance);
        }
    }

    //Build queue Manager Method
    public static void RemoveCollectionReferance(this GameObject gameObject, BuildQueueManager referance)
    {
        ReferanceTracker tracker = gameObject.GetComponent<ReferanceTracker>();
        if (CheckTrackerReferance(tracker, gameObject))
        {
            tracker.RemoveBuildQueueManagerReferance(referance);
        }
    }

    /*====================================================================
    *            ========= Misc Methods =========
    * =================================================================*/

    //Called when an object is destroyed so any and all referances can be eliminated
    public static void DestroyAllReferances(this GameObject gameObject)
    {
        ReferanceTracker tracker = gameObject.GetComponent<ReferanceTracker>();
        if (CheckTrackerReferance(tracker, gameObject))
        {
            tracker.DestroyAllReferances();
        }
    }

    private static bool CheckTrackerReferance(ReferanceTracker tracker, GameObject gameObject)
    {
        if(tracker == null)
        {
            //throw new MissingComponentException("The ReferanceTracker Component was missing from: " + gameObject.name);
            return false;
        }

        return true;
    }
}

