﻿/*ResourceManager.cs
 * This needs a short explanation
 * 
 * 
 * 
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CoreSys.Objects;

namespace CoreSys {
	public static class ResourceManager {
		//UserInput.CS
		private static Bounds invalidBounds = new Bounds(new Vector3(-99999, -99999, -99999), new Vector3(0, 0, 0));
		public static Bounds InvalidBounds { get { return invalidBounds; } }
		private static Vector3 invalidPosition = new Vector3(-99999, -99999, -99999);
		public static Vector3 InvalidPosition { get { return invalidPosition; } }
		public static float ScrollSpeed { get { return 0.5f; } }
		public static int ScrollWidth { get { return 20; } }
		public static float MinCameraHeight { get { return 1; } }
		public static float MaxCameraHeight { get { return 50; } }

		private static Texture2D healthyTexture, damagedTexture, criticalTexture;
		public static Texture2D HealthyTexture { get { return healthyTexture; } }
		public static Texture2D DamagedTexture { get { return damagedTexture; } }
		public static Texture2D CriticalTexture { get { return criticalTexture; } }
		private static Dictionary< ResourceType, Texture2D > resourceHealthBarTextures;

		public static Texture2D GetResourceHealthBar(ResourceType resourceType) {
			if(resourceHealthBarTextures != null && resourceHealthBarTextures.ContainsKey(resourceType)) return resourceHealthBarTextures[resourceType];
			return null;
		}

		public static void SetResourceHealthBarTextures(Dictionary< ResourceType, Texture2D > images) {
			resourceHealthBarTextures = images;
		}
		
		//HUD.cs
		public static void StoreSelectBoxItems(GUISkin skin, Texture2D healthy, Texture2D damaged, Texture2D critical) {
			selectBoxSkin = skin;
			healthyTexture = healthy;
			damagedTexture = damaged;
			criticalTexture = critical;
		}
		private static GUISkin selectBoxSkin;
		public static GUISkin SelectBoxSkin { get { return selectBoxSkin; } }


		//Building.cs
		public static int BuildSpeed { get { return 2; } }

        //The targeting priorities for each object type
        public static int GetBaseTargetPriority(PlayerObjectType type)
        {
            switch(type)
            {
                case PlayerObjectType.Default :
                    return 0;
                case PlayerObjectType.Fighter :
                    return 1;
                case PlayerObjectType.Frigate :
                    return 4;
                case PlayerObjectType.Destroyer :
                    return 8;
                case PlayerObjectType.Cruiser :
                    return 16;
                case PlayerObjectType.Battleship:
                    return 32;
                default :
                    return 0;
            }
        }
	}
}