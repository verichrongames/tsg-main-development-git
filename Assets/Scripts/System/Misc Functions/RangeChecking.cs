﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreSys.CustomTypes;

namespace CoreSys.Misc
{
    public static class RangeChecking
    {

        public static RectangleSides DetermineSide(Transform targetTransform, Transform targeterTransform, IPlayerObject targetPlayerObject)
        {
            Vector3 targetForward = -targetTransform.forward.normalized;
            Vector3 targetSide = -targetTransform.right.normalized;
            Vector3 towardsTarget = targetTransform.position - targeterTransform.position;
            towardsTarget = towardsTarget.normalized;

            float dotProductForward = Vector3.Dot(towardsTarget, targetForward);
            float dotProductRight = Vector3.Dot(towardsTarget, targetSide);

            float angleForward = Vector3.Angle(towardsTarget, targetForward);
            float angleRight = Vector3.Angle(towardsTarget, targetSide);

            //Debug.Log("Forward Dot Product: " + dotProductForward);
            //Debug.Log("Right Dot Product: " + dotProductRight);
           
            //Debug.Log(" Right Limit Angle" + Vector3.Angle(targetTransform.position - targetPlayerObject.BoundingPoints.UpperRightPoint, -targetTransform.forward));
            //Debug.Log("Right Angle" + Mathf.Round(Vector3.Angle(towardsTarget, targetForward) * 10) / 10);
            targetPlayerObject.UpdateBoundingPoints();

            if (dotProductForward > 0) //If Front
            {
                if (dotProductRight > 0) //If Right
                {
                    if (angleForward <= targetPlayerObject.BoundingAngles.FrontRightAllowance) //If Front Side
                    {
                        Debug.Log("Front Side");
                    }
                    else
                    {
                        Debug.Log("Right Side");
                    }
                    //else if (dotProductRight <= targetPlayerObject.BoundingAngles.RightFrontAllowance) //If Right side
                    //{

                    //}
                }
                else //If Left
                {
                    if (angleForward <= targetPlayerObject.BoundingAngles.FrontLeftAllowance) //If Front Side
                    {
                        Debug.Log("Front Side");
                    }
                    else
                    {
                        Debug.Log("Left Side");
                    }
                    //else if (dotProductRight >= targetPlayerObject.BoundingAngles.LeftFrontAllowance) //If Left Side
                    //{

                    //}
                }

            }
            else //If Back
            {
                if (dotProductRight > 0) //If Right
                {
                    if (angleForward >= targetPlayerObject.BoundingAngles.BackRightAllowance) //If Back Side
                    {
                        Debug.Log("Back Side");
                    }
                    else
                    {
                        Debug.Log("Right Side");
                    }
                    //else if(dotProductRight >= targetPlayerObject.BoundingAngles.LeftBackAllowance) //If Right Side
                    //{

                    //}
                }
                else //If Left
                {
                    if (angleForward >= targetPlayerObject.BoundingAngles.BackLeftAllowance) //If Back Side
                    {
                        Debug.Log("Back Side");
                    }
                    else
                    {
                        Debug.Log("Left Side");
                    }
                    //else if (dotProductRight <= targetPlayerObject.BoundingAngles.RightBackAllowance) //If Left Side
                    //{

                    //}
                }
            }

            //if (dotProductForward > 0 && dotProductRight > 0) //If Upper Right
            //{
            //    if (Vector3.Angle(towardsTarget, targetForward) <= targetPlayerObject.BoundingAngles.FrontRightAllowance)
            //    {
            //        Debug.Log("Front Facing -- Right Orientation");
            //        return RectangleSides.Front;
            //    }
            //    else if (Vector3.Angle(towardsTarget, targetSide) <= targetPlayerObject.BoundingAngles.RightFrontAllowance)
            //    {
            //        Debug.Log("Right Facing -- Front Orientation");
            //        return RectangleSides.Right;
            //    }
            //}
            //else if (dotProductForward > 0 && dotProductRight < 0) //If Upper Left
            //{

            //    if (Vector3.Angle(towardsTarget, targetForward) <= targetPlayerObject.BoundingAngles.FrontLeftAllowance)
            //    {
            //        Debug.Log("Front Facing -- Left Orientation");
            //        return RectangleSides.Front;
            //    }
            //    else if (Vector3.Angle(towardsTarget, targetSide) >= targetPlayerObject.BoundingAngles.LeftFrontAllowance)
            //    {
            //        Debug.Log("Left Facing -- Front Orientation");
            //        return RectangleSides.Left;
            //    }
            //}
            //else if (dotProductForward < 0 && dotProductRight < 0) //If lower left
            //{
            //    if (Vector3.Angle(towardsTarget, targetForward) >= targetPlayerObject.BoundingAngles.BackLeftAllowance)
            //    {
            //        Debug.Log("Back Facing -- Left Orientation");
            //        return RectangleSides.Back;
            //    }
            //    else if (Vector3.Angle(towardsTarget, targetSide) >= targetPlayerObject.BoundingAngles.LeftBackAllowance)
            //    {
            //        Debug.Log("Left Facing -- Back Orientation");
            //        return RectangleSides.Left;
            //    }
            //}
            //else if (dotProductForward < 0 && dotProductRight > 0) //If lower right
            //{
            //    if (Vector3.Angle(towardsTarget, targetSide) <= targetPlayerObject.BoundingAngles.RightBackAllowance)
            //    {
            //        Debug.Log("Right Facing -- Back Orientation");
            //        return RectangleSides.Right;
            //    }
            //    else if (Vector3.Angle(towardsTarget, targetForward) >= targetPlayerObject.BoundingAngles.BackRightAllowance)
            //    {
            //        Debug.Log("Back Facing -- Right Orientation");
            //        return RectangleSides.Back;
            //    }
            //}
            return RectangleSides.None;
        }

        public static void CheckRangeRectangle()
        {

        }
    }
}
