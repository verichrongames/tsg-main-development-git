﻿using UnityEditor;
using UnityEngine;
using CoreSys.Lobby;
using CoreSys.Exceptions;
using CoreSys.Lobby.Chat;

/*=================================
 *  Custom inspector GUI for the
 *  chat notifications fields.
 * ==============================*/

[CustomEditor(typeof(ChatNotifications)), CanEditMultipleObjects]
public class ChatNotificationsInspector : Editor
{

    public SerializedProperty
        defaultState,
        notificationsState,
        userEnteredChannel,
        userLeftChannel,
        messageNotSentError,
        connectionFailure,
        failedToJoinChannel,
        disconnectedFromServer,
        disconnectedFromChannel,
        kickedFromChannel;

    void OnEnable()
    {
        // Setup the SerializedProperties
        notificationsState = serializedObject.FindProperty("notificationsState");
        userEnteredChannel = serializedObject.FindProperty("userEnteredChannel");
        userLeftChannel = serializedObject.FindProperty("userLeftChannel");
        messageNotSentError = serializedObject.FindProperty("messageNotSentError");
        connectionFailure = serializedObject.FindProperty("connectionFailure");
        failedToJoinChannel = serializedObject.FindProperty("failedToJoinChannel");
        disconnectedFromServer = serializedObject.FindProperty("disconnectedFromServer");
        disconnectedFromChannel = serializedObject.FindProperty("disconnectedFromChannel");
        kickedFromChannel = serializedObject.FindProperty("kickedFromChannel");
        defaultState = serializedObject.FindProperty("defaultState");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.HelpBox("Enter the text you wish to be displayed for the notifications listed. If you want parts of the text to be subsituted for a field such as a username, enclose with a {} and a word describing the variable that would fill it's place. \n\nie. You have been kicked from {channel}", UnityEditor.MessageType.Info);
        EditorGUILayout.PropertyField(notificationsState);

        NotificationType st = (NotificationType)notificationsState.enumValueIndex;

        switch (st)
        {
            case NotificationType.UserEntered:
                EditorGUILayout.PropertyField(userEnteredChannel, new GUIContent("Notification Text:"));
                if (CheckForEmptyField(userEnteredChannel))
                {
                    EditorGUILayout.HelpBox("Field Must Have A Value", UnityEditor.MessageType.Error);
                }
                break;
            case NotificationType.UserLeft:
                EditorGUILayout.PropertyField(userLeftChannel, new GUIContent("Notification Text:"));
                if (CheckForEmptyField(userLeftChannel))
                {
                    EditorGUILayout.HelpBox("Field Must Have A Value", UnityEditor.MessageType.Error);
                }
                break;
            case NotificationType.MessageNotSentError:
                EditorGUILayout.PropertyField(messageNotSentError, new GUIContent("Notification Text:"));
                if (CheckForEmptyField(messageNotSentError))
                {
                    EditorGUILayout.HelpBox("Field Must Have A Value", UnityEditor.MessageType.Error);
                }
                break;
            case NotificationType.ConnectionFailure:
                EditorGUILayout.PropertyField(connectionFailure, new GUIContent("Notification Text:"));
                if (CheckForEmptyField(connectionFailure))
                {
                    EditorGUILayout.HelpBox("Field Must Have A Value", UnityEditor.MessageType.Error);
                }
                break;
            case NotificationType.FailedToJoinChannel:
                EditorGUILayout.PropertyField(failedToJoinChannel, new GUIContent("Notification Text:"));
                if (CheckForEmptyField(failedToJoinChannel))
                {
                    EditorGUILayout.HelpBox("Field Must Have A Value", UnityEditor.MessageType.Error);
                }
                break;
            case NotificationType.DisconnectedFromServer:
                EditorGUILayout.PropertyField(disconnectedFromServer, new GUIContent("Notification Text:"));
                if (CheckForEmptyField(disconnectedFromServer))
                {
                    EditorGUILayout.HelpBox("Field Must Have A Value", UnityEditor.MessageType.Error);
                }
                break;
            case NotificationType.DisconnectedFromChannel:
                EditorGUILayout.PropertyField(disconnectedFromChannel, new GUIContent("Notification Text:"));
                if (CheckForEmptyField(disconnectedFromChannel))
                {
                    EditorGUILayout.HelpBox("Field Must Have A Value", UnityEditor.MessageType.Error);
                }
                break;
            case NotificationType.KickedFromChannel:
                EditorGUILayout.PropertyField(kickedFromChannel, new GUIContent("Notification Text:"));
                if (CheckForEmptyField(kickedFromChannel))
                {
                    EditorGUILayout.HelpBox("Field Must Have A Value", UnityEditor.MessageType.Error);
                }
                break;
            case NotificationType.Default:
                EditorGUILayout.PropertyField(defaultState, new GUIContent("Notification Text:"));
                if (CheckForEmptyField(defaultState))
                {
                    EditorGUILayout.HelpBox("Field Must Have A Value", UnityEditor.MessageType.Error);
                }
                break;
        }

        serializedObject.ApplyModifiedProperties();


        if (userEnteredChannel.stringValue == "" ||
            userLeftChannel.stringValue == "" ||
            messageNotSentError.stringValue == "" ||
            connectionFailure.stringValue == "" ||
            failedToJoinChannel.stringValue == "" ||
            disconnectedFromServer.stringValue == "" ||
            disconnectedFromChannel.stringValue == "" ||
            kickedFromChannel.stringValue == "" ||
            defaultState.stringValue == "")
        {
            throw new EmptyFieldException("All Notification Fields Must Have a Value");
        }
    }

    private bool CheckForEmptyField(SerializedProperty field)
    {
        if(field.stringValue == "")
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}