﻿using UnityEngine;
using UnityEngine.UI;
using CoreSys.UI;
using UnityEditor;


namespace CoreSys.UI
{
    [CustomEditor(typeof(SelectableText), true)]
    [CanEditMultipleObjects]
    public class SelectableTextInspector : GraphicEditor
    {
        public SerializedProperty CaretReferance;
        public SerializedProperty TextBacker;

        public SerializedProperty IsFocused;
        public SerializedProperty SelectionColor;
        public SerializedProperty CanDragSelect;
        public SerializedProperty MultiLine;
        public SerializedProperty m_RichText;

        protected override void OnEnable()
        {
            base.OnEnable();
            CaretReferance = serializedObject.FindProperty("caretReferance");
            TextBacker = serializedObject.FindProperty("textBacker");
            IsFocused = serializedObject.FindProperty("isFocused");
            SelectionColor = serializedObject.FindProperty("selectionColor");
            CanDragSelect = serializedObject.FindProperty("canDragSelect");
            MultiLine = serializedObject.FindProperty("multiLine");
            m_RichText = serializedObject.FindProperty("m_RichText");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(CaretReferance);
            EditorGUILayout.PropertyField(TextBacker);
            EditorGUILayout.PropertyField(IsFocused);
            EditorGUILayout.PropertyField(SelectionColor);
            EditorGUILayout.PropertyField(CanDragSelect);
            EditorGUILayout.PropertyField(MultiLine);
            EditorGUILayout.PropertyField(m_RichText);

            serializedObject.ApplyModifiedProperties();
        }
    }
}
