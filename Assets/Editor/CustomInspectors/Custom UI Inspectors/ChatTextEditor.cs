﻿using UnityEngine;
using UnityEngine.UI;
using CoreSys.UI;
using UnityEditor;

namespace CoreSys.UI
{
    // TODO REVIEW
    // Have material live under text
    // move stencil mask into effects *make an efects top level element like there is
    // paragraph and character

    /// <summary>
    /// Editor class used to edit UI Labels.
    /// </summary>

    [CustomEditor(typeof(ChatText), true)]
    [CanEditMultipleObjects]
    public class ChatTextEditor : GraphicEditor
    {
        SerializedProperty m_Text;
        SerializedProperty m_UnformattedText;
        SerializedProperty m_FontData;
        SerializedProperty m_UnformattedTextBool;
        SerializedProperty HandleParsing;

        protected override void OnEnable()
        {
            base.OnEnable();
            m_Text = serializedObject.FindProperty("m_Text");
            m_UnformattedText = serializedObject.FindProperty("m_UnformattedText");
            m_UnformattedTextBool = serializedObject.FindProperty("m_UnformattedTextBool");
            m_FontData = serializedObject.FindProperty("m_FontData");
            HandleParsing = serializedObject.FindProperty("HandleParsing");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(m_Text);
            if(m_UnformattedTextBool.boolValue)
                EditorGUILayout.PropertyField(m_UnformattedText);
            EditorGUILayout.PropertyField(m_FontData);
            EditorGUILayout.PropertyField(m_UnformattedTextBool);
            EditorGUILayout.PropertyField(HandleParsing);
            AppearanceControlsGUI();
            serializedObject.ApplyModifiedProperties();
        }
    }
}