﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class bl_LoginManager : MonoBehaviour {

    public MenuManager setMenuManager;
    public Menu setMainMenu;
    public Menu setLoginMenu;
    public static MenuManager menuManager;
    public static Menu mainMenu;
    public static Menu loginMenu;
    //Call all script when Login
    public delegate void LoginEvent(string name,int kills,int deaths,int score,int status); //Use this for threadsafe login
    public static LoginEvent OnLogin;
    [Space(5)]
    public Text Description = null;
    static Text mDescrip = null;
    public Image BlackScreen = null;
    [Space(5)]
    public bl_LoadingEffect Loading = null;
    public static bl_LoadingEffect LoadingCache = null;

    public const string SavedUser = "UserName";
    private Color alpha = new Color(0, 0, 0, 0);

    void Awake()
    {
        menuManager = setMenuManager;
        mainMenu = setMainMenu;
        loginMenu = setLoginMenu;
        if (Loading != null) { LoadingCache = Loading; }

        mDescrip = Description;
        OnLogin += onLogin;
        StartCoroutine(FadeOut());
    }

    void OnDisable()
    {
        OnLogin -= onLogin;
    }

    public static void UpdateDescription(string t)
    {
        mDescrip.text = t;
    }

    public static void OnLoginEvent(string name, int kills, int deaths, int score,int status)
    {
        if (OnLogin != null)
            OnLogin(name,kills,deaths,score,status);
    }

    void onLogin(string n,int k,int d,int s,int st)
    {
        BlackScreen.gameObject.SetActive(true);
        StartCoroutine(FadeIn());
    }

    /// <summary>
    /// Effect of Fade In
    /// </summary>
    /// <returns></returns>
    IEnumerator FadeIn()
    {
        alpha = BlackScreen.color;

        while (alpha.a < 1.0f)
        {
            alpha.a += Time.deltaTime ;
            BlackScreen.color = alpha;
            yield return null;
        }
    }
    /// <summary>
    /// Effect of Effect Fade Out
    /// </summary>
    /// <returns></returns>
    IEnumerator FadeOut()
    {
        alpha.a = 1f;
        while (alpha.a > 0.0f)
        {
            alpha.a -= Time.deltaTime;
            BlackScreen.color = alpha;
            yield return null;
        }
        BlackScreen.gameObject.SetActive(false);
    }
}