﻿using UnityEngine;
using System.Collections;
using System.Linq;

[RequireComponent(typeof(AudioSource))]
public class MenuManager : MonoBehaviour 
{
    public Menu currentMenu;
    public Menu setUsername;
    public AudioClip menuSound;
    private AudioSource menuAudio;
    public UsernameDisplay unDisplay;

    public void Start()
    {
        menuAudio = GetComponent<AudioSource>();
        menuAudio.PlayOneShot(menuSound);
        string userName = PlayerPrefs.GetString("Username");
        if (userName != "")
            ShowMenu(currentMenu);
        else
            ShowMenu(setUsername);
    }

    public void ShowMenu(Menu menu)
    {
        if (currentMenu != null)
            currentMenu.isOpen = false;

        currentMenu = menu;
        menuAudio.PlayOneShot(menuSound);
        currentMenu.isOpen = true;
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void SinglePlayerLoad()
    {
        Application.LoadLevel(1);
    }

    public void StartServer()
    {
        BoltLauncher.StartServer(UdpKit.UdpEndPoint.Parse("0.0.0.0:27000"));
        BoltNetwork.EnableUPnP();
        BoltNetwork.OpenPortUPnP(27000);
        BoltNetwork.LoadScene("TSGBlueNebulaBolt");
    }

    public void JoinServer()
    {
        BoltLauncher.StartClient();
        BoltNetwork.Connect(UdpKit.UdpEndPoint.Parse("127.0.0.1:27000"));
    }
}
