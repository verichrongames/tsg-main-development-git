﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Menu : MonoBehaviour 
{
    public Button submitButton;
    public EnterHandler enterHandler;
    public Menu mainMenu;
    private Animator _animator;
    private CanvasGroup _canvasGroup;
    private string connectAddress, username;

    public bool isOpen
    {
        get { return _animator.GetBool("isOpen"); }
        set { _animator.SetBool("isOpen", value); }
    }

    public void Awake()
    {
        _animator = GetComponent<Animator>();
        _canvasGroup = GetComponent<CanvasGroup>();

        var rect = GetComponent<RectTransform>();
        rect.offsetMax = rect.offsetMin = new Vector2(0, 0);
    }

    public void Update()
    {
        if (!_animator.GetCurrentAnimatorStateInfo(0).IsName("Open"))
        {
            _canvasGroup.blocksRaycasts = _canvasGroup.interactable = false;
        }
        else
        {
            _canvasGroup.blocksRaycasts = _canvasGroup.interactable = true;
            if (enterHandler)
                enterHandler.enabled = true;
        }
    }

    public void ConnectString(string InputFieldString)
    {
        connectAddress = InputFieldString;
    }

    public void UsernameString(string InputFieldString)
    {
        username = InputFieldString;   
    }

    public void SetUsername(Button thisButton)
    {
        if (username != null)
        {
            PlayerPrefs.SetString("Username", username);
            if(enterHandler)
                enterHandler.enabled = false;
            MenuManager controller = transform.parent.GetComponent<MenuManager>();
            controller.unDisplay.resetUsername();
            controller.ShowMenu(mainMenu);
        }
        else
        {
            ButtonText(thisButton);
        }
    }

    public void DeleteUsername()
    {
        PlayerPrefs.DeleteKey("Username");
        MenuManager controller = transform.parent.GetComponent<MenuManager>();
        controller.ShowMenu(controller.setUsername);
    }

    private IEnumerator ButtonText(Button thisButton)
    {
        Text buttonText = thisButton.GetComponent<Text>();
        string tempText = buttonText.text;
        buttonText.text = "Error! Empty Field";
        yield return new WaitForSeconds(1);
        buttonText.text = tempText;
    }

    public void JoinServer()
    {
        BoltLauncher.StartClient();
        BoltNetwork.Connect(UdpKit.UdpEndPoint.Parse(connectAddress+":27000"));
    }
}
