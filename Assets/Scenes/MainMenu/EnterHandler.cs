﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

//Place this script on buttons to be able to use enter to click the button
public class EnterHandler : MonoBehaviour 
{
    public MenuManager menuController;
    public Button thisButton;
    public Menu parentMenu;

	void Start () 
    {
        menuController = GetComponent<MenuManager>();
        thisButton = GetComponent<Button>();
        parentMenu.enterHandler = this;
        this.enabled = false;
	}

    public void OnEnable()
    {
        KeyboardEvents.MiscButtonHeldEvent += EnterSwitchDown;
    }

    public void OnDisable()
    {
        KeyboardEvents.MiscButtonHeldEvent -= EnterSwitchDown;
    }

    private void EnterSwitchDown(string clickType)
    {
        if (clickType == "Enter")
        {
            ExecuteEvents.Execute<IPointerClickHandler>(thisButton.gameObject, new PointerEventData(EventSystem.current), ExecuteEvents.pointerClickHandler);
            this.enabled = false;
        }
    }

}
