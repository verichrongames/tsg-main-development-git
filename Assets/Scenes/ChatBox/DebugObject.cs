﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

namespace CoreSys.Lobby.Chat
{
    [ExecuteInEditMode]
    public class DebugObject : MonoBehaviour
    {
        public bool enableDebugging;
        public RecieveChatData recieveChatData;
        public SendChatData sendChatData;
        public ChatBox chatBox;
        public Text debugText;
        public ChatHistory chatHistory;

        public string debugUsername;

        void Update()
        {
            if(Application.isEditor)
            {
                ActivateChildObjects();
                UpdateOnDebug();
            }
        }

        void Start()
        {
            debugText.text = "Typing in channel: " + testChannelName[channelToPick] + "\n" + "Username :" + testUsernames[usernameToPick];
            if (enableDebugging && Application.isPlaying)
            {
                JoinDebugChannel();
            }
        }


        //Activates all debug objects within scene
        private void ActivateChildObjects()
        {
            foreach(Transform child in transform)
            {
                child.gameObject.SetActive(enableDebugging);
            }
        }

        //Updates other objects that need to know if debug mode is on
        private void UpdateOnDebug()
        {
            sendChatData.debugModeEnabled = enableDebugging;
        }


        public int channelToPick = 0;
        public string[] testChannelName;

        public int usernameToPick = 0;
        public string[] testUsernames;

        //Cycles to the next channel
        public void NextChannel()
        {
            if (channelToPick < 4)
            {
                channelToPick++;
            }
            else
            {
                channelToPick = 0;
            }
            debugText.text = "Typing in channel: " + testChannelName[channelToPick] + "\n" + "Username :" + testUsernames[usernameToPick];
        }

        //Cycles to the next username
        public void NextUsername()
        {
            if (usernameToPick < 9)
            {
                usernameToPick++;
            }
            else
            {
                usernameToPick = 0;
            }
            debugText.text = "Typing in channel: " + testChannelName[channelToPick] + "\n" + "Username :" + testUsernames[usernameToPick];
        }

        public void UserEnteredChannel()
        {
            recieveChatData.ClientJoinedChannel(testUsernames[usernameToPick], "Debug Channel");
        }

        public void UserLeftChannel()
        {
            recieveChatData.ClientLeftChannel(testUsernames[usernameToPick], "Debug Channel");
        }

        //Joins a default debug channel
        private void JoinDebugChannel()
        {
            recieveChatData.NewChannelInfo("Debug Channel", new List<string>(), new List<string>(), true, ChannelType.Public);
        }

        //Sends a test debug PM to yourself from yourself
        public void SendPrivateMessage()
        {
            sendChatData.SendPrivateMessage(testUsernames[usernameToPick], testUsernames[usernameToPick], "Test PM Message");
        }

        //Creates a new channel
        public void NewChannel()
        {
            if(channelToPick < testChannelName.Length)
            {
                //sendChatData.SendNewMessage("", "New Channel", testChannelName[channelToPick]);
                List<string> message = new List<string>();
                List<string> clients = new List<string>();
                message.Add(String.Empty);
                clients.Add(String.Empty);
                recieveChatData.NewChannelInfo(testChannelName[channelToPick], message, clients, true, ChannelType.Public);
                channelToPick++;
            }
        }

        public void SendPrivateMessageRelay(string fromUsername, string toUsername, string message)
        {
            if(enableDebugging)
            {
                PrivateMessageType newMessage = new PrivateMessageType(fromUsername, toUsername, message);
                recieveChatData.NewPrivateMessage(newMessage);
            }
            else
            {
                sendChatData.SendPrivateMessage(fromUsername, toUsername, message);
            }
        }

        //Recieves messages and relayes or edits them if debug mode is on
        public void SendMessageRelay(string username, string message, string channelName)
        {
            if(enableDebugging)
            {
                PublicMessageType newMessage = new PublicMessageType(testUsernames[usernameToPick], message, chatHistory.currentChatRoom.Name);
                recieveChatData.NewPublicMessage(newMessage);
            }
            else
            {
                sendChatData.SendNewMessage(username, message, channelName);
            }
        }

        //Recieves join channel request and forwards it approprietly
        public void JoinChannelRequestRelay(string userName, string channelName)
        {
            if (enableDebugging)
            {
                recieveChatData.NewChannelInfo(channelName, new List<string>(), new List<string>(), true, ChannelType.Public);
            }
            else
            {
                sendChatData.SendChannelJoinRequest(userName, channelName);
            }
        }

    }
}

