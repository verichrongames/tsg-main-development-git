﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*============================================
 *  Used to encapsulate messages to channels,
 *  not private messages.
 *  
 * This type is sent to the server, and is
 * also recieved from the server
 * =========================================*/

public class PublicMessageType
{

    public PublicMessageType() { }
    /// <summary>
    /// Public message to/from channel
    /// </summary>
    /// <param name="fromUsername"></param>
    /// <param name="message"></param>
    /// <param name="channelName"></param>
    /// <param name="messageType">"Private", "Public"</param>
    public PublicMessageType(string fromUsername, string message, string channelName)
    {
        this.message = message;
        this.channelName = channelName;
        this.fromUsername = fromUsername;
    }


    /// <summary>
    /// Public message to/from channel
    /// </summary>
    /// <param name="fromUserID"></param>
    /// <param name="message"></param>
    /// <param name="channelName"></param>
    /// <param name="messageType">"Private", "Public"</param>
    public PublicMessageType(int fromUserID, string message, string channelName)
    {
        this.fromUserID = fromUserID;
        this.message = message;
        this.channelName = channelName;
    }

    /// <summary>
    /// Channel Join Request
    /// </summary>
    /// <param name="fromUsername"></param>
    /// <param name="channelName"></param>
    public PublicMessageType(string fromUserID, string channelName)
    {
        this.fromUsername = fromUserID;
        this.channelName = channelName;
    }

    public string fromUsername;
    public int fromUserID;

    public string message;
    public string channelName;
}
