﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*============================================
 *  Used to encapsulate notifications and
 *  channel updates/information that is not a
 *  message from another user.
 *  
 * This is recieved from the server, not sent 
 * to the server.
 * 
 * It's purpose is to facuilitate a small type
 * to handle notifications such as new users
 * entering channel or user leaving channel.
 * =========================================*/

namespace CoreSys.Lobby.Chat
{
    public class NotificationMessageType
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="channelName"></param>
        /// <param name="userList"></param>
        /// <param name="notificationType">"UserEntered", "UserLeft"</param>
        public NotificationMessageType(int userID, string channelName, string notificationType)
        {
            this.userID = userID;
            this.channelName = channelName;
            this.notificationType = notificationType;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="channelName"></param>
        /// <param name="notificationType">"UserEntered", "UserLeft"</param>
        public NotificationMessageType(string username, string channelName, string notificationType)
        {
            this.username = username;
            this.channelName = channelName;
            this.notificationType = notificationType;
        }

        /// <summary>
        /// Notification Type.
        /// "UserEntered", "UserLeft"
        /// </summary>
        public string notificationType;
        public string username;
        public int userID;
        public string channelName;
    }
}
