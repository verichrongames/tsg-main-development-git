using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class PrivateMessageType
{
    public string toUsername;
    public string fromUsername;
    public string message;

    public PrivateMessageType() { }
    /// <summary>
    /// Private message to/from
    /// </summary>
    /// <param name="fromUsername"></param>
    /// <param name="toUsername"></param>
    /// <param name="message"></param>
    public PrivateMessageType(string fromUsername, string toUsername, string message)
    {
        this.fromUsername = fromUsername;
        this.toUsername = toUsername;
        this.message = message;
    }
}
