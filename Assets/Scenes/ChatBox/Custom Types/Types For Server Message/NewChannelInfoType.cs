﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*============================================
 *  Used to encapsulate all information
 *  regarding the response back to the client
 *  after requesting to join a new channel.
 *  
 * This type is recieved from the server,
 * it is not sent to the server.
 * 
 * This is to facuilitate a stright-forward type
 * to recieve information regarding the clients
 * request to join a new channel
 * =========================================*/

namespace CoreSys.Lobby.Chat
{
    public class NewChannelInfoType
    {
        /// <summary>
        /// Information about a new channel
        /// </summary>
        /// <param name="success">Was joining a success or failure</param>
        /// <param name="userList">List of users in channel seperated by "||" </param>
        /// <param name="channelName">name of channel</param>
        /// <param name="channelHistory">last few messages in channel</param>
        public NewChannelInfoType(bool success, string userList, string channelName, string channelHistory)
        {
            this.success = success;
            this.userList = userList;
            this.channelName = channelName;
            this.channelHistory = channelHistory;
        }

        public bool success; //whether the channel join was a success
        public string userList; //List of all user currently in the channel seperated by "||"
        public string channelName; //Name of the channel 
        public string channelHistory; //Server samed message history for the channel
    }
}
