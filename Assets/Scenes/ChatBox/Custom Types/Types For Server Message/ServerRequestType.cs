﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*============================================
 *  Used to make requests of the server, 
 *  such as joining a new channel. And later
 *  on creating or removing a channel.
 *  
 * This is sent to the server,a nd is not
 * recieved by the client.
 * 
 * This is to facuilitate a small data type
 * to make requests from the server.
 * =========================================*/

namespace CoreSys.Lobby.Chat
{
    public class ServerRequestType
    {
        public ServerRequestType(string requestType, string channelName, int userID)
        {
            this.requestType = requestType;
            this.channelName = channelName;
            this.userID = userID;
        }

        public ServerRequestType(string requestType, string channelName, string username)
        {
            this.requestType = requestType;
            this.channelName = channelName;
            this.userName = username;
        }

        public string requestType; //Request type, "JoinChannel" would probably be the only valid request at this time.
        public string channelName; //Name of channel user is making request of
        public string userName; //Username of user making request (may go unused?)
        public int userID; //ID of user making request
    }
}
