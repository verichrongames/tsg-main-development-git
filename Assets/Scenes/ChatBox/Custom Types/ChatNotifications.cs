﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


namespace CoreSys.Lobby.Chat
{
    public class ChatNotifications : MonoBehaviour
    {
        void Start()
        {

        }

        void Update()
        {

        }

        public NotificationType notificationsState;

        public string defaultState;
        public string userEnteredChannel;
        public string userLeftChannel;
        public string messageNotSentError;
        public string connectionFailure;
        public string failedToJoinChannel;
        public string disconnectedFromServer;
        public string disconnectedFromChannel;
        public string kickedFromChannel;

        public string GetNotificationText(NotificationType notification)
        {
            switch(notification)
            {
                case NotificationType.Default :
                    return defaultState;
                case NotificationType.UserEntered :
                    return userEnteredChannel;
                case NotificationType.UserLeft :
                    return userLeftChannel;
                case NotificationType.MessageNotSentError :
                    return messageNotSentError;
                case NotificationType.ConnectionFailure :
                    return connectionFailure;
                case NotificationType.FailedToJoinChannel :
                    return failedToJoinChannel;
                case NotificationType.DisconnectedFromServer :
                    return disconnectedFromServer;
                case NotificationType.DisconnectedFromChannel :
                    return disconnectedFromChannel;
                case NotificationType.KickedFromChannel :
                    return kickedFromChannel;
                default :
                    return defaultState;
            }
        }
    }
}

