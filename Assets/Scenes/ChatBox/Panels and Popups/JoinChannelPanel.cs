﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using CoreSys.Networking;
using CoreSys.Lobby.Chat;
using UnityEngine.UI;

namespace CoreSys.Lobby.Chat
{
    class JoinChannelPanel : MonoBehaviour
    {
        private InputField inputField;
        public DebugObject debugObject;

        void Start()
        {
            inputField = GetComponentInChildren<InputField>();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                Submit();
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Cancel();
            }
        }

        //Cancels the join channel action
        public void Cancel()
        {
            ClosePanel();
        }

        //Submits the panel information to the server
        public void Submit()
        {
            debugObject.JoinChannelRequestRelay(GameManager.Username, inputField.text);
            ClosePanel();
        }

        //Closes the panel out
        private void ClosePanel()
        {
            Destroy(this.gameObject);
        }
    }
}
