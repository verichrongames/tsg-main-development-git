﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Text;
using UnityEngine.UI;
using CoreSys.Extensions;

namespace CoreSys.Lobby.Chat
{
    class PrivateMessagePanel : MonoBehaviour
    {

        public InputField usernameField;
        public InputField messageField;
        public DebugObject debugObject;

        void Start()
        {
            List<InputField> fields = new List<InputField>();
            fields.AddRange(GetComponentsInChildren<InputField>());

            usernameField = fields.FirstOrDefault(x => x.gameObject.name.Contains("username", StringComparison.OrdinalIgnoreCase));
            messageField = fields.FirstOrDefault(x => x.gameObject.name.Contains("message", StringComparison.OrdinalIgnoreCase));
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                Submit();
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Cancel();
            }
        }

        //Cancels the join channel action
        public void Cancel()
        {
            ClosePanel();
        }

        //Submits the panel information to the server
        public void Submit()
        {
            debugObject.SendPrivateMessageRelay(GameManager.Username, usernameField.text, messageField.text);
            ClosePanel();
        }

        //Closes the panel out
        private void ClosePanel()
        {
            Destroy(this.gameObject);
        }
    }
}
