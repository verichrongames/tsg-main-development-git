﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/*============================================
 *  Extended Inputfield class so I can
 *  try and manage what behavior I can.
 *  
 * This class locks the text in the InputField
 * so it cnanot be edited, however it can still
 * be highlighted.
 * =========================================*/

public class LockedInputField : InputField , IPointerClickHandler{

    int lockedCaretPosition;
    bool firstSelect = false;
    RectTransform rectTransform;

    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    private string m_LockedText;
    public string LockedText
    {
        get
        {
            return m_LockedText;
        }

        set
        {
            m_LockedText = value;
            text = value;
        }
    }

    void Update()
    {

    }

    public void Submit()
    {
        ExecuteEvents.Execute<ISubmitHandler>(gameObject, new BaseEventData(EventSystem.current), ExecuteEvents.submitHandler);
    }

    //Necessary to unselect text from the SelectAll() method in the base class upon first click
    public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);
        StartCoroutine(ResetCaret(eventData));
    }

    //Resets the caret to stop the select all, needs conditional to only run once per OnSelect().
    IEnumerator ResetCaret(PointerEventData eventData)
    {
        yield return new WaitForEndOfFrame();
        base.OnPointerDown(eventData);
    }

    //Reverts to the locked text if there is a change to the field
    public void LockFieldChanges()
    {
        text = m_LockedText;
    }
}
