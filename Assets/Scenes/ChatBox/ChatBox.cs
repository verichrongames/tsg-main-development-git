﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoreSys.Lobby.Chat
{
    public class ChatBox : MonoBehaviour
    {
        public ChatHistory chatHistory;
        public Canvas parentCanvas;
        public DebugObject debugObject;
        public GameObject joinChannelPanelPrefab;
        public GameObject privateMessagePanelPrefab;

        //Handles bringing up the PM UI
        public void PrivateMessage()
        {
            GameObject pmPanel = Instantiate(privateMessagePanelPrefab);
            pmPanel.transform.SetParent(parentCanvas.transform);

            RectTransform rectTransform = pmPanel.GetComponent<RectTransform>();
            ZeroTransformOffsets(rectTransform);

            pmPanel.GetComponent<PrivateMessagePanel>().debugObject = debugObject;

            pmPanel.SetActive(true);
        }

        public void PrivateMessage(string username)
        {
            GameObject pmPanel = Instantiate(privateMessagePanelPrefab);
            pmPanel.transform.SetParent(parentCanvas.transform);

            RectTransform rectTransform = pmPanel.GetComponent<RectTransform>();
            ZeroTransformOffsets(rectTransform);

            PrivateMessagePanel pmPanelComponent = pmPanel.GetComponent<PrivateMessagePanel>();
            pmPanelComponent.debugObject = debugObject;
            pmPanelComponent.usernameField.text = username;

            pmPanelComponent.messageField.Select();
            pmPanel.SetActive(true);
        }

        //Handles bringing up the join channel UI
        public void JoinChannel()
        {
            GameObject joinChannelPanel = Instantiate(joinChannelPanelPrefab);
            joinChannelPanel.transform.SetParent(parentCanvas.transform);

            RectTransform rectTransform = joinChannelPanel.GetComponent<RectTransform>();
            ZeroTransformOffsets(rectTransform);

            joinChannelPanel.GetComponent<JoinChannelPanel>().debugObject = debugObject;


            joinChannelPanel.SetActive(true);
        }

        private void ZeroTransformOffsets(RectTransform transform)
        {
            transform.offsetMax = Vector2.zero;
            transform.offsetMin = Vector2.zero;
        }

    }
}
