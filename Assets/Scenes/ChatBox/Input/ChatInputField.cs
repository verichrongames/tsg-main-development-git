﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace CoreSys.Lobby.Chat
{
    public class ChatInputField : MonoBehaviour
    {
        public InputField textInput;
        public SendChatData sendMessage;
        public DebugObject debugObject;
        public ChatHistory chatHistory;
        public string currentChannelName;


        //Testing Members

        //public Text debugText;

        //int channelToPick = 0;
        //string[] testChannelName = new string[5]
        //{
        //    "Main",
        //    "Battle",
        //    "Private",
        //    "Tab 4",
        //    "Tab 5"
        //};

        //int usernameToPick = 0;
        //string[] testUsernames = new string[10]
        //{
        //    "Douglasg14b",
        //    "Catalyst",
        //    "Fredrick",
        //    "Zongrash",
        //    "GrrGoons",
        //    "DaBigFunk",
        //    "xXXSniperBoyXXx",
        //    "Le Reddit",
        //    "AspectStar",
        //    "Hruu171"
        //};

        //public void NextChannel()
        //{
        //    if (channelToPick < 4)
        //    {
        //        channelToPick++;
        //    }
        //    else
        //    {
        //        channelToPick = 0;
        //    }
        //    debugText.text = "Typing in channel: " + testChannelName[channelToPick] + "\n" + "Username :" + testUsernames[usernameToPick];
        //}

        //public void NextUsername()
        //{
        //    if (usernameToPick < 9)
        //    {
        //        usernameToPick++;
        //    }
        //    else
        //    {
        //        usernameToPick = 0;
        //    }
        //    debugText.text = "Typing in channel: " + testChannelName[channelToPick] + "\n" + "Username :" + testUsernames[usernameToPick];
        //}

        //public void SendPrivateMessage()
        //{
        //    sendMessage.SendPrivateMessage(testUsernames[usernameToPick], "Test PM Message");
        //}

        //void Start()
        //{
        //    debugText.text = "Typing in channel: " + testChannelName[channelToPick] + "\n" + "Username :" + testUsernames[usernameToPick];
        //}

        //Checks for an enter to submit
        public void CheckForSubmit()
        {
            //No idea how to get the actual value changed from this event so I have to check for keys being held down. Terribly awkward
            if (Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.KeypadEnter))
            {
                if(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
                {
                    //textInput.text += "/n";
                }
                else
                {
                    if(textInput.text != "")
                    {
                        Submit();
                        ClearTextBox();
                    }
                }
            }
        }

        //Submits and sends the text to the debug relay
        private void Submit()
        {
            string inputText = textInput.text.TrimEnd(' ', '\n');
            if(chatHistory.currentChatRoom.RoomType == ChannelType.Public)
            {
                debugObject.SendMessageRelay(GameManager.Username, inputText, chatHistory.currentChatRoom.Name);
            }
            else if(chatHistory.currentChatRoom.RoomType == ChannelType.PrivateMessage)
            {
                debugObject.SendPrivateMessageRelay(GameManager.Username, chatHistory.currentChatRoom.Name, inputText);
            }
        }

        private void ClearTextBox()
        {
            textInput.text = "";
        }
    }
}
