﻿using System;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using CoreSys.Networking;


namespace CoreSys.Lobby.Chat
{
    public class ServerIntermediary : MonoBehaviour //This switch takes any message type and directs it to a the correct function
    {
        public RecieveChatData recieveChatData;
        public SendChatData sendChatData;

        private void Start()
        {
            ServerListener.chatIntermediary = this;
        }

        public void Update()
        {
            ServerListener.CheckForData();
        }

        public void RecieveHandle(ServerMessage message)
        {
            switch (message.messageType)
            {
                default:
                    Debug.Log(String.Format("MessageHandler Error: Message Type Invalid - Cannot Handle Message")); //Shit should never happen
                    break;
                case 3:
                    if (message.messageBodyInt == 0) //New Client Joined
                    {
                        recieveChatData.ClientJoinedChannel(message.chatMessage.fromUsername, message.chatMessage.channelName);
                    }
                    else if (message.messageBodyInt == 1) //Client Left
                    {
                        recieveChatData.ClientLeftChannel(message.chatMessage.fromUsername, message.chatMessage.channelName);
                    }
                    else if (message.messageBodyInt == 2) //New Public message
                    {
                        recieveChatData.NewPublicMessage(message.chatMessage);
                    }
                    else if (message.messageBodyInt == 4)
                    {
                        recieveChatData.NewChannelInfo(message.channelName, message.channelLog, message.clientList, true, ChannelType.Public);
                    }
                    break;
                case 4:
                    if (message.messageBodyInt == 0)
                    {
                        recieveChatData.NewPrivateMessage(message.privateMessage);
                    }
                    else if (message.messageBodyInt == 1)
                    {
                        //Failed to send PM to recipient
                    }
                    break;
                case 11:

                    break;
            }
        }

        public void PrivateMessageSend(PrivateMessageType newMessage)
        {
            ServerMessage serverMessage = new ServerMessage(4, 0, newMessage);
            ServerCommand serverCommand = new ServerCommand();
            serverCommand.SendServerMessage(serverMessage, GameManager.serverSocket);
        }

        public void PublicMessageSend(PublicMessageType newMessage)
        {
            ServerMessage serverMessage = new ServerMessage(3, 2, newMessage);
            ServerCommand serverCommand = new ServerCommand();
            serverCommand.SendServerMessage(serverMessage, GameManager.serverSocket);
        }

        public void JoinNewChannelSend(PublicMessageType newMessage)
        {
            ServerMessage serverMessage = new ServerMessage(3, 0, newMessage);
            ServerCommand serverCommand = new ServerCommand();
            serverCommand.SendServerMessage(serverMessage, GameManager.serverSocket);
        }
    }
}
