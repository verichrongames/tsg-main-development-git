﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace CoreSys.Lobby.Chat
{
    public class RecieveChatData : MonoBehaviour
    {
        public SendChatData sendMessage;
        public ChatHistory chatHistory;
        public UserListManager userListManager;

        /*============================================================
         *           ========= Public API =========
         * =========================================================*/


        /// <summary>
        /// Recieves new chat messages for subscribed channels
        /// </summary>
        public void NewPublicMessage(PublicMessageType message)
        {
            chatHistory.NewRoomMessage(message.fromUsername, message.message, message.channelName);
        }

        /// <summary>
        /// Sends a new private message to this client
        /// </summary>
        public void NewPrivateMessage(PrivateMessageType message)
        {
            chatHistory.NewPrivateMessage(message.fromUsername, message.message);
        }

        /// <summary>
        /// Notifies subscirber to this channel that a client has entered
        /// </summary>
        /// <param name="username">Name of the client that has left</param>
        /// <param name="channelName"> Name of the channel the client has left</param>
        public void ClientJoinedChannel(string username, string channelName)
        {
            chatHistory.UserEnteredChannel(channelName, username);
        }

        /// <summary>
        /// Notifies subscirber to this channel that a client has left
        /// </summary>
        /// <param name="username">Name of the client that has entered</param>
        /// <param name="channelName">Name of the channel the client has entered</param>
        public void ClientLeftChannel(string username, string channelName)
        {
            chatHistory.UserLeftChannel(channelName, username);
        }

        /// <summary>
        /// Recieves new channel info when joining new channel
        /// </summary>
        /// <param name="channelName">The name of the channel</param>
        /// <param name="log">The caches messages of the channel, or error message</param>
        /// <param name="userList">The comma seperated lsit of users in the channel</param>
        /// <param name="success"> Bool if sucessfully joined or not</param>
        public void NewChannelInfo(string channelName, List<string> log, List<string> userList, bool success, ChannelType channelType)
        {
            chatHistory.JoinNewChannel(channelName, log, userList, success, channelType);
        }

        /*============================================================
         *           ========= Private  Methods =========
         * =========================================================*/

        private void RecieveErrorMessage(string error)
        {

        }
    }

    /// <summary>
    /// The message type that is being sent or recieved
    /// </summary>
    public enum MessageType
    {
        Private,
        Public,
        Notification
    }

    public enum ChannelType
    {
        PrivateMessage,
        Public
    }

    public enum NotificationType
    {
        Default,
        UserEntered,
        UserLeft,
        MessageNotSentError,
        ConnectionFailure,
        FailedToJoinChannel,
        DisconnectedFromServer,
        DisconnectedFromChannel,
        KickedFromChannel
    }
}
