﻿using UnityEngine;
using System.Collections;

namespace CoreSys.Lobby.Chat
{
    public class SendChatData : MonoBehaviour
    {
        public RecieveChatData recieveMessage;
        public ServerIntermediary serverIntermediary;

        public bool debugModeEnabled;

        //Sends a new chat room message to the server
        public void SendNewMessage(string name, string message, string channelName)
        {
            PublicMessageType newMessage = new PublicMessageType(name, message, channelName);
            if(!debugModeEnabled)
            {
                serverIntermediary.PublicMessageSend(newMessage);
            }
            else
            {
                recieveMessage.NewPublicMessage(newMessage);
            }
        }

        //Sends a new private message to the server
        public void SendPrivateMessage(string fromName, string toName, string message)
        {
            PrivateMessageType newMessage = new PrivateMessageType(fromName, toName, message);
            if (!debugModeEnabled)
            {
                serverIntermediary.PrivateMessageSend(newMessage);
            }
            else
            {
                recieveMessage.NewPrivateMessage(newMessage);
            }
        }

        //Sends a channel join request to the server
        public void SendChannelJoinRequest(string userName, string channelName)
        {
            PublicMessageType newMessage = new PublicMessageType(userName, channelName);
            serverIntermediary.JoinNewChannelSend(newMessage);
        }
    }
}
