﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoreSys.Lobby.Chat
{
    public class TabManager : MonoBehaviour
    {
        public GridLayoutGroup gridLayout;
        public RectTransform rectTransform;
        public Image image;
        public GameObject tabPrefab;

        private float sizeMultiplier;

        List<ChatTab> chatTabs = new List<ChatTab>();

        void Start()
        {
            //float min = rectTransform.offsetMin.x;
            //float maxX = rectTransform.offsetMax.x;
            //float maxY = rectTransform.offsetMax.y;

            //float deltaX = rectTransform.sizeDelta.x;

            //float differance = maxX - min;

            //float width = rectTransform.rect.width;

            //float increaseBy = width * 0.5f;

            //maxX += increaseBy;

            //rectTransform.offsetMax = new Vector2(maxX, maxY);
        }
        
        void Update()
        {
            GetElementHeight();
        }

        private void GetElementHeight()
        {
            Vector2 max = rectTransform.anchorMax;
            Vector2 min = rectTransform.anchorMin;

            float height = max.y - min.y;
        }

        //Creats and returns a new tab, if tabs are >= 5 it returns null and does not make the tab
        public ChatTab NewTab(string tabName)
        {
            if (chatTabs.Count < 50)
            {
                GameObject newPrefab = Instantiate(tabPrefab);
                ChatTab newTab = newPrefab.GetComponent<ChatTab>();
                chatTabs.Add(newTab);

                SetTabName(newTab, tabName);
                SetTabParent(newTab);
                SetNewTabTransform(newTab);
                newTab.isActive = true;
                return newTab;
            }
            return null;
        }

        //Sets the tab name
        private void SetTabName(ChatTab tab, string name)
        {
            tab.TabName = name;
        }

        //Sets the tabs parent to this object
        private void SetTabParent(ChatTab tab)
        {
            tab.transform.SetParent(transform);
        }
        //Sets the tab transforms
        private void SetNewTabTransform(ChatTab newTab)
        {
            //Vector2 min = newTab.cachedRectTransform.anchorMin;
            //Vector2 max = newTab.cachedRectTransform.anchorMax;

            //min.x = 0.2f * (chatTabs.Count - 1);
            //max.x = 0.2f * chatTabs.Count;

            //newTab.cachedRectTransform.anchorMin = min;
            //newTab.cachedRectTransform.anchorMax = max;

            Vector2 offsetMax = Vector2.zero;
            Vector2 offsetMin = Vector2.zero;

            newTab.cachedRectTransform.offsetMax = offsetMax;
            newTab.cachedRectTransform.offsetMin = offsetMin;

            ReadjustAllTabRects(chatTabs.Count);
        }

        //Below may or amy not be buggy

        //Returns the minimum x of that tab
        private float CalculateTabMin(int tabIndex, int tabCount)
        {
            if (tabCount <= 5)
            {
                return 0.2f * tabIndex;
            }
            else
            {
                float sizeOfTabs = 1f / tabCount; //Size of the tab by percentage of 1
                return sizeOfTabs * tabIndex; //Returns the minimum rect point for that new tab
            }
        }

        //Returns the maximum x of that tab
        private float CalculateTabMax(int tabIndex, int tabCount)
        {
            if (tabCount <= 5)
            {
                return 0.2f * (tabIndex + 1);
            }
            else
            {
                float sizeOfTabs = 1f / tabCount; //Size of the tab by percentage of 1
                return sizeOfTabs * (tabIndex + 1); //Returns the max rect point for that new tab
            }
        }

        //Readjusts the rect's of all tabs to match the new tab count when a new tab is added
        private void ReadjustAllTabRects(int tabCount)
        {
            for(int i = 0; i < tabCount; i++)
            {
                Vector2 min = chatTabs[i].cachedRectTransform.anchorMin;
                Vector2 max = chatTabs[i].cachedRectTransform.anchorMax;

                min.x = CalculateTabMin(i, tabCount);
                max.x = CalculateTabMax(i, tabCount);

                chatTabs[i].cachedRectTransform.anchorMin = min;
                chatTabs[i].cachedRectTransform.anchorMax = max;
            }
            AdjustTabBarRect(tabCount);
        }

        //Adjusts the tab bar rect to accomidate new tabs
        private void AdjustTabBarRect(int tabCount)
        {
            /* The tab bar can fit 5 tabs, each one uses 0.2/1 of the space.
             * If the number of tabs is greater than 5 you set the multiplyer
             * as 0.2 * the # of tabs to expand the frame out by 20% per new tab
             * you then get the base width, multiply it by the multiplyer
             * and add that to the X offset max to expand the panel */
            if (tabCount > 5)
            {
                sizeMultiplier = (tabCount - 5) * 0.2f;
                rectTransform.offsetMax = Vector2.zero;

                float width = rectTransform.rect.width;
                float increaseBy = width * sizeMultiplier;

                width += increaseBy;

                rectTransform.offsetMax = new Vector2(increaseBy, 0);
            }
        }




    }
}
