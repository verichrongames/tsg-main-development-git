﻿using UnityEngine;
using System;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

/*====================================================
 * This class will manage each chat tab
 * =================================================*/

namespace CoreSys.Lobby.Chat
{
    public class ChatTab : MonoBehaviour
    {

        public ChatHistory chatHistory;
        public RectTransform cachedRectTransform;
        public Text cachedTextComponent;
        public Button cachedButton;

        public Color defaultTextColor;
        public Color unreadTextColor;
        public Color pingTextColor;

        private string m_TabName;
        public string TabName
        {
            get
            {
                return m_TabName;
            }

            set
            {
                cachedTextComponent.text = value;
                m_TabName = value;
            }
        }
        public bool isActive;
        public bool isSelected;

        //Recieves referance to chat history on creation
        public void PassChatHistoryReferance(ChatHistory referance)
        {
            chatHistory = referance;
        }

        //Recieves call from the room it is aprt of when it's active state is changed
        public void SetActive(bool value)
        {
            isActive = value;
            isSelected = value;
            SetTabColor();
        }

        //Sets the text color of the tab if it was unread messages or not
        public void SetTextColor(bool read, bool ping)
        {
            if(read)
            {
                cachedTextComponent.color = defaultTextColor;
            }
            else if (!read && !ping)
            {
                cachedTextComponent.color = unreadTextColor;
            }
            else if (!read && ping)
            {
                cachedTextComponent.color = pingTextColor;

            }
        }

        //Sets the selected or unselected color of the tab
        private void SetTabColor()
        {
            if(isSelected)
            {
                ColorBlock colorBlock = cachedButton.colors;
                colorBlock.colorMultiplier = 1.7f;
                cachedButton.colors = colorBlock;
            }
            else
            {
                ColorBlock colorBlock = cachedButton.colors;
                colorBlock.colorMultiplier = 1.0f;
                cachedButton.colors = colorBlock;
            }
        }

        //Recieves call from button to switch to this tab
        public void SwitchTabs()
        {
            if(!isSelected)
            {
                chatHistory.SwitchTabs(TabName);
            }
        }
    }
}

