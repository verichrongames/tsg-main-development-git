﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace CoreSys.Lobby.Chat
{
    public class UserTab : MonoBehaviour, IPointerClickHandler
    {
        public Text userText;
        public ChatBox chatBox;
        public RectTransform cachedRectTransform;
        private UserType m_User;
        public UserType user
        {
            get
            {
                return m_User;
            }

            set
            {
                m_User = value;
                userText.text = value.FormattedName;
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            int clicks = eventData.clickCount;
            if (clicks > 1)
            {
                chatBox.PrivateMessage(user.Name);
            }
        }
    }
}
