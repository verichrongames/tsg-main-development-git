﻿using UnityEngine;
using System;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

namespace CoreSys.Lobby.Chat
{
    public class UserListManager : MonoBehaviour
    {

        public GameObject userTabPrefab;
        public ChatBox chatBox;
        public RectTransform rectTransform;
        List<UserTab> userList = new List<UserTab>();
        public Text userListHeader;
        private float sizeMultiplier;

        void Start()
        {

        }

        void Update()
        {

        }

        //Currently the entire list is re-populated each time someone joins or leaves, will need to fix.
        public void PopulateUserList(List<UserType> userList, bool repopulate)
        {
            if (repopulate && this.userList.Count > 0)
            {
                for (int i = this.userList.Count - 1; i >= 0; i--)
                {
                    DestroyUserTab(this.userList[i]);
                }
            }
            foreach(UserType user in userList)
            {
                AddNewUserToList(user);
            }
        }

        public void AddNewUserToList(UserType user)
        {
            GameObject newTab = Instantiate(userTabPrefab);
            UserTab userTab = newTab.GetComponent<UserTab>();
            userTab.chatBox = chatBox;
            userTab.user = user;
            userList.Add(userTab);

            SetTabParent(userTab);
            SetNewTabTransform(userTab);
            UpdateUserCount(userList.Count);
        }

        public void UserLeftRoom(UserType user)
        {

        }

        private void UpdateUserCount(int count)
        {
            userListHeader.text = "<b>" + count + " Users In Channel </b>";
        }

        private void DestroyUserTab(UserTab tab)
        {
            int index = userList.FindIndex(x => x.name == tab.name);
            Destroy(userList[index].gameObject);
            userList.RemoveAt(index);
        }

        //Sets the tabs parent to this object
        private void SetTabParent(UserTab tab)
        {
            tab.transform.SetParent(transform);
        }

        //Sets the tab transforms
        private void SetNewTabTransform(UserTab newTab)
        {
            Vector2 offsetMax = new Vector2(-2.0f, 0);
            Vector2 offsetMin = new Vector2(2.0f, 0);

            newTab.cachedRectTransform.offsetMax = offsetMax;
            newTab.cachedRectTransform.offsetMin = offsetMin;

            ReadjustAllTabRects(userList.Count);
        }

        //Readjusts the rect's of all tabs to match the new tab count when a new tab is added
        private void ReadjustAllTabRects(int tabCount)
        {
            for (int i = 0; i < tabCount; i++)
            {
                Vector2 min = userList[i].cachedRectTransform.anchorMin;
                Vector2 max = userList[i].cachedRectTransform.anchorMax;

                min.y = CalculateTabMin(i, tabCount);
                max.y = CalculateTabMax(i, tabCount);

                userList[i].cachedRectTransform.anchorMin = min;
                userList[i].cachedRectTransform.anchorMax = max;
            }
            AdjustTabBarRect(tabCount);
        }

        //Returns the minimum y of that tab
        private float CalculateTabMin(int tabIndex, int tabCount)
        {
            if (tabCount <= 10)
            {
                return 1 - (0.1f * (tabIndex + 1));
            }
            else
            {
                float sizeOfTabs = 1f / tabCount; //Size of the tab by percentage of 1
                return sizeOfTabs * tabIndex; //Returns the minimum rect point for that new tab
            }
        }

        //Returns the maximum y of that tab
        private float CalculateTabMax(int tabIndex, int tabCount)
        {
            if (tabCount <= 10)
            {
                return 1 - (0.1f * tabIndex);
            }
            else
            {
                float sizeOfTabs = 1f / tabCount; //Size of the tab by percentage of 1
                return sizeOfTabs * (tabIndex + 1); //Returns the max rect point for that new tab
            }
        }

        //Adjusts the tab bar rect to accomidate new tabs
        private void AdjustTabBarRect(int tabCount)
        {
            /* The user tab bar can fit 10 tabs, each one uses 0.1/1 of the space.
             * If the number of tabs is greater than 10 you set the multiplyer
             * as 0.1 * the # of tabs to expand the frame out by 10% per new tab
             * you then get the base width, multiply it by the multiplyer
             * and add that to the X offset max to expand the panel */
            if (tabCount > 10)
            {
                sizeMultiplier = (tabCount - 10) * 0.1f;
                rectTransform.offsetMin = Vector2.zero;

                float height = rectTransform.rect.height;
                Debug.Log("UserBar Before Height:" + height);
                float newHeight = height * sizeMultiplier;

                height += newHeight;

                rectTransform.offsetMin = new Vector2(0, -newHeight);
                Debug.Log("UserBar After Height:" + rectTransform.rect.height);
            }
        }
    }
}

