﻿using System.Text;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine;
using System.Collections;

namespace CoreSys.UI
{
    class FakePanel : CustomSelectable
        //IEventSystemHandler,
        //ICanvasElement,
        //IPointerClickHandler,
        //IBeginDragHandler,
        //IDragHandler,
        //IEndDragHandler,
        //IPointerDownHandler,
        //ISelectHandler,
        //IDeselectHandler,
        //IPointerEnterHandler,
        //IPointerExitHandler,
        //IUpdateSelectedHandler
    {

        void Update()
        {
            //if(Application.isPlaying)
            //{
            //    UpdateGeometry();
            //}
        }

        void Awake()
        {
            selectableTextEvents = GetComponent<SelectableTextEvents>();
        }

        void Start()
        {
        }

        void OnEnable()
        {
            selectableTextEvents.ControlAEvent += SelectAll;
            selectableTextEvents.ControlCEvent += Copy;
        }

        void OnDisable()
        {
            selectableTextEvents.ControlAEvent -= SelectAll;
            selectableTextEvents.ControlCEvent -= Copy;
        }

        public Text textBacker;
        public Color selectionColor;
        public bool canDragSelect;
        public bool multiLine = true; //Non-fucntional at this time

        private SelectableTextEvents selectableTextEvents;

        int m_CaretPosition = 0;
        int m_CaretSelectPosition = 0;
        protected int caretPositionInternal { get { return m_CaretPosition + Input.compositionString.Length; } set { m_CaretPosition = value; ClampPos(ref m_CaretPosition); } }
        protected int caretSelectPositionInternal { get { return m_CaretSelectPosition + Input.compositionString.Length; } set { m_CaretSelectPosition = value; ClampPos(ref m_CaretSelectPosition); } }
        private bool hasSelection { get { return caretPositionInternal != caretSelectPositionInternal; } }
        bool caretVisible = true; //TODO: setup caret visibility
        CanvasRenderer caretCanvasRenderer;
        RectTransform caretRectTransform;

        int drawStart;
        int drawEnd;
        private const float kHScrollSpeed = 0.05f;
        private const float kVScrollSpeed = 0.10f;
        static private readonly char[] kSeparators = { ' ', '.', ',' };
        private List<UIVertex> VBO = new List<UIVertex>(); //VBO = vertex buffer object
        private UIVertex[] cursorVerts = null;
        private TextGenerator m_InputTextCache;
        protected TextGenerator cachedInputTextGenerator
        {
            get
            {
                if (m_InputTextCache == null)
                    m_InputTextCache = new TextGenerator();

                return m_InputTextCache;
            }
        }

        bool isDragging;
        bool dragPositionOutOfBounds;
        Coroutine dragCoroutine;

        //public void OnPointerClick(PointerEventData eventData)
        //{
        //    if (eventData.button != PointerEventData.InputButton.Left)
        //        return;
        //    MarkGeometryAsDirty();
        //}

        //public void OnPointerDown(PointerEventData eventData)
        //{
        //    Vector2 position = ScreenToLocal(eventData.position);
        //    int charIndex = GetCharacterIndexFromPosition(position);

        //    Debug.Log(charIndex);
        //    caretSelectPositionInternal = caretPositionInternal = charIndex;
        //}

        //public override void OnSelect(BaseEventData eventData)
        //{
        //    base.OnSelect(eventData);
        //    Debug.Log("Selected");
        //}

        //public override void OnDeselect(BaseEventData eventData)
        //{
        //    base.OnDeselect(eventData);
        //    Debug.Log("De-Selected");
        //}

        //public override void OnPointerEnter(PointerEventData eventData)
        //{
        //    base.OnPointerEnter(eventData);
        //    Debug.Log("Pointer Enter");

        //}

        //public override void OnPointerExit(PointerEventData eventData)
        //{
        //    base.OnPointerExit(eventData);
        //    Debug.Log("Pointer Exit");

        //}

        //public void OnUpdateSelected(BaseEventData eventData)
        //{
        //    Debug.Log("Update Selected");
        //}
        /*====================================================================
         *                   Text Selection and Copying
         * ================================================================*/

        private void SelectAll()
        {
            caretPositionInternal = textBacker.text.Length;
            caretSelectPositionInternal = 0;
            MarkGeometryAsDirty();
        }

        private void Copy()
        {
            clipboard = GetSelectedString();
        }

        // TODO: Why doesnt this use GUIUtility.systemCopyBuffer??
        static string clipboard
        {
            get
            {
                TextEditor te = new TextEditor();
                te.Paste();
                return te.content.text;
            }
            set
            {
                TextEditor te = new TextEditor();
                te.content = new GUIContent(value);
                te.OnFocus();
                te.Copy();
            }
        }

        private string GetSelectedString()
        {
            if (!hasSelection)
                return "";

            int startPos = caretPositionInternal;
            int endPos = caretSelectPositionInternal;

            // Ensure pos is always less then selPos to make the code simpler
            if (startPos > endPos)
            {
                int temp = startPos;
                startPos = endPos;
                endPos = temp;
            }

            StringBuilder builder = new StringBuilder();
            for (int i = startPos; i < endPos; ++i)
            {
                builder.Append(textBacker.text[i]);
            }
            return builder.ToString();
        }

        /*====================================================================
         *                   Mouse Dragging Methods
         * ================================================================*/

        public void OnBeginDrag(PointerEventData eventData)
        {
            if(canDragSelect)
            {
                isDragging = true;
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            if(canDragSelect)
            {
                Vector2 localMousePosition;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(textBacker.rectTransform, eventData.position, eventData.pressEventCamera, out localMousePosition);
                caretSelectPositionInternal = GetCharacterIndexFromPosition(localMousePosition) + drawStart;
                MarkGeometryAsDirty();

                dragPositionOutOfBounds = !RectTransformUtility.RectangleContainsScreenPoint(textBacker.rectTransform, eventData.position, eventData.pressEventCamera);
                if(dragPositionOutOfBounds && dragCoroutine == null)
                {
                    dragCoroutine = StartCoroutine(MouseDragOutsideRect(eventData));
                }
            }
        }

        IEnumerator MouseDragOutsideRect(PointerEventData eventData)
        {
            while (canDragSelect && dragPositionOutOfBounds)
            {
                Vector2 localMousePos;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(textBacker.rectTransform, eventData.position, eventData.pressEventCamera, out localMousePos);

                Rect rect = textBacker.rectTransform.rect;

                if (multiLine)
                {
                    if (localMousePos.y > rect.yMax)
                        MoveUp(true, true);
                    else if (localMousePos.y < rect.yMin)
                        MoveDown(true, true);
                }
                else
                {
                    if (localMousePos.x < rect.xMin)
                        MoveLeft(true, false);
                    else if (localMousePos.x > rect.xMax)
                        MoveRight(true, false);
                }
                //UpdateLabel();
                float delay = multiLine ? kVScrollSpeed : kHScrollSpeed;
                yield return new WaitForSeconds(delay);
            }
            dragCoroutine = null;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (canDragSelect)
            {
                isDragging = false;
            }
        }

        /*====================================================================
         *  Pointer location and text index location methods
         * ================================================================*/

        // Converts the mouses screen point to a local point for the Text Backer
        private Vector2 ScreenToLocal(Vector2 screenPoint)
        {
            Canvas textCanvas = textBacker.canvas;
            if (textCanvas == null)
                return screenPoint;

            Vector3 position = Vector3.zero;
            if (textCanvas.renderMode == RenderMode.ScreenSpaceOverlay)
            {
                position = textBacker.transform.InverseTransformPoint(screenPoint);
            }
            else if (textCanvas.worldCamera != null)
            {
                Ray mouseRay = textCanvas.worldCamera.ScreenPointToRay(screenPoint);
                float distance;
                Plane plane = new Plane(textBacker.transform.forward, textBacker.transform.position);
                plane.Raycast(mouseRay, out distance);
                position = textBacker.transform.InverseTransformPoint(mouseRay.GetPoint(distance));
            }

            return new Vector2(position.x, position.y);
        }

        // Given an input position in local space on the Text return the index for the selection cursor at this position.
        private int GetCharacterIndexFromPosition(Vector2 position)
        {
            TextGenerator textGenerator = textBacker.cachedTextGenerator;

            int atLine = GetCharacterLineFromPosition(position, textGenerator);
            if (atLine < 0)
                return 0;
            if (atLine >= textGenerator.lineCount)
                return textGenerator.characterCountVisible;

            int startCharIndex = textGenerator.lines[atLine].startCharIdx;
            int endCharIndex = GetLineEndPosition(textGenerator, atLine);

            //Iterates through the line and compares the distance of the
            //Character in each position (on the x plane) to the start of the line
            //Also compares the distance of the cursor to the start of the line
            //Returns index when the differance between the beginning x location of the character
            //and the click is less than the differance between the end of the chacter and the click.
            for (int i = startCharIndex; i < endCharIndex; i++ )
            {
                if (i >= textGenerator.characterCountVisible)
                    break;

                UICharInfo charInfo = textGenerator.characters[i];
                Vector2 charPosition = charInfo.cursorPos / textBacker.pixelsPerUnit;

                float distanceToCharStart = position.x - charPosition.x;
                float distanceToCharEnd = charPosition.x + (charInfo.charWidth / textBacker.pixelsPerUnit) - position.x;
                if (distanceToCharStart < distanceToCharEnd)
                    return i;
            }

            return endCharIndex;
        }

        //Determines and returns the line number of the text the 
        private int GetCharacterLineFromPosition(Vector2 position, TextGenerator textGenerator)
        {
            float height = textBacker.rectTransform.rect.yMax; //Max height of Text Backer

            // Position is above first line.
            if (position.y > height)
                return -1;

            for (int i = 0; i < textGenerator.lineCount; ++i)
            {
                float lineHeight = textGenerator.lines[i].height / textBacker.pixelsPerUnit;
                if (position.y <= height && position.y > (height - lineHeight))
                    return i;
                height -= lineHeight;
            }

            // Position is after last line.
            return textGenerator.lineCount;
        }

        private int GetLineEndPosition(TextGenerator generator, int line)
        {
            line = Mathf.Max(line, 0);
            if (line + 1 < generator.lines.Count)
                return generator.lines[line + 1].startCharIdx;
            return generator.characterCountVisible;
        }

        //private int GetLineStartPosition(TextGenerator generator, int line)
        //{

        //}

        /*====================================================================
         *  Pointer caret and selection geometry methods
         * ================================================================*/

        private void UpdateGeometry()
        {
            if(caretCanvasRenderer == null && textBacker != null)
            {
                GameObject caret = new GameObject(transform.name + "Caret");
                caret.hideFlags = HideFlags.DontSave;
                caret.transform.SetParent(textBacker.transform.parent);
                caret.transform.SetAsFirstSibling();
                caret.layer = gameObject.layer;

                caretRectTransform = caret.AddComponent<RectTransform>();
                caretCanvasRenderer = caret.AddComponent<CanvasRenderer>();
                caretCanvasRenderer.SetMaterial(Graphic.defaultGraphicMaterial, null);

                caret.AddComponent<LayoutElement>().ignoreLayout = true;

                AssignPositioningIfNeeded();
            }

            if (caretCanvasRenderer == null)
                return;

            OnFillVBO(VBO);

            if (VBO.Count == 0)
                caretCanvasRenderer.SetVertices(null, 0);
            else
                caretCanvasRenderer.SetVertices(VBO.ToArray(), VBO.Count);

            VBO.Clear();
        }

        private void OnFillVBO(List<UIVertex> vbo)
        {
            //if (!isFocused)
            //    return;

            Rect inputRect = textBacker.rectTransform.rect;
            Vector2 extents = inputRect.size;

            // get the text alignment anchor point for the text in local space
            Vector2 textAnchorPivot = Text.GetTextAnchorPivot(textBacker.alignment);
            Vector2 refPoint = Vector2.zero;
            refPoint.x = Mathf.Lerp(inputRect.xMin, inputRect.xMax, textAnchorPivot.x);
            refPoint.y = Mathf.Lerp(inputRect.yMin, inputRect.yMax, textAnchorPivot.y);

            // Ajust the anchor point in screen space
            Vector2 roundedRefPoint = textBacker.PixelAdjustPoint(refPoint);

            // Determine fraction of pixel to offset text mesh.
            // This is the rounding in screen space, plus the fraction of a pixel the text anchor pivot is from the corner of the text mesh.
            Vector2 roundingOffset = roundedRefPoint - refPoint + Vector2.Scale(extents, textAnchorPivot);
            roundingOffset.x = roundingOffset.x - Mathf.Floor(0.5f + roundingOffset.x);
            roundingOffset.y = roundingOffset.y - Mathf.Floor(0.5f + roundingOffset.y);

            if (!hasSelection)
                GenerateCursor(vbo, roundingOffset);
            else
                GenerateHightlight(vbo, roundingOffset);
        }

        private void GenerateCursor(List<UIVertex> vbo, Vector2 roundingOffset)
        {
            if (!caretVisible)
                return;
            if(cursorVerts == null)
                CreateCursorVerts();

            float width = 1f;
            float height = textBacker.fontSize;
            int adjustedPosition = Mathf.Max(0, caretPositionInternal - drawStart);
            TextGenerator generator = textBacker.cachedTextGenerator;

            if(generator == null)
                return;

            if (textBacker.resizeTextForBestFit)
                height = generator.fontSizeUsedForBestFit / textBacker.pixelsPerUnit;

            Vector2 startPosition = Vector2.zero;

            // Calculate startPosition
            if (generator.characterCountVisible + 1 > adjustedPosition || adjustedPosition == 0)
            {
                UICharInfo cursorChar = generator.characters[adjustedPosition];
                startPosition.x = cursorChar.cursorPos.x;
                startPosition.y = cursorChar.cursorPos.y;
            }
            startPosition.x /= textBacker.pixelsPerUnit;

            // TODO: Only clamp when Text uses horizontal word wrap.
            if (startPosition.x > textBacker.rectTransform.rect.xMax)
                startPosition.x = textBacker.rectTransform.rect.xMax;

            cursorVerts[0].position = new Vector3(startPosition.x, startPosition.y - height, 0.0f);
            cursorVerts[1].position = new Vector3(startPosition.x + width, startPosition.y - height, 0.0f);
            cursorVerts[2].position = new Vector3(startPosition.x + width, startPosition.y, 0.0f);
            cursorVerts[3].position = new Vector3(startPosition.x, startPosition.y, 0.0f);

            if (roundingOffset != Vector2.zero)
            {
                for (int i = 0; i < cursorVerts.Length; i++)
                {
                    UIVertex uiv = cursorVerts[i];
                    uiv.position.x += roundingOffset.x;
                    uiv.position.y += roundingOffset.y;
                    vbo.Add(uiv);
                }
            }
            else
            {
                for (int i = 0; i < cursorVerts.Length; i++)
                {
                    vbo.Add(cursorVerts[i]);
                }
            }

            startPosition.y = Screen.height - startPosition.y;
            Input.compositionCursorPos = startPosition;
        }

        private void GenerateHightlight(List<UIVertex> vbo, Vector2 roundingOffset)
        {
            int startChar = Mathf.Max(0, caretPositionInternal - drawStart);
            int endChar = Mathf.Max(0, caretSelectPositionInternal - drawStart);

            // Ensure pos is always less then selPos to make the code simpler
            if (startChar > endChar)
            {
                int temp = startChar;
                startChar = endChar;
                endChar = temp;
            }

            endChar -= 1;
            TextGenerator generator = textBacker.cachedTextGenerator;

            int currentLineIndex = DetermineCharacterLine(startChar, generator);
            float height = textBacker.fontSize;

            if (textBacker.resizeTextForBestFit)
                height = generator.fontSizeUsedForBestFit / textBacker.pixelsPerUnit;

            if (generator != null && generator.lines.Count > 0)
            {
                // TODO: deal with multiple lines with different line heights.
                height = generator.lines[0].height;
            }

            if (textBacker.resizeTextForBestFit && generator != null)
            {
                height = generator.fontSizeUsedForBestFit;
            }

            //int currentLineIndex = DetermineCharacterLine(startChar, gen);
            //float height = textBacker.fontSize;

            //if (textBacker.resizeTextForBestFit)
            //    height = gen.fontSizeUsedForBestFit / textBacker.pixelsPerUnit;

            //if (cachedInputTextGenerator != null && cachedInputTextGenerator.lines.Count > 0)
            //{
            //    // TODO: deal with multiple lines with different line heights.
            //    height = cachedInputTextGenerator.lines[0].height;
            //}

            //if (textBacker.resizeTextForBestFit && cachedInputTextGenerator != null)
            //{
            //    height = cachedInputTextGenerator.fontSizeUsedForBestFit;
            //}

            int nextLineStartIdx = GetLineEndPosition(generator, currentLineIndex);

            UIVertex vert = UIVertex.simpleVert;
            vert.uv0 = Vector2.zero;
            vert.color = selectionColor;

            int currentChar = startChar;
            while (currentChar <= endChar && currentChar < generator.characterCountVisible)
            {
                if (currentChar + 1 == nextLineStartIdx || currentChar == endChar)
                {
                    UICharInfo startCharInfo = generator.characters[startChar];
                    UICharInfo endCharInfo = generator.characters[currentChar];
                    Vector2 startPosition = new Vector2(startCharInfo.cursorPos.x / textBacker.pixelsPerUnit, startCharInfo.cursorPos.y);
                    Vector2 endPosition = new Vector2((endCharInfo.cursorPos.x + endCharInfo.charWidth) / textBacker.pixelsPerUnit, startPosition.y - height / textBacker.pixelsPerUnit);

                    // Checking xMin as well due to text generator not setting possition if char is not rendered.
                    if (endPosition.x > textBacker.rectTransform.rect.xMax || endPosition.x < textBacker.rectTransform.rect.xMin)
                        endPosition.x = textBacker.rectTransform.rect.xMax;

                    vert.position = new Vector3(startPosition.x, endPosition.y, 0.0f) + (Vector3)roundingOffset;
                    vbo.Add(vert);

                    vert.position = new Vector3(endPosition.x, endPosition.y, 0.0f) + (Vector3)roundingOffset;
                    vbo.Add(vert);

                    vert.position = new Vector3(endPosition.x, startPosition.y, 0.0f) + (Vector3)roundingOffset;
                    vbo.Add(vert);

                    vert.position = new Vector3(startPosition.x, startPosition.y, 0.0f) + (Vector3)roundingOffset;
                    vbo.Add(vert);

                    startChar = currentChar + 1;
                    currentLineIndex++;

                    nextLineStartIdx = GetLineEndPosition(generator, currentLineIndex);
                }
                currentChar++;
            }
        }

        //??
        private int DetermineCharacterLine(int charPos, TextGenerator generator)
        {
            for (int i = 0; i < generator.lineCount - 1; ++i)
            {
                if (generator.lines[i + 1].startCharIdx > charPos)
                    return i;
            }

            return generator.lineCount - 1;
        }

        private void CreateCursorVerts()
        {
            cursorVerts = new UIVertex[4];

            for (int i = 0; i < cursorVerts.Length; i++)
            {
                cursorVerts[i] = UIVertex.simpleVert;
                cursorVerts[i].color = textBacker.color;
                cursorVerts[i].uv0 = Vector2.zero;
            }
        }
        
        //??
        private void AssignPositioningIfNeeded()
        {
            if (textBacker != null && caretRectTransform != null &&
                (caretRectTransform.localPosition != textBacker.rectTransform.localPosition ||
                 caretRectTransform.localRotation != textBacker.rectTransform.localRotation ||
                 caretRectTransform.localScale != textBacker.rectTransform.localScale ||
                 caretRectTransform.anchorMin != textBacker.rectTransform.anchorMin ||
                 caretRectTransform.anchorMax != textBacker.rectTransform.anchorMax ||
                 caretRectTransform.anchoredPosition != textBacker.rectTransform.anchoredPosition ||
                 caretRectTransform.sizeDelta != textBacker.rectTransform.sizeDelta ||
                 caretRectTransform.pivot != textBacker.rectTransform.pivot))
            {
                caretRectTransform.localPosition = textBacker.rectTransform.localPosition;
                caretRectTransform.localRotation = textBacker.rectTransform.localRotation;
                caretRectTransform.localScale = textBacker.rectTransform.localScale;
                caretRectTransform.anchorMin = textBacker.rectTransform.anchorMin;
                caretRectTransform.anchorMax = textBacker.rectTransform.anchorMax;
                caretRectTransform.anchoredPosition = textBacker.rectTransform.anchoredPosition;
                caretRectTransform.sizeDelta = textBacker.rectTransform.sizeDelta;
                caretRectTransform.pivot = textBacker.rectTransform.pivot;
            }
        }

        //??
        protected void ClampPos(ref int pos)
        {
            if (pos < 0) pos = 0;
            else if (pos > textBacker.text.Length) pos = textBacker.text.Length;
            
        }


        /*====================================================================
        *  Supporting Misc. Methods
        * ================================================================*/


        private void MarkGeometryAsDirty()
        {
            //CanvasUpdateRegistry.RegisterCanvasElementForGraphicRebuild(this);
        }

        //Calls for canvas rebuild after MarkGeometryAsDirty gets called
        public virtual void Rebuild(CanvasUpdate update)
        {
            switch (update)
            {
                case CanvasUpdate.LatePreRender:
                    UpdateGeometry();
                    break;
            }
        }

        private int FindtPrevWordBegin()
        {
            if (caretSelectPositionInternal - 2 < 0)
                return 0;

            int spaceLoc = textBacker.text.LastIndexOfAny(kSeparators, caretSelectPositionInternal - 2);

            if (spaceLoc == -1)
                spaceLoc = 0;
            else
                spaceLoc++;

            return spaceLoc;
        }

        private int FindtNextWordBegin()
        {
            if (caretSelectPositionInternal + 1 >= textBacker.text.Length)
                return textBacker.text.Length;

            int spaceLoc = textBacker.text.IndexOfAny(kSeparators, caretSelectPositionInternal + 1);

            if (spaceLoc == -1)
                spaceLoc = textBacker.text.Length;
            else
                spaceLoc++;

            return spaceLoc;
        }

        //??
        private int LineUpCharacterPosition(int originalPos, bool goToFirstChar)
        {
            if (originalPos >= cachedInputTextGenerator.characterCountVisible)
                return 0;

            UICharInfo originChar = cachedInputTextGenerator.characters[originalPos];
            int originLine = DetermineCharacterLine(originalPos, cachedInputTextGenerator);

            // We are on the last line return last character
            if (originLine - 1 < 0)
                return goToFirstChar ? 0 : originalPos;


            int endCharIdx = cachedInputTextGenerator.lines[originLine].startCharIdx - 1;

            for (int i = cachedInputTextGenerator.lines[originLine - 1].startCharIdx; i < endCharIdx; ++i)
            {
                if (cachedInputTextGenerator.characters[i].cursorPos.x >= originChar.cursorPos.x)
                    return i;
            }
            return endCharIdx;
        }

        //??
        private int LineDownCharacterPosition(int originalPos, bool goToLastChar)
        {
            if (originalPos >= cachedInputTextGenerator.characterCountVisible)
                return textBacker.text.Length;

            UICharInfo originChar = cachedInputTextGenerator.characters[originalPos];
            int originLine = DetermineCharacterLine(originalPos, cachedInputTextGenerator);

            // We are on the last line return last character
            if (originLine + 1 >= cachedInputTextGenerator.lineCount)
                return goToLastChar ? textBacker.text.Length : originalPos;

            // Need to determine end line for next line.
            int endCharIdx = GetLineEndPosition(cachedInputTextGenerator, originLine + 1);

            for (int i = cachedInputTextGenerator.lines[originLine + 1].startCharIdx; i < endCharIdx; ++i)
            {
                if (cachedInputTextGenerator.characters[i].cursorPos.x >= originChar.cursorPos.x)
                    return i;
            }
            return endCharIdx;
        }

        //??
        private void MoveUp(bool shift, bool goToFirstChar)
        {
            if (hasSelection && !shift)
            {
                // If we have a selection and press up without shift,
                // set caret position to start of selection before we move it up.
                caretPositionInternal = caretSelectPositionInternal = Mathf.Min(caretPositionInternal, caretSelectPositionInternal);
            }

            int position = multiLine ? LineUpCharacterPosition(caretSelectPositionInternal, goToFirstChar) : 0;

            if (shift)
                caretSelectPositionInternal = position;
            else
                caretSelectPositionInternal = caretPositionInternal = position;
        }

        //??
        private void MoveDown(bool shift, bool goToLastChar)
        {
            if (hasSelection && !shift)
            {
                // If we have a selection and press down without shift,
                // set caret position to end of selection before we move it down.
                caretPositionInternal = caretSelectPositionInternal = Mathf.Max(caretPositionInternal, caretSelectPositionInternal);
            }

            int position = multiLine ? LineDownCharacterPosition(caretSelectPositionInternal, goToLastChar) : textBacker.text.Length;

            if (shift)
                caretSelectPositionInternal = position;
            else
                caretPositionInternal = caretSelectPositionInternal = position;
        }

        //??
        private void MoveLeft(bool shift, bool ctrl)
        {
            if (hasSelection && !shift)
            {
                // By convention, if we have a selection and move left without holding shift,
                // we just place the cursor at the start.
                caretPositionInternal = caretSelectPositionInternal = Mathf.Min(caretPositionInternal, caretSelectPositionInternal);
                return;
            }

            int position;
            if (ctrl)
                position = FindtPrevWordBegin();
            else
                position = caretSelectPositionInternal - 1;

            if (shift)
                caretSelectPositionInternal = position;
            else
                caretSelectPositionInternal = caretPositionInternal = position;
        }

        private void MoveRight(bool shift, bool ctrl)
        {
            if (hasSelection && !shift)
            {
                // By convention, if we have a selection and move right without holding shift,
                // we just place the cursor at the end.
                caretPositionInternal = caretSelectPositionInternal = Mathf.Max(caretPositionInternal, caretSelectPositionInternal);
                return;
            }

            int position;
            if (ctrl)
                position = FindtNextWordBegin();
            else
                position = caretSelectPositionInternal + 1;

            if (shift)
                caretSelectPositionInternal = position;
            else
                caretSelectPositionInternal = caretPositionInternal = position;
        }
    }
}
