﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

namespace CoreSys.UI
{
    public class SelectableTextEvents : MonoBehaviour
    {
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            ShiftKeyHeld();
            ShiftKeyUp();
            ShiftKeyDown();

            ControlKeyHeld();
            ControlKeyUp();
            ControlKeyDown();

            CKeyHeld();
            CKeyUp();
            CKeyDown();

            AKeyHeld();
            AKeyUp();
            AKeyDown();

            ControlA();
            ControlC();
        }

        public bool controlHeld = false;
        public bool aHeld = false;
        public bool cHeld = false;

        public delegate void MiscKeysHandler();
        public delegate void PrimaryKeysHandler();
        public delegate void KeyComboHandler();

        public event MiscKeysHandler ShiftHeldEvent;
        public event MiscKeysHandler ShiftUpEvent;
        public event MiscKeysHandler ShiftDownEvent;

        private void ShiftKeyHeld()
        {
            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
            {
                if (ShiftHeldEvent != null)
                {
                    ShiftHeldEvent();
                }
            }
        }
        private void ShiftKeyUp()
        {
            if (Input.GetKeyUp(KeyCode.LeftShift) || Input.GetKeyUp(KeyCode.RightShift))
            {
                if (ShiftUpEvent != null)
                {
                    ShiftUpEvent();
                }
            }
        }
        private void ShiftKeyDown()
        {
            if (Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift))
            {
                if (ShiftDownEvent != null)
                {
                    ShiftDownEvent();
                }
            }
        }


        public event MiscKeysHandler ControlHeldEvent;
        public event MiscKeysHandler ControlUpEvent;
        public event MiscKeysHandler ControlDownEvent;

        private void ControlKeyHeld()
        {
            if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
            {
                if (ControlHeldEvent != null)
                {
                    ControlHeldEvent();
                }
            }
        }
        private void ControlKeyUp()
        {
            if (Input.GetKeyUp(KeyCode.LeftControl) || Input.GetKeyUp(KeyCode.RightControl))
            {
                if (ControlUpEvent != null)
                {
                    ControlUpEvent();
                }
                controlHeld = false;
            }
        }
        private void ControlKeyDown()
        {
            if (Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.RightControl))
            {
                if (ControlDownEvent != null)
                {
                    ControlDownEvent();
                }
                controlHeld = true;
            }
        }

        public event PrimaryKeysHandler CHeldEvent;
        public event PrimaryKeysHandler CUpEvent;
        public event PrimaryKeysHandler CDownEvent;

        private void CKeyHeld()
        {
            if (Input.GetKey(KeyCode.C) || Input.GetKey(KeyCode.C))
            {
                if (CHeldEvent != null)
                {
                    CHeldEvent();
                }
            }
        }
        private void CKeyUp()
        {
            if (Input.GetKeyUp(KeyCode.C) || Input.GetKeyUp(KeyCode.C))
            {
                if (CUpEvent != null)
                {
                    CUpEvent();
                }
                cHeld = false;
            }
        }
        private void CKeyDown()
        {
            if (Input.GetKeyDown(KeyCode.C) || Input.GetKeyDown(KeyCode.C))
            {
                if (CDownEvent != null)
                {
                    CDownEvent();
                }
                cHeld = true;
            }
        }

        public event PrimaryKeysHandler AHeldEvent;
        public event PrimaryKeysHandler AUpEvent;
        public event PrimaryKeysHandler ADownEvent;

        private void AKeyHeld()
        {
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.A))
            {
                if (AHeldEvent != null)
                {
                    AHeldEvent();
                }
            }
        }
        private void AKeyUp()
        {
            if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.A))
            {
                if (AUpEvent != null)
                {
                    AUpEvent();
                }
                aHeld = false;
            }
        }
        private void AKeyDown()
        {
            if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.A))
            {
                if (ADownEvent != null)
                {
                    ADownEvent();
                }
                aHeld = true;
            }
        }

        public event KeyComboHandler ControlAEvent;
        public event KeyComboHandler ControlCEvent;

        private void ControlA()
        {
            if (controlHeld && aHeld)
            {
                if (ControlAEvent != null)
                {
                    ControlAEvent();
                    Debug.Log("ctrl a");
                }
            }
        }
        private void ControlC()
        {
            if (controlHeld && cHeld)
            {
                if (ControlCEvent != null)
                {
                    ControlCEvent();
                }
            }
        }
    }
}
