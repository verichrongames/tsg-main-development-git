﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CoreSys.UI
{
    public static class ChatFontUpdateTracker
    {
        static Dictionary<Font, List<ChatText>> m_Tracked = new Dictionary<Font, List<ChatText>>();

        public static void TrackText(ChatText t)
        {
            if (t.font == null)
                return;

            List<ChatText> exists;
            m_Tracked.TryGetValue(t.font, out exists);
            if (exists == null)
            {
                exists = new List<ChatText>();
                m_Tracked.Add(t.font, exists);

                t.font.textureRebuildCallback += RebuildForFont(t.font);
            }

            for (int i = 0; i < exists.Count; i++)
            {
                if (exists[i] == t)
                    return;
            }

            exists.Add(t);
        }

        private static Font.FontTextureRebuildCallback RebuildForFont(Font f)
        {
            return () =>
            {
                List<ChatText> texts;
                m_Tracked.TryGetValue(f, out texts);

                if (texts == null)
                    return;

                for (var i = 0; i < texts.Count; i++)
                    texts[i].FontTextureChanged();
            };
        }

        public static void UntrackText(ChatText t)
        {
            if (t.font == null)
                return;

            List<ChatText> texts;
            m_Tracked.TryGetValue(t.font, out texts);

            if (texts == null)
                return;

            texts.Remove(t);
        }
    }
}