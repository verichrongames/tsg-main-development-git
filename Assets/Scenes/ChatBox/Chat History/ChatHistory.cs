﻿using UnityEngine;
using System;
using System.Text.RegularExpressions;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.UI;
using CoreSys.UI;

namespace CoreSys.Lobby.Chat
{
    public class ChatHistory : MonoBehaviour
    {

        public ChatText textBacker;
        public Scrollbar verticalScrollBar;
        public CustomScrollRect scrollRect;
        public TabManager tabManager;
        public ChatNotifications chatNotifications;
        public UserListManager userListManager;

        public string channelJoinFailMessage;

        public string MyUsername;

        void Start()
        {
            //chatChannels.Add(new ChatRoomType("Main", "", 0, MessageType.Public));
        }

        public ChatRoomType currentChatRoom;
        List<ChatRoomType> chatChannels = new List<ChatRoomType>();


        /*====================================================
         *      ===== Public Methods =====
         * =================================================*/

        //Recieves call for new message within a chat room
        public void NewRoomMessage(string name, string message, string channelName)
        {
            if (chatChannels.Count > 0)
            {
                ChatRoomType chatRoom = FindChannel(channelName);
                if (chatRoom == null)
                {
                    chatRoom = CreateNewChannel(channelName, ChannelType.Public);
                }
                string formattedText = FormatMessage(name, message);
                AppendNewMessage(formattedText, chatRoom);
            }
            else
            {
                ChatRoomType chatRoom = CreateNewChannel(channelName, ChannelType.Public);
                chatRoom.IsActive = true;
                currentChatRoom = chatRoom;
                currentChatRoom.RoomTab.SetActive(true);

                string formattedText = FormatMessage(name, message);
                AppendNewMessage(formattedText, chatRoom);
            }
        }

        //Recieves call for a new private message
        public void NewPrivateMessage(string name, string message)
        {
            ChatRoomType privateChat = FindChannel(name);
            if (privateChat == null)
            {
                privateChat = CreateNewChannel(name, ChannelType.PrivateMessage);
            }

            string formattedText = FormatMessage(name, message);
            AppendNewMessage(formattedText, privateChat);
        }

        //Recieves call to enter a new channel
        public void JoinNewChannel(string channelName, List<string> log, List<string> userList, bool success, ChannelType channelType)
        {
            ChatRoomType newChannel = CreateNewChannel(channelName, channelType);
            if(!success)
            {
                newChannel.Text += channelJoinFailMessage;
            }
            else
            {
                newChannel.Text = StitchStringList(log);
                List<UserType> usersInChannel = new List<UserType>();
                foreach(string user in userList)
                {
                    UserType newUserType = new UserType(user);
                    newUserType.FormattedName = "<color=" + CalculateColorHash(user) + ">" + user + "</color>";
                    usersInChannel.Add(newUserType);
                }

                newChannel.RoomTab.SwitchTabs();
                newChannel.AddListOfUsers(usersInChannel);
                //UserEnteredChannel(channelName, MyUsername);
            }
        }

        //Recieves a call whenever a user has left a channel this client is subscribed to
        public void UserLeftChannel(string channelName, string username)
        {
            ChatRoomType chatRoom = FindChannel(channelName);
            if(chatRoom != null)
            {
                chatRoom.UserLeftRoom(username);
            }
            AppendChannelNotifications(chatRoom, NotificationType.UserLeft, username, "");
            //Make call for user mamagement
        }

        //Recieves a call whenever a user has entered a channel this client is subscribed to
        public void UserEnteredChannel(string channelName, string username)
        {
            ChatRoomType chatRoom = FindChannel(channelName);
            if(chatRoom != null)
            {
                UserType user = new UserType(username);
                user.FormattedName = "<color=" + CalculateColorHash(username) + ">" + username + "</color>";
                chatRoom.UserEnteredRoom(user);
                AppendChannelNotifications(chatRoom, NotificationType.UserEntered, username, "");
            }
            //Make call for user mamagement
        }

        /*====================================================
         *      ===== Message Handling Methods =====
         * =================================================*/


        //Finds and returns the requested channel, if not found one is created and returned
        private ChatRoomType FindChannel(string channelName)
        {
            int channelLocation = chatChannels.FindIndex(x => x.Name == channelName);
            if(channelLocation >= 0)
            {
                return chatChannels[channelLocation];
            }
            else
            {
                return null;
            }
        }

        //Creates and returns new channel and tab, adds new channel to list
        private ChatRoomType CreateNewChannel(string channelName, ChannelType roomType)
        {
            ChatTab newTab = tabManager.NewTab(channelName);
            newTab.PassChatHistoryReferance(this);
            ChatRoomType newRoom = new ChatRoomType(channelName, "", 0, roomType, newTab, userListManager);
            chatChannels.Add(newRoom);
            return newRoom;
        }

        //Formats and returns the string that will be added to the text string
        private string FormatMessage(string name, string message)
        {
            string hexcolor = CalculateColorHash(name);
            string formattedText = "<color=" + hexcolor + "><size=12>(" + DateTime.Now.ToString("hh:mm:ss tt") + ")</size> " + name + "</color>: " + message;
            return formattedText;
        }

        //Appends the new message to the channel
        private void AppendNewMessage(string formattedMessage, ChatRoomType channel)
        {
            channel.Text = LimitTextLength(16000, channel.Text, formattedMessage) + "\n";
            if(channel.IsActive)
            {
                string reformattedText = formattedMessage + "\n"; //Adds the new line to each message before sending it to the TextGenerator
                textBacker.text = reformattedText;
                StartCoroutine(ZeroScrollBar());
                StartCoroutine(GetLineCount(textBacker));
            }

            //Test
            Debug.Log(textBacker.cachedTextGenerator.lineCount);
        }

        IEnumerator GetLineCount(ChatText text)
        {
            yield return new WaitForEndOfFrame();

            Debug.Log("Line Count: " + text.cachedTextGenerator.lineCount);
        }

        //Zeros the scrollbar at the end of frame
        IEnumerator ZeroScrollBar()
        {
            yield return new WaitForSeconds(0.04f);
            verticalScrollBar.value = 0;
        }

        private string LimitTextLength(int length, string textBody, string toAdd)
        {
            int bodyLength = textBody.Length;
            int toAddLength = toAdd.Length;

            if(textBody.Length + toAdd.Length > length)
            {
                int differance = textBody.Length + toAdd.Length - length;
                return textBody.Remove(0, differance);
            }
            else
            {
                return textBody + toAdd;
            }
        }

        /*====================================================
         *    ===== User List Handling Methods =====
         * =================================================*/


       
        /*====================================================
         *   ===== Tab and Scrollbar Handling Methods =====
         * =================================================*/

        //Recieves Call To Switch Tab
        public void SwitchTabs(string tabName)
        {
            ChatRoomType chatRoom = FindChannel(tabName);
            if(chatRoom != null)
            {
                SwitchChatRoom(chatRoom);
            }
        }

        //Performs actions to switch to a new tab
        private void SwitchChatRoom(ChatRoomType chatRoom)
        {
            if(currentChatRoom != null)
            {
                SaveRoomScrollLocation(currentChatRoom);
                currentChatRoom.IsActive = false; //Old chat is not active
            }
            chatRoom.IsActive = true; //New chat is active
            currentChatRoom = chatRoom; //reassign current chat room
            SwitchChannelText(chatRoom);
            StartCoroutine(SetScrollBarLocation(chatRoom));
        }

        //Saves the chat rooms scroll location if it is switched away from
        private void SaveRoomScrollLocation(ChatRoomType chatRoom)
        {
            chatRoom.OldScrollBarHeight = verticalScrollBar.size;
            chatRoom.OldScrollBarLocation = verticalScrollBar.value;
        }

        //Sets the scrollbar location for a chatroom when switching back
        //Is a coroutine becuase it needs to happen after the scrollbar has adjusted it's size next frame
        IEnumerator SetScrollBarLocation(ChatRoomType chatRoom)
        {
            yield return new WaitForSeconds(0.04f);
            scrollRect.Rebuild(CanvasUpdate.PostLayout); //Rebuilds the scrollbar size

            if (chatRoom.OldScrollBarLocation >= 0.01f)
            {
                verticalScrollBar.value = CalculateScrollBarLocation(chatRoom.OldScrollBarLocation, chatRoom.OldScrollBarHeight, verticalScrollBar.size);
            }
            else
            {
                verticalScrollBar.value = 0;
            }
        }

        /* Calculates the scrollbars location when a user switches back to an old tab they has scrolled in
         * Formula: 1 - ((1 - oldLocation)*(newHeight/oldHeight)
         * This is necessary so that if a user switches to another tab, and goes back to the tab he switched from
         * his scrollbar will stay in the same location relative to where it was in the text even if that chat channel
         * has become larger and the scrollbar has re-scaled.
         */
        private float CalculateScrollBarLocation(float oldLocation, float oldHeight, float newHeight)
        {
            return 1 - ((1 - oldLocation)*(newHeight/oldHeight));   
        }

        //Switches the text to the text of the alternate chat room
        private void SwitchChannelText(ChatRoomType chatRoom)
        {
            textBacker.text = chatRoom.Text;
        }

        /*====================================================
         *      ===== Misc Channel Methods =====
         * =================================================*/

        private string StitchStringList(List<string> strings)
        {
            string output = String.Empty;
            foreach(string message in strings)
            {
                output += message;
            }

            return output;
        }

        //Appends a new message to the channel
        private void AppendChannelNotifications(ChatRoomType channel, NotificationType notificationType, string username, string channelName)
        {
            if (channelName == "")
            {
                channelName = channel.Name;
            }
            string message = chatNotifications.GetNotificationText(notificationType);

            Regex regex = new Regex(@"(?<=\{)[^}]*(?=\})", RegexOptions.IgnoreCase);
            MatchCollection matches = regex.Matches(message);

            foreach (Match match in matches)
            {
                foreach (Capture capture in match.Captures)
                {
                    if (capture.Value.ToLower().Contains("user"))
                    {
                        message = message.Replace("{" + capture.Value + "}", username);
                    }
                    else if (capture.Value.ToLower().Contains("channel") || capture.Value.ToLower().Contains("room"))
                    {
                        message = message.Replace("{" + capture.Value + "}", channel.Name);
                    }
                }
            }

            channel.Text += message + "\n";
            if (channel.IsActive)
            {
                textBacker.text = message + "\n";
            }
        }


        /*===========================================================================
         *          =======  String To Hex Color Hashing  =======
         *                 Will need to move to it's own class
         * ========================================================================*/

        //Manages the calls to create and convert the hash
        string CalculateColorHash(string name)
        {
            float[] saturation = new float[3] { 0.35f, 0.5f, 0.65f };
            float[] lightness = new float[3] { 0.25f, 0.35f, 0.45f };

            int hash = StringToHash(name);
            float[] HSL = CalculateHSL(hash, lightness, saturation);
            Color stringColor = HSL2RGB(HSL[0], HSL[1], HSL[2]);

            int[] colorInt = new int[3] { (int)stringColor.r, (int)stringColor.g, (int)stringColor.b };
            string hexcolor = "#" + colorInt[0].ToString("X") + colorInt[1].ToString("X") + colorInt[2].ToString("X");

            return hexcolor;

        }

        private int StringToHash(string name)
        {
            int seed = 131;
            int seed2 = 137;
            long hash = 0;
            name += 'x';

            long MAX_SAFE_INT = 9007199254740991 / seed2;

            char[] charArray = name.ToCharArray();

            for(int i = 0; i < name.Length; i++)
            {
                if(hash >= MAX_SAFE_INT)
                {
                    hash = hash / seed2;
                }
                hash = hash * seed + (int)charArray[i];
            }
            return Mathf.Abs((int)hash); //Abs to avoid negative numbers
        }

        //This Calculates the HSL for the hash
        private float[] CalculateHSL(int hash, float[] lightness, float[] saturation)
        {
            float H, S, L;

            H = hash % 359;
            hash = hash / 360;
            S = saturation[hash % saturation.Length];
            hash = hash / saturation.Length;
            float test = hash % lightness.Length;
            L = lightness[hash % lightness.Length];

            float[] returnValue = new float[3]{ H, S, L };
            return returnValue;
        }

        //Converts HSL to RGB
        public Color HSL2RGB(double H, double S, double L)
        {
            H = H / 360;
            double v;
            double r, g, b;

            r = L;   // default to gray
            g = L;
            b = L;
            v = (L <= 0.5) ? (L * (1.0 + S)) : (L + S - L * S);
            if (v > 0)
            {
                double m;
                double sv;
                int sextant;
                double fract, vsf, mid1, mid2;

                m = L + L - v;
                sv = (v - m) / v;
                H *= 6.0;
                sextant = (int)H;
                fract = H - sextant;
                vsf = v * sv * fract;
                mid1 = m + vsf;
                mid2 = v - vsf;
                switch (sextant)
                {
                    case 0:
                        r = v;
                        g = mid1;
                        b = m;
                        break;
                    case 1:
                        r = mid2;
                        g = v;
                        b = m;
                        break;
                    case 2:
                        r = m;
                        g = v;
                        b = mid1;
                        break;
                    case 3:
                        r = m;
                        g = mid2;
                        b = v;
                        break;
                    case 4:
                        r = mid1;
                        g = m;
                        b = v;
                        break;
                    case 5:
                        r = v;
                        g = m;
                        b = mid2;
                        break;
                }
            }
            Color rgb = new Color();
            rgb.r = Convert.ToByte(r * 255.0f);
            rgb.g = Convert.ToByte(g * 255.0f);
            rgb.b = Convert.ToByte(b * 255.0f);
            return rgb;
        }

    }

    /// <summary>
    /// Holds all data for a local chat room
    /// </summary>
    public class ChatRoomType
    {
        public ChatRoomType(string name, string text, int lineCount, ChannelType roomType, ChatTab roomTab, UserListManager userListManager)
        {
            RoomTab = roomTab;

            Name = name;
            Text = text;
            LineCount = lineCount;
            RoomType = roomType;
            this.userListManager = userListManager;
        }

        public UserListManager userListManager;
        public float OldScrollBarHeight { get; set; } //The size of the old scrollbar
        public float OldScrollBarLocation { get; set; } //The "value" of the scrollbar
        public string Name {get; set;} // Channel Name
        private List<UserType> m_UsersList = new List<UserType>();

        //List of all users in channel
        public List<UserType> UsersList {get { return m_UsersList; }} 

        //Adds a list of usernames to the channel
        public void AddListOfUsers(List<UserType> users)
        {
            m_UsersList.AddRange(users);
            if(IsActive)
            {
                userListManager.PopulateUserList(UsersList, true);
            }
        }

        //Adds the new user to the list and notifies the UI
        public void UserEnteredRoom(UserType newUser)
        {
            m_UsersList.Add(newUser);
            UsersList.Sort();
            if(IsActive)
            {
                userListManager.PopulateUserList(UsersList, true); //TOFIX don't repopulate the entire list each time someone leaves or enters
            }
        }

        //Removes the user from the list and notifies the UI
        public void UserLeftRoom(string username)
        {
            int indexOfName = m_UsersList.FindIndex(x => x.Name == username);
            if(indexOfName != -1)
            {
                UsersList.RemoveAt(indexOfName);
                if(IsActive)
                {
                    userListManager.PopulateUserList(UsersList, true);
                }
            }
        }
        //Sets the text value. If the text value is set and this is not an active tab, then the tabs color changes to notify of unread messages
        private string m_Text;
        public string Text
        {
            get
            {
                return m_Text;
            }

            set
            {
                m_Text = value;
                if(IsActive)
                {

                }
                else if(!IsActive && RoomType == ChannelType.Public)
                {
                    RoomTab.SetTextColor(IsActive, false);
                }
                else if (!IsActive && RoomType == ChannelType.PrivateMessage)
                {
                    RoomTab.SetTextColor(IsActive, true);
                }
            }
        }

        //Signifies if the chat is active, when the chat becomes the active chat the tab color will change to signify no unread messages
        private bool m_IsActive;
        public bool IsActive
        {
            get
            {
                return m_IsActive;
            }

            set
            {
                m_IsActive = value;
                RoomTab.SetActive(value);
                if(value)
                {
                    RoomTab.SetTextColor(value, false);
                }
            }
        }
        public int LineCount {get; set;}
        public ChannelType RoomType {get; set;}
        public ChatTab RoomTab { get; set; }
    }

    /// <summary>
    /// Encapsualted type for all chat messages
    /// </summary>
    public class ChatMessageType
    {
        public ChatMessageType(string username, string content, string channelName, MessageType messageType)
        {
            Username = username;
            content = Contents;
            ChannelName = channelName;
            MessageType = messageType;
        }

        public string Username { get; set; }
        public string Contents { get; set; }
        public string ChannelName { get; set; }
        public MessageType MessageType { get; set; }
    }

    /// <summary>
    /// A Class that contains a user
    /// </summary>
    public class UserType : IComparable<UserType>
    {
        public UserType(string username)
        {
            Name = username;
        }

        public string Name { get; set; }
        public string FormattedName { get; set; }
        public int ID { get; set; }

        public int CompareTo(UserType b)
        {
            return this.Name.CompareTo(b.Name);
        }
    }
}
