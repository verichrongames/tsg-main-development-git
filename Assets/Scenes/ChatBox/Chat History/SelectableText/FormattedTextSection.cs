﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*================================================
 *  Will store all the information for
 *  each section of formatted text
 * =============================================*/

namespace CoreSys.UI
{
    public class FormattedTextSection
    {
        public FormattedTextSection(string rawString, string cleanString, int indexStart, int indexEnd, List<RichTextType> openingBrackets, List<RichTextType> closingBrackets)
        {
            this.rawString = rawString;
            this.cleanString = cleanString;
            this.indexStart = indexStart;
            this.indexEnd = indexEnd;
            this.formattingOpening = openingBrackets;
            this.formattingClosing = closingBrackets;
        }

        public FormattedTextSection(string rawString, string cleanString, List<RichTextType> openingBrackets, List<RichTextType> closingBrackets)
        {
            this.rawString = rawString;
            this.cleanString = cleanString;
            this.formattingOpening = openingBrackets;
            this.formattingClosing = closingBrackets;
        }

        public string cleanString { get; set; }
        public string rawString { get; set; }

        public int indexStart { get; set; }
        public int indexEnd { get; set; }

        List<RichTextType> formattingOpening { get; set; }
        List<RichTextType> formattingClosing { get; set; }
    }
}
