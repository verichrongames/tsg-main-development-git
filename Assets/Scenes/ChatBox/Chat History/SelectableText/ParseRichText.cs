﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CoreSys.UI
{
    public static class ParseRichText
    {
        static string regexPattern = "(<[^>]+>)";

        public static List<FormattedTextSection> ParseText(string value)
        {
            return SplitIntoSections(value);
        }

        //Splits the input value into raw sections
        private static List<FormattedTextSection> SplitIntoSections(string value)
        {
            List<FormattedTextSection> sectionsFinal = new List<FormattedTextSection>();
            string[] splitValue = Regex.Split(value, regexPattern);


            //Flagged if the current section runs into a closing bracket while parsing.
            //To identify if the next non-bracket word is the start of a new section
            bool currentHasClosingBrackets = false;
            List<string> currentSectionRaw = new List<string>();
            List<string> currentSectionClean = new List<string>();
            List<RichTextType> openingBrackets = new List<RichTextType>();
            List<RichTextType> closingBrackets = new List<RichTextType>();

            int i = 0;
            foreach (string split in splitValue)
            {
                i++;
                if (split.StartsWith("<"))
                {
                    if (split.StartsWith("</"))
                    {
                        currentSectionRaw.Add(split);
                        closingBrackets.Add(new RichTextType(split));
                        currentHasClosingBrackets = true;
                    }
                    else
                    {
                        currentSectionRaw.Add(split);
                        openingBrackets.Add(new RichTextType(split));
                    }
                }
                else
                {
                    if (!currentHasClosingBrackets)
                    {
                        currentSectionRaw.Add(split);
                        currentSectionClean.Add(split);
                    }
                    else
                    {
                        sectionsFinal.Add(MakeFormattedText(currentSectionRaw, currentSectionClean, openingBrackets, closingBrackets));
                        currentSectionRaw.Clear();
                        currentSectionClean.Clear();
                        openingBrackets.Clear();
                        closingBrackets.Clear();

                        currentSectionClean.Add(split);
                        currentSectionRaw.Add(split);

                        currentHasClosingBrackets = false;
                    }
                }
            }

            sectionsFinal.Add(MakeFormattedText(currentSectionRaw, currentSectionClean, openingBrackets, closingBrackets));

            return sectionsFinal;

        }

        //Adds a new section to the sections final
        private static FormattedTextSection MakeFormattedText(List<string> currentSectionRaw, List<string> currentSectionClean, List<RichTextType> openingBrackets, List<RichTextType> closingBrackets)
        {
            return new FormattedTextSection(StitchStrings(currentSectionRaw), StitchStrings(currentSectionClean), openingBrackets, closingBrackets);
        }

        //Stitches together an array of strings into a single string
        private static string StitchStrings(List<string> value)
        {
            string output = String.Empty;
            foreach(string section in value)
            {
                output += section;
            }
            return output;
        }

    }
}
