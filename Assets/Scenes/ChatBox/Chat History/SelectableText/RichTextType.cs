﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*================================================
 *  Stores the info on the type of rich text
 *  that is present in the text section
 * =============================================*/

namespace CoreSys.UI
{
    public class RichTextType
    {
        public RichTextType(string value)
        {
            formatValue = value;
        }
        public string formatValue { get; set; }

        public bool opensInThisSection { get; set; }
        public bool closesInThisSection { get; set; }
    }
}
