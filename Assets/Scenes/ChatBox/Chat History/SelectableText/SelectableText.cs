﻿using System.Text;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine;
using System.Collections;
using System.Reflection;
using System;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace CoreSys.UI
{
    [ExecuteInEditMode]
    public class SelectableText
        : MonoBehaviour,
        IEventSystemHandler,
        ICanvasElement,
        //IPointerClickHandler,
        IBeginDragHandler,
        IDragHandler,
        IEndDragHandler,
        ISelectHandler,
        IDeselectHandler,
        IPointerDownHandler,
        //IPointerUpHandler,
        //IPointerEnterHandler,
        //IPointerExitHandler,
        IUpdateSelectedHandler
    {

        void Update()
        {
            //if(Application.isPlaying)
            //{
            //    UpdateGeometry();
            //}
            if(UnityEngine.Application.isEditor)
            {
                //textBacker.UnformattedTextBool = RichText;
            }
        }

        void Awake()
        {
        }

        void Start()
        {
            MarkGeometryAsDirty();
        }

        void OnEnable()
        {

        }

        void OnDisable()
        {

        }


        //public GameObject caret;
        public GameObject caretReferance;
        public bool isFocused;
        public Color selectionColor;
        public bool canDragSelect;
        public bool multiLine = true; //Non-fucntional at this time

        [SerializeField]
        private bool m_RichText;

        public bool RichText
        {
            get
            {
                return m_RichText;
            }
            set
            {
                textBacker.UnformattedTextBool = value;
                m_RichText = value;
            }
        }

        /*=======================================
         *       Public Fields For Caret
         * ====================================*/
        public bool hasSelection { get { return caretPositionInternal != caretSelectPositionInternal; } }
        public ChatText textBacker;



        int m_CaretPosition = 0;
        int m_CaretSelectPosition = 0;
        protected int caretPositionInternal { get { return m_CaretPosition + Input.compositionString.Length; } set { m_CaretPosition = value; ClampPos(ref m_CaretPosition); } }
        protected int caretSelectPositionInternal { get { return m_CaretSelectPosition + Input.compositionString.Length; } set { m_CaretSelectPosition = value; ClampPos(ref m_CaretSelectPosition); } }
        bool caretVisible = true; //TODO: setup caret visibility
        CanvasRenderer caretCanvasRenderer;
        RectTransform caretRectTransform;
        int drawStart;
        int drawEnd;
        private const float kHScrollSpeed = 0.05f;
        private const float kVScrollSpeed = 0.10f;
        static private readonly char[] kSeparators = { ' ', '.', ',' };
        private List<UIVertex> VBO = new List<UIVertex>(); //VBO = vertex buffer object
        private UIVertex[] cursorVerts = null;
        private TextGenerator m_InputTextCache;
        protected TextGenerator cachedInputTextGenerator
        {
            get
            {
                if (m_InputTextCache == null)
                    m_InputTextCache = new TextGenerator();

                return m_InputTextCache;
            }
        }

        bool m_IsDragging;
        bool IsDragging
        {
            get
            {
                return m_IsDragging;
            }

            set
            {
                m_IsDragging = value;

                textBacker.NormalTextBool = !value;
                textBacker.UnformattedTextBool = value;
            }
        }
        bool dragPositionOutOfBounds;
        Coroutine dragCoroutine;

        public void OnPointerClick(PointerEventData eventData)
        {
            if (eventData.button != PointerEventData.InputButton.Left)
                return;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (eventData.button != PointerEventData.InputButton.Left)
                return;

            Vector2 position = ScreenToLocal(eventData.position);
            int charIndex = GetCharacterIndexFromPosition(position);
            caretSelectPositionInternal = caretPositionInternal = charIndex;
            UnityEngine.Debug.Log("Pointer Down At: " + charIndex);
            MarkGeometryAsDirty();

            // Selection tracking
            EventSystem.current.SetSelectedGameObject(gameObject, eventData);

            //isPointerDown = true;
            //EvaluateAndTransitionToSelectionState(eventData);
        }

        public void OnSelect(BaseEventData eventData)
        {
            //base.OnSelect(eventData);
            isFocused = true;
        }

        public void OnDeselect(BaseEventData eventData)
        {
            //base.OnDeselect(eventData);
            isFocused = false;
        }

        //public override void OnPointerEnter(PointerEventData eventData)
        //{
        //    base.OnPointerEnter(eventData);
        //    Debug.Log("Pointer Enter");

        //}

        //public override void OnPointerExit(PointerEventData eventData)
        //{
        //    base.OnPointerExit(eventData);
        //    Debug.Log("Pointer Exit");

        //}

        /*====================================================================
         *                   Input Key Detection Methods
         * ================================================================*/

        //Recieved update ticks when selected
        public void OnUpdateSelected(BaseEventData eventData)
        {
            UnityEngine.Debug.Log("Update Selected");

            if (!isFocused)
                return;

            bool consumedEvent = false;
            while (Event.PopEvent(m_ProcessingEvent))
            {
                if (m_ProcessingEvent.rawType == EventType.KeyDown)
                {
                    consumedEvent = true;
                    KeyPressed(m_ProcessingEvent);
                }
            }
            if (consumedEvent)
                MarkGeometryAsDirty();

            eventData.Use();
        }


        /// Handle the specified event.
        private Event m_ProcessingEvent = new Event();


        protected void KeyPressed(Event evt)
        {
            var currentEventModifiers = evt.modifiers;
            RuntimePlatform rp = UnityEngine.Application.platform;
            bool isMac = (rp == RuntimePlatform.OSXEditor || rp == RuntimePlatform.OSXPlayer || rp == RuntimePlatform.OSXWebPlayer);
            bool ctrl = isMac ? (currentEventModifiers & EventModifiers.Command) != 0 : (currentEventModifiers & EventModifiers.Control) != 0;
            bool shift = (currentEventModifiers & EventModifiers.Shift) != 0;
            bool alt = (currentEventModifiers & EventModifiers.Alt) != 0;
            bool ctrlOnly = ctrl && !alt && !shift;

            switch (evt.keyCode)
            {
                case KeyCode.Home:
                    {
                        //MoveTextStart(shift);
                        break;
                    }

                case KeyCode.End:
                    {
                        //MoveTextEnd(shift);
                        break;
                    }

                // Select All
                case KeyCode.A:
                    {
                        if (ctrlOnly)
                        {
                            SelectAll();
                            break;
                        }
                        break;
                    }

                // Copy
                case KeyCode.C:
                    {
                        if (ctrlOnly)
                        {
                            clipboard = GetSelectedString();
                            break;
                        }
                        break;
                    }
                case KeyCode.LeftArrow:
                    {
                        MoveLeft(shift, ctrl);
                        break;
                    }

                case KeyCode.RightArrow:
                    {
                        MoveRight(shift, ctrl);
                        break;
                    }

                case KeyCode.UpArrow:
                    {
                        MoveUp(shift, ctrl);
                        break;
                    }

                case KeyCode.DownArrow:
                    {
                        MoveDown(shift, ctrl);
                        break;
                    }
            }
            }

        /*====================================================================
         *                   Text Selection and Copying
         * ================================================================*/

        private void SelectAll()
        {
            caretPositionInternal = textBacker.text.Length;
            caretSelectPositionInternal = 0;
            MarkGeometryAsDirty();
        }

        private void Copy()
        {
            clipboard = GetSelectedString();
        }

        // TODO: Why doesnt this use GUIUtility.systemCopyBuffer??
        static string clipboard
        {
            get
            {
                TextEditor te = new TextEditor();
                te.Paste();
                return te.content.text;
            }
            set
            {
                //ClipboardHelper.clipBoard = value;
                //ClipboardHelper.CopyToClipboard(value, value);
                //TestClipboard.CopyHtmlToClipBoard(value);
                //TextEditor te = new TextEditor();
                //te.content = new GUIContent(value);
                //te.OnFocus();
                //te.Copy();
                string[] args = new string[1];
                args[0] = value;
                //Process process = new Process();
                //process.StartInfo.FileName = "CopyToClipboard.exe";
                //process.StartInfo.Arguments = args;
                Process.Start(@"Assets\Scenes\ChatBox\Chat History\SelectableText\CopyToClipboard.exe", value);
                //StreamWriter writer = new StreamWriter("important.txt");
                //writer.Write("Word ");
                //writer.WriteLine("word 2");
                //writer.WriteLine("Line");
                //writer.Close();
            }
        }

        private string GetSelectedString()
        {
            if (!hasSelection)
                return "";

            int startPos = caretPositionInternal;
            int endPos = caretSelectPositionInternal;

            // Ensure pos is always less then selPos to make the code simpler
            if (startPos > endPos)
            {
                int temp = startPos;
                startPos = endPos;
                endPos = temp;
            }

            StringBuilder builder = new StringBuilder();
            for (int i = startPos; i < endPos; ++i)
            {
                builder.Append(textBacker.text[i]);
            }
            return builder.ToString();
        }

        /*====================================================================
         *                   Mouse Dragging Methods
         * ================================================================*/

        public void OnBeginDrag(PointerEventData eventData)
        {
            if(canDragSelect)
            {
                IsDragging = true;
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            if(canDragSelect)
            {
                UnityEngine.Debug.Log("On Drag At: " + caretPositionInternal);
                Vector2 localMousePosition;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(textBacker.rectTransform, eventData.position, eventData.pressEventCamera, out localMousePosition);
                caretSelectPositionInternal = GetCharacterIndexFromPosition(localMousePosition) + drawStart;
                MarkGeometryAsDirty();

                dragPositionOutOfBounds = !RectTransformUtility.RectangleContainsScreenPoint(textBacker.rectTransform, eventData.position, eventData.pressEventCamera);
                if(dragPositionOutOfBounds && dragCoroutine == null)
                {
                    dragCoroutine = StartCoroutine(MouseDragOutsideRect(eventData));
                }
            }
        }

        IEnumerator MouseDragOutsideRect(PointerEventData eventData)
        {
            while (canDragSelect && dragPositionOutOfBounds)
            {
                Vector2 localMousePos;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(textBacker.rectTransform, eventData.position, eventData.pressEventCamera, out localMousePos);

                Rect rect = textBacker.rectTransform.rect;

                if (multiLine)
                {
                    if (localMousePos.y > rect.yMax)
                        MoveUp(true, true);
                    else if (localMousePos.y < rect.yMin)
                        MoveDown(true, true);
                }
                else
                {
                    if (localMousePos.x < rect.xMin)
                        MoveLeft(true, false);
                    else if (localMousePos.x > rect.xMax)
                        MoveRight(true, false);
                }
                //UpdateLabel();
                float delay = multiLine ? kVScrollSpeed : kHScrollSpeed;
                yield return new WaitForSeconds(delay);
            }
            dragCoroutine = null;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (canDragSelect)
            {
                UnityEngine.Debug.Log("End Drag At: " + caretSelectPositionInternal);
                IsDragging = false;
            }
        }

        /*====================================================================
         *  Pointer location and text index location methods
         * ================================================================*/

        // Converts the mouses screen point to a local point for the Text Backer
        private Vector2 ScreenToLocal(Vector2 screenPoint)
        {
            Canvas textCanvas = textBacker.canvas;
            if (textCanvas == null)
                return screenPoint;

            Vector3 position = Vector3.zero;
            if (textCanvas.renderMode == RenderMode.ScreenSpaceOverlay)
            {
                position = textBacker.transform.InverseTransformPoint(screenPoint);
            }
            else if (textCanvas.worldCamera != null)
            {
                Ray mouseRay = textCanvas.worldCamera.ScreenPointToRay(screenPoint);
                float distance;
                Plane plane = new Plane(textBacker.transform.forward, textBacker.transform.position);
                plane.Raycast(mouseRay, out distance);
                position = textBacker.transform.InverseTransformPoint(mouseRay.GetPoint(distance));
            }

            return new Vector2(position.x, position.y);
        }

        // Given an input position in local space on the Text return the index for the selection cursor at this position.
        private int GetCharacterIndexFromPosition(Vector2 position)
        {
            //TextGenerator returns faulty values sometimes, which are massive numbers in the negatives or positives. 
            //This will allow the method to ignroe those and move past them by only returning if the valeus are less than this
            float maxAllowableDistance = 1000;
            TextGenerator textGenerator = textBacker.cachedTextGenerator;

            int atLine = GetCharacterLineFromPosition(position, textGenerator);
            if (atLine < 0)
                return 0;
            if (atLine >= textGenerator.lineCount)
                return textGenerator.characterCountVisible;

            int startCharIndex = textGenerator.lines[atLine].startCharIdx;
            int endCharIndex = GetLineEndPosition(textGenerator, atLine);

            //Iterates through the line and compares the distance of the
            //Character in each position (on the x plane) to the start of the line
            //Also compares the distance of the cursor to the start of the line
            //Returns index when the differance between the beginning x location of the character
            //and the click is less than the differance between the end of the chacter and the click.
            for (int i = startCharIndex; i < endCharIndex; i++ )
            {
                if (i >= textGenerator.characterCountVisible)
                    break;

                UICharInfo charInfo = textGenerator.characters[i];
                Vector2 charPosition = charInfo.cursorPos / textBacker.pixelsPerUnit;

                float distanceToCharStart = position.x - charPosition.x;
                float distanceToCharEnd = charPosition.x + (charInfo.charWidth / textBacker.pixelsPerUnit) - position.x;
                if ((distanceToCharStart > -maxAllowableDistance && distanceToCharStart < maxAllowableDistance) && (distanceToCharEnd > -maxAllowableDistance && distanceToCharEnd < maxAllowableDistance))
                    if (distanceToCharStart < distanceToCharEnd)
                        return i;
            }

            return endCharIndex;
        }

        //Determines and returns the line number of the text the 
        private int GetCharacterLineFromPosition(Vector2 position, TextGenerator textGenerator)
        {
            float height = textBacker.rectTransform.rect.yMax; //Max height of Text Backer

            // Position is above first line.
            if (position.y > height)
                return -1;

            for (int i = 0; i < textGenerator.lineCount; ++i)
            {
                float lineHeight = textGenerator.lines[i].height / textBacker.pixelsPerUnit;
                if (position.y <= height && position.y > (height - lineHeight))
                    return i;
                height -= lineHeight;
            }

            // Position is after last line.
            return textGenerator.lineCount;
        }

        private int GetLineEndPosition(TextGenerator generator, int line)
        {
            line = Mathf.Max(line, 0);
            if (line + 1 < generator.lines.Count)
                return generator.lines[line + 1].startCharIdx;
            return generator.characterCountVisible;
        }

        //private int GetLineStartPosition(TextGenerator generator, int line)
        //{

        //}

        /*====================================================================
         *  Pointer caret and selection geometry methods
         * ================================================================*/

        private void UpdateGeometry()
        {
            if(caretCanvasRenderer == null && textBacker != null)
            {
                GameObject caret = caretReferance;
                //GameObject caret = new GameObject(transform.name + "Caret");
                //caret.hideFlags = HideFlags.DontSave;
                //caret.transform.SetParent(textBacker.transform);
                //caret.transform.SetAsFirstSibling();
                //caret.layer = gameObject.layer;

                caretRectTransform = caret.GetComponent<RectTransform>();
                caretCanvasRenderer = caret.GetComponent<CanvasRenderer>();
                caretCanvasRenderer.SetMaterial(Graphic.defaultGraphicMaterial, null);
                //caretCanvasRenderer.isMask = true;

                //caret.AddComponent<LayoutElement>().ignoreLayout = true;

                AssignPositioningIfNeeded();
            }
            //AssignPositioningIfNeeded();
            if (caretCanvasRenderer == null)
                return;

            OnFillVBO(VBO);

            if (VBO.Count == 0)
                caretCanvasRenderer.SetVertices(null, 0);
            else
                caretCanvasRenderer.SetVertices(VBO);

            //caretReferance.GetComponent<Caret>().SetAllDirty();
            //caretReferance.
                //caretCanvasRenderer.SetVertices(VBO.ToArray(), VBO.Count);

            VBO.Clear();
            if(!caretReferance.activeInHierarchy)
                StartCoroutine(LateEnable(caretReferance));
            //caretReferance.GetComponent<Caret>().SetAllDirty();

        }

        //Necessary to set up the caret properly the first time.
        //If this is not done the caret ignores masks
        IEnumerator LateEnable(GameObject gameObject)
        {
            yield return new WaitForEndOfFrame();
            //caretReferance.GetComponent<Caret>().SetAllDirty();
            gameObject.SetActive(true);
        }

        private void OnFillVBO(List<UIVertex> vbo)
        {
            //if (!isFocused)
            //    return;

            Rect inputRect = textBacker.rectTransform.rect;
            Vector2 extents = inputRect.size;

            // get the text alignment anchor point for the text in local space
            Vector2 textAnchorPivot = Text.GetTextAnchorPivot(textBacker.alignment);
            Vector2 refPoint = Vector2.zero;
            refPoint.x = Mathf.Lerp(inputRect.xMin, inputRect.xMax, textAnchorPivot.x);
            refPoint.y = Mathf.Lerp(inputRect.yMin, inputRect.yMax, textAnchorPivot.y);

            // Ajust the anchor point in screen space
            Vector2 roundedRefPoint = textBacker.PixelAdjustPoint(refPoint);

            // Determine fraction of pixel to offset text mesh.
            // This is the rounding in screen space, plus the fraction of a pixel the text anchor pivot is from the corner of the text mesh.
            Vector2 roundingOffset = roundedRefPoint - refPoint + Vector2.Scale(extents, textAnchorPivot);
            roundingOffset.x = roundingOffset.x - Mathf.Floor(0.5f + roundingOffset.x);
            roundingOffset.y = roundingOffset.y - Mathf.Floor(0.4f + roundingOffset.y);

            if (!hasSelection)
                GenerateCursor(vbo, roundingOffset);
            else
                GenerateHightlight(vbo, roundingOffset);
        }

        private void GenerateCursor(List<UIVertex> vbo, Vector2 roundingOffset)
        {
            if (!caretVisible)
                return;
            if(cursorVerts == null)
                CreateCursorVerts();

            float width = 1f;
            float height = textBacker.fontSize;
            int adjustedPosition = Mathf.Max(0, caretPositionInternal - drawStart);
            TextGenerator generator;

            if(IsDragging)
            {
                generator = textBacker.cachedUnformattedTextGenerator;
            }
            else
            {
                generator = textBacker.cachedTextGenerator;
            }

            if(generator == null)
                return;

            if (textBacker.resizeTextForBestFit)
                height = generator.fontSizeUsedForBestFit / textBacker.pixelsPerUnit;

            Vector2 startPosition = Vector2.zero;

            // Calculate startPosition
            if (generator.characterCountVisible + 1 > adjustedPosition || adjustedPosition == 0)
            {
                UICharInfo cursorChar = generator.characters[adjustedPosition];
                startPosition.x = cursorChar.cursorPos.x;
                startPosition.y = cursorChar.cursorPos.y;
            }
            startPosition.x /= textBacker.pixelsPerUnit;

            // TODO: Only clamp when Text uses horizontal word wrap.
            if (startPosition.x > textBacker.rectTransform.rect.xMax)
                startPosition.x = textBacker.rectTransform.rect.xMax;

            cursorVerts[0].position = new Vector3(startPosition.x, startPosition.y - height, 0.0f);
            cursorVerts[1].position = new Vector3(startPosition.x + width, startPosition.y - height, 0.0f);
            cursorVerts[2].position = new Vector3(startPosition.x + width, startPosition.y, 0.0f);
            cursorVerts[3].position = new Vector3(startPosition.x, startPosition.y, 0.0f);

            if (roundingOffset != Vector2.zero)
            {
                for (int i = 0; i < cursorVerts.Length; i++)
                {
                    UIVertex uiv = cursorVerts[i];
                    uiv.position.x += roundingOffset.x;
                    uiv.position.y += roundingOffset.y;
                    vbo.Add(uiv);
                }
            }
            else
            {
                for (int i = 0; i < cursorVerts.Length; i++)
                {
                    vbo.Add(cursorVerts[i]);
                }
            }

            startPosition.y = UnityEngine.Screen.height - startPosition.y;
            Input.compositionCursorPos = startPosition;
        }

        private void GenerateHightlight(List<UIVertex> vbo, Vector2 roundingOffset)
        {
            int startChar = Mathf.Max(0, caretPositionInternal - drawStart);
            int endChar = Mathf.Max(0, caretSelectPositionInternal - drawStart);

            // Ensure pos is always less then selPos to make the code simpler
            if (startChar > endChar)
            {
                int temp = startChar;
                startChar = endChar;
                endChar = temp;
            }

            endChar -= 1;
            TextGenerator generator;

            if (IsDragging)
            {
                generator = textBacker.cachedUnformattedTextGenerator;
            }
            else
            {
                generator = textBacker.cachedTextGenerator;
            }

            int currentLineIndex = DetermineCharacterLine(startChar, generator);
            float height = textBacker.fontSize;

            if (textBacker.resizeTextForBestFit)
                height = generator.fontSizeUsedForBestFit / textBacker.pixelsPerUnit;

            if (generator != null && generator.lines.Count > 0)
            {
                // TODO: deal with multiple lines with different line heights.
                height = generator.lines[0].height;
            }

            if (textBacker.resizeTextForBestFit && generator != null)
            {
                height = generator.fontSizeUsedForBestFit;
            }

            int nextLineStartIdx = GetLineEndPosition(generator, currentLineIndex);

            UIVertex vert = UIVertex.simpleVert;
            vert.uv0 = Vector2.zero;
            vert.color = selectionColor;

            int currentChar = startChar;
            while (currentChar <= endChar && currentChar < generator.characterCountVisible)
            {
                if (currentChar + 1 == nextLineStartIdx || currentChar == endChar)
                {
                    UICharInfo startCharInfo = generator.characters[startChar];
                    UICharInfo endCharInfo = generator.characters[currentChar];
                    Vector2 startPosition = new Vector2(startCharInfo.cursorPos.x / textBacker.pixelsPerUnit, startCharInfo.cursorPos.y);
                    Vector2 endPosition = new Vector2((endCharInfo.cursorPos.x + endCharInfo.charWidth) / textBacker.pixelsPerUnit, startPosition.y - height / textBacker.pixelsPerUnit);

                    // Checking xMin as well due to text generator not setting possition if char is not rendered.
                    if (endPosition.x > textBacker.rectTransform.rect.xMax || endPosition.x < textBacker.rectTransform.rect.xMin)
                        endPosition.x = textBacker.rectTransform.rect.xMax;

                    vert.position = new Vector3(startPosition.x, endPosition.y, 0.0f) + (Vector3)roundingOffset;
                    vbo.Add(vert);

                    vert.position = new Vector3(endPosition.x, endPosition.y, 0.0f) + (Vector3)roundingOffset;
                    vbo.Add(vert);

                    vert.position = new Vector3(endPosition.x, startPosition.y, 0.0f) + (Vector3)roundingOffset;
                    vbo.Add(vert);

                    vert.position = new Vector3(startPosition.x, startPosition.y, 0.0f) + (Vector3)roundingOffset;
                    vbo.Add(vert);

                    startChar = currentChar + 1;
                    currentLineIndex++;

                    nextLineStartIdx = GetLineEndPosition(generator, currentLineIndex);
                }
                currentChar++;
            }
        }

        //??
        private int DetermineCharacterLine(int charPos, TextGenerator generator)
        {
            for (int i = 0; i < generator.lineCount - 1; ++i)
            {
                if (generator.lines[i + 1].startCharIdx > charPos)
                    return i;
            }

            return generator.lineCount - 1;
        }

        private void CreateCursorVerts()
        {
            cursorVerts = new UIVertex[4];

            for (int i = 0; i < cursorVerts.Length; i++)
            {
                cursorVerts[i] = UIVertex.simpleVert;
                cursorVerts[i].color = textBacker.color;
                cursorVerts[i].uv0 = Vector2.zero;
            }
        }
        
        //Positions the caret's position and rect transform to match the text backer
        private void AssignPositioningIfNeeded()
        {
            if (textBacker != null && caretRectTransform != null &&
                (caretRectTransform.localPosition != textBacker.rectTransform.localPosition ||
                 caretRectTransform.localRotation != textBacker.rectTransform.localRotation ||
                 caretRectTransform.localScale != textBacker.rectTransform.localScale ||
                 caretRectTransform.anchorMin != textBacker.rectTransform.anchorMin ||
                 caretRectTransform.anchorMax != textBacker.rectTransform.anchorMax ||
                 caretRectTransform.anchoredPosition != textBacker.rectTransform.anchoredPosition ||
                 caretRectTransform.sizeDelta != textBacker.rectTransform.sizeDelta ||
                 caretRectTransform.pivot != textBacker.rectTransform.pivot))
            {
                //caretRectTransform.localPosition = textBacker.rectTransform.localPosition;
                //caretRectTransform.localRotation = textBacker.rectTransform.localRotation;
                //caretRectTransform.localScale = textBacker.rectTransform.localScale;
                //caretRectTransform.anchorMin = textBacker.rectTransform.anchorMin;
                //caretRectTransform.anchorMax = textBacker.rectTransform.anchorMax;
                //caretRectTransform.anchoredPosition = textBacker.rectTransform.anchoredPosition;
                //caretRectTransform.sizeDelta = textBacker.rectTransform.sizeDelta;
                caretRectTransform.pivot = textBacker.rectTransform.pivot;
            }
        }

        //??
        protected void ClampPos(ref int pos)
        {
            if (pos < 0) pos = 0;
            else if (pos > textBacker.text.Length) pos = textBacker.text.Length;
            
        }


        /*====================================================================
        *  Supporting Misc. Methods
        * ================================================================*/


        private void MarkGeometryAsDirty()
        {
            CanvasUpdateRegistry.RegisterCanvasElementForGraphicRebuild(this);
        }

        //Calls for canvas rebuild after MarkGeometryAsDirty gets called
        public virtual void Rebuild(CanvasUpdate update)
        {
            switch (update)
            {
                case CanvasUpdate.LatePreRender:
                    UpdateGeometry();
                    break;
            }
        }

        public bool IsDestroyed()
        {
            return false;
        }

        private int FindtPrevWordBegin()
        {
            if (caretSelectPositionInternal - 2 < 0)
                return 0;

            int spaceLoc = textBacker.text.LastIndexOfAny(kSeparators, caretSelectPositionInternal - 2);

            if (spaceLoc == -1)
                spaceLoc = 0;
            else
                spaceLoc++;

            return spaceLoc;
        }

        private int FindtNextWordBegin()
        {
            if (caretSelectPositionInternal + 1 >= textBacker.text.Length)
                return textBacker.text.Length;

            int spaceLoc = textBacker.text.IndexOfAny(kSeparators, caretSelectPositionInternal + 1);

            if (spaceLoc == -1)
                spaceLoc = textBacker.text.Length;
            else
                spaceLoc++;

            return spaceLoc;
        }

        //??
        private int LineUpCharacterPosition(int originalPos, bool goToFirstChar)
        {
            if (originalPos >= cachedInputTextGenerator.characterCountVisible)
                return 0;

            UICharInfo originChar = cachedInputTextGenerator.characters[originalPos];
            int originLine = DetermineCharacterLine(originalPos, cachedInputTextGenerator);

            // We are on the last line return last character
            if (originLine - 1 < 0)
                return goToFirstChar ? 0 : originalPos;


            int endCharIdx = cachedInputTextGenerator.lines[originLine].startCharIdx - 1;

            for (int i = cachedInputTextGenerator.lines[originLine - 1].startCharIdx; i < endCharIdx; ++i)
            {
                if (cachedInputTextGenerator.characters[i].cursorPos.x >= originChar.cursorPos.x)
                    return i;
            }
            return endCharIdx;
        }

        //??
        private int LineDownCharacterPosition(int originalPos, bool goToLastChar)
        {
            if (originalPos >= cachedInputTextGenerator.characterCountVisible)
                return textBacker.text.Length;

            UICharInfo originChar = cachedInputTextGenerator.characters[originalPos];
            int originLine = DetermineCharacterLine(originalPos, cachedInputTextGenerator);

            // We are on the last line return last character
            if (originLine + 1 >= cachedInputTextGenerator.lineCount)
                return goToLastChar ? textBacker.text.Length : originalPos;

            // Need to determine end line for next line.
            int endCharIdx = GetLineEndPosition(cachedInputTextGenerator, originLine + 1);

            for (int i = cachedInputTextGenerator.lines[originLine + 1].startCharIdx; i < endCharIdx; ++i)
            {
                if (cachedInputTextGenerator.characters[i].cursorPos.x >= originChar.cursorPos.x)
                    return i;
            }
            return endCharIdx;
        }

        //??
        private void MoveUp(bool shift, bool goToFirstChar)
        {
            if (hasSelection && !shift)
            {
                // If we have a selection and press up without shift,
                // set caret position to start of selection before we move it up.
                caretPositionInternal = caretSelectPositionInternal = Mathf.Min(caretPositionInternal, caretSelectPositionInternal);
            }

            int position = multiLine ? LineUpCharacterPosition(caretSelectPositionInternal, goToFirstChar) : 0;

            if (shift)
                caretSelectPositionInternal = position;
            else
                caretSelectPositionInternal = caretPositionInternal = position;
        }

        //??
        private void MoveDown(bool shift, bool goToLastChar)
        {
            if (hasSelection && !shift)
            {
                // If we have a selection and press down without shift,
                // set caret position to end of selection before we move it down.
                caretPositionInternal = caretSelectPositionInternal = Mathf.Max(caretPositionInternal, caretSelectPositionInternal);
            }

            int position = multiLine ? LineDownCharacterPosition(caretSelectPositionInternal, goToLastChar) : textBacker.text.Length;

            if (shift)
                caretSelectPositionInternal = position;
            else
                caretPositionInternal = caretSelectPositionInternal = position;
        }

        //??
        private void MoveLeft(bool shift, bool ctrl)
        {
            if (hasSelection && !shift)
            {
                // By convention, if we have a selection and move left without holding shift,
                // we just place the cursor at the start.
                caretPositionInternal = caretSelectPositionInternal = Mathf.Min(caretPositionInternal, caretSelectPositionInternal);
                return;
            }

            int position;
            if (ctrl)
                position = FindtPrevWordBegin();
            else
                position = caretSelectPositionInternal - 1;

            if (shift)
                caretSelectPositionInternal = position;
            else
                caretSelectPositionInternal = caretPositionInternal = position;
        }

        private void MoveRight(bool shift, bool ctrl)
        {
            if (hasSelection && !shift)
            {
                // By convention, if we have a selection and move right without holding shift,
                // we just place the cursor at the end.
                caretPositionInternal = caretSelectPositionInternal = Mathf.Max(caretPositionInternal, caretSelectPositionInternal);
                return;
            }

            int position;
            if (ctrl)
                position = FindtNextWordBegin();
            else
                position = caretSelectPositionInternal + 1;

            if (shift)
                caretSelectPositionInternal = position;
            else
                caretSelectPositionInternal = caretPositionInternal = position;
        }
    }

    public class UnityClipboardHelper
    {
        private static PropertyInfo m_systemCopyBufferProperty = null;
        private static PropertyInfo GetSystemCopyBufferProperty()
        {
            if (m_systemCopyBufferProperty == null)
            {
                Type T = typeof(GUIUtility);
                m_systemCopyBufferProperty = T.GetProperty("systemCopyBuffer", BindingFlags.Static | BindingFlags.NonPublic);
                if (m_systemCopyBufferProperty == null)
                    throw new Exception("Can't access internal member 'GUIUtility.systemCopyBuffer' it may have been removed / renamed");
            }
            return m_systemCopyBufferProperty;
        }
        public static string clipBoard
        {
            get
            {
                PropertyInfo P = GetSystemCopyBufferProperty();
                return (string)P.GetValue(null, null);
            }
            set
            {
                PropertyInfo P = GetSystemCopyBufferProperty();
                P.SetValue(null, value, null);
            }
        }
    }

    /// <summary>      
    /// Helper to  encode and set HTML fragment to clipboard.<br/>      
    /// See <br/>      
    /// <seealso  cref="CreateDataObject"/>.      
    ///  </summary>      
    /// <remarks>      
    /// The MIT License  (MIT) Copyright (c) 2014 Arthur Teplitzki.      
    ///  </remarks>      
    public static class ClipboardHelper
    {
        #region Fields and Consts

        /// <summary>      
        /// The string contains index references to  other spots in the string, so we need placeholders so we can compute the  offsets. <br/>      
        /// The  <![CDATA[<<<<<<<]]>_ strings are just placeholders.  We'll back-patch them actual values afterwards. <br/>      
        /// The string layout  (<![CDATA[<<<]]>) also ensures that it can't appear in the body  of the html because the <![CDATA[<]]> <br/>      
        /// character must be escaped. <br/>      
        /// </summary>      
        private const string Header = @"Version:0.9      
StartHTML:<<<<<<<<1      
EndHTML:<<<<<<<<2      
StartFragment:<<<<<<<<3      
EndFragment:<<<<<<<<4      
StartSelection:<<<<<<<<3      
EndSelection:<<<<<<<<4";

        /// <summary>      
        /// html comment to point the beginning of  html fragment      
        /// </summary>      
        public const string StartFragment = "<!--StartFragment-->";

        /// <summary>      
        /// html comment to point the end of html  fragment      
        /// </summary>      
        public const string EndFragment = @"<!--EndFragment-->";

        /// <summary>      
        /// Used to calculate characters byte count  in UTF-8      
        /// </summary>      
        private static readonly char[] _byteCount = new char[1];

        #endregion


        /// <summary>      
        /// Create <see  cref="DataObject"/> with given html and plain-text ready to be  used for clipboard or drag and drop.<br/>      
        /// Handle missing  <![CDATA[<html>]]> tags, specified startend segments and Unicode  characters.      
        /// </summary>      
        /// <remarks>      
        /// <para>      
        /// Windows Clipboard works with UTF-8  Unicode encoding while .NET strings use with UTF-16 so for clipboard to  correctly      
        /// decode Unicode string added to it from  .NET we needs to be re-encoded it using UTF-8 encoding.      
        /// </para>      
        /// <para>      
        /// Builds the CF_HTML header correctly for  all possible HTMLs<br/>      
        /// If given html contains start/end  fragments then it will use them in the header:      
        ///  <code><![CDATA[<html><body><!--StartFragment-->hello  <b>world</b><!--EndFragment--></body></html>]]></code>      
        /// If given html contains html/body tags  then it will inject start/end fragments to exclude html/body tags:      
        ///  <code><![CDATA[<html><body>hello  <b>world</b></body></html>]]></code>      
        /// If given html doesn't contain html/body  tags then it will inject the tags and start/end fragments properly:      
        /// <code><![CDATA[hello  <b>world</b>]]></code>      
        /// In all cases creating a proper CF_HTML  header:<br/>      
        /// <code>      
        /// <![CDATA[      
        /// Version:1.0      
        /// StartHTML:000000177      
        /// EndHTML:000000329      
        /// StartFragment:000000277      
        /// EndFragment:000000295      
        /// StartSelection:000000277      
        /// EndSelection:000000277      
        /// <!DOCTYPE HTML PUBLIC  "-//W3C//DTD HTML 4.0 Transitional//EN">      
        ///  <html><body><!--StartFragment-->hello  <b>world</b><!--EndFragment--></body></html>      
        /// ]]>      
        /// </code>      
        /// See format specification here: [http://msdn.microsoft.com/library/default.asp?url=/workshop/networking/clipboard/htmlclipboard.asp][9]      
        /// </para>      
        /// </remarks>      
        /// <param name="html">a  html fragment</param>      
        /// <param  name="plainText">the plain text</param>      
        public static DataObject CreateDataObject(string html, string plainText)
        {
            html = html ?? String.Empty;
            var htmlFragment = GetHtmlDataString(html);

            // re-encode the string so it will work  correctly (fixed in CLR 4.0)      
            if (Environment.Version.Major < 4 && html.Length != Encoding.UTF8.GetByteCount(html))
                htmlFragment = Encoding.Default.GetString(Encoding.UTF8.GetBytes(htmlFragment));

            var dataObject = new DataObject();
            dataObject.SetData(DataFormats.Html, htmlFragment);
            //dataObject.SetData(DataFormats.Text, plainText);
            //dataObject.SetData(DataFormats.UnicodeText, plainText);
            return dataObject;
        }

        /// <summary>      
        /// Clears clipboard and sets the given  HTML and plain text fragment to the clipboard, providing additional  meta-information for HTML.<br/>      
        /// See <see  cref="CreateDataObject"/> for HTML fragment details.<br/>      
        /// </summary>      
        /// <example>      
        ///  ClipboardHelper.CopyToClipboard("Hello <b>World</b>",  "Hello World");      
        /// </example>      
        /// <param name="html">a  html fragment</param>      
        /// <param  name="plainText">the plain text</param>      
        public static void CopyToClipboard(string html, string plainText)
        {
            var dataObject = CreateDataObject(html, plainText);
            Clipboard.SetDataObject(dataObject, true);
        }

        /// <summary>      
        /// Generate HTML fragment data string with  header that is required for the clipboard.      
        /// </summary>      
        /// <param name="html">the  html to generate for</param>      
        /// <returns>the resulted  string</returns>      
        private static string GetHtmlDataString(string html)
        {
            var sb = new StringBuilder();
            sb.AppendLine(Header);
            sb.AppendLine(@"<!DOCTYPE HTML  PUBLIC ""-//W3C//DTD HTML 4.0  Transitional//EN"">");

            // if given html already provided the  fragments we won't add them      
            int fragmentStart, fragmentEnd;
            int fragmentStartIdx = html.IndexOf(StartFragment, StringComparison.OrdinalIgnoreCase);
            int fragmentEndIdx = html.LastIndexOf(EndFragment, StringComparison.OrdinalIgnoreCase);

            // if html tag is missing add it  surrounding the given html (critical)      
            int htmlOpenIdx = html.IndexOf("<html", StringComparison.OrdinalIgnoreCase);
            int htmlOpenEndIdx = htmlOpenIdx > -1 ? html.IndexOf('>', htmlOpenIdx) + 1 : -1;
            int htmlCloseIdx = html.LastIndexOf("</html", StringComparison.OrdinalIgnoreCase);

            if (fragmentStartIdx < 0 && fragmentEndIdx < 0)
            {
                int bodyOpenIdx = html.IndexOf("<body", StringComparison.OrdinalIgnoreCase);
                int bodyOpenEndIdx = bodyOpenIdx > -1 ? html.IndexOf('>', bodyOpenIdx) + 1 : -1;

                if (htmlOpenEndIdx < 0 && bodyOpenEndIdx < 0)
                {
                    // the given html doesn't  contain html or body tags so we need to add them and place start/end fragments  around the given html only      
                    sb.Append("<html><body>");
                    sb.Append(StartFragment);
                    fragmentStart = GetByteCount(sb);
                    sb.Append(html);
                    fragmentEnd = GetByteCount(sb);
                    sb.Append(EndFragment);
                    sb.Append("</body></html>");
                }
                else
                {
                    // insert start/end fragments  in the proper place (related to html/body tags if exists) so the paste will  work correctly      
                    int bodyCloseIdx = html.LastIndexOf("</body", StringComparison.OrdinalIgnoreCase);

                    if (htmlOpenEndIdx < 0)
                        sb.Append("<html>");
                    else
                        sb.Append(html, 0, htmlOpenEndIdx);

                    if (bodyOpenEndIdx > -1)
                        sb.Append(html, htmlOpenEndIdx > -1 ? htmlOpenEndIdx : 0, bodyOpenEndIdx - (htmlOpenEndIdx > -1 ? htmlOpenEndIdx : 0));

                    sb.Append(StartFragment);
                    fragmentStart = GetByteCount(sb);

                    var innerHtmlStart = bodyOpenEndIdx > -1 ? bodyOpenEndIdx : (htmlOpenEndIdx > -1 ? htmlOpenEndIdx : 0);
                    var innerHtmlEnd = bodyCloseIdx > -1 ? bodyCloseIdx : (htmlCloseIdx > -1 ? htmlCloseIdx : html.Length);
                    sb.Append(html, innerHtmlStart, innerHtmlEnd - innerHtmlStart);

                    fragmentEnd = GetByteCount(sb);
                    sb.Append(EndFragment);

                    if (innerHtmlEnd < html.Length)
                        sb.Append(html, innerHtmlEnd, html.Length - innerHtmlEnd);

                    if (htmlCloseIdx < 0)
                        sb.Append("</html>");
                }
            }
            else
            {
                // handle html with existing  startend fragments just need to calculate the correct bytes offset (surround  with html tag if missing)      
                if (htmlOpenEndIdx < 0)
                    sb.Append("<html>");
                int start = GetByteCount(sb);
                sb.Append(html);
                fragmentStart = start + GetByteCount(sb, start, start + fragmentStartIdx) + StartFragment.Length;
                fragmentEnd = start + GetByteCount(sb, start, start + fragmentEndIdx);
                if (htmlCloseIdx < 0)
                    sb.Append("</html>");
            }

            // Back-patch offsets (scan only the  header part for performance)      
            sb.Replace("<<<<<<<<1", Header.Length.ToString("D9"), 0, Header.Length);
            sb.Replace("<<<<<<<<2", GetByteCount(sb).ToString("D9"), 0, Header.Length);
            sb.Replace("<<<<<<<<3", fragmentStart.ToString("D9"), 0, Header.Length);
            sb.Replace("<<<<<<<<4", fragmentEnd.ToString("D9"), 0, Header.Length);

            return sb.ToString();
        }

        /// <summary>      
        /// Calculates the number of bytes produced  by encoding the string in the string builder in UTF-8 and not .NET default  string encoding.      
        /// </summary>      
        /// <param name="sb">the  string builder to count its string</param>      
        /// <param  name="start">optional: the start index to calculate from (default  - start of string)</param>      
        /// <param  name="end">optional: the end index to calculate to (default - end  of string)</param>      
        /// <returns>the number of bytes  required to encode the string in UTF-8</returns>      
        private static int GetByteCount(StringBuilder sb, int start = 0, int end = -1)
        {
            int count = 0;
            end = end > -1 ? end : sb.Length;
            for (int i = start; i < end; i++)
            {
                _byteCount[0] = sb[i];
                count += Encoding.UTF8.GetByteCount(_byteCount);
            }
            return count;
        }
    }

    internal static class TestClipboard
    {
        public static void CopyHtmlToClipBoard(string html)
        {
            Encoding enc = Encoding.UTF8;

            string begin = "Version:0.9\r\nStartHTML:{0:000000}\r\nEndHTML:{1:000000}"
              + "\r\nStartFragment:{2:000000}\r\nEndFragment:{3:000000}\r\n";

            string html_begin = "<html>\r\n<head>\r\n"
              + "<meta http-equiv=\"Content-Type\""
              + " content=\"text/html; charset=" + enc.WebName + "\">\r\n"
              + "<title>HTML clipboard</title>\r\n</head>\r\n<body>\r\n"
              + "<!--StartFragment-->";

            string html_end = "<!--EndFragment-->\r\n</body>\r\n</html>\r\n";

            string begin_sample = String.Format(begin, 0, 0, 0, 0);

            int count_begin = enc.GetByteCount(begin_sample);
            int count_html_begin = enc.GetByteCount(html_begin);
            int count_html = enc.GetByteCount(html);
            int count_html_end = enc.GetByteCount(html_end);

            string html_total = String.Format(
              begin
              , count_begin
              , count_begin + count_html_begin + count_html + count_html_end
              , count_begin + count_html_begin
              , count_begin + count_html_begin + count_html
              ) + html_begin + html + html_end;

            DataObject obj = new DataObject();
            obj.SetData(DataFormats.Html, new MemoryStream(
              enc.GetBytes(html_total)));

            Clipboard.SetDataObject(obj, true);
        }
    }

}